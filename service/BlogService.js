const axios = require("axios");

export const getBlogs = (min, max) => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + `blogs/${min}/${max}`);
};

export const getBlogSlugs = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "blog_slugs/");
};

export const getBlogDetails = (slug) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + `blog_details/${slug}`
  );
};

export const getCategorySlugs = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "blog_categories");
};

export const getCategoryBlogs = (categorySlug, min, max) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL +
      `blogcategorylist/${categorySlug}/${min}/${max}/`
  );
};
