import axios from "axios";

export class CompareService {
  getProductsSmall() {
    return fetch("/data/compare.json")
      .then((res) => res.json())
      .then((d) => d.data);
  }

  getCompareByCourse(courseID, universities) {
    return axios.get(
      process.env.NEXT_PUBLIC_BACKEND_URL +
        `compare/${courseID}?${universities}`
    );
  }

  getCompareBySpecialization(courseID, specializationID, universities) {
    return axios.get(
      process.env.NEXT_PUBLIC_BACKEND_URL +
        `compare/${courseID}/${specializationID}?${universities}`
    );
  }
}
