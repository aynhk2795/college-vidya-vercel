import axios from "axios";

export const getVideoData = (videoID) => {
  return axios.get(
    `https://www.googleapis.com/youtube/v3/videos?id=${videoID}&key=${process.env.NEXT_PUBLIC_YOUTUBE_API_KEY}&part=snippet,contentDetails,statistics,status`
  );
};
