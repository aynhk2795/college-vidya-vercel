import axios from "axios";

export const getTopUniversity = (data) => {
  return axios.post(
    process.env.NEXT_PUBLIC_BACKEND_URL + `topuniversity/`,
    data
  );
};
