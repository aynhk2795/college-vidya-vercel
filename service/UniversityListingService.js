import axios from "axios";

export const getUniversityListingByCourse = (userID, courseID) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL +
      `userbyuniversity/${userID}/${courseID}/`
  );
};

export const getUniversityListingBySpecialization = (
  userID,
  courseID,
  specializationID
) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL +
      `userbyuniversity/${userID}/${courseID}/${specializationID}/`
  );
};

export const getUniversityPreviewByCourse = (courseID) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + `universitywisely/${courseID}/`
  );
};

export const getUniversityPreviewBySpecialization = (
  courseID,
  specializationID
) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL +
      `universitywisely/${courseID}/${specializationID}/`
  );
};
