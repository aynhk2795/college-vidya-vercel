const axios = require("axios");

export const getMenu = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "menu");
};

export const getInfoBanners = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "get_info_banners");
};

export const getOneStopData = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "getonestopsolutions");
};

export const getOneStopImages = () => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + "getonestopsolutionimages"
  );
};

export const getFeatureData = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "getfeatures");
};

export const getCompareFeatureData = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "getcomparefeatures");
};

export const getUniversityData = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "universities");
};

export const getFaqData = (pageID) => {
  return axios.get(`${process.env.NEXT_PUBLIC_BACKEND_URL}getfaq/${pageID}`);
};

export const getCounsellorData = () => {
  return axios.get(
    process.env.NEXT_PUBLIC_CV_COUNSELLOR_BACKEND_URL + "counsellors/all"
  );
};

export const getOTP = (number) => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + `otp/${number}`);
};

export const getVoiceOfLeaders = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "voice_of_leader/");
};

export const getTeam = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "our_team/");
};

export const getCVTV = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "cv_tv/");
};

export const getStates = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "state/");
};

export const getBanners = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "getbanner/");
};

export const getTagLine = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "tag_line/");
};
