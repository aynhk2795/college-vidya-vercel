export class UserService {
  constructor(axiosJWT) {
    this.axiosJWT = axiosJWT;
  }

  getUserDetails() {
    return this.axiosJWT.get(`user/`);
  }
}
