import axios from "axios";

export const addLead = (data) => {
  return axios.post(process.env.NEXT_PUBLIC_BACKEND_URL + "leads/", data);
};

export const getLeadDetailsByCourseID = (courseID, userID) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + `lead_edit/${courseID}/${userID}`
  );
};

export const getLeadDetailsBySpecializationID = (
  courseID,
  specializationID,
  userID
) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL +
      `lead_edit/${courseID}/${specializationID}/${userID}`
  );
};
