const axios = require("axios");

export const getCourseSlugs = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "get_course_slugs");
};

export const getCourseDetail = (slug) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + `get_course_view/${slug}`
  );
};

export const getSpecializationDetail = (courseSlug, specializationSlug) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL +
      `get_course_view/${courseSlug}/${specializationSlug}`
  );
};

export const getQuestions = (courseID, specializationID) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL +
      `get_question/${courseID}/${specializationID}`
  );
};
