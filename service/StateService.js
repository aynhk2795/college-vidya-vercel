export class CountryService {
  getCountries() {
    return fetch("data/state.json")
      .then((res) => res.json())
      .then((d) => d.data);
  }
}
