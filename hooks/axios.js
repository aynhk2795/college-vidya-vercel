import axios from "axios";
import jwt_decode from "jwt-decode";

import { useAuth } from "./auth";

export const useAxiosJWT = () => {
  const { accessToken, refreshTokens } = useAuth();
  const cancelTokenSource = axios.CancelToken.source();

  const axiosJWT = axios.create({
    baseURL: process.env.NEXT_PUBLIC_BACKEND_URL,
    headers: {
      Authorization: "Bearer " + accessToken,
    },
    withCredentials: true,
  });

  axiosJWT.interceptors.request.use(async (req) => {
    let currentDate = new Date();
    const decodedToken = jwt_decode(accessToken);

    if (decodedToken.exp * 1000 < currentDate.getTime()) {
      const data = await refreshTokens();
      if (data) {
        req.headers.Authorization = "Bearer " + data.jwt;
      }
    } else {
      req.headers.Authorization = "Bearer " + accessToken;
    }
    return req;
  });

  const handleCancelRequest = () => {
    cancelTokenSource.cancel("Request was canceled");
  };

  return { axiosJWT, handleCancelRequest };
};
