import axios from "axios";
import { getCookie, setCookie, hasCookie, deleteCookie } from "cookies-next";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useState } from "react";
import useSWR from "swr";

export const useAuth = (redirectOut = false) => {
  const router = useRouter();

  const [user, setUser] = useState(null);
  const [accessToken, setAccessToken] = useState(null);
  const [refreshAccessToken, setRefreshAccessToken] = useState(null);

  useEffect(() => {
    if (hasCookie("cvuid")) {
      setUser(getCookie("cvuid"));
    }

    if (hasCookie("atexp")) {
      setAccessToken(getCookie("atexp"));
    }

    if (hasCookie("ratnxp")) {
      setRefreshAccessToken(getCookie("ratnxp"));
    }

    if (redirectOut) {
      if (!hasCookie("cvuid") || !hasCookie("atexp") || !hasCookie("ratnxp")) {
        router.replace("/");
      }
    }
  }, [router, redirectOut]);

  useSWR("validate-token", async () => {
    if (accessToken) {
      let currentDate = new Date();
      const decodedToken = jwt_decode(accessToken);

      if (decodedToken.exp * 1000 < currentDate.getTime()) {
        await refreshTokens();
      }
    }
  });

  const refreshTokens = async () => {
    try {
      const res = await axios.post(
        process.env.NEXT_PUBLIC_BACKEND_URL + "token_refresh_user/",
        {
          token: refreshAccessToken,
          userid: user,
        }
      );

      setCookie("atexp", res.data.jwt);
      setCookie("ratnxp", res.data.refresh);

      return res.data;
    } catch (e) {
      logout();
    }
  };

  const register = (data) => {
    return axios.post(process.env.NEXT_PUBLIC_BACKEND_URL + "sign_up/", data);
  };

  const login = (data) => {
    return axios.post(
      process.env.NEXT_PUBLIC_BACKEND_URL + "user_login/",
      data
    );
  };

  const logout = () => {
    axios
      .post(process.env.NEXT_PUBLIC_BACKEND_URL + "logout/")
      .then(() => {
        deleteCookie("cvuid");
        deleteCookie("atexp");
        deleteCookie("ratnxp");

        window.location.href = "/";
      })
      .catch(() => {
        deleteCookie("cvuid");
        deleteCookie("atexp");
        deleteCookie("ratnxp");

        window.location.href = "/";
      });
  };

  return {
    user,
    accessToken,
    refreshAccessToken,
    refreshTokens,
    register,
    login,
    logout,
  };
};
