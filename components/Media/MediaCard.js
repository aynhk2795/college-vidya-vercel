import React from "react";
import { Col } from "react-bootstrap";
import Link from "next/link";
import { Card } from "primereact/card";
import Image from "next/image";
import media1 from "/public/images/media1.png";
import logo1 from "/public/images/ht_logo.png";

const MediaList = [
  {
    id: 1,
    url: "link1",
    logo: logo1,
    title: "I wanted to create a platform that is free and ",
  },
  {
    id: 2,
    url: "link2",
    logo: logo1,
    title: " a platform that is free and  I wanted to create a platform",
  },
  {
    id: 3,
    url: "link3",
    logo: logo1,
    title: " a platform that is free and  I wanted to create a platform",
  },
  {
    id: 4,
    url: "link4",
    logo: logo1,
    title: " a platform that is free and  I wanted to create a platform",
  },
  {
    id: 5,
    url: "link1",
    logo: logo1,
    title: " a platform that is free and  I wanted to create a platform",
  },
  {
    id: 6,
    url: "link1",
    logo: logo1,
    title: " a platform that is free and  I wanted to create a platform",
  },
  {
    id: 7,
    url: "link1",
    logo: logo1,
    title: " a platform that is free and  I wanted to create a platform",
  },
  {
    id: 8,
    url: "link1",
    logo: logo1,
    title: " a platform that is free and  I wanted to create a platform",
  },
];

const MediaCard = () => {
  return (
    <>
      {MediaList.map((list) => (
        <Col key={list.id} md={4}>
          <Link href={list.url}>
            <a>
              <Card className="mb-4 hover-underline-animation">
                <Image src={list.logo} alt="" />
                <p className="fs-14 my-2">{list.title}</p>
                <Image src={media1} alt="" />
                <p className="fs-14 mt-2 mb-0 text-primary">
                  Read the full article{" "}
                </p>
              </Card>
            </a>
          </Link>
        </Col>
      ))}
    </>
  );
};

export default MediaCard;
