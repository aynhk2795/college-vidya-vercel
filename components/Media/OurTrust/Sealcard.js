import React from "react";
import { Card } from "primereact/card";
import { AiFillSafetyCertificate } from "react-icons/ai";

const Sealcard = () => {
  return (
    <>
      <Card className="text-center my-2">
        <AiFillSafetyCertificate fontSize={50} />
        <p className="fw-bold">Secured</p>
        <p>We Don’t Share or Sell your personal Information.</p>
      </Card>
    </>
  );
};

export default Sealcard;
