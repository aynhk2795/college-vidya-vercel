import React from "react";
import { Col } from "react-bootstrap";
import Link from "next/link";
import Image from "next/image";
import logo1 from "/public/images/leader-voice1.jpeg";
import styles from "/styles/LeaderCard.module.scss";
import AnimatedYoutube from "/components/Global/AnimatedYoutube";

const LeaderList = [
  {
    id: 1,
    url: "link1",
    image: logo1,
    title: "I wanted to create a platform that is free and ",
  },
  {
    id: 2,
    url: "link2",
    image: logo1,
    title: "I wanted to create a platform that is free and ",
  },
  {
    id: 3,
    url: "link3",
    image: logo1,
    title: "I wanted to create a platform that is free and ",
  },
  {
    id: 4,
    url: "link4",
    image: logo1,
    title: "I wanted to create a platform that is free and ",
  },
  {
    id: 5,
    url: "link4",
    image: logo1,
    title: "I wanted to create a platform that is free and ",
  },
];

const LeaderCard = ({ voiceOfLeaders }) => {
  return (
    <>
      {voiceOfLeaders.map((voice) => (
        <Col key={voice.id} md={3}>
          <Link href={voice.link} passHref>
            <a target={"_blank"}>
              <div
                className={`${styles.leadercard} mb-4 hover-underline-animation`}>
                <Image src={voice.thumbnail} width={306} height={171} alt="" />
                <div className="position-relative px-3">
                  <div className="position-absolute ms-3 top-0 translate-middle-y">
                    <AnimatedYoutube />
                  </div>
                  <p className="fs-14 text-dark pt-4 pb-4 m-0">{voice.title}</p>
                </div>
              </div>
            </a>
          </Link>
        </Col>
      ))}
    </>
  );
};

export default LeaderCard;
