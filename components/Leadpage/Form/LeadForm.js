import React, { useEffect, useState } from "react";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Dropdown } from "primereact/dropdown";
import { Calendar } from "primereact/calendar";
import { Row, Col } from "react-bootstrap";
import { RadioButton } from "primereact/radiobutton";
import Image from "next/image";
import male from "/public/images/male.png";
import female from "/public/images/female.png";
import { FaLock } from "react-icons/fa";
import Link from "next/link";
import styles from "/styles/LeadPageForm.module.scss";
import moment from "moment";
import { getStates } from "../../../service/MiscService";
import { addLead } from "../../../service/LeadService";
import { useRouter } from "next/router";

export const ReactHookFormDemo = ({
  menuData,
  courseData,
  specializationData,
}) => {
  const router = useRouter();

  let allSpecializationData = [];

  if (menuData) {
    allSpecializationData = menuData
      .map((domain) => domain.courses)
      .flat()
      .filter((course) => course.id === courseData.id)[0].specializations;
  }

  const [addingLead, setAddingLead] = useState(false);

  const [states, setStates] = useState([]);
  const [calculatedAge, setCalculatedAge] = useState(null);

  const validationSchema = Yup.object().shape({
    gender: Yup.string().required("Please select a gender"),
    name: Yup.string().required("Please enter your name"),
    email: Yup.string()
      .email("Invalid email")
      .required("Please enter your email address"),
    mobile_number: Yup.number()
      .typeError("Invalid number")
      .required("Number is required")
      .test(
        "len",
        "Number must be exactly 10 digits",
        (val) => val.toString().length === 10
      ),
    dob: Yup.string()
      .typeError("Invalid date")
      .required("Please enter your date of birth"),
    state: Yup.string().required("Please select a state"),
    specialization: Yup.string().nullable(),
  });

  const {
    register,
    control,
    getValues,
    setValue,
    formState: { errors },
    handleSubmit,
  } = useForm({
    defaultValues: { specialization: "" },
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  useEffect(() => {
    if (getValues("dob")) {
      const dob = getValues("dob");
      const dateToday = new Date();

      setCalculatedAge(moment.duration(dateToday - dob).years() + " Years");
    } else {
      setCalculatedAge(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getValues("dob")]);

  useEffect(() => {
    getStates().then((response) => setStates(response.data));
  }, []);

  const onSubmit = (data) => {
    setAddingLead(true);

    let formData = new FormData();
    formData.append("user_id", -1);
    formData.append("gender", data.gender);
    formData.append("name", data.name);
    formData.append("email", data.email);
    formData.append("mobile", data.mobile_number);
    formData.append("dob", data.dob);
    formData.append("course", courseData.id);
    formData.append(
      "specializations",
      specializationData ? specializationData.id : data.specialization
    );
    formData.append("state", data.state);
    // formData.append("question_answers", JSON.stringify({}));

    addLead(formData)
      .then((response) => {
        setAddingLead(false);

        router.push(
          `/colleges-universities/${courseData.slug}/${
            specializationData
              ? specializationData.slug
              : allSpecializationData.some(
                  (specialization) =>
                    specialization.id === parseInt(data.specialization)
                )
              ? allSpecializationData.filter(
                  (specialization) =>
                    specialization.id === parseInt(data.specialization)
                )[0].slug
              : ""
          }?uid=${response.data.user_id}`
        );
      })
      .catch(() => {
        setAddingLead(false);
        iziToast.error({
          title: "Error",
          message: "Oops! Something went wrong",
          timeout: 2000,
          position: "topRight",
        });
      });
  };

  return (
    <>
      <style>
        {`
  .lead_form_wrap label {
    position: absolute;
    left: 5px;
    top: -10px;
    background-color: #fff;
    padding: 0 7px;
}
  `}
      </style>

      <div className={`${styles.lead_page_form}`}>
        <div className="flex justify-content-center">
          <div>
            <form onSubmit={handleSubmit(onSubmit)} className="p-fluid">
              <Row className="mb-4">
                <Col>
                  <div className="field position-relative">
                    <div
                      className="card d-flex flex-row"
                      style={{
                        border: "1px solid #5e6c84",
                        borderRadius: "6px",
                      }}>
                      <RadioButton
                        className="position-absolute top-50 end-0 translate-middle"
                        inputId="male"
                        value="male"
                        onChange={(e) =>
                          setValue("gender", e.value, { shouldValidate: true })
                        }
                        checked={getValues("gender") === "male"}
                      />
                      <label
                        htmlFor="male"
                        className="w-100 d-flex align-items-center px-3 py-2">
                        <Image src={male} width={30} height={30} alt="" /> Male
                      </label>
                    </div>
                  </div>
                </Col>
                <Col>
                  <div className="field position-relative">
                    <div
                      className="card d-flex flex-row"
                      style={{
                        border: "1px solid #5e6c84",
                        borderRadius: "6px",
                      }}>
                      <RadioButton
                        className="position-absolute top-50 end-0 translate-middle"
                        inputId="female"
                        value="female"
                        onChange={(e) =>
                          setValue("gender", e.value, { shouldValidate: true })
                        }
                        checked={getValues("gender") === "female"}
                      />
                      <label
                        htmlFor="female"
                        className="w-100 d-flex align-items-center px-3 py-2">
                        <Image src={female} width={30} height={30} alt="" />{" "}
                        Female
                      </label>
                    </div>
                  </div>
                </Col>
                <span className="p-error text-xs d-block">
                  {errors["gender"]?.message}
                </span>
              </Row>

              <div className="field position-relative mb-4">
                <span className="lead_form_wrap">
                  <InputText
                    {...register("name")}
                    className={`w-100 ${errors.name ? "p-invalid block" : ""}`}
                  />

                  <span className="p-error text-xs d-block">
                    {errors["name"]?.message}
                  </span>

                  <label htmlFor="name">Full Name*</label>
                </span>
              </div>

              <div className="field position-relative mb-4">
                <span className="lead_form_wrap p-input-icon-right">
                  <i className="pi pi-envelope" />
                  <InputText
                    {...register("email")}
                    className={`w-100 ${errors.email ? "p-invalid block" : ""}`}
                  />

                  <span className="p-error text-xs d-block">
                    {errors["email"]?.message}
                  </span>

                  <label>Email*</label>
                </span>
              </div>

              <Row>
                <Col>
                  <div className="field position-relative mb-4">
                    <span className="lead_form_wrap">
                      <InputText
                        {...register("mobile_number")}
                        className={`w-100 ${
                          errors.mobile_number ? "p-invalid block" : ""
                        }`}
                      />
                      <label> Number*</label>

                      <span className="p-error text-xs d-block">
                        {errors["mobile_number"]?.message}
                      </span>
                    </span>
                  </div>
                </Col>
                <Col>
                  <div className="field position-relative mb-4">
                    <span className="lead_form_wrap">
                      <div className="position-relative">
                        <Calendar
                          value={getValues("dob")}
                          visible={false}
                          inputMode={"numeric"}
                          onChange={(e) =>
                            setValue("dob", e.value, { shouldValidate: true })
                          }
                          dateFormat="dd/mm/yy"
                          mask="99/99/9999"
                          className={`w-100 ${
                            errors.dob ? "p-invalid block" : ""
                          }`}
                        />
                        {calculatedAge ? (
                          <span
                            className="position-absolute end-0 top-50 translate-middle-y me-2 px-3 py-1 rounded fs-14"
                            style={{ backgroundColor: "aliceblue" }}>
                            {calculatedAge}
                          </span>
                        ) : null}
                      </div>
                      <span className="p-error text-xs d-block">
                        {errors["dob"]?.message}
                      </span>
                      <label htmlFor="date">DD/MM/YYYY*</label>
                    </span>
                  </div>
                </Col>
              </Row>

              <div className="field position-relative mb-4">
                <span className="lead_form_wrap">
                  <Dropdown
                    options={states}
                    value={getValues("state")}
                    onChange={(e) =>
                      setValue("state", e.value, { shouldValidate: true })
                    }
                    className={`w-100 ${errors.state ? "p-invalid block" : ""}`}
                    optionValue="id"
                    optionLabel="state"
                  />
                  <span className="p-error text-xs d-block">
                    {errors["state"]?.message}
                  </span>
                  <label htmlFor="country">State*</label>
                </span>
              </div>

              {!specializationData ? (
                <div className="field position-relative mb-4">
                  <span className="lead_form_wrap">
                    <Dropdown
                      options={allSpecializationData}
                      value={getValues("specialization")}
                      onChange={(e) =>
                        setValue("specialization", e.value, {
                          shouldValidate: true,
                        })
                      }
                      className={`w-100 ${
                        errors.state ? "p-invalid block" : ""
                      }`}
                      optionValue="id"
                      optionLabel="name"
                    />
                    <span className="p-error text-xs d-block">
                      {errors["specialization"]?.message}
                    </span>
                    <label>Specialization</label>
                  </span>
                </div>
              ) : null}

              <Button
                type="submit"
                icon="pi pi-arrow-right"
                iconPos="right"
                label="Find Best University"
                className="mt-0 py-3 border-0 bgsecondary"
                loading={addingLead}
              />
            </form>
            <div className="text-center ">
              <div className="mt-3 text-center bg-secondary d-inline-block rounded">
                <span className="fs-12 fw-normal px-3 text-white">
                  <FaLock className="lock-icon" /> Your personal information is
                  secure with us
                </span>
              </div>
            </div>
            <div className="text-center mt-2 fs-14">
              <small>
                By clicking, you agree to our{" "}
                <Link href="/">
                  <a className="text-decoration-none">
                    Privacy Policy,Terms of Use
                  </a>
                </Link>{" "}
                & Disclaimers
              </small>
              <small className="text-secondary d-block">
                {" "}
                (Standard T&C Apply)
              </small>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ReactHookFormDemo;
