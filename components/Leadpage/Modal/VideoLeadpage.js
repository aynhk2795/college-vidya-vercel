import React from "react";
import { Modal, Accordion, Badge } from "react-bootstrap";
import styles from "/styles/knowintwomins.module.scss";
import AnimatedYoutube from "/components/Global/AnimatedYoutube";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title
          id="contained-modal-title-vcenter"
          className="d-flex justify-content-center">
          <span className="me-2"> Watch Video </span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={`${styles.knowintwoModal}`}>
        <iframe
          width="100%"
          height="450"
          src={props.video_link}
          title="YouTube video player"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen></iframe>
      </Modal.Body>
    </Modal>
  );
}
const VideoLeadpage = (props) => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <p
        className="m-0 cursor-pointer d-flex align-items-center gap-2"
        onClick={() => setModalShow(true)}>
        Watch Video <AnimatedYoutube />
      </p>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        video_link={props.video_link}
      />
    </>
  );
};

export default VideoLeadpage;
