import React from "react";
import { Modal, Accordion, Badge } from "react-bootstrap";
import styles from "/styles/knowintwomins.module.scss";
import AnimatedYoutube from "/components/Global/AnimatedYoutube";
import AnimatedPodcast from "/components/Global/AnimatedPodcast";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title
          id="contained-modal-title-vcenter"
          className="d-flex justify-content-center">
          <span className="me-2"> Listen Podcast </span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={`${styles.knowintwoModal}`}>
        <iframe
          srcDoc={props.audio_link}
          loading="lazy"
          width="100%"
          height="200"
          frameBorder="0"
          scrolling="no"
        />
      </Modal.Body>
    </Modal>
  );
}
const AudioLeadpage = (props) => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <p
        className="m-0 cursor-pointer d-flex align-items-center gap-2"
        onClick={() => setModalShow(true)}>
        Listen Podcast <AnimatedPodcast />
      </p>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        audio_link={props.audio_link}
      />
    </>
  );
};

export default AudioLeadpage;
