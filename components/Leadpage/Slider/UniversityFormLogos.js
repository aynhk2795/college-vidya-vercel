import React from "react";
import { Card, Row, Col } from "react-bootstrap";
import SidebarHeading from "/components/universityListingPage/Sidebar/SidebarHeading";
import Image from "next/image";
import Assistance from "/components/universityListingPage/Modal/Assistance";
import student from "/public/images/NMIMS.jpeg";
import student1 from "/public/images/logo-univ.jpeg";
import student2 from "/public/images/NMIMS.jpeg";
import { BsFillTelephoneFill } from "react-icons/bs";
import { GoLocation } from "react-icons/go";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "swiper/css/autoplay";
import "swiper/css/grid";
import { EffectFade, Navigation, Autoplay, Grid, Pagination } from "swiper";
import { generate } from "shortid";

const UniversityFormLogos = ({ universityPreviewList }) => {
  return (
    <>
      <div className="pt-5">
        <Swiper
          slidesPerView={3}
          // effect={"fade"}
          grid={{
            rows: 2,
            fill: "row",
          }}
          pagination={false}
          spaceBetween={20}
          // pagination={{
          //   clickable: true,
          // }}
          speed={6000}
          autoplay={{
            delay: 0,
            disableOnInteraction: false,
          }}
          loop={false}
          navigation={false}
          modules={[Grid, Navigation, EffectFade, Autoplay, Pagination]}
          className="mediaSwiper">
          {universityPreviewList &&
            universityPreviewList.map((list) => (
              <SwiperSlide key={generate()}>
                <Card className="p-2">
                  <Image
                    src={list.university.logo}
                    width={150}
                    height={50}
                    objectFit="contain"
                    alt=""
                  />
                </Card>
              </SwiperSlide>
            ))}
        </Swiper>
      </div>
    </>
  );
};

export default UniversityFormLogos;
