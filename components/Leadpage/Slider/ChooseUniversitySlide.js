import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import screen1 from "/public/images/media/hindustan-times.png";
import { Card } from "primereact/card";
import { Button } from "react-bootstrap";
// import PrimaryButton from "/components/Global/Buttons/PrimaryButton";

import Link from "next/link";
import { generate } from "shortid";

export default function App({ universityPreviewList }) {
  return (
    <>
      <div>
        <Swiper
          slidesPerView={3}
          spaceBetween={20}
          loop={false}
          navigation={true}
          // pagination={{
          //   clickable: false,
          // }}
          pagination={false}
          breakpoints={{
            "@0.00": {
              slidesPerView: 1,
              spaceBetween: 20,
            },
            "@0.75": {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            "@1.00": {
              slidesPerView: 3,
              spaceBetween: 20,
            },
          }}
          modules={[Navigation, Pagination]}
          className="chooseSwiper">
          {universityPreviewList &&
            universityPreviewList.map((list) => (
              <SwiperSlide className="text-center" key={generate()}>
                <span className="text-decoration-none">
                  <Card>
                    <Image
                      src={list.university.logo}
                      width={100}
                      height={50}
                      alt=""
                    />
                    <p className="my-1 fs-12 text-uppercase text-truncate">
                      {list.university.name}
                    </p>
                    <hr />
                    <p className="my-1 fs-14">Rating : {list.avg_rating}/5</p>
                    <p className="mb-0 fs-14 mb-3 textprimary">
                      Fee :{" "}
                      <span className="fw-bold">
                        ₹{list.fee.toLocaleString()}
                      </span>
                    </p>
                    <Button
                      onClick={() => window.scrollTo(0, 0)}
                      className="w-100 bgprimary">
                      Compare{" "}
                    </Button>
                  </Card>
                </span>
              </SwiperSlide>
            ))}
        </Swiper>
      </div>
    </>
  );
}
