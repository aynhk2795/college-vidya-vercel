import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import ChooseUniversitySlide from "/components/Leadpage/Slider/ChooseUniversitySlide";
import TooltipFree from "/components/Global/Tooltip/TooltipFree";
import styles from "/styles/ChooseUniversity.module.scss";

const ChooseUniversity = ({ universityPreviewList }) => {
  return (
    <>
      <section className={`${styles.choosebg} choose-university`}>
        <Container className="py-5">
          <Row>
            <Col md={10} className="mx-auto">
              <Row>
                <Col
                  md={4}
                  className="d-flex align-items-center justify-content-center">
                  <h2>
                    Choose Your <br />
                    <span className="textsecondary">
                      University Wisely!
                    </span>{" "}
                    <TooltipFree info="Compare universities on parameters like, Approvals, Student rating, College Vidya Score, LMS etc" />
                  </h2>
                </Col>
                <Col md={8}>
                  <ChooseUniversitySlide
                    universityPreviewList={universityPreviewList}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};

export default ChooseUniversity;
