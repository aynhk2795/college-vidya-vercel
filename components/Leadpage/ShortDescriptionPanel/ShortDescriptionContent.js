import React from "react";
import styles from "./ShortDescriptionContent.module.scss";

const ShortDescriptionContent = (props) => {
  return (
    <div
      className={`${styles.course_short_desc} course_short_desc`}
      dangerouslySetInnerHTML={{ __html: props.short_description }}
    />
  );
};

export default ShortDescriptionContent;
