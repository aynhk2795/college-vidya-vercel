const ShortDescriptionImage = (props) => {
  return (
    <>
      <img src={props.thumbnail} alt="" className="w-100" />
    </>
  );
};

export default ShortDescriptionImage;
