import React from "react";

const ShortDescriptionHeading = (props) => {
  return (
    <>
      <h3>{props.short_description_heading}</h3>
    </>
  );
};

export default ShortDescriptionHeading;
