import React from "react";
import { Card, Row, Col } from "react-bootstrap";
import styles from "/styles/longDescriptionHEading.module.scss";

const LongDescriptionHEading = (props) => {
  return (
    <>
      <Card
        className={`${styles.longDescriptionHEading} position-absolute start-50 translate-middle top-0`}>
        <Row>
          <Col className="text-center py-3">
            <p className="fw-bold">Approvals</p>
            <p>{props.approvals}</p>
          </Col>
          <Col className="text-center py-3">
            <p className="fw-bold">Duration</p>
            <p>{props.duration_range}</p>
          </Col>
          <Col className="text-center py-3">
            <p className="fw-bold">Eligibility</p>
            <p>{props.eligibility}</p>
          </Col>
        </Row>
      </Card>
    </>
  );
};

export default LongDescriptionHEading;
