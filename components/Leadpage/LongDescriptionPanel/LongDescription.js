import React from "react";
import { Card, Row, Col } from "react-bootstrap";
import LongDescriptionHEading from "/components/Leadpage/LongDescriptionPanel/LongDescriptionHEading";
import styles from "/styles/LongDescription.module.scss";

const LongDescription = (props) => {
  return (
    <>
      <Card className={`${styles.longContentwrap} mb-5 mt-5`}>
        <LongDescriptionHEading
          approvals={props.approvals}
          duration_range={props.duration_range}
          eligibility={props.eligibility}
        />
        <div dangerouslySetInnerHTML={{ __html: props.description }} />
      </Card>
    </>
  );
};

export default LongDescription;
