import React from "react";
import Coursepage from "../../pages/coursepage";

const CoursePage = ({ menuData }) => {
  return (
    <>
      <Coursepage menuData={menuData} />
    </>
  );
};

export default CoursePage;
