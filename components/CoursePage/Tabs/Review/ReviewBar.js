import React from "react";
import { Row, Col } from "react-bootstrap";
import { FaGraduationCap } from "react-icons/fa";
import RatingStar from "../../../../components/UniversityPage/RatingStar.js";
import { ProgressBar } from "primereact/progressbar";
import { Accordion, AccordionTab } from "primereact/accordion";
import styles from "./ReviewBar.module.scss";
import { generate } from "shortid";

const universitypagereview = [
  {
    heading: "How are ratings calculated?",
    description:
      "To calculate the overall star rating and percentage breakdown by star, we don’t use a simple average. Instead, our system conside_leftrs things like how recent a review is and if the reviewer bought the item on Amazon. It also analyses reviews to verify trustworthiness.",
  },
];

const ReviewBar = () => {
  return (
    <>
      <h3 className="mb-3 pt-4">Review</h3>
      <Row>
        <Col md={12} className="mb-4">
          <div className="d-flex align-items-start">
            <div className="bg-grey d-inline-block py-2 px-3 rounded me-3 mt-1">
              <FaGraduationCap fontSize={20} />
            </div>
            <div>
              <p className="m-0 fw-bold">Students</p>
              <p className="m-0 fs-12 mb-1">4.2 out of 5</p>
              <RatingStar value={4} />
            </div>
          </div>
          <div className={`mt-3`}>
            <div className="row">
              <div className={`${styles.side_left}`}>5 star</div>
              <div className={`${styles.middle}`}>
                <ProgressBar className="mb-2" value={80}></ProgressBar>
              </div>
              <div className={`${styles.side_right}`}>{80}%</div>
            </div>
            <div className="row">
              <div className={`${styles.side_left}`}>4 star</div>
              <div className={`${styles.middle}`}>
                <ProgressBar className="mb-2" value={50}></ProgressBar>
              </div>
              <div className={`${styles.side_right}`}>{50}%</div>
            </div>{" "}
            <div className="row">
              <div className={`${styles.side_left}`}>3 star</div>
              <div className={`${styles.middle}`}>
                <ProgressBar className="mb-2" value={60}></ProgressBar>
              </div>
              <div className={`${styles.side_right}`}>{60}%</div>
            </div>{" "}
            <div className="row">
              <div className={`${styles.side_left}`}>2 star</div>
              <div className={`${styles.middle}`}>
                <ProgressBar className="mb-2" value={20}></ProgressBar>
              </div>
              <div className={`${styles.side_right}`}>{20}%</div>
            </div>{" "}
            <div className="row">
              <div className={`${styles.side_left}`}>1 star</div>
              <div className={`${styles.middle}`}>
                <ProgressBar className="mb-2" value={10}></ProgressBar>
              </div>
              <div className={`${styles.side_right}`}>{10}%</div>
            </div>
          </div>
        </Col>
      </Row>

      <Accordion>
        {universitypagereview.map((list) => (
          <AccordionTab header={list.heading} key={generate()}>
            <p>{list.description}</p>
          </AccordionTab>
        ))}
      </Accordion>
    </>
  );
};

export default ReviewBar;
