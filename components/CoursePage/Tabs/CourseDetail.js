import React from "react";
import Heading from "./Heading/Heading";
import styles from "./UniversityFacts.module.scss";

const UniversityFacts = () => {
  return (
    <>
      <Heading title="Course Detail" />
      <ul className={`${styles.list}`}>
        <li className="mb-3">
          Narsee Monjee Institute of Management (NMIMS) was established in 1981.
        </li>
        <li className="mb-3">
          The University is approved by UGC-DEB and also approved by the
          Association of Indian University.
        </li>
        <li className="mb-3">
          NMIMS distance learning provides the best quality education to the
          student and working professionals without attending the traditional
          classroom education.
        </li>
        <li className="mb-3">
          NMIMS Global Access School for Continuing Education also provides
          placement assistance to the students for applying to renowned
          companies.
        </li>
        <li className="mb-3">
          The National Assessment and Accreditation Council (NAAC) attributes
          the NMIMS with Grade A+ with its 3rd cycle of judgment in 2018.
        </li>
        <li className="mb-3">
          Presently the NMIMS Global Access School For Continuining Education
          has 55000+ students and more than 9500 strong alumni.
        </li>
      </ul>
    </>
  );
};

export default UniversityFacts;
