import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import screen1 from "/public/images/great-lakes.jpeg";
import screen2 from "/public/images/Top-University-Lovely-Professional-University.jpeg";
import Card from "react-bootstrap/Card";
import Link from "next/link";
import Heading from "../Tabs/Heading/Heading";

const Compare = [
  {
    id: 1,
    logo: screen1,
    name: "Manipal University",
    tag: "Read More",
  },
  {
    id: 2,
    logo: screen2,
    name: "Lovely Professional University",
    tag: "Read More",
  },
  {
    id: 3,
    logo: screen1,
    name: "Amrita Online Ahead",
    tag: "Read More",
  },
  {
    id: 4,
    logo: screen1,
    name: "Post-Covid work environment: Upskilling for impact in 2022",
    tag: "Read More",
  },
  {
    id: 5,
    logo: screen1,
    name: "Post-Covid work environment: Upskilling for impact in 2022",
    tag: "Read More",
  },
  {
    id: 6,
    logo: screen1,
    name: "Post-Covid work environment: Upskilling for impact in 2022",
    tag: "Read More",
  },
  {
    id: 7,
    logo: screen1,
    name: "Post-Covid work environment: Upskilling for impact in 2022",
    tag: "Read More",
  },
];

export default function App() {
  return (
    <>
      <Heading title="Other Universities Which Provide Same Course" />

      <Swiper
        slidesPerView={4}
        spaceBetween={20}
        loop={true}
        navigation={false}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          "@0.00": {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          "@0.75": {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          "@1.00": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.75": {
            slidesPerView: 3,
            spaceBetween: 20,
          },
        }}
        modules={[Navigation, Pagination]}
        className="mediaSwiper">
        {Compare.map((list) => (
          <SwiperSlide className="mb-4 mt-3 pb-4" key={list.id}>
            <Link href="/">
              <a className="text-decoration-none">
                <Card className="shadow-1 position-relative">
                  <Image src={list.logo} className="rounded" alt="" />

                  <div className="px-3 pb-3">
                    <p
                      className="fs-12 mt-3 mb-1 text-uppercase"
                      style={{ letterSpacing: "2px" }}>
                      Online & Distance MBA
                    </p>
                    <p className="fw-bold text-dark">{list.name}</p>

                    <p className="mb-0 bgprimary d-block text-white fs-14 px-3 rounded mt-3 text-center py-2">
                      {list.tag}
                    </p>
                  </div>
                </Card>
              </a>
            </Link>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
}
