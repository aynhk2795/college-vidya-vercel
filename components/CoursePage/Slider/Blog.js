import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import screen1 from "/public/images/blog1.jpeg";
import screen2 from "/public/images/blog2.jpeg";
import screen3 from "/public/images/blog3.jpeg";
import Card from "react-bootstrap/Card";
import Link from "next/link";

const Blog = [
  {
    id: 1,
    logo: screen1,
    name: "Post-Covid work environment: Upskilling for impact in 2022",
    date: "21 Mar 2022",
  },
  {
    id: 2,
    logo: screen2,
    name: "Post-Covid work environment: Upskilling for impact in 2022",
    date: "21 Mar 2022",
  },
  {
    id: 3,
    logo: screen3,
    name: "Post-Covid work environment: Upskilling for impact in 2022",
    date: "21 Mar 2022",
  },
  {
    id: 4,
    logo: screen1,
    name: "Post-Covid work environment: Upskilling for impact in 2022",
    date: "21 Mar 2022",
  },
  {
    id: 5,
    logo: screen2,
    name: "Post-Covid work environment: Upskilling for impact in 2022",
    date: "21 Mar 2022",
  },
  {
    id: 6,
    logo: screen3,
    name: "Post-Covid work environment: Upskilling for impact in 2022",
    date: "21 Mar 2022",
  },
  {
    id: 7,
    logo: screen1,
    name: "Post-Covid work environment: Upskilling for impact in 2022",
    date: "21 Mar 2022",
  },
];

export default function App() {
  return (
    <>
      {/* <Heading title="Blog" /> */}

      <Swiper
        slidesPerView={4}
        spaceBetween={20}
        loop={true}
        navigation={false}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          "@0.00": {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          "@0.75": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.00": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.75": {
            slidesPerView: 3,
            spaceBetween: 20,
          },
        }}
        modules={[Navigation, Pagination]}
        className="mediaSwiper">
        {Blog.map((list) => (
          <SwiperSlide className="mb-4 mt-3 pb-4" key={list.id}>
            <Link href="/">
              <a className="text-decoration-none">
                <Card className="shadow-1 position-relative">
                  <Image src={list.logo} className="rounded" alt="" />

                  <div className="px-3 pb-3">
                    <p
                      className="fs-12 mt-3 mb-1 text-uppercase"
                      style={{ letterSpacing: "2px" }}>
                      NMIMS Distance Learning
                    </p>
                    <p className="fw-bold text-dark">{list.name}</p>
                    <p className="mb-0 text-secondary">{list.date}</p>
                    <p className="mb-0 bg-secondary d-inline-block text-white fs-14 px-3 rounded mt-3">
                      Read More
                    </p>
                  </div>
                </Card>
              </a>
            </Link>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
}
