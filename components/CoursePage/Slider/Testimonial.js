import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import screen1 from "/public/images/experts/expert-team5.jpeg";
import Card from "react-bootstrap/Card";
import Heading from "../Tabs/Heading/Heading";

const Testimonial = [
  {
    id: 1,
    logo: screen1,
    name: "Hanna Lisem",
    tag: "Project Manager",
    desc: "Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat",
  },
  {
    id: 2,
    logo: screen1,
    name: "Ronne Galle ",
    tag: "Engineer",
    desc: "Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat",
  },
  {
    id: 3,
    logo: screen1,
    name: "Missy Limana",
    tag: "Project Manager",
    desc: "Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat",
  },
  {
    id: 4,
    logo: screen1,
    name: "Hanna Lisem",
    tag: "Project Manager",
    desc: "Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat",
  },
  {
    id: 5,
    logo: screen1,
    name: "Missy Limana",
    tag: "Project Manager",
    desc: "Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat",
  },
  {
    id: 6,
    logo: screen1,
    name: "Hanna Lisem",
    tag: "Project Manager",
    desc: "Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat",
  },
  {
    id: 7,
    logo: screen1,
    name: "Missy Limana",
    tag: "Project Manager",
    desc: "Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat",
  },
];

export default function App() {
  return (
    <>
      <Heading title="Testimonial" />

      <Swiper
        slidesPerView={4}
        spaceBetween={20}
        loop={true}
        navigation={false}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          "@0.00": {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          "@0.75": {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          "@1.00": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.75": {
            slidesPerView: 3,
            spaceBetween: 20,
          },
        }}
        modules={[Navigation, Pagination]}
        className="mediaSwiper">
        {Testimonial.map((list) => (
          <SwiperSlide className="mb-4 mt-3 pb-4" key={list.id}>
            <Card className="shadow-1 position-relative">
              <div className="w-50 mx-auto mt-4">
                <Image
                  src={list.logo}
                  className="rounded-circle shadow-1"
                  alt=""
                />
              </div>

              <div className="px-3 pb-3">
                <p className="fw-bold text-dark text-center h5 mt-3">
                  {list.name}
                </p>
                <p
                  className="fs-12 mb-1 text-uppercase text-center"
                  style={{ letterSpacing: "2px" }}>
                  {list.tag}
                </p>

                <p className="text-center text-secondary">{list.desc}</p>
              </div>
            </Card>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
}
