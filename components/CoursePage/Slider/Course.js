import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import screen1 from "/public/images/course_image.png";
import screen2 from "/public/images/course_img1.png";
import screen3 from "/public/images/course_img3.jpeg";
import Card from "react-bootstrap/Card";
import Link from "next/link";
import Heading from "../Tabs/Heading/Heading";
import { BsChevronRight } from "react-icons/bs";

const Course = [
  {
    id: 1,
    logo: screen1,
    name: "MBA (Distance Online MBA Programs)",
    desc: "The MBA programme allows students to improve their decision-making, analytical, leadership...",
    tag: "Read More",
  },
  {
    id: 2,
    logo: screen2,
    name: "MCA (Distance Online MCA Programs)",
    desc: "The MBA programme allows students to improve their decision-making, analytical, leadership...",
    tag: "Read More",
  },
  {
    id: 3,
    logo: screen3,
    name: "M.Sc (Distance Online M.Sc Programs)",
    desc: "The MBA programme allows students to improve their decision-making, analytical, leadership...",
    tag: "Read More",
  },
  {
    id: 4,
    logo: screen1,
    name: "M.Sc (Distance Online M.Sc Programs)",
    desc: "The MBA programme allows students to improve their decision-making, analytical, leadership...",
    tag: "Read More",
  },
  {
    id: 5,
    logo: screen1,
    name: "M.Sc (Distance Online M.Sc Programs)",
    desc: "The MBA programme allows students to improve their decision-making, analytical, leadership...",
    tag: "Read More",
  },
  {
    id: 6,
    logo: screen1,
    name: "M.Sc (Distance Online M.Sc Programs)",
    desc: "The MBA programme allows students to improve their decision-making, analytical, leadership...",
    tag: "Read More",
  },
  {
    id: 7,
    logo: screen1,
    name: "M.Sc (Distance Online M.Sc Programs)",
    desc: "The MBA programme allows students to improve their decision-making, analytical, leadership...",
    tag: "Read More",
  },
];

export default function App() {
  return (
    <>
      <Heading title="Courses" />
      <Swiper
        slidesPerView={4}
        spaceBetween={20}
        loop={true}
        navigation={false}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          "@0.00": {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          "@0.75": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.00": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.75": {
            slidesPerView: 3,
            spaceBetween: 20,
          },
        }}
        modules={[Navigation, Pagination]}
        className="mediaSwiper">
        {Course.map((list) => (
          <SwiperSlide className="mb-4 mt-3 pb-4" key={list.id}>
            <Link href="/">
              <a className="text-decoration-none">
                <Card className="shadow-1 position-relative">
                  <Image src={list.logo} className="rounded" alt="" />

                  <div className="px-3 pb-3">
                    <p
                      className="fs-12 mt-3 mb-1 text-uppercase"
                      style={{ letterSpacing: "2px" }}>
                      NMIMS Distance Learning
                    </p>
                    <p className="fw-bold text-dark">{list.name}</p>
                    <p className="mb-0 text-secondary fs-12">{list.desc}</p>
                    <p className="mb-0 d-inline-block fs-14 textprimary rounded mt-3">
                      {list.tag} <BsChevronRight className="fw-bold" />
                    </p>
                  </div>
                </Card>
              </a>
            </Link>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
}
