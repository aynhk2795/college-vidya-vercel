import React from "react";
import styles from "./OttaHeading.module.scss";

const OttaHeading = (props) => {
  return (
    <>
      <h3 className={`${styles.otta_heading} fw-bold`}>{props.title}</h3>
    </>
  );
};

export default OttaHeading;
