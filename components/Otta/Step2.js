import React from "react";
import { Form } from "react-bootstrap";
import { generate } from "shortid";
import OttaHeading from "./OttaHeading";
import LabelHeart from "/components/Otta/LabelHeart";
import styles from "/components/Otta/QuestionForm.module.scss";

const questionlist = [
  {
    control_id: "f23",
    label: <LabelHeart labelText="Recognition and reward" />,
  },
  {
    control_id: "f24",
    label: <LabelHeart labelText="Transparency and respect" />,
  },
  {
    control_id: "f25",
    label: <LabelHeart labelText="Challenging work" />,
  },
  {
    control_id: "f26",
    label: <LabelHeart labelText="Challenging work" />,
  },
  {
    control_id: "f27",
    label: <LabelHeart labelText="Diversity and inclusion" />,
  },
  {
    control_id: "f28",
    label: <LabelHeart labelText="Development and progression" />,
  },

  {
    control_id: "f29",
    label: <LabelHeart labelText="Diversity and inclusion" />,
  },
  {
    control_id: "f30",
    label: <LabelHeart labelText="Development and progression" />,
  },
  {
    control_id: "f31",
    label: <LabelHeart labelText="Recognition and reward" />,
  },
  {
    control_id: "f32",
    label: <LabelHeart labelText="Transparency and respect" />,
  },
  {
    control_id: "f33",
    label: <LabelHeart labelText="Diversity and inclusion" />,
  },
  {
    control_id: "f34",
    label: <LabelHeart labelText="Development and progression" />,
  },
];
const Step2 = () => {
  return (
    <>
      <div className={`${styles.otta_header} text-center pb-4 pt-5`}>
        <OttaHeading title="Where would you like to work?" />
      </div>
      <div className={`${styles.option_wrapper} option_wrapper`}>
        <div className="d-flex flex-wrap col-md-5 mx-auto py-3">
          {questionlist.map((list) => (
            <Form.Group key={generate()} controlId={list.control_id}>
              <Form.Check type="checkbox" label={list.label} />
            </Form.Group>
          ))}
        </div>
      </div>
    </>
  );
};

export default Step2;
