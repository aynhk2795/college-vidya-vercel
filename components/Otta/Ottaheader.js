import React from "react";
import { Row, Col, Modal } from "react-bootstrap";
import Logo from "/components/Global/Logo";
import Login from "./Modal/Login";
import { useAuth } from "../../hooks/auth";
import { useState, useEffect } from "react";
import { UserService } from "../../service/UserService";
import { useAxiosJWT } from "../../hooks/axios";
import LoginProfile from "../Global/LoginProfile";
import SignUp from "./Modal/SignUp";
import { IoReload } from "react-icons/io5";
import Tippy from "@tippyjs/react";
import { AiOutlineQuestionCircle } from "react-icons/ai";
import ContactForm from "../Contact/ContactForm";

const Ottaheader = ({ menuData }) => {
  const { user, logout } = useAuth();
  const { axiosJWT } = useAxiosJWT();

  const [username, setUsername] = useState(null);
  const [loggedIn, setLoggedIn] = useState(false);

  const [showAskUs, setShowAskUs] = useState(false);

  useEffect(() => {
    if (user) {
      const userService = new UserService(axiosJWT);
      userService
        .getUserDetails()
        .then((response) => {
          setLoggedIn(true);
          setUsername(response.data.name);
        })
        .catch(() => {
          logout();
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return (
    <>
      <div>
        <Row className="py-2 px-4">
          <Col>
            <Logo />
          </Col>
          <Col className="d-flex align-items-center justify-content-end col-8">
            <div className="d-flex align-items-center">
              {loggedIn ? (
                <LoginProfile username={username} />
              ) : (
                <SignUp menuData={menuData} />
              )}
            </div>
            <Tippy
              content={<span className="fs-14 fw-normal">Start Over</span>}
              animation="scale"
              allowHTML={true}>
              <span>
                <a href="/suggest-me-an-university" className="text-dark ms-3">
                  <IoReload fontSize={20} />{" "}
                </a>
              </span>
            </Tippy>
            <Tippy
              content={<span className="fs-14 fw-normal">Ask Us Anything</span>}
              animation="scale"
              allowHTML={true}>
              <span
                onClick={() => setShowAskUs(true)}
                className="text-dark ms-3 cursor-pointer">
                <AiOutlineQuestionCircle fontSize={20} />
              </span>
            </Tippy>
          </Col>
        </Row>
      </div>
      <Modal show={showAskUs} onHide={() => setShowAskUs(false)} centered>
        <Modal.Header closeButton>
          <Modal.Title>Ask Us Anything</Modal.Title>
        </Modal.Header>
        <Modal.Body className="px-4">
          <ContactForm />
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Ottaheader;
