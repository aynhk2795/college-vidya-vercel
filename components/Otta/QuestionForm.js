import React, { useEffect, useRef, useState } from "react";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Tabs,
  Tab,
  Row,
  Col,
  Container,
  Stack,
  ProgressBar,
  Form,
  Spinner,
} from "react-bootstrap";
import { Button as BSButton } from "react-bootstrap";
import { AiOutlineClockCircle } from "react-icons/ai";
import Avatar from "/components/universityListingPage/Avatar";
import OttaHeading from "./OttaHeading";
import { RadioButton } from "primereact/radiobutton";
import Image from "next/image";
import { generate } from "shortid";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import LabelHeart from "./LabelHeart";
import degree from "/public/images/degree.svg";
import { Button } from "primereact/button";
import styles from "/components/Otta/QuestionForm.module.scss";
import { getQuestions } from "../../service/CourseService";
import { flushSync } from "react-dom";
import BudgetWheel from "./BudgetWheel";
import { useAuth } from "../../hooks/auth";
import { InputText } from "primereact/inputtext";
import { Calendar } from "primereact/calendar";
import { Dropdown } from "primereact/dropdown";
import moment from "moment/moment";
import { addLead } from "../../service/LeadService";
import { useRouter } from "next/router";
import male from "/public/images/male.png";
import female from "/public/images/female.png";
import { FaLock } from "react-icons/fa";
import Link from "next/link";
import { getStates } from "../../service/MiscService";

export default function QuestionForm({ completeMenuData }) {
  const router = useRouter();

  const { user } = useAuth();
  const [loggedIn, setLoggedIn] = useState(false);

  const [states, setStates] = useState([]);
  const [calculatedAge, setCalculatedAge] = useState(null);

  const [questionsAnswered, setQuestionsAnswered] = useState(false);
  const [fetchingQuestions, setFetchingQuestions] = useState(false);

  const formRef = useRef(null);

  const [validationSchema, setValidationSchema] = useState(
    Yup.object().shape({})
  );

  const {
    register,
    handleSubmit,
    setValue,
    getValues,
    formState: { errors },
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  const [currentTab, setCurrentTab] = useState(0);
  const [now, nowSet] = React.useState(0);

  const [questions, setQuestions] = useState([
    {
      id: generate(),
      question: "Which degree are you interested in?",
      type: "radio",
      options: completeMenuData.map(({ id, name }) => ({ id, name })),
      required: true,
      key: "domain-question",
    },
    {
      id: generate(),
      question: "Which course would you like to pursue?",
      type: "radio",
      options: [],
      required: true,
      key: "course-question",
    },
    {
      id: generate(),
      question: "Have a particular specialization in mind?",
      type: "radio",
      options: [],
      required: true,
      key: "specialization-question",
    },
  ]);

  useEffect(() => {
    if (user) {
      setLoggedIn(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  useEffect(() => {
    if (getValues("domain_id")) {
      setValue("course_id", undefined, { shouldValidate: true });
      let allQuestions = [...questions];

      const courseQuestion = allQuestions[1];

      courseQuestion.options = completeMenuData
        .filter((menu) => menu.id === getValues("domain_id"))[0]
        .courses.map(({ id, display_name: name, icon }) => ({
          id,
          name,
          icon,
        }));

      allQuestions = allQuestions.filter(
        (question) => question.id !== courseQuestion.id
      );
      allQuestions.splice(1, 0, courseQuestion);

      setQuestions(allQuestions);
      setQuestions([...questions].slice(0, 3));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getValues("domain_id")]);

  useEffect(() => {
    if (getValues("course_id")) {
      setValue("specialization_id", -1, { shouldValidate: true });
      let allQuestions = [...questions];

      const specializationQuestion = allQuestions[2];

      specializationQuestion.options = completeMenuData
        .filter((menu) => menu.id === getValues("domain_id"))[0]
        .courses.filter((course) => course.id === getValues("course_id"))[0]
        .specializations.map(({ id, display_name: name }) => ({ id, name }));

      allQuestions = allQuestions.filter(
        (question) => question.id !== specializationQuestion.id
      );
      allQuestions.splice(2, 0, specializationQuestion);

      setQuestions(allQuestions);
      setQuestions([...questions].slice(0, 3));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getValues("course_id")]);

  useEffect(() => {
    if (
      getValues("specialization_id") &&
      getValues("specialization_id") !== -1
    ) {
      setQuestions([...questions].slice(0, 3));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getValues("specialization_id")]);

  useEffect(() => {
    if (getValues("dob")) {
      const dob = getValues("dob");
      const dateToday = new Date();

      setCalculatedAge(moment.duration(dateToday - dob).years() + " Years");
    } else {
      setCalculatedAge(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getValues("dob")]);

  useEffect(() => {
    getStates().then((response) => setStates(response.data));
  }, []);

  const createValidationSchema = (currentQuestion) => {
    let schema = {};

    if (!loggedIn && questionsAnswered) {
      schema["gender"] = Yup.string().required("Please select a gender");
      schema["full_name"] = Yup.string().required("Please enter your name");
      schema["email"] = Yup.string()
        .email("Invalid email")
        .required("Please enter your email address");
      schema["mobile_number"] = Yup.number()
        .typeError("Invalid number")
        .required("Number is required")
        .test(
          "len",
          "Number must be exactly 10 digits",
          (val) => val.toString().length === 10
        );
      schema["dob"] = Yup.string()
        .typeError("Invalid date")
        .required("Please enter your date of birth");
      schema["state"] = Yup.string().required("Please select a state");
    }

    if (currentQuestion.key === "domain-question") {
      schema["domain_id"] = Yup.number().required(
        `Please select a degree type`
      );
    } else if (currentQuestion.key === "course-question") {
      schema["course_id"] = Yup.number().required(`Please select a course`);
    } else if (currentQuestion.key === "specialization-question") {
      schema["specialization_id"] = Yup.number();
    }

    //Dynamic Question Validation
    else if (currentQuestion.required && currentQuestion.type === "radio") {
      schema[currentQuestion.id.toString()] = Yup.string().required(
        "Please select an option"
      );
    }

    setValidationSchema(Yup.object().shape(schema));
  };

  useEffect(() => {
    if (currentTab !== 0) {
      nowSet((currentTab / questions.length) * 100);
    }
    if (
      loggedIn &&
      questions.length > 3 &&
      currentTab === questions.length - 1
    ) {
      nowSet(100);
    }

    if (currentTab === 1) {
      nowSet(10);
    }

    if (currentTab === 2) {
      nowSet(20);
    }

    if (currentTab <= questions.length) {
      createValidationSchema(questions[currentTab]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentTab, questions, questionsAnswered]);

  useEffect(() => {
    if (loggedIn && questionsAnswered) {
      handleSubmit(onSubmit)();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loggedIn, questionsAnswered]);

  const onSubmit = (data) => {
    if (!loggedIn && questionsAnswered) {
      setFetchingQuestions(true);

      const lead_data = {
        gender: data.gender,
        name: data.full_name,
        email: data.email,
        mobile_number: data.mobile_number,
        dob: data.dob,
        state: data.state,
        course_id: data.course_id,
        specialization_id:
          data.specialization_id === -1 ? "" : data.specialization_id,
        state: data.state,
      };

      delete data.domain_id;
      delete data.gender;
      delete data.full_name;
      delete data.email;
      delete data.mobile_number;
      delete data.dob;
      delete data.state;
      delete data.course_id;
      delete data.specialization_id;
      delete data.state;

      let formData = new FormData();
      formData.append("user_id", -1);
      formData.append("gender", lead_data.gender);
      formData.append("name", lead_data.name);
      formData.append("email", lead_data.email);
      formData.append("mobile", lead_data.mobile_number);
      formData.append("dob", lead_data.dob);
      formData.append("course", lead_data.course_id);
      formData.append("specializations", lead_data.specialization_id);
      formData.append("state", lead_data.state);
      formData.append("question_answers", JSON.stringify(data));

      addLead(formData)
        .then((response) => {
          setFetchingQuestions(false);

          router.push(
            `/colleges-universities/${
              completeMenuData
                .map((menuData) => menuData.courses)
                .flat()
                .filter((course) => course.id === lead_data.course_id)[0].slug
            }/${
              lead_data.specialization_id !== ""
                ? completeMenuData
                    .map((menuData) => menuData.courses)
                    .flat()
                    .map((course) => course.specializations)
                    .flat()
                    .filter(
                      (specialization) =>
                        specialization.id === lead_data.specialization_id
                    )[0].slug
                : ""
            }/?uid=${response.data.user_id}`
          );
        })
        .catch(() => {
          setFetchingQuestions(false);
          iziToast.error({
            title: "Error",
            message: "Oops! Something went wrong",
            timeout: 2000,
            position: "topRight",
          });
        });
    }

    if (loggedIn && questionsAnswered) {
      setFetchingQuestions(true);

      const lead_data = {
        user_id: user,
        course_id: data.course_id,
        specialization_id:
          data.specialization_id === -1 ? "" : data.specialization_id,
      };

      delete data.domain_id;
      delete data.course_id;
      delete data.specialization_id;

      let formData = new FormData();
      formData.append("user_id", user);
      formData.append("course", lead_data.course_id);
      formData.append("specializations", lead_data.specialization_id);
      formData.append("question_answers", JSON.stringify(data));

      addLead(formData)
        .then(() => {
          setFetchingQuestions(false);
          router.push(
            `/colleges-universities/${
              completeMenuData
                .map((menuData) => menuData.courses)
                .flat()
                .filter((course) => course.id === lead_data.course_id)[0].slug
            }/${
              lead_data.specialization_id !== ""
                ? completeMenuData
                    .map((menuData) => menuData.courses)
                    .flat()
                    .map((course) => course.specializations)
                    .flat()
                    .filter(
                      (specialization) =>
                        specialization.id === lead_data.specialization_id
                    )[0].slug
                : ""
            }/?uid=${user}`
          );
        })
        .catch(() => {
          setFetchingQuestions(false);
          iziToast.error({
            title: "Error",
            message: "Oops! Something went wrong",
            timeout: 2000,
            position: "topRight",
          });
        });
    }

    if (currentTab !== questions.length - 1) {
      setCurrentTab((prev) => prev + 1);
    } else {
      if (currentTab === 2) {
        //Fetch Questions
        const course_id = data.course_id;
        const specialization_id =
          data.specialization_id === -1 ? "" : data.specialization_id;

        setFetchingQuestions(true);
        getQuestions(course_id, specialization_id)
          .then((response) => {
            setFetchingQuestions(false);
            const receivedQuestions = response.data.data[0].questions;

            let currentQuestions = [...questions];
            currentQuestions = currentQuestions.concat(receivedQuestions);

            flushSync(() => {
              setQuestions(currentQuestions);
            });
            setCurrentTab((prev) => prev + 1);
          })
          .catch(() => {
            setFetchingQuestions(false);
          });
      } else {
        if (currentTab === questions.length - 1) {
          if (document.getElementById("budget-val")) {
            const budget = document.getElementById("budget-val").innerHTML;
            data.budget = budget;
          }
          nowSet(100);
          setQuestionsAnswered(true);
        }
      }
    }
  };

  return (
    <Container fluid className={`${styles.otta_container} px-0 mt-3`}>
      <Form onSubmit={handleSubmit(onSubmit)} ref={formRef}>
        {currentTab === 0 ? (
          <div className={`${styles.notifybar}`}>
            <p className={`m-0 text-center text-white fs-12 py-2`}>
              <AiOutlineClockCircle fontSize={18} /> Your best matches are 2
              minutes away!
            </p>
          </div>
        ) : (
          <div className="position-relative">
            <ProgressBar
              className={`${styles.progress_bar} rounded-0`}
              now={now}
            />
            <div
              className={`position-absolute top-0 start-50 translate-middle`}>
              <Avatar />
            </div>
          </div>
        )}
        <Row>
          <Col>
            {!questionsAnswered ? (
              <Tabs activeKey={currentTab} className={`${styles.ottastep}`}>
                {questions.map((question, index) =>
                  index === 0 ? (
                    <Tab
                      title={""}
                      key={question.id}
                      eventKey={index}
                      disabled={index === 0}
                      className={`${styles.otta_panel}`}>
                      <>
                        <div
                          className={`${styles.otta_header} text-center py-4`}>
                          <OttaHeading title={question.question} />
                        </div>
                        <div
                          className={`${styles.option_wrapper} option_wrapper`}>
                          <div className="d-flex flex-wrap col-md-12 mx-auto py-3 justify-content-center mt-4 position-relative ">
                            {question.options.map((option, index) => (
                              <div
                                className="position-relative"
                                key={generate()}>
                                <RadioButton
                                  inputId={option.name}
                                  value={option.id}
                                  {...register("domain_id")}
                                  checked={getValues("domain_id") === option.id}
                                  onChange={(e) =>
                                    setValue("domain_id", e.value, {
                                      shouldValidate: true,
                                    })
                                  }
                                />
                                <label
                                  htmlFor={option.name}
                                  className={`${styles.otta_degree} d-flex flex-column justify-content-center m-3 px-3`}>
                                  <Image src={degree} alt="" />{" "}
                                  <span className="d-inline-block mt-3 text-center fs-14">
                                    {option.name}
                                  </span>
                                </label>
                              </div>
                            ))}
                            <span
                              className="p-error text-xs position-absolute start-50 translate-middle-x"
                              style={{ top: "-13px" }}>
                              {errors["domain_id"]?.message}
                            </span>
                          </div>
                        </div>
                      </>
                    </Tab>
                  ) : index === 1 ? (
                    <Tab
                      title={""}
                      key={question.id}
                      eventKey={index}
                      disabled={index === 0}
                      className={`${styles.otta_panel}`}>
                      <>
                        <div
                          className={`${styles.otta_header} text-center py-4`}>
                          <OttaHeading title={question.question} />
                        </div>
                        <div
                          className={`${styles.option_wrapper} option_wrapper`}>
                          <div
                            className={`${styles.course_wrap} d-flex flex-wrap col col-sm-8 col-md-8 col-lg-7 col-xl-5  mx-auto py-3 justify-content-center mt-4 position-relative`}>
                            {question.options.map((option) => (
                              <div
                                className="position-relative"
                                key={generate()}>
                                <RadioButton
                                  inputId={option.name}
                                  value={option.id}
                                  {...register("course_id")}
                                  checked={getValues("course_id") === option.id}
                                  onChange={(e) =>
                                    setValue("course_id", e.value, {
                                      shouldValidate: true,
                                    })
                                  }
                                />
                                <label
                                  htmlFor={option.name}
                                  className={`${styles.otta_course} d-flex flex-column justify-content-center m-2 px-2`}>
                                  {" "}
                                  <img
                                    src={option.icon}
                                    width={30}
                                    height={30}
                                    alt=""
                                    className="mx-auto"
                                  />{" "}
                                  <span className="d-inline-block mt-3 text-center fs-14">
                                    {option.name}
                                  </span>
                                </label>
                              </div>
                            ))}

                            <span
                              className="p-error text-xs position-absolute start-50 translate-middle-x"
                              style={{ top: "-13px" }}>
                              {errors["course_id"]?.message}
                            </span>
                          </div>
                        </div>
                      </>
                    </Tab>
                  ) : index === 2 ? (
                    <Tab
                      title={""}
                      key={question.id}
                      eventKey={index}
                      disabled={index === 0}
                      className={`${styles.otta_panel}`}>
                      <>
                        <div
                          className={`${styles.otta_header} text-center py-4`}>
                          <OttaHeading title={question.question} />
                        </div>
                        <div
                          className={`${styles.option_wrapper} option_wrapper`}>
                          <div className="d-flex flex-wrap col-md-5 mx-auto py-3 justify-content-center mt-4 position-relative">
                            {question.options.map((option) => (
                              <div
                                className="position-relative"
                                key={generate()}>
                                <RadioButton
                                  inputId={option.name}
                                  value={option.id}
                                  {...register("specialization_id")}
                                  checked={
                                    getValues("specialization_id") === option.id
                                  }
                                  onChange={(e) =>
                                    setValue("specialization_id", e.value, {
                                      shouldValidate: true,
                                    })
                                  }
                                />
                                <label htmlFor={option.name}>
                                  {" "}
                                  {getValues("specialization_id") ===
                                  option.id ? (
                                    <LabelHeart labelText={option.name} />
                                  ) : (
                                    option.name
                                  )}
                                </label>
                              </div>
                            ))}
                          </div>
                        </div>
                      </>
                    </Tab>
                  ) : (
                    <Tab
                      title={""}
                      key={question.id}
                      eventKey={index}
                      disabled={index === 0}
                      className={`${styles.otta_panel}`}>
                      <>
                        <div
                          className={`${styles.otta_header} text-center py-4`}>
                          <OttaHeading title={question.question} />
                        </div>
                        <div
                          className={`${styles.option_wrapper} option_wrapper`}>
                          <div className="d-flex flex-wrap col-md-5 mx-auto py-3 justify-content-center mt-4 position-relative">
                            {question.type === "radio" ? (
                              question.options.map((option) => {
                                const randomFor = generate();
                                return (
                                  <div
                                    className={`${styles.yes_no} position-relative`}
                                    key={generate()}>
                                    <RadioButton
                                      inputId={`${option.answer}-${randomFor}`}
                                      value={option.answer}
                                      {...register(question.id.toString())}
                                      checked={
                                        getValues(question.id.toString()) ===
                                        option.answer
                                      }
                                      onChange={(e) =>
                                        setValue(
                                          question.id.toString(),
                                          e.value,
                                          {
                                            shouldValidate: true,
                                          }
                                        )
                                      }
                                    />
                                    <label
                                      htmlFor={`${option.answer}-${randomFor}`}>
                                      {" "}
                                      {getValues(question.id.toString()) ===
                                      option.answer ? (
                                        <LabelHeart labelText={option.answer} />
                                      ) : (
                                        option.answer
                                      )}
                                    </label>
                                  </div>
                                );
                              })
                            ) : question.type === "budget" ? (
                              <BudgetWheel />
                            ) : null}
                            <span
                              className="p-error text-xs position-absolute start-50 translate-middle-x"
                              style={{ top: "-13px" }}>
                              {errors[question.id.toString()]?.message}
                            </span>
                          </div>
                        </div>
                      </>
                    </Tab>
                  )
                )}
              </Tabs>
            ) : !loggedIn ? (
              <div className={`${styles.otta_panel}`}>
                <div className={`${styles.otta_header} text-center pb-4 pt-5`}>
                  <OttaHeading title="More about you" />
                </div>
                <div className={`${styles.option_wrapper} option_wrapper`}>
                  <div className="col col-sm-12 col-md-10 col-lg-8 col-xl-5 mx-auto py-3 justify-content-center mt-4 position-relative shadow-1 bg-white p-3 p-sm-5 py-3 py-sm-5 rounded">
                    <Row>
                      <Col>
                        <RadioButton
                          inputId={"male"}
                          checked={getValues("gender") === "male"}
                          onChange={() =>
                            setValue("gender", "male", {
                              shouldValidate: true,
                            })
                          }
                        />
                        <label
                          className="d-flex align-items-center py-4"
                          htmlFor={"male"}>
                          {" "}
                          <Image
                            className="bg-white rounded-circle"
                            src={male}
                            width={30}
                            height={30}
                            alt=""
                          />{" "}
                          <span className="ms-2">Male</span>
                        </label>
                      </Col>

                      <Col>
                        <RadioButton
                          inputId={"female"}
                          checked={getValues("gender") === "female"}
                          onChange={() =>
                            setValue("gender", "female", {
                              shouldValidate: true,
                            })
                          }
                        />
                        <label
                          className="d-flex align-items-center py-4"
                          htmlFor={"female"}>
                          <Image
                            className="bg-white rounded-circle"
                            src={female}
                            width={30}
                            height={30}
                            alt=""
                          />{" "}
                          <span className="ms-2">Female</span>
                        </label>
                      </Col>
                      <span className="p-error text-xs d-block">
                        {errors["gender"]?.message}
                      </span>
                    </Row>
                    <Row className="pt-5">
                      <Col className="position-relative">
                        <div
                          className={`${styles.otta_lastform_label} pb-1 position-absolute top-0 start-0 translate-middle-y ms-3 bg-white px-3 rounded fs-14`}>
                          Full Name
                        </div>
                        <InputText
                          {...register("full_name")}
                          className={`w-100 ${
                            errors.full_name ? "p-invalid block" : ""
                          }`}
                        />
                      </Col>
                      <span className="p-error text-xs d-block">
                        {errors["full_name"]?.message}
                      </span>
                    </Row>
                    <Row className="pt-5">
                      <Col className="position-relative">
                        <div
                          className={`${styles.otta_lastform_label} pb-1 position-absolute top-0 start-0 translate-middle-y ms-3 bg-white px-3 rounded fs-14`}>
                          Email Address
                        </div>
                        <InputText
                          {...register("email")}
                          className={`w-100 ${
                            errors.email ? "p-invalid block" : ""
                          }`}
                        />
                      </Col>
                      <span className="p-error text-xs d-block">
                        {errors["email"]?.message}
                      </span>
                    </Row>
                    <Row className="pt-5">
                      <Col className="position-relative">
                        <div
                          className={`${styles.otta_lastform_label} pb-1 position-absolute top-0 start-0 translate-middle-y ms-3 bg-white px-3 rounded fs-14`}>
                          Mobile Number
                        </div>
                        <InputText
                          {...register("mobile_number")}
                          className={`w-100 ${
                            errors.mobile_number ? "p-invalid block" : ""
                          }`}
                        />
                        <span className="p-error text-xs d-block">
                          {errors["mobile_number"]?.message}
                        </span>
                      </Col>
                      <Col className="position-relative">
                        <div
                          className={`${styles.otta_lastform_label} pb-1 position-absolute top-0 start-0 translate-middle-y ms-3 bg-white px-3 rounded fs-14`}
                          style={{ zIndex: "1" }}>
                          Date of Birth (DD/MM/YYY)
                        </div>
                        <div className="position-relative">
                          <Calendar
                            value={getValues("dob")}
                            inputMode={"numeric"}
                            onChange={(e) =>
                              setValue("dob", e.value, { shouldValidate: true })
                            }
                            dateFormat="dd/mm/yy"
                            mask="99/99/9999"
                            className={`w-100 ${
                              errors.dob ? "p-invalid block" : ""
                            }`}
                          />
                          {calculatedAge ? (
                            <span
                              className="position-absolute end-0 top-50 translate-middle-y me-2 px-3 py-2 rounded fs-14"
                              style={{ backgroundColor: "aliceblue" }}>
                              {calculatedAge}
                            </span>
                          ) : null}
                        </div>
                        <span className="p-error text-xs d-block">
                          {errors["dob"]?.message}
                        </span>
                      </Col>
                    </Row>
                    <Row className="pt-5">
                      <Col className="position-relative">
                        <div
                          className={`${styles.otta_lastform_label} pb-1 position-absolute top-0 start-0 translate-middle-y ms-3 bg-white px-3 rounded fs-14`}
                          style={{ zIndex: "1" }}>
                          State
                        </div>
                        <Dropdown
                          options={states}
                          value={getValues("state")}
                          onChange={(e) =>
                            setValue("state", e.value, { shouldValidate: true })
                          }
                          className={`w-100 ${
                            errors.state ? "p-invalid block" : ""
                          }`}
                          optionValue="id"
                          optionLabel="state"
                        />
                      </Col>
                      <span className="p-error text-xs d-block">
                        {errors["state"]?.message}
                      </span>
                    </Row>
                    <Row className="pt-1">
                      <Col>
                        <div className="text-center ">
                          <div className="mt-3 text-center bg-secondary d-inline-block rounded">
                            <span className="fs-12 fw-normal px-3 text-white d-flex align-items-center py-1">
                              <FaLock className="lock-icon" /> Your personal
                              information is secure with us
                            </span>
                          </div>
                        </div>
                        <div className="text-center mt-2 fs-14">
                          <small>
                            By clicking, you agree to our{" "}
                            <Link href="/privacy-policy">
                              <a className="text-decoration-none">
                                Privacy Policy,Terms of Use
                              </a>
                            </Link>
                            &amp; Disclaimers
                          </small>
                          <small className="text-secondary d-block">
                            {" "}
                            (Standard T&amp;C Apply)
                          </small>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </div>
              </div>
            ) : (
              <div className={`${styles.otta_panel}`}>
                <div className={`${styles.otta_header} text-center pb-4 pt-5`}>
                  <OttaHeading title="Fetching best universities for you" />
                </div>
                <div className={`${styles.option_wrapper} option_wrapper`}>
                  <div className="d-flex items-center justify-content-center">
                    <Spinner animation="border my-5" />
                  </div>
                </div>
              </div>
            )}
          </Col>
        </Row>
        <Stack
          gap={3}
          direction="horizontal"
          style={{ zIndex: "111" }}
          className={`${styles.otta_header} d-flex justify-content-center  py-3 border-top position-fixed w-100 bottom-0`}>
          {currentTab !== 0 ? (
            <BSButton
              className={`${styles.prevBtn} py-2 py-sm-3 col col-sm-3`}
              disabled={currentTab === 0}
              onClick={() => {
                if (!questionsAnswered) {
                  setCurrentTab((prev) => prev - 1);
                } else {
                  nowSet(
                    ((questions.length - 2) / (questions.length - 1)) * 100
                  );
                }
                setQuestionsAnswered(false);
              }}>
              Back
            </BSButton>
          ) : null}
          <Button
            type="submit"
            id="submit-btn-now"
            className={`${styles.nextBtn} py-2 py-sm-3 col col-sm-3`}
            label={
              questionsAnswered
                ? "Submit"
                : loggedIn &&
                  questions.length > 3 &&
                  currentTab === questions.length - 1
                ? "Submit"
                : "Next"
            }
            loading={fetchingQuestions}
          />
        </Stack>
      </Form>
    </Container>
  );
}
