import React from "react";
import { Form } from "react-bootstrap";
import { generate } from "shortid";
import OttaHeading from "./OttaHeading";
import LabelHeart from "/components/Otta/LabelHeart";
import styles from "/components/Otta/QuestionForm.module.scss";

const questionlist = [
  {
    control_id: "f35",
    label: <LabelHeart labelText="Diversity and inclusion" />,
  },
  {
    control_id: "f36",
    label: <LabelHeart labelText="Development and progression" />,
  },

  {
    control_id: "f37",
    label: <LabelHeart labelText="Diversity and inclusion" />,
  },
  {
    control_id: "f38",
    label: <LabelHeart labelText="Development and progression" />,
  },
  {
    control_id: "f39",
    label: <LabelHeart labelText="Recognition and reward" />,
  },
  {
    control_id: "f40",
    label: <LabelHeart labelText="Transparency and respect" />,
  },
  {
    control_id: "f41",
    label: <LabelHeart labelText="Diversity and inclusion" />,
  },
  {
    control_id: "f42",
    label: <LabelHeart labelText="Development and progression" />,
  },
  {
    control_id: "f43",
    label: <LabelHeart labelText="Recognition and reward" />,
  },
  {
    control_id: "f44",
    label: <LabelHeart labelText="Transparency and respect" />,
  },
  {
    control_id: "f45",
    label: <LabelHeart labelText="Development and progression" />,
  },
  {
    control_id: "f46",
    label: <LabelHeart labelText="Recognition and reward" />,
  },
];
const Step3 = () => {
  return (
    <>
      <div className={`${styles.otta_header} text-center pb-4 pt-5`}>
        <OttaHeading title="Where would you like to work?" />
      </div>
      <div className={`${styles.option_wrapper} option_wrapper`}>
        <div className="d-flex flex-wrap col-md-5 mx-auto py-3">
          {questionlist.map((list) => (
            <Form.Group key={generate()} controlId={list.control_id}>
              <Form.Check type="checkbox" label={list.label} />
            </Form.Group>
          ))}
        </div>
      </div>
    </>
  );
};

export default Step3;
