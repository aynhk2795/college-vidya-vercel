import React from "react";
import { BsFillSuitHeartFill } from "react-icons/bs";
import styles from "./LabelHeart.module.scss";

const LabelHeart = (props) => {
  return (
    <>
      <span
        className={`${styles.label_icon} me-2`}
        style={{ marginTop: "-2px" }}>
        <BsFillSuitHeartFill />
      </span>
      <span>{props.labelText}</span>
    </>
  );
};

export default LabelHeart;
