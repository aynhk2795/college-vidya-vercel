import Link from "next/link";
import React from "react";
import { Form, Row, Col } from "react-bootstrap";
import { generate } from "shortid";
import OttaHeading from "./OttaHeading";
// import LabelHeart from "/components/Otta/LabelHeart";
// import Form from 'react-bootstrap/Form';
import styles from "/components/Otta/QuestionForm.module.scss";

const SuggestLeadForm = () => {
  return (
    <>
      <div className={`${styles.otta_header} text-center pb-4 pt-5`}>
        <OttaHeading title="More about you" />
      </div>
      <div className={`${styles.option_wrapper} option_wrapper`}>
        <Row>
          <Col md={3} className="mx-auto">
            <Form.Label className="bg-transparent ps-0">Name</Form.Label>
            <Form.Control type="text" id="fname" className="py-3" />
            <Form.Text className="text-danger d-none" id="passwordHelpBlock">
              Please fill in this field.
            </Form.Text>

            <Form.Label className="bg-transparent ps-0">Email</Form.Label>
            <Form.Control type="email" id="email" className="py-3" />
            <Form.Text className="text-danger d-none" id="passwordHelpBlock">
              Please fill in this field.
            </Form.Text>

            <Form.Label className="bg-transparent ps-0">Number</Form.Label>
            <Form.Control type="text" id="number" className="py-3" />
            <Form.Text className="text-danger d-none" id="passwordHelpBlock">
              Please fill in this field.
            </Form.Text>

            <Form.Label className="bg-transparent ps-0">State</Form.Label>
            <Form.Control type="text" id="state" className="py-3" />
            <Form.Text className="text-danger d-none" id="passwordHelpBlock">
              Please fill in this field.
            </Form.Text>

            <Form.Label className="bg-transparent ps-0">
              Date of birth
            </Form.Label>
            <Form.Control type="date" id="dob" className="py-3" />
            <Form.Text className="text-danger d-none" id="passwordHelpBlock">
              Please fill in this field.
            </Form.Text>

            <p className="fs-12 my-4 text-center">
              By signing up you agree to our{" "}
              <span className="fw-bold">
                <Link href="/ourpolicy">
                  <a>Privacy Policy</a>
                </Link>
              </span>{" "}
              and <span className="fw-bold"> </span> for Candidates.
            </p>
            <p className="m-0 position-relative">
              <hr />
              <span
                className={`${styles.bgspan} position-absolute top-0 start-50 translate-middle px-4 text-secondary`}>
                OR
              </span>
            </p>
            <p className="fs-12 my-4 text-center">
              <span className="fw-bold">
                {" "}
                <Link href="/terms">
                  <a>Terms & Conditions</a>
                </Link>
              </span>{" "}
              for Candidates.
            </p>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default SuggestLeadForm;
