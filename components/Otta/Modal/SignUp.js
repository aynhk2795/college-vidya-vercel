import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { BsFillPersonFill } from "react-icons/bs";
import AuthModal from "../../Global/Modals/AuthModal";

const SignUp = ({ menuData, screenSize }) => {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
      <p
        className="m-0 cursor-pointer px-2 textprimary rounded ms-2 fs-14"
        style={{ border: "1px solid #0074dc", padding: "9px 15px" }}
        onClick={() => setModalShow(true)}>
        {screenSize && screenSize.width < 1200 ? (
          <>
            <BsFillPersonFill />{" "}
          </>
        ) : (
          "Sign in "
        )}
      </p>

      <Modal
        show={modalShow}
        onHide={() => setModalShow(false)}
        backdrop="static"
        size={"md"}
        centered>
        <AuthModal
          menuData={menuData}
          initialMode={"login"}
          closeModal={() => setModalShow(false)}
        />
      </Modal>
    </>
  );
};

export default SignUp;
