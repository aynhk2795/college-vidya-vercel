import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import OnlyLogo from "../../Global/OnlyLogo";
import AnimatedBird from "../../Global/AnimatedBird";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { FaLock } from "react-icons/fa";
import Link from "next/link";
import styles from "./LoginForm.module.scss";
import SignUp from "./SignUp";
import { Form } from "react-bootstrap";

//Form Validation Components
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { setCookie } from "cookies-next";
import { getOTP } from "../../../service/MiscService";
import { flushSync } from "react-dom";
import { InputNumber } from "primereact/inputnumber";
import { useAuth } from "../../../hooks/auth";
import { useRouter } from "next/router";

function MyVerticallyCenteredModal(props) {
  const { login } = useAuth();
  const router = useRouter();

  const [state, setState] = useState(false);
  const [sendingOTP, setSendingOTP] = useState(false);
  const [OTPfromServer, setOTPfromServer] = useState(null);
  const [OTPfromUser, setOTPfromUser] = useState(null);
  const [stepOneDetails, setStepOneDetails] = useState(null);
  const [submittingForm, setSubmittingForm] = useState(false);

  const validationSchema = Yup.object().shape({
    mobile_number: Yup.number()
      .typeError("Invalid number")
      .required("Number is required")
      .test(
        "len",
        "Number must be exactly 10 digits",
        (val) => val.toString().length === 10
      ),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {},
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  const submitStepOne = (data) => {
    setSendingOTP(true);

    getOTP(data.mobile_number)
      .then((response) => {
        setSendingOTP(false);

        flushSync(() => {
          setOTPfromServer(response.data.otp);
          setStepOneDetails(data);
        });
        setState(true);
      })
      .catch(() => {
        setSendingOTP(false);

        iziToast.error({
          title: "Error",
          message: "Oops! Something went wrong",
          timeout: 2000,
          position: "topRight",
        });
      });
  };

  const submitStepTwo = () => {
    setSubmittingForm(true);

    if (OTPfromServer.toString() === OTPfromUser.toString()) {
      const data = {
        mobile: stepOneDetails.mobile_number,
      };

      login(data)
        .then((response) => {
          iziToast.success({
            title: "Success",
            message: "Login Successful",
            timeout: 2000,
            position: "topRight",
          });

          let date = new Date();
          date.setDate(date.getDate() + 30);

          setCookie("cvuid", response.data.user_id, {
            expires: date,
          });
          setCookie("atexp", response.data.jwt, {
            expires: date,
          });
          setCookie("ratnxp", response.data.refresh, {
            expires: date,
          });

          setSubmittingForm(false);
          router.reload();
        })
        .catch((error) => {
          setSubmittingForm(false);

          if (error.response && error.response.status === 403) {
            iziToast.error({
              title: "Error",
              message: "No account with this mobile number",
              timeout: 2000,
              position: "topRight",
            });
          } else {
            iziToast.error({
              title: "Error",
              message: "Oops! Something went wrong",
              timeout: 2000,
              position: "topRight",
            });
          }
        });
    } else {
      setSubmittingForm(false);

      iziToast.error({
        title: "Error",
        message: "Invalid OTP",
        timeout: 2000,
        position: "topRight",
      });
    }
  };

  return (
    <Modal
      {...props}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header
        closeButton
        className={`${styles.closebtn} border-0 justify-content-center`}>
        <Modal.Title id="contained-modal-title-vcenter">
          <div className="d-flex align-items-center">
            <OnlyLogo />
            <span className="fs-14">#ChunoWahiJoHaiSahi</span> <AnimatedBird />
          </div>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="form-demo">
          <div className="flex justify-content-center">
            <div>
              {state ? (
                <div className="otp-panel">
                  <div className="pb-4">
                    <p style={{ fontSize: "20px" }} className="m-0 fw-bold">
                      Please enter,
                      <br /> 6 digit OTP sent on{" "}
                      {stepOneDetails ? stepOneDetails.mobile_number : ""}
                    </p>
                    <p
                      className="fs-14 mt-3 textprimary cursor-pointer"
                      onClick={() => setState(false)}>
                      Change Mobile Number
                    </p>
                  </div>
                  <label className="mb-2" htmlFor="Mobile Number">
                    Enter OTP
                  </label>
                  <InputNumber
                    className="w-100"
                    onChange={(e) => setOTPfromUser(e.value)}
                    useGrouping={false}
                  />
                  <Button
                    className={`${styles.bluebtn} w-100 mt-3 mb-4 d-flex justify-content-center`}
                    disabled={
                      !OTPfromUser || OTPfromUser.toString().length !== 6
                    }
                    label={"Confirm OTP"}
                    onClick={() => submitStepTwo()}
                    loading={submittingForm}
                  />
                  <div className="text-center">
                    {" "}
                    <span className="fs-12">
                      Didn&apos;t receive the OTP yet?
                    </span>{" "}
                    <Link href="">
                      <a>Resend OTP</a>
                    </Link>
                  </div>
                </div>
              ) : (
                <Form onSubmit={handleSubmit(submitStepOne)}>
                  <div className="pb-4">
                    <p style={{ fontSize: "20px" }} className="m-0 fw-bold">
                      To sign in,
                      <br /> please enter your mobile number
                    </p>
                  </div>
                  <label className="mb-2" htmlFor="Mobile Number">
                    Mobile Number
                  </label>
                  <span className="p-input-icon-left w-100 ">
                    <i className="pi pi-mobile" />
                    <InputText
                      {...register("mobile_number")}
                      className={`w-100 ${
                        errors.mobile_number ? "p-invalid block" : ""
                      }`}
                    />
                  </span>
                  <span className="p-error text-xs">
                    {errors.mobile_number?.message}
                  </span>
                  <Button
                    type="submit"
                    className={`${styles.bluebtn} w-100 mt-3 mb-4 d-flex justify-content-center`}
                    loading={sendingOTP}
                    label={"Send OTP"}
                  />
                  <p className="text-center mt-3 d-flex align-items-center justify-content-center">
                    First time user?{" "}
                    <span className="textprimary">
                      <SignUp menuData={props.menuData} />
                    </span>
                  </p>
                </Form>
              )}

              <div className="mt-3 text-center">
                <span className="badge fw-normal px-3">
                  <FaLock className="lock-icon" /> Your personal information is
                  secure with us
                </span>
              </div>
            </div>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}
const Login = ({ menuData }) => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <p
        className="m-0 cursor-pointer px-2 textprimary rounded ms-2 fs-14"
        style={{ border: "1px solid #0074dc", padding: "9px 15px" }}
        onClick={() => setModalShow(true)}>
        Sign in
      </p>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        menuData={menuData}
      />
    </>
  );
};

export default Login;
