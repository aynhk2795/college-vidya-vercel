import React from "react";
import { Form } from "react-bootstrap";
import { generate } from "shortid";
import OttaHeading from "./OttaHeading";
import LabelHeart from "/components/Otta/LabelHeart";
import styles from "/components/Otta/QuestionForm.module.scss";

const questionlist = [
  {
    control_id: "f1",
    label: <LabelHeart labelText="Challenging work" />,
  },
  {
    control_id: "f2",
    label: <LabelHeart labelText="Diversity and inclusion" />,
  },
  {
    control_id: "f3",
    label: <LabelHeart labelText="Development and progression" />,
  },
  {
    control_id: "f4",
    label: <LabelHeart labelText="Recognition and reward" />,
  },
  {
    control_id: "f5",
    label: <LabelHeart labelText="Transparency and respect" />,
  },
  {
    control_id: "f7",
    label: <LabelHeart labelText="Challenging work" />,
  },
  {
    control_id: "f8",
    label: <LabelHeart labelText="Diversity and inclusion" />,
  },
  {
    control_id: "f9",
    label: <LabelHeart labelText="Development and progression" />,
  },
  {
    control_id: "f10",
    label: <LabelHeart labelText="Recognition and reward" />,
  },
  {
    control_id: "f11",
    label: <LabelHeart labelText="Transparency and respect" />,
  },
  {
    control_id: "f12",
    label: <LabelHeart labelText="Diversity and inclusion" />,
  },
  {
    control_id: "f13",
    label: <LabelHeart labelText="Development and progression" />,
  },
  {
    control_id: "f14",
    label: <LabelHeart labelText="Recognition and reward" />,
  },
];
const Step1 = () => {
  return (
    <>
      <div className={`${styles.otta_header} text-center py-4`}>
        <OttaHeading title="Which 3 are most important to you in a new role?" />
      </div>
      <div className={`${styles.option_wrapper} option_wrapper`}>
        <div className="d-flex flex-wrap col-md-5 mx-auto py-3">
          {questionlist.map((list) => (
            <Form.Group key={generate()} controlId={list.control_id}>
              <Form.Check type="checkbox" label={list.label} />
            </Form.Group>
          ))}
        </div>
      </div>
    </>
  );
};

export default Step1;
