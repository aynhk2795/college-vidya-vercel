import React from "react";
import { Card, Col, Row, Badge } from "react-bootstrap";
import Link from "next/link";
import Image from "next/image";
import AnimatedYoutube from "/components/Global/AnimatedYoutube";

const CollectionCard = ({ content }) => {
  return (
    <>
      <Row className="row-cols-1 row-cols-sm-2 row-cols-md-2 2 row-cols-lg-3">
        {content.map((data) => (
          <Col className="mb-4" key={data.id}>
            <Link href={data.link} passHref>
              <a target={"_blank"}>
                <Card>
                  <Image src={data.thumbnail} width={341} height={191} alt="" />
                  <Card.Body>
                    <Card.Text className="text-dark">{data.title}</Card.Text>

                    <p className="text-dark d-flex justify-content-between align-items-center">
                      <span>
                        <AnimatedYoutube />
                      </span>

                      <Badge pill bg="dark">
                        {data.duration}
                      </Badge>
                    </p>
                  </Card.Body>
                </Card>
              </a>
            </Link>
          </Col>
        ))}
      </Row>
    </>
  );
};

export default CollectionCard;
