import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Link from "next/link";
import { RiDoubleQuotesL } from "react-icons/ri";

const FeedbackVideoSlider = () => {
  return (
    <>
      <Swiper
        slidesPerView={4}
        spaceBetween={20}
        loop={true}
        navigation={true}
        // pagination={{
        //   clickable: true,
        // }}
        pagination={false}
        breakpoints={{
          "@0.00": {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          "@0.75": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.00": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.75": {
            slidesPerView: 3,
            spaceBetween: 20,
          },
        }}
        modules={[Navigation, Pagination]}
        className="mediaSwiper">
        <SwiperSlide className="mb-4 mt-3 pb-4">
          <Link href="/">
            <a className="text-decoration-none">
              <div
                className="position-relative p-4 rounded"
                style={{ border: "1px solid #000" }}>
                <video
                  controls
                  src={"/images/feedbacks/review1.mp4"}
                  autoPlay={false}
                  style={{ width: "100%", height: "200px" }}
                  muted
                />

                <div className="px-3 pb-3">
                  <p
                    className="fs-12 mt-3 mb-1 text-uppercase text-primary"
                    style={{ letterSpacing: "2px" }}>
                    - Poonam Singh
                  </p>
                  {/* <p className="fw-bold text-dark">
                    <RiDoubleQuotesL fontSize={40} className="text-secondary" />{" "}
                    description here...
                  </p> */}
                </div>
              </div>
            </a>
          </Link>
        </SwiperSlide>
        <SwiperSlide className="mb-4 mt-3 pb-4">
          <Link href="/">
            <a className="text-decoration-none">
              <div
                className="position-relative p-4 rounded"
                style={{ border: "1px solid #000" }}>
                <video
                  controls
                  src={"/images/feedbacks/review2.mp4"}
                  autoPlay={false}
                  style={{ width: "100%", height: "200px" }}
                  muted
                />

                <div className="px-3 pb-3">
                  <p
                    className="fs-12 mt-3 mb-1 text-uppercase text-primary"
                    style={{ letterSpacing: "2px" }}>
                    - Ruchika Yadav
                  </p>
                  {/* <p className="fw-bold text-dark">
                    <RiDoubleQuotesL fontSize={40} className="text-secondary" />{" "}
                    description here...
                  </p> */}
                </div>
              </div>
            </a>
          </Link>
        </SwiperSlide>
        <SwiperSlide className="mb-4 mt-3 pb-4">
          <Link href="/">
            <a className="text-decoration-none">
              <div
                className="position-relative p-4 rounded"
                style={{ border: "1px solid #000" }}>
                <video
                  controls
                  src={"/images/feedbacks/review3.mp4"}
                  autoPlay={false}
                  style={{ width: "100%", height: "200px" }}
                  muted
                />

                <div className="px-3 pb-3">
                  <p
                    className="fs-12 mt-3 mb-1 text-uppercase text-primary"
                    style={{ letterSpacing: "2px" }}>
                    - Raman Gupta
                  </p>
                  {/* <p className="fw-bold text-dark">
                    <RiDoubleQuotesL fontSize={40} className="text-secondary" />{" "}
                    description here...
                  </p> */}
                </div>
              </div>
            </a>
          </Link>
        </SwiperSlide>
      </Swiper>
    </>
  );
};

export default FeedbackVideoSlider;
