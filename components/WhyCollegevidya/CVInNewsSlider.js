import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import screen1 from "/public/images/news/news2.jpeg";
import screen2 from "/public/images/news/news1.webp";
import screen3 from "/public/images/news/news2.jpeg";
import logo1 from "/public/images/news/hindustan.webp";
import Link from "next/link";
import { RiDoubleQuotesL } from "react-icons/ri";

const CVInNewsSlider = () => {
  const Blog = [
    {
      id: 1,
      picture: screen1,
      name: "Nimit Kochar ",
      desc: "It was difficult for me to make the decision whether distance education is the right step to take . ",
    },
    {
      id: 2,
      picture: screen2,
      name: "Kavita Kapadia ",
      desc: "It was difficult for me to make the decision whether distance education is the right step to take. ",
    },
    {
      id: 3,
      picture: screen3,
      name: "Nimit Kochar ",
      desc: "It was difficult for me to make the decision whether distance education is the right step to take. ",
    },
    {
      id: 4,
      picture: screen1,
      name: "Nimit Kochar ",
      desc: "It was difficult for me to make the decision whether distance education is the right step to take. ",
    },
    {
      id: 5,
      picture: screen2,
      name: "Nimit Kochar ",
      desc: "It was difficult for me to make the decision whether distance education is the right step to take.   ",
    },
  ];
  return (
    <div>
      <Swiper
        slidesPerView={4}
        spaceBetween={20}
        loop={true}
        navigation={true}
        pagination={false}
        breakpoints={{
          "@0.00": {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          "@0.75": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.00": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.75": {
            slidesPerView: 4,
            spaceBetween: 20,
          },
        }}
        modules={[Navigation, Pagination]}
        className="mediaSwiper">
        {Blog.map((list) => (
          <SwiperSlide className="mb-4 mt-3 pb-4" key={list.id}>
            <Link href="/">
              <a className="text-decoration-none">
                <div className="position-relative p-4">
                  <Image
                    src={list.picture}
                    className="rounded shadow-1"
                    alt=""
                  />

                  <div className="px-3 pb-3">
                    <p className="text-dark mt-3 mb-3">{list.desc}</p>
                    <Image
                      src={logo1}
                      width={120}
                      height={15}
                      className="rounded"
                      alt=""
                    />
                  </div>
                </div>
              </a>
            </Link>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default CVInNewsSlider;
