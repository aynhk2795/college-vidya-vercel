import React from "react";
import Image from "next/image";
import { Container } from "react-bootstrap";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import Link from "next/link";
import { Navigation, Pagination, Autoplay } from "swiper";
import styles from "/components/WhyCollegevidya/ThreeSlider.module.scss";
import { generate } from "shortid";

const ThreeSlider = ({ universityData }) => {
  return (
    <div>
      <div className={`${styles.main_wrap} position-relative`}>
        <Swiper
          slidesPerView={8}
          spaceBetween={30}
          freeMode={true}
          loop={true}
          navigation={false}
          speed={6000}
          grabCursor={true}
          autoplay={{
            delay: 0,
            disableOnInteraction: false,
          }}
          pagination={false}
          breakpoints={{
            "@0.00": {
              slidesPerView: 3,
              spaceBetween: 20,
            },
            "@0.75": {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            "@1.00": {
              slidesPerView: 9,
              spaceBetween: 20,
            },
          }}
          modules={[Navigation, Pagination, Autoplay]}
          className="mediaSwiper">
          {universityData
            .slice(0, universityData.length / 3)
            .map((university) => (
              <SwiperSlide className="mb-3" key={generate()}>
                <Link href="/">
                  <a className="text-decoration-none">
                    <div className="shadow-1 position-relative text-center bg-white rounded  py-0 py-sm-2">
                      <Image
                        src={university.logo}
                        width={120}
                        height={50}
                        className="rounded"
                        objectFit="contain"
                        alt=""
                      />
                    </div>
                  </a>
                </Link>
              </SwiperSlide>
            ))}
        </Swiper>
        <Swiper
          slidesPerView={8}
          spaceBetween={30}
          freeMode={true}
          loop={true}
          dir="rtl"
          navigation={false}
          speed={6000}
          grabCursor={true}
          autoplay={{
            delay: 0,
            disableOnInteraction: false,
          }}
          pagination={false}
          breakpoints={{
            "@0.00": {
              slidesPerView: 3,
              spaceBetween: 20,
            },
            "@0.75": {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            "@1.00": {
              slidesPerView: 9,
              spaceBetween: 20,
            },
          }}
          modules={[Navigation, Pagination, Autoplay]}
          className="mediaSwiper">
          {universityData
            .slice(
              universityData.length / 3,
              universityData.length / 3 + universityData.length / 3
            )
            .map((university) => (
              <SwiperSlide className="mb-3" key={generate()}>
                <Link href="/">
                  <a className="text-decoration-none">
                    <div className="shadow-1 position-relative text-center bg-white rounded py-0 py-sm-2">
                      <Image
                        src={university.logo}
                        width={120}
                        height={50}
                        className="rounded"
                        objectFit="contain"
                        alt=""
                      />
                    </div>
                  </a>
                </Link>
              </SwiperSlide>
            ))}
        </Swiper>
        <Swiper
          slidesPerView={8}
          spaceBetween={30}
          freeMode={true}
          loop={true}
          navigation={false}
          speed={6000}
          grabCursor={true}
          autoplay={{
            delay: 0,
            disableOnInteraction: false,
          }}
          pagination={false}
          breakpoints={{
            "@0.00": {
              slidesPerView: 3,
              spaceBetween: 20,
            },
            "@0.75": {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            "@1.00": {
              slidesPerView: 9,
              spaceBetween: 20,
            },
          }}
          modules={[Navigation, Pagination, Autoplay]}
          className="mediaSwiper">
          {universityData
            .slice(
              universityData.length / 3 + universityData.length / 3,
              universityData.length - 1
            )
            .map((university) => (
              <SwiperSlide key={generate()}>
                <Link href="/">
                  <a className="text-decoration-none">
                    <div className="shadow-1 position-relative text-center bg-white rounded py-0 py-sm-2">
                      <Image
                        src={university.logo}
                        width={120}
                        height={50}
                        className="rounded"
                        objectFit="contain"
                        alt=""
                      />
                    </div>
                  </a>
                </Link>
              </SwiperSlide>
            ))}
        </Swiper>

        <div
          className={`${styles.scroll_text} position-absolute top-50 translate-middle-y start-0 h-100 d-flex justify-content-center flex-column`}
          style={{ zIndex: "1" }}>
          <Container className={`${styles.slider_text_wrap}`}>
            <p className="mb-1">Join the College Vidya Experience Today!!</p>
            <h1 className="display-1 mb-0 m-0 my-sm-3">
              1<span className="textprimary">00</span>K+
            </h1>
            <h1 className={`${styles.heading} m-0 my-sm-3`}>
              Students Trust College Vidya
              <br />
              for Admission to Online Universities
            </h1>
            <p className="mt-3">#ChunoWahiJoHaiSahi</p>
          </Container>
        </div>
      </div>
    </div>
  );
};

export default ThreeSlider;
