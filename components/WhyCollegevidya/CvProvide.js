import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import WhyCollegevidyaHeading from "./WhyCollegevidyaHeading";
import styles from "/components/WhyCollegevidya/CvProvide.module.scss";

const CvProvide = () => {
  return (
    <>
      <Container className={`${styles.cvprovide} my-2 py-2 my-sm-5 py-sm-5`}>
        <WhyCollegevidyaHeading title="What College Vidya Provide" />
        <p className="text-center">
          Supposing so be resolving breakfast am or perfectly. It drew a hill
          from me.
        </p>
        <Row
          className={`${styles.colorbox_wrap} mt-5 rounded px-1 py-3 p-sm-5`}>
          <Col md={4} className={`${styles.colorbox_wrap_inner}`}>
            <div className={`${styles.colorbox}`}>
              <p className={`${styles.text_stroke}`}>01</p>
              <h2 className="heading">
                Compare All Approved 75+ Online Universities
              </h2>
              <p>Compare Online & Distance Universities for the Best Course</p>
            </div>
          </Col>
          <Col md={4} className={`${styles.colorbox_wrap_inner}`}>
            <div className={`${styles.colorbox}`}>
              <p className={`${styles.text_stroke}`}>02</p>
              <h2 className="heading">
                Suggestions for Best Online University
              </h2>
              <p>
                Use Suggest Me University feature to find Best Online Course +
                Online University for you
              </p>
            </div>
          </Col>
          <Col md={4} className={`${styles.colorbox_wrap_inner}`}>
            <div className={`${styles.colorbox}`}>
              <p className={`${styles.text_stroke}`}>03</p>
              <h2 className="heading">
                Individual Career Video + Audio Counselling
              </h2>
              <p>
                Get Personal Career Counseling for Free from 2500+ Qualified
                Mentors
              </p>
            </div>
          </Col>
          <Col md={4} className={`${styles.colorbox_wrap_inner}`}>
            <div className={`${styles.colorbox}`}>
              <p className={`${styles.text_stroke}`}>04</p>
              <h2 className="heading">Proceed to University</h2>
              <p>Connect officially to the university through our Portal</p>
            </div>
          </Col>
          <Col md={4} className={`${styles.colorbox_wrap_inner}`}>
            <div className={`${styles.colorbox}`}>
              <p className={`${styles.text_stroke}`}>05</p>
              <h2 className="heading">Full Degree Support</h2>
              <p>Personal Student Support Services throughout Degree</p>
            </div>
          </Col>
          <Col md={4} className={`${styles.colorbox_wrap_inner}`}>
            <div className={`${styles.colorbox}`}>
              <p className={`${styles.text_stroke}`}>06</p>
              <h2 className="heading">4.9/5 Stars on Google</h2>
              <p>
                Over 301 positive reviews on Google & Featured in Over 12
                Leading News Platforms
              </p>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default CvProvide;
