import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import screen1 from "/public/images/feedbacks/moment_truth.webp";
import screen2 from "/public/images/feedbacks/mr_singh.webp";
import screen3 from "/public/images/feedbacks/pandey.webp";
import Link from "next/link";
import { RiDoubleQuotesL } from "react-icons/ri";

const FeedbackTextSlider = () => {
  const Blog = [
    {
      id: 1,
      picture: screen1,
      name: " Nimit Kochar ",
      desc: "With the help of compare feature, I finally have selected a university for distance MBA. As my need is to choose the best university with a pocket-friendly fee structure, so I found one. It was a great experience with College Vidya. ",
    },
    {
      id: 2,
      picture: screen2,
      name: " Kavita Kapadia ",
      desc: "It was difficult for me to make the decision whether distance education is the right step to take but College Vidya’s Compare Feature helped me to look for approvals and choosing a valid university. Now I’ve taken admission to the best university.   ",
    },
    {
      id: 3,
      picture: screen3,
      name: "Priya Prakash ",
      desc: "The compare feature helped me a lot in comparing universities, their courses, their fees and also helped me in making the best decision. Now I have taken admission to one of the best universities.",
    },
  ];
  return (
    <>
      <Swiper
        slidesPerView={4}
        spaceBetween={20}
        loop={true}
        navigation={true}
        // pagination={{
        //   clickable: true,
        // }}
        pagination={false}
        breakpoints={{
          "@0.00": {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          "@0.75": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.00": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.75": {
            slidesPerView: 3,
            spaceBetween: 20,
          },
        }}
        modules={[Navigation, Pagination]}
        className="mediaSwiper">
        {Blog.map((list) => (
          <SwiperSlide className="mb-4 mt-3 pb-4" key={list.id}>
            <Link href="/">
              <a className="text-decoration-none">
                <div
                  className="position-relative p-4 rounded"
                  style={{ border: "1px solid #000" }}>
                  <Image
                    src={list.picture}
                    className="rounded position-absolute top-0 end-0"
                    width={100}
                    height={100}
                    alt=""
                  />

                  <div className="px-3 pb-3">
                    <p
                      className="fs-12 mt-3 mb-1 text-uppercase text-primary"
                      style={{ letterSpacing: "2px" }}>
                      - {list.name}
                    </p>
                    <p className="fw-bold text-dark">
                      <RiDoubleQuotesL
                        fontSize={40}
                        className="text-secondary"
                      />{" "}
                      {list.desc}
                    </p>
                  </div>
                </div>
              </a>
            </Link>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
};

export default FeedbackTextSlider;
