import React from "react";
import styles from "./WhyCollegevidyaHeading.module.scss";

const WhyCollegevidyaHeading = (props) => {
  return (
    <>
      <div className={`${styles.WhyCollegevidyaHeading}`}>
        <h2 className="text-center content-heading">{props.title}</h2>
      </div>
    </>
  );
};

export default WhyCollegevidyaHeading;
