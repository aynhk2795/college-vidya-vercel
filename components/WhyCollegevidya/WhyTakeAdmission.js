import React from "react";
import { Row, Col } from "react-bootstrap";
import WhyCollegevidyaHeading from "./WhyCollegevidyaHeading";

const WhyTakeAdmission = () => {
  return (
    <>
      <WhyCollegevidyaHeading title="Why take Admission from College vidya" />
      <p className="text-center">
        100k+ Students Trust College Vidya for Admission to Online Universitiess
      </p>
      <Row>
        <Col md={12} className="mx-auto">
          <div
            style={{
              backgroundColor: "aliceblue",
              border: "1px solid #0074d738",
            }}
            className="mb-4 p-4 rounded">
            <h3 className="d-flex align-items-center">
              <span
                className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle"
                style={{
                  backgroundColor: "#fff",
                  width: "40px",
                  height: "40px",
                }}>
                1
              </span>{" "}
              Best Suggestion from Unbiased & Certified Advisors through Compare
            </h3>
            <p>
              A university will never tell you other options that are better for
              you but we will. We will give all the options of approved
              universities that you can choose from and help you in selecting
              one on your factors.
            </p>
          </div>
          <div
            style={{
              backgroundColor: "aliceblue",
              border: "1px solid #0074d738",
            }}
            className="mb-4 p-4 rounded">
            <h3 className="d-flex align-items-center">
              <span
                className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle"
                style={{
                  backgroundColor: "#fff",
                  width: "40px",
                  height: "40px",
                }}>
                2
              </span>{" "}
              Dedicated Career Assistance
            </h3>
            <p>
              Universities have a few counsellors over thousands of students and
              are hence unable to solve your queries of each & every individual.
              College Vidya provides plenty of counsellors to listen to students
              and solve their queries on the behalf of the universities.
            </p>
          </div>
          <div
            style={{
              backgroundColor: "aliceblue",
              border: "1px solid #0074d738",
            }}
            className="mb-4 p-4 rounded">
            <h3 className="d-flex align-items-center">
              <span
                className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle"
                style={{
                  backgroundColor: "#fff",
                  width: "40px",
                  height: "40px",
                }}>
                3
              </span>
              Always Pick Your Call
            </h3>
            <p>
              We always pick up your call when no one does. If you have any
              query related from admission to completion of your course we
              always listen.
            </p>
          </div>
          <div
            style={{
              backgroundColor: "aliceblue",
              border: "1px solid #0074d738",
            }}
            className="mb-4 p-4 rounded">
            <h3 className="d-flex align-items-center">
              <span
                className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle"
                style={{
                  backgroundColor: "#fff",
                  width: "40px",
                  height: "40px",
                }}>
                4
              </span>
              Coordination with university (You will get 2 counselor- University
              + College Vidya)
            </h3>
            <p>
              If you take admission via College Vidya then Colllege Vidya will
              be responsible for all your problems or queries further. You can
              contact the counsellors of College Vidya and they will coordinate
              with the university regarding any issue and we do it Free.
            </p>
          </div>
          <div
            style={{
              backgroundColor: "aliceblue",
              border: "1px solid #0074d738",
            }}
            className="mb-4 p-4 rounded">
            <h3 className="d-flex align-items-center">
              <span
                className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle"
                style={{
                  backgroundColor: "#fff",
                  width: "40px",
                  height: "40px",
                }}>
                5
              </span>
              Free Services
            </h3>
            <p>
              Our services are completely free and our counsellors are available
              all the time to assist you not just for admission but also for the
              queries till the last date of your degree completion.
            </p>
          </div>
          <div
            style={{
              backgroundColor: "aliceblue",
              border: "1px solid #0074d738",
            }}
            className="mb-4 p-4 rounded">
            <h3 className="d-flex align-items-center">
              <span
                className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle"
                style={{
                  backgroundColor: "#fff",
                  width: "40px",
                  height: "40px",
                }}>
                6
              </span>
              Your Payment goes into University Account
            </h3>
            <p>
              College Vidya is an information portal and it is illegal to accept
              payments on behalf of the university. To be honest, College Vidya
              does not even have a bank account. All the payments made by
              students go directly to the University account and we request
              everyone to make payment only on the official website of
              University.
            </p>
          </div>
        </Col>
      </Row>
    </>
  );
};

export default WhyTakeAdmission;
