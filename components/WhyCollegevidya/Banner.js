import React from "react";
import { Container, Col } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import logo from "/public/images/university/01nmims-University.png";
import styles from "/components/WhyCollegevidya/WhyBanner.module.scss";

const Banner = () => {
  const logos = [
    {
      logo: logo,
    },
    {
      logo: logo,
    },
    {
      logo: logo,
    },
    {
      logo: logo,
    },
    {
      logo: logo,
    },
    {
      logo: logo,
    },
    {
      logo: logo,
    },
  ];

  return (
    <>
      <div className={`${styles.banner_video_wrap} position-relative`}>
        <video
          controls={false}
          src={"/video/chandan.mp4"}
          autoPlay={false}
          style={{ width: "100%" }}
          muted
        />
        <Container>
          <div className="banner-caption position-absolute top-50 text-white translate-middle-y">
            <Col md={6}>
              <p className="h1 textprimary why_heading">Why Choose</p>
              <h1 className="mt-1 display-2">College Vidya ?</h1>
              <p className="banner-text">
                Choose the online course you wish to pursue from our home page
                and get full details of all the approved online and distance
                universities/colleges providing the course within 2 minutes!
              </p>

              <Button
                className="py-3 px-5 mt-1 mt-sm-3 textprimary border-0 watch-video"
                style={{
                  backgroundColor: "aliceblue",
                  borderRadius: "25px",
                  fontSize: "16px",
                }}>
                Watch Video
              </Button>
            </Col>
          </div>
        </Container>
      </div>
    </>
  );
};

export default Banner;
