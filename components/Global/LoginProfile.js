import React from "react";
import { useRef } from "react";
import { Menu } from "primereact/menu";
import { MdOutlineKeyboardArrowDown } from "react-icons/md";
import { useRouter } from "next/router";
import { useAuth } from "../../hooks/auth";

const LoginProfile = (props) => {
  const menu = useRef(null);
  const router = useRouter();
  const { logout } = useAuth();

  const items = [
    {
      label: "My Account",
      className: "user-select-none",

      items: [
        {
          label: "Profile",
          icon: "pi pi-user",
          className: "fs-14",
          command: () => {
            router.push("/user/profile");
          },
        },
        {
          label: "Sign Out",
          icon: "pi pi-arrow-right",
          className: "fs-14",
          command: () => {
            logout();
          },
        },
      ],
    },
  ];
  return (
    <>
      <Menu model={items} popup ref={menu} id="login_account" />

      {props.screenSize && props.screenSize.width < 1200 ? (
        <div
          className="m-0 cursor-pointer px-1 py-2 textprimary rounded ms-2 fs-12 d-flex"
          style={{ padding: "9px 15px" }}
          onClick={(event) => menu.current.toggle(event)}
          aria-controls="popup_menu"
          aria-haspopup>
          <p className="text-primary m-0">Hi,</p>
        </div>
      ) : (
        <div
          className="m-0 cursor-pointer px-1 py-2 textprimary rounded ms-2 fs-12"
          style={{ padding: "9px 15px", minWidth: "130px" }}
          onClick={(event) => menu.current.toggle(event)}
          aria-controls="popup_menu"
          aria-haspopup>
          <p className="text-primary m-0">
            👋{" "}
            <span className="textprimary text-capitalize">
              Hi, {props.username && props.username.slice(0, 7)}...
              <MdOutlineKeyboardArrowDown
                fontSize={18}
                style={{ marginTop: "-2px" }}
              />
            </span>
          </p>
        </div>
      )}
    </>
  );
};

export default LoginProfile;
