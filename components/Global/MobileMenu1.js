import React, { useEffect, useRef, useState } from "react";
import { SlideMenu } from "primereact/slidemenu";
import { Button } from "primereact/button";
import styles from "../../styles/MobileMenu.module.scss";
import MobileMenuHeader from "./MobileMenuHeader";
import MobileMenuOpenHeader from "./MobileMenuOpenHeader";
import MobileMenuHeaderLabel from "./MobileMenuHeaderLabel";
import Link from "next/link";
import { generate } from "shortid";
import commit from "/public/images/stamp.png";
import { FcCallback, FcVoicePresentation } from "react-icons/fc";
import Image from "next/image";
import { useRouter } from "next/router";
import MobileMenuCourseTag from "./MobileMenuCourseTag";

const MobileMenu1 = ({ screenSize, menuData }) => {
  const router = useRouter();
  const menu = useRef(null);

  const [topMenu, setTopMenu] = useState([]);

  const [courseMenu, setCourseMenu] = useState([]);

  const items = [
    {
      label: <MobileMenuHeader menu={menu} />,
    },

    {
      label: "Compare Universities",
      className: "mobile_label_tag",
    },
    ...courseMenu,
    {
      label: "Top University",
      // icon:'pi pi-fw pi-calendar',
      items: topMenu,
    },
    {
      label: (
        <MobileMenuHeaderLabel title="Suggest University in 2 mins" tag="New" />
      ),
      command: () => {
        document
          .getElementsByTagName("body")[0]
          .classList.remove("position-fixed");
        router.push("/suggest-me-an-university");
      },
    },
    {
      separator: true,
    },
    {
      label: "Why Trust us",
      className: "mobile_label_tag",
    },

    {
      label: (
        <MobileMenuHeaderLabel
          title="CollegeVidya Commitment"
          imageicon={<Image src={commit} width={35} height={25} alt="" />}
        />
      ),
      command: () => {
        document
          .getElementsByTagName("body")[0]
          .classList.remove("position-fixed");
        router.push("/our-trust");
      },
    },
    {
      label: " Verify Your University ",
      command: () => {
        window.open("https://verify.collegevidya.com", "_blank");
      },
    },
    {
      label: " Why College Vidya? ",
      command: () => {
        document
          .getElementsByTagName("body")[0]
          .classList.remove("position-fixed");
        router.push("/why-collegevidya");
      },
    },
    {
      label: " Important Facts ",
      command: () => {
        document
          .getElementsByTagName("body")[0]
          .classList.remove("position-fixed");
        router.push("/checklist");
      },
    },
    {
      label: "Important Videos(CV TV)",
      command: () => {
        document
          .getElementsByTagName("body")[0]
          .classList.remove("position-fixed");
        router.push("/collection");
      },
    },
    {
      label: "Contact Us",
      className: "mobile_label_tag",
      command: () => {
        document
          .getElementsByTagName("body")[0]
          .classList.remove("position-fixed");
        router.push("/contact-us");
      },
    },
    {
      label: (
        <MobileMenuHeaderLabel
          icon={<FcCallback fontSize={18} />}
          title="New User : 18004205757"
        />
      ),
    },
    {
      label: (
        <MobileMenuHeaderLabel
          icon={<FcVoicePresentation fontSize={20} />}
          title="Existing User : 18003097947"
        />
      ),
    },
  ];

  useEffect(() => {
    if (menuData) {
      const men = menuData
        .map((domain) => domain.courses)
        .flat()
        .map(function (course) {
          return {
            label: (
              <Link href={`/top-universities-colleges/${course.slug}`}>
                <a
                  onClick={() =>
                    document
                      .getElementsByTagName("body")[0]
                      .classList.remove("position-fixed")
                  }
                  className={`text-dark text-decoration-none`}>
                  {course.name}
                </a>
              </Link>
            ),
          };
        });

      setTopMenu(
        [
          {
            label: (
              <MobileMenuCourseTag
                displayName={"Top University"}
                navigateBackward={navigateBackward}
              />
            ),
          },
        ].concat(men)
      );
    }
  }, [menuData]);

  useEffect(() => {
    if (menuData) {
      const menDa = menuData.map(function (domain) {
        return {
          label: domain.name,
          items: [
            {
              label: (
                <MobileMenuCourseTag
                  displayName={domain.name}
                  navigateBackward={navigateBackward}
                />
              ),
            },
          ].concat(
            domain.courses.map(function (course) {
              return {
                label: course.name,
                items: [
                  {
                    label: (
                      <MobileMenuCourseTag
                        displayName={course.name}
                        navigateBackward={navigateBackward}
                      />
                    ),
                  },
                ]
                  .concat([
                    {
                      label: (
                        <Link href={`/courses/${course.slug}`}>
                          <a
                            onClick={() =>
                              document
                                .getElementsByTagName("body")[0]
                                .classList.remove("position-fixed")
                            }
                            className={`text-dark text-decoration-none`}>
                            {`General ${course.display_name}`}
                          </a>
                        </Link>
                      ),
                    },
                  ])
                  .concat(
                    course.specializations.map(function (specialization) {
                      return {
                        label: (
                          <Link
                            key={generate()}
                            href={`/courses/${course.slug}/${specialization.slug}`}>
                            <a
                              onClick={() =>
                                document
                                  .getElementsByTagName("body")[0]
                                  .classList.remove("position-fixed")
                              }
                              className={`text-dark text-decoration-none`}>
                              {specialization.name}
                            </a>
                          </Link>
                        ),
                      };
                    })
                  ),
              };
            })
          ),
        };
      });

      setCourseMenu(menDa);
    }
  }, [menuData]);

  const navigateBackward = () => {
    document.getElementsByClassName("p-slidemenu-backward")[0].click();
  };

  return (
    <>
      <div>
        <SlideMenu
          ref={menu}
          model={items}
          popup
          onShow={() =>
            document
              .getElementsByTagName("body")[0]
              .classList.add("position-fixed")
          }
          onHide={() =>
            document
              .getElementsByTagName("body")[0]
              .classList.remove("position-fixed")
          }
          viewportHeight={screenSize.height}
          className={`${styles.mobilemenu_dialog} w-100`}
          menuWidth={screenSize.width}></SlideMenu>
        <div className="d-flex align-items-center pt-2">
          <Button
            type="button"
            icon="pi pi-bars"
            className="bg-white border-0 rounded-0 text-dark"
            onClick={(event) => menu.current.toggle(event)}
          />
          <MobileMenuOpenHeader menuData={menuData} screenSize={screenSize} />
        </div>
      </div>
    </>
  );
};

export default MobileMenu1;
