import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Socialicons from "/components/Global/Socialicons";
import Image from "next/image";
import Link from "next/link";
import Disclamer from "/components/Global/Disclamer";
import appimg from "/public/images/google_play.png";
import Logo from "/components/Global/Logo";
import { FcCheckmark } from "react-icons/fc";
import { BsPlusCircleFill } from "react-icons/bs";
import styles from "/styles/Footer.module.scss";

const Footer = ({ menuData }) => {
  return (
    <>
      <style>
        {`
      
      a {
        color:$text_offwhite;
      }
      `}
      </style>
      <Container>
        <div
          className={`${styles.cta_footer} shadow-1 d-flex align-items-center justify-content-between py-5 px-5 rounded w-100 bgdark flex-column flex-sm-row`}>
          <div className="d-flex align-items-center">
            <FcCheckmark
              fontSize={40}
              width={40}
              height={40}
              className={`bg-white rounded-circle p-2 me-3`}
              style={{ minWidth: "40px" }}
            />{" "}
            <p
              style={{ fontSize: "40px" }}
              className={`${styles.checklist_line} text_offwhite m-0 d-flex align-items-center`}>
              College Vidya Checklist
            </p>
          </div>
          <Link href="/checklist">
            <a className="bg-white text-dark px-4 py-3 rounded shadow-1 d-inline-block">
              View
            </a>
          </Link>
        </div>
      </Container>
      <section
        className="mt-5 bgdark"
        style={{
          fontSize: "14px",
          color: "#989eb6",
        }}>
        <Container className="position-relative py-5">
          <Row>
            <div className="col-md-3">
              <Logo />
              <h2>
                SEARCH THE BEST <br />
                UNIVERSITY WITH US!
              </h2>
            </div>
            <Col md={3} className="mb-4">
              <h6 className={`footer-heading position-relative fw-bold mb-3`}>
                Online &amp; Distance MBA
              </h6>
              <ul className="list-unstyled">
                {menuData[0].courses[0].specializations
                  .slice(0, 15)
                  .map((specialization) => (
                    <li key={specialization.id}>
                      <Link
                        href={`/courses/${menuData[0].courses[0].slug}/${specialization.slug}`}>
                        <a
                          className={` ${styles.link_text} text-decoration-none`}>
                          {specialization.display_name}
                        </a>
                      </Link>
                    </li>
                  ))}
                <li>
                  <Link href="/online-distance/online-distance-pg-course">
                    <a className={` ${styles.link_text} text-decoration-none`}>
                      View All <BsPlusCircleFill />
                    </a>
                  </Link>
                </li>
              </ul>
            </Col>
            <Col md={3} className="mb-4">
              <h6 className={`fw-bold footer-heading position-relative mb-3`}>
                Online &amp; Distance UG Courses
              </h6>
              <ul className="list-unstyled">
                {menuData[1].courses.map((course) => (
                  <li key={course.id}>
                    <Link href={`/courses/${course.slug}`}>
                      <a
                        className={` ${styles.link_text} text-decoration-none`}>
                        {course.display_name}
                      </a>
                    </Link>
                  </li>
                ))}
                <li>
                  <Link href="/online-distance/online-distance-ug-course">
                    <a className={` ${styles.link_text} text-decoration-none`}>
                      View All <BsPlusCircleFill />
                    </a>
                  </Link>
                </li>
              </ul>

              <h6 className={`fw-bold footer-heading position-relative mb-3`}>
                Online &amp; Distance PG Courses
              </h6>
              <ul className="list-unstyled">
                {menuData[0].courses.map((course) => (
                  <li key={course.id}>
                    <Link href={`/courses/${course.slug}`}>
                      <a
                        className={` ${styles.link_text} text-decoration-none`}>
                        {course.display_name}
                      </a>
                    </Link>
                  </li>
                ))}
                <li>
                  <Link href="/online-distance/online-distance-pg-course">
                    <a className={` ${styles.link_text} text-decoration-none`}>
                      View All <BsPlusCircleFill />
                    </a>
                  </Link>
                </li>
              </ul>
            </Col>
            <Col md={3} className="mb-4">
              <h6 className={`footer-heading position-relative fw-bold mb-3`}>
                Online &amp; Distance Best Colleges for
              </h6>
              <ul className="list-unstyled">
                {menuData
                  .map((domain) => domain.courses)
                  .flat()
                  .map((course) => (
                    <li key={course.id}>
                      <Link href={`/top-universities-colleges/${course.slug}`}>
                        <a
                          className={` ${styles.link_text} text-decoration-none`}>
                          Best Colleges for {course.display_name}
                        </a>
                      </Link>
                    </li>
                  ))}
              </ul>
            </Col>
          </Row>

          <Row>
            <Col md={3}>
              <p>Download The App</p>
              <Link href="/">
                <a>
                  <Image src={appimg} width={150} height={45} alt="" />
                </a>
              </Link>
            </Col>
            <Col md={6}>
              <p>
                India has a net of 9.6 Million students that will enroll in
                online education by the end of 2022. Still, the online education
                sector in India is unorganized and students face a lot of
                difficulties in getting information on it. College Vidya aims to
                tackle the current difficulties of students. College Vidya is
                India&apos;s first online platform that brings you all the
                online universities at a single platform. College Vidya provides
                unbiased information about every online course and the
                university providing this course.
              </p>
              <p>
                The online portal of College Vidya is aimed to complete
                information to the students about every aspect of online
                education without being biased.
              </p>
              <p>
                College Vidya gives the power to the students to get the best
                universities in online education. College Vidya&apos;s compare
                feature gives the comparison of every online university on the
                various parameters such as E-learning system, EMI, Faculties,
                and fees.
              </p>
            </Col>
            <Col md={3}>
              <p>Contact us :</p>
              <Link href="/">
                <a className={` ${styles.link_text} text-decoration-none`}>
                  info@collegevidya.com
                </a>
              </Link>
              <div className="mt-3">
                <Socialicons />
              </div>
              <div>
                <Link href="tel:1800-420-5757">
                  <a className="position-relative border rounded text-white px-4 pt-3 pb-2 mt-1 d-inline-block">
                    <small className="position-absolute top-0 translate-middle bgprimary text-white rounded fs-10 px-2 ms-3 myshine">
                      Toll Free
                    </small>{" "}
                    1800-420-5757
                  </a>
                </Link>
              </div>
            </Col>
          </Row>
          <Row>
            <Disclamer />
          </Row>
        </Container>
      </section>
    </>
  );
};

export default Footer;
