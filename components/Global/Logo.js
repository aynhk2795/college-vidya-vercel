import Link from "next/link";
import React from "react";
import Image from "next/image";
import mlogo from "/public/images/logo.svg";
import styles from "../../styles/MobileMenu.module.scss";

const Logo = (props) => {
  return (
    <>
      <a
        href="/"
        className={`${
          props.myclass ? styles.mynewclass : ""
        } d-flex align-items-center`}>
        <Image src={mlogo} width={90} height={45} alt="" />
      </a>
    </>
  );
};

export default Logo;
