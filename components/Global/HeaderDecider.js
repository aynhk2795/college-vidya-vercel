import React from "react";
import Header from "./Header";
import MobileMenu1 from "./MobileMenu1";

const HeaderDecider = ({ screenSize, menuData, taglineData }) => {
  return (
    <>
      {screenSize && screenSize.width < 1200 ? (
        <MobileMenu1 screenSize={screenSize} menuData={menuData} />
      ) : (
        <Header menuData={menuData} taglineData={taglineData} />
      )}
    </>
  );
};

export default HeaderDecider;
