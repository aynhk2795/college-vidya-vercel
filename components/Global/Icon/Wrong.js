import React from "react";
import { AiOutlineClose } from "react-icons/ai";

const Wrong = () => {
  return (
    <>
      <AiOutlineClose />
    </>
  );
};

export default Wrong;
