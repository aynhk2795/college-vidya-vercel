import React from "react";
import { AiOutlineCheck } from "react-icons/ai";

const Right = () => {
  return (
    <>
      <AiOutlineCheck />
    </>
  );
};

export default Right;
