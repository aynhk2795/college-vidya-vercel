import React from "react";
import { AiOutlineSafety } from "react-icons/ai";

const CircularIcon = (props) => {
  return <>{props.icon}</>;
};

export default CircularIcon;
