import Logo from "./Logo";
import SuggestMeMObile from "./SuggestMeMObile";

const MobileMenuOpenHeader = () => {
  return (
    <>
      <div className="d-flex align-items-center w-100 pe-3">
        <Logo />
        <div className="ms-auto">
          <SuggestMeMObile />
        </div>
      </div>
    </>
  );
};

export default MobileMenuOpenHeader;
