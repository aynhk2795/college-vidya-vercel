import Link from "next/link";
import React from "react";
import {
  AiFillFacebook,
  AiFillTwitterCircle,
  AiFillLinkedin,
  AiFillYoutube,
} from "react-icons/ai";
import styles from "/styles/Footer.module.scss";

const Social_icons = () => {
  return (
    <>
      <ul className="list-unstyled d-flex">
        <li className="me-2">
          <Link href="/">
            <a>
              <AiFillFacebook
                className={` ${styles.link_text} text-decoration-none h4`}
              />
            </a>
          </Link>
        </li>
        <li className="me-2">
          <Link href="/">
            <a>
              <AiFillTwitterCircle
                className={` ${styles.link_text} text-decoration-none h4`}
              />
            </a>
          </Link>
        </li>
        <li className="me-2">
          <Link href="/">
            <a>
              <AiFillLinkedin
                className={` ${styles.link_text} text-decoration-none h4`}
              />
            </a>
          </Link>
        </li>
        <li className="me-2">
          <Link href="/">
            <a>
              <AiFillYoutube
                className={` ${styles.link_text} text-decoration-none h4`}
              />
            </a>
          </Link>
        </li>
      </ul>
    </>
  );
};

export default Social_icons;
