import React, { useRef } from "react";
import PropTypes from "prop-types";

import { SlideMenu } from "primereact/slidemenu";
import { Sidebar } from "primereact/sidebar";
// import { Button } from 'primereact/button';
import { useState } from "react";
import { Offcanvas, OffcanvasHeader, Button } from "react-bootstrap";
import { RiMenu2Fill } from "react-icons/ri";
import Logo from "/components/Global/Logo";
import Image from "next/image";
import google_app from "../../public/images/google_play.png";
import check from "../../public/images/badge.png";
import { PanelMenu } from "primereact/panelmenu";
import Link from "next/link";
import { BsTelephoneFill } from "react-icons/bs";
import { AiTwotoneSetting } from "react-icons/ai";
import stamp from "../../public/images/stamp.png";
import AnimatedBird from "../../components/Global/AnimatedBird";
import styles from "../../styles/MobileMenu.module.scss";
// import useWindowSize from '../../pages/use-window-size';

const MobileMenu = (props) => {
  // const size = useWindowSize();

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const items = [
    {
      label: "PG Course",

      items: [
        {
          label: "Online  & distance MBA",

          items: [
            {
              label: "HR Management",
            },
            {
              label: "Finance Management",
            },
          ],
        },
        {
          label: "Online  & distance MCA",
        },
        {
          label: "Online  & distance M.COM",
        },
      ],
    },
    {
      label: "UG Course",
      items: [
        {
          label: "Online  & distance BBA",
          items: [
            {
              label: "UHR Management",
            },
            {
              label: "UFinance Management",
            },
          ],
        },
        {
          label: "Right",
        },
        {
          label: "Center",
        },
        {
          label: "Justify",
        },
      ],
    },

    {
      label: "Events",
      // icon:'pi pi-fw pi-calendar',
      items: [
        {
          label: "Edit",
          // icon:'pi pi-fw pi-pencil',
          items: [
            {
              label: "Save",
              // icon:'pi pi-fw pi-calendar-plus'
            },
            {
              label: "Delete",
              // icon:'pi pi-fw pi-calendar-minus'
            },
          ],
        },
        {
          label: "Archieve",
          // icon:'pi pi-fw pi-calendar-times',
          items: [
            {
              label: "Remove",
              // icon:'pi pi-fw pi-calendar-minus'
            },
          ],
        },
      ],
    },
    {
      label: "Resourses",
      items: [
        {
          label: "Resourses1",
        },
        {
          label: "Resourses2",
        },
        {
          label: "Resourses3",
        },
        {
          label: "Resourses4",
        },
      ],
    },
  ];
  return (
    <>
      <div className="d-flex align-items-center justify-content-between px-3 py-2">
        <div className="d-flex align-items-center">
          <Button variant="light" className="shadow-sm" onClick={handleShow}>
            <RiMenu2Fill fontSize={26} />
          </Button>
          <Logo myclass={true} />
        </div>
        <div>
          <Button
            style={{ backgroundColor: "#f0f8ff" }}
            className="fs-10 position-relative d-flex align-items-center border-0 textprimary">
            <span className="me-1">Suggest university in 2 mins</span>{" "}
            <Image src={check} width={17} height={17} alt="banner" />
            <span className="badge bgsecondary position-absolute top-0 start-0 translate-middle shadow-sm">
              New
            </span>
          </Button>
        </div>
      </div>

      <Offcanvas show={show} onHide={handleClose}>
        <Offcanvas.Header closeButton className="shadow-sm">
          <Offcanvas.Title>
            <small className="ps-3 fs-14 fw-normal">#ChunoWahiJoHaiSahi</small>
            <AnimatedBird />
          </Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <PanelMenu model={items} />
          <p className={`${styles.mob_sub_text}  ps-3 fs-13 mb-2`}>Suggest</p>
          <Link href="/">
            <a className="text-dark d-flex align-items-center mb-3 ps-3 fs-14">
              Suggest university in 2 mins{" "}
              <span className="badge bgsecondary shadow-sm ms-2">New</span>
            </a>
          </Link>

          <p className={`${styles.mob_sub_text}  ps-3 fs-13 mb-2`}>
            Why Trust us
          </p>
          <Link href="/">
            <a className="text-dark d-block mb-2 ps-3 d-flex align-items-center fs-15">
              Collge Vidya Commitment{" "}
              <Image src={stamp} width={50} height={35} alt="" />
            </a>
          </Link>
          <Link href="/">
            <a className="text-dark d-block mb-3 ps-3 fs-15">
              Important Videos(CV TV)
            </a>
          </Link>
          <p className={`${styles.mob_sub_text}  ps-3 fs-13 mb-2`}>
            Contact us
          </p>
          <Link href="/">
            <a className="text-dark d-block mb-3 ps-3 fs-15">
              <BsTelephoneFill style={{ color: "green" }} /> New User :
              18004205757
            </a>
          </Link>
          <Link href="/">
            <a className="text-dark d-block mb-3 ps-3 fs-15">
              {" "}
              <AiTwotoneSetting /> Existing User : 18003097947
            </a>
          </Link>
          <p className={`${styles.mob_sub_text}  ps-3 fs-13 mb-2`}>
            Download App
          </p>
          <Link href="">
            <a className="d-block mb-3 ps-3">
              <Image src={google_app} width={110} height={35} alt="" />
            </a>
          </Link>
        </Offcanvas.Body>
      </Offcanvas>

      {/* <div>
      {size.width}px
    </div> */}
    </>
  );
};

MobileMenu.propTypes = {};

export default MobileMenu;
