import React from "react";
import Image from "next/image";
import check from "../../public/images/badge.png";
import { Button } from "react-bootstrap";
import Link from "next/link";

const SuggestMeMObile = () => {
  return (
    <>
      <Link href="/suggest-me-an-university">
        <a>
          {" "}
          <div
            style={{ backgroundColor: "#f0f8ff" }}
            className="fs-10 position-relative d-flex align-items-center border-0 textprimary py-2 px-2 rounded">
            <span className="me-1">Suggest university in 2 mins</span>{" "}
            <Image src={check} width={17} height={17} alt="banner" />
            <span className="badge bgsecondary position-absolute top-0 start-0 translate-middle shadow-sm">
              New
            </span>
          </div>
        </a>
      </Link>
    </>
  );
};

export default SuggestMeMObile;
