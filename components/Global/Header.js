import React, { useState } from "react";
import { Sidebar } from "primereact/sidebar";
import { Menubar } from "primereact/menubar";
import Image from "next/image";
import logo from "/public/images/logo.svg";
import SearchPanel from "/components/Search/SearchPanel";
import Link from "next/link";
import {
  BsHandThumbsUp,
  BsJournalCheck,
  BsArrow90DegRight,
  BsCameraVideo,
  BsChatLeftDots,
  BsSortAlphaUpAlt,
  BsJournalText,
  BsPeople,
  BsQuestionSquare,
  BsCaretRightSquare,
} from "react-icons/bs";
import Megamenu from "./Megamenu";
import AnimatedBird from "./AnimatedBird";
import badge from "/public/images/badge.png";
import styles from "./Header.module.scss";
import { useEffect } from "react";
import TopHeader from "./TopHeader";
import { useRouter } from "next/router";
import Checklist from "../universityListingPage/Modal/Checklist";
import { Modal } from "react-bootstrap";
import CourseModal from "../Homepage/Modal/CourseModal";

const Header = ({ menuData, screenSize, taglineData }) => {
  const router = useRouter();
  const [megamenuVisible, setMegamenuVisible] = useState(false);
  const [abroadModalVisible, setAbroadModalVisible] = useState(false);

  const [topMenu, setTopMenu] = useState([]);
  const [show, setShow] = useState(true);

  const items = [
    {
      label: (
        <Megamenu
          megamenuVisible={megamenuVisible}
          setMegamenuVisible={setMegamenuVisible}
          menuData={menuData}
          alertVisible={show}
          setAbroadModalVisible={setAbroadModalVisible}
        />
      ),
      className: "megamenu",
    },
    {
      label: "Top Universities",
      items: topMenu,
      className: "alpho",
      command: () => {
        setTimeout(() => {
          if (
            document
              .getElementsByClassName("alpho")[0]
              .classList.contains("p-menuitem-active")
          ) {
            document
              .getElementsByClassName("alpho")[0]
              .classList.remove("p-menuitem-active");
          } else {
            document
              .getElementsByClassName("alpho")[0]
              .classList.add("p-menuitem-active");
          }
        }, 100);
      },
    },
    {
      label: "Resources",

      command: () => {
        setTimeout(() => {
          if (
            document
              .getElementsByClassName("secondo")[0]
              .classList.contains("p-menuitem-active")
          ) {
            document
              .getElementsByClassName("secondo")[0]
              .classList.remove("p-menuitem-active");
          } else {
            document
              .getElementsByClassName("secondo")[0]
              .classList.add("p-menuitem-active");
          }
        }, 100);
      },
      items: [
        {
          label: (
            <Link href="/our-trust">
              <a className="text-dark">
                <BsHandThumbsUp className="me-1" /> CollegeVidya Commitment
              </a>
            </Link>
          ),
        },
        {
          label: (
            <Link href="/checklist">
              <a className="text-dark">
                <BsJournalCheck className="me-1" /> Important Facts
              </a>
            </Link>
          ),
        },
        {
          label: (
            <Link href="/why-collegevidya">
              <a className="text-dark">
                <BsQuestionSquare className="me-1" /> Why CollegeVidya
              </a>
            </Link>
          ),
        },
        {
          label: (
            <div style={{ padding: "0.75rem 1.25rem" }} className="text-dark">
              <BsArrow90DegRight className="me-1" />
              Verify Your University
            </div>
          ),
          command: () => {
            window.open("https://verify.collegevidya.com/", "_target");
          },
        },
        {
          label: (
            <Link href="/">
              <a className="text-dark">
                <BsCameraVideo className="me-1" /> Video Counselling{" "}
              </a>
            </Link>
          ),
        },
        {
          label: (
            <Link href="/contact-us">
              <a className="text-dark">
                <BsChatLeftDots className="me-1" /> Contact Us
              </a>
            </Link>
          ),
        },
        {
          label: (
            <Link href="/about-us">
              <a className="text-dark">
                <BsSortAlphaUpAlt className="me-1" /> About Us{" "}
              </a>
            </Link>
          ),
        },
        {
          label: (
            <Link href="/media">
              <a className="text-dark">
                <BsCaretRightSquare className="me-1" /> Media{" "}
              </a>
            </Link>
          ),
        },
        {
          label: (
            <Link href="/privacy-policy">
              <a className="text-dark">
                <BsJournalText className="me-1" /> Our Policy{" "}
              </a>
            </Link>
          ),
        },
        {
          label: (
            <Link href="/meet-team">
              <a className="text-dark">
                <BsPeople className="me-1" /> Meet the Team{" "}
              </a>
            </Link>
          ),
        },
      ],
      className: "secondo",
    },
    {
      label: (
        <Link href="/suggest-me-an-university">
          <a
            className="rounded d-flex align-items-center position-relative"
            style={{ padding: "11px 15px", backgroundColor: "#f0f8ff" }}>
            {" "}
            <Image src={badge} width={20} height={20} alt="" />{" "}
            <span className="ms-1">Suggest me a university</span>{" "}
            <span className="position-absolute start-0 ms-2 top-0 translate-middle bgsecondary text-white px-2 py-1 rounded fs-10">
              New
            </span>
          </a>
        </Link>
      ),
      className: `${styles.suggest_me} me-0`,
    },
  ];

  useEffect(() => {
    const alpho = document.getElementsByClassName("alpho")[0];
    const secondo = document.getElementsByClassName("secondo")[0];
    const megamenu = document.getElementsByClassName("megamenu")[0];

    alpho.addEventListener("mouseenter", () => {
      setMegamenuVisible(false);
      alpho.classList.add("p-menuitem-active");
    });

    alpho.addEventListener("mouseleave", () => {
      alpho.classList.remove("p-menuitem-active");
    });

    secondo.addEventListener("mouseenter", () => {
      setMegamenuVisible(false);
      secondo.classList.add("p-menuitem-active");
    });

    secondo.addEventListener("mouseleave", () => {
      secondo.classList.remove("p-menuitem-active");
    });

    // megamenu.addEventListener("mouseenter", () => {
    //   alpho.classList.remove("p-menuitem-active");
    //   secondo.classList.remove("p-menuitem-active");
    //   setMegamenuVisible(true);
    // });

    // megamenu.addEventListener("mouseleave", () => {
    //   setMegamenuVisible(false)
    // })
  }, []);

  const start = (
    <div className="d-flex align-items-center">
      <a href="/">
        <Image alt="logo" src={logo} className="mr-2" />
      </a>
      <span className="text-dark fs-14">{<Checklist />}</span>
      <AnimatedBird />
    </div>
  );

  useEffect(() => {
    if (menuData) {
      const men = menuData
        .map((domain) => domain.courses)
        .flat()
        .map(function (course) {
          return {
            label: (
              <Link href={`/top-universities-colleges/${course.slug}`}>
                <a className={`text-dark text-decoration-none`}>
                  {course.name}
                </a>
              </Link>
            ),
          };
        });

      setTopMenu(men);
    }
  }, [menuData]);

  return (
    <>
      {router.pathname === "/" ? (
        <TopHeader show={show} setShow={setShow} taglineData={taglineData} />
      ) : null}

      <div className="container">
        <Menubar
          className={`${styles.custom_menu} bg-transparent border-0`}
          model={items}
          start={start}
          end={<SearchPanel menuData={menuData} screenSize={screenSize} />}
        />
      </div>
      <Modal
        show={abroadModalVisible}
        onHide={() => setAbroadModalVisible(false)}
        centered
        style={{ zIndex: "9999" }}>
        <CourseModal closeModal={() => setAbroadModalVisible(false)} />
      </Modal>
    </>
  );
};

export default Header;
