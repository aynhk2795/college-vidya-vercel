import { useState } from "react"
import LoginModal from "./LoginModal"
import SignUpModal from "./SignUpModal"

const AuthModal = (props) => {

    const [mode, setMode] = useState(props.initialMode)

    return (
        mode === 'signup' ? <SignUpModal menuData={props.menuData} setMode={setMode} closeModal={() => props.closeModal()} /> : <LoginModal setMode={setMode} closeModal={() => props.closeModal()} />
    )
}

export default AuthModal