import React, { useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { Sidebar } from "primereact/sidebar";
import AnimatedBird from "/components/Global/AnimatedBird";
import SearchBar from "/components/Search/SearchBar";
import SearchSuggestion from "/components/Search/Searchsuggestion";
import { BsSearch } from "react-icons/bs";

const Search = ({ screenSize }) => {
  const [visibleFullScreen, setVisibleFullScreen] = useState(false);
  return (
    <>
      <Sidebar
        visible={visibleFullScreen}
        fullScreen
        onHide={() => setVisibleFullScreen(false)}>
        <Container>
          <Row>
            <Col md={6} className="mx-auto">
              <div>
                <h1 className="text-center">
                  <span className="text-primary">College Vidya</span>{" "}
                  <span>Search</span>
                </h1>

                <p className="text-center mb-3 fs-14">
                  #ChunoWahiJoHaiSahi <AnimatedBird />
                </p>

                <SearchBar screenSize={screenSize} />
                <SearchSuggestion screenSize={screenSize} />
              </div>
            </Col>
          </Row>
        </Container>
      </Sidebar>
      <span onClick={() => setVisibleFullScreen(true)}>
        <BsSearch fontSize={20} />
      </span>
    </>
  );
};

export default Search;
