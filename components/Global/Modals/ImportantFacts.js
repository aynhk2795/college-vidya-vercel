import React from "react";
import { AiOutlineRight } from "react-icons/ai";
import { Modal } from "react-bootstrap";
import styles from "/styles/ProsCons.module.scss";
import Right from "/components/Global/Icon/Right";
import Wrong from "/components/Global/Icon/Wrong";
import { FcIdea } from "react-icons/fc";
import CheckList from "../../CheckList/CheckList";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      show={props.show}
      onHide={() => props.onHide()}
      size="xl"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Important Facts
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={`${styles.prosconsModal}`}>
        <CheckList />
      </Modal.Body>
    </Modal>
  );
}
const ImportantFacts = ({ pros_cons }) => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <p
        onClick={() => setModalShow(true)}
        className="text-white px-3 py-2 rounded fs-13 text-center cursor-pointer"
        style={{ backgroundColor: "#00d8a1", minWidth: "155px" }}>
        <FcIdea fontSize={14} /> Important Facts
      </p>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        pros_cons={pros_cons}
      />
    </>
  );
};

export default ImportantFacts;
