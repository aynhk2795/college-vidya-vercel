import { Form, Modal } from "react-bootstrap";

//Form Validation Components
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { flushSync } from "react-dom";
import { useState } from "react";
import Link from "next/link";
import { Button } from "primereact/button";
import { InputNumber } from "primereact/inputnumber";
import { InputText } from "primereact/inputtext";
import { Dropdown } from "primereact/dropdown";
import { setCookie } from "cookies-next";
import { useRouter } from "next/router";
import { FaLock, FaTimes } from "react-icons/fa";
import { RadioButton } from "primereact/radiobutton";
import { Calendar } from "primereact/calendar";
// import styles from "/components/Otta/Modal/LoginForm.module.scss";
import AnimatedBird from "../AnimatedBird";
import OnlyLogo from "../OnlyLogo";
import { getOTP } from "../../../service/MiscService";
import { useAuth } from "../../../hooks/auth";
import { useEffect } from "react";
import styles from "./SignUpModal.module.scss";
import moment from "moment";

const SignUpModal = (props) => {
  const [calculatedAge, setCalculatedAge] = useState(null);
  const router = useRouter();
  const { register: registerUser, login } = useAuth();

  const menudata = props.menuData.map((domain) => domain.courses).flat();

  const [state, setState] = useState(false);
  const [sendingOTP, setSendingOTP] = useState(false);
  const [OTPfromServer, setOTPfromServer] = useState(null);
  const [OTPfromUser, setOTPfromUser] = useState(null);
  const [stepOneDetails, setStepOneDetails] = useState(null);
  const [submittingForm, setSubmittingForm] = useState(false);

  const [resendTimer, setResendTimer] = useState(59);

  const validationSchema = Yup.object().shape({
    gender: Yup.string().required("Please select a gender"),
    full_name: Yup.string().required("Name is required"),
    dob: Yup.string()
      .typeError("Invalid date")
      .required("Please enter your date of birth"),
    mobile_number: Yup.number()
      .typeError("Invalid number")
      .required("Number is required")
      .test(
        "len",
        "Number must be exactly 10 digits",
        (val) => val.toString().length === 10
      ),
    course: Yup.number().required("Course is required"),
  });

  useEffect(() => {
    const timer = window.setInterval(() => {
      if (state && resendTimer > 0) {
        setResendTimer((prevTime) => prevTime - 1);
      }
    }, 1000);
    return () => {
      window.clearInterval(timer);
    };
  }, [state, resendTimer]);

  const {
    register,
    getValues,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {},
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  useEffect(() => {
    if (getValues("dob")) {
      const dob = getValues("dob");
      const dateToday = new Date();

      setCalculatedAge(moment.duration(dateToday - dob).years() + " Years");
    } else {
      setCalculatedAge(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getValues("dob")]);

  const submitStepOne = (data) => {
    setSendingOTP(true);

    getOTP(data.mobile_number)
      .then((response) => {
        setSendingOTP(false);

        flushSync(() => {
          setOTPfromServer(response.data.otp);
          setStepOneDetails(data);
        });
        setState(true);
        setResendTimer(59);
      })
      .catch(() => {
        setSendingOTP(false);

        iziToast.error({
          title: "Error",
          message: "Oops! Something went wrong",
          timeout: 2000,
          position: "topRight",
        });
      });
  };

  const submitStepTwo = () => {
    setSubmittingForm(true);

    if (OTPfromServer.toString() === OTPfromUser.toString()) {
      let formData = new FormData();
      formData.append("name", stepOneDetails.full_name);
      formData.append("mobile", stepOneDetails.mobile_number);
      formData.append("course", stepOneDetails.course);
      formData.append("gender", stepOneDetails.gender);
      formData.append("dob", stepOneDetails.dob);

      registerUser(formData)
        .then(() => {
          const data = {
            mobile: stepOneDetails.mobile_number,
          };

          login(data)
            .then((response) => {
              iziToast.success({
                title: "Success",
                message: "Registration Successful",
                timeout: 2000,
                position: "topRight",
              });

              let date = new Date();
              date.setDate(date.getDate() + 30);

              setCookie("cvuid", response.data.user_id, {
                expires: date,
              });
              setCookie("atexp", response.data.jwt, {
                expires: date,
              });
              setCookie("ratnxp", response.data.refresh, {
                expires: date,
              });

              setSubmittingForm(false);
              router.reload();
            })
            .catch(() => {
              setSubmittingForm(false);

              iziToast.error({
                title: "Error",
                message: "Oops! Something went wrong",
                timeout: 2000,
                position: "topRight",
              });
            });
        })
        .catch(() => {
          setSubmittingForm(false);

          iziToast.error({
            title: "Error",
            message: "Oops! Something went wrong",
            timeout: 2000,
            position: "topRight",
          });
        });
    } else {
      setSubmittingForm(false);

      iziToast.error({
        title: "Error",
        message: "Invalid OTP",
        timeout: 2000,
        position: "topRight",
      });
    }
  };

  return (
    <>
      <Modal.Header className="border-0 justify-content-center position-relative">
        <Modal.Title
          id="contained-modal-title-vcenter"
          className="ps-4 d-flex align-items-center"
        >
          <div className="d-flex align-items-center">
            <OnlyLogo />
            <span className="fs-14">#ChunoWahiJoHaiSahi</span> <AnimatedBird />
          </div>
        </Modal.Title>
        <span
          className="position-absolute cursor-pointer"
          onClick={() => props.closeModal()}
          style={{ top: "8px", right: "16px" }}
        >
          <FaTimes fontSize={18} />
        </span>
      </Modal.Header>
      <Modal.Body className={`${styles.prosconsModal}`}>
        <div className="form-demo">
          <div className="flex justify-content-center">
            {state ? (
              <div className="otp-panel">
                <div className="pb-4">
                  <p style={{ fontSize: "20px" }} className="m-0 fw-bold">
                    Please enter,
                    <br /> 6 digit OTP sent on{" "}
                    {stepOneDetails ? stepOneDetails.mobile_number : ""}
                  </p>
                  <p
                    className="fs-14 mt-3 textprimary cursor-pointer"
                    onClick={() => setState(false)}
                  >
                    Change Mobile Number
                  </p>
                </div>
                <label className="mb-2" htmlFor="Mobile Number">
                  Enter OTP
                </label>
                <Form.Control
                  type="number"
                  className="w-100"
                  onChange={(e) => setOTPfromUser(e.target.value)}
                  useGrouping={false}
                  pattern="[0-9]*"
                />
                <Button
                  className={`${styles.bluebtn} w-100 mt-3 mb-4 d-flex justify-content-center`}
                  disabled={!OTPfromUser || OTPfromUser.toString().length !== 6}
                  label={"Confirm OTP"}
                  onClick={() => submitStepTwo()}
                  loading={submittingForm}
                />
                <div className="text-center">
                  {" "}
                  {resendTimer <= 0 ? (
                    <span className="fs-12">
                      Didn&apos;t receive the OTP yet?
                    </span>
                  ) : null}
                  {resendTimer > 0 ? (
                    <div className="fs-12">
                      You can resend OTP after{" "}
                      <span style={{ color: "orange" }}>
                        {Math.floor(resendTimer / 60)}:{resendTimer % 60}
                      </span>
                    </div>
                  ) : (
                    <span
                      className="text-primary cursor-pointer"
                      onClick={() => handleSubmit(submitStepOne)()}
                    >
                      &nbsp;Resend
                    </span>
                  )}
                </div>
              </div>
            ) : (
              <Form onSubmit={handleSubmit(submitStepOne)} className={`${styles.signup_form}`}>
                <div>
                  <p style={{ fontSize: "20px" }} className="m-0 mb-4 fw-bold">
                    To sign up,
                    <br /> please enter the following details
                  </p>
                  <div className={`${styles.form_row} d-flex flex-column`}>
                    <div className="d-flex">
                      <div className="field-radiobutton me-4">
                        <RadioButton inputId="male" name="gender" checked={getValues('gender') === 'male'} onChange={() => setValue('gender', 'male', { shouldValidate: true })} />
                        <label className="ms-2" htmlFor="male">
                          Male
                        </label>
                      </div>
                      <div className="field-radiobutton">
                        <RadioButton inputId="female" name="gender" checked={getValues('gender') === 'female'} onChange={() => setValue('gender', 'female', { shouldValidate: true })} />
                        <label className="ms-2" htmlFor="female">
                          Female
                        </label>
                      </div>
                    </div>
                    <p className="p-error text-xs d-block">
                      {errors.gender?.message}
                    </p>
                  </div>
                  <div className={`${styles.form_row}`}>
                    <label
                      className="mb-2 position-absolute translate-middle-y ms-3 bg-white px-2 fs-14"
                      htmlFor="Mobile Number"
                    >
                      Name
                    </label>
                    <InputText
                      {...register("full_name")}
                      className={`w-100 ${errors.full_name ? "p-invalid block" : ""
                        }`}
                    />
                    <span className="p-error text-xs">
                      {errors.full_name?.message}
                    </span>
                  </div>
                  <div className={`${styles.form_row}`}>
                    <label
                      className="mb-2 position-absolute translate-middle-y ms-3 bg-white px-2 fs-14"
                      htmlFor="dob"
                      style={{ zIndex: "1" }}
                    >
                      Date of Birth{" "}(DD/MM/YYYY)
                    </label>
                    <div className="position-relative">
                      <Calendar
                        value={getValues("dob")}
                        //   inputMode={"numeric"}

                        onChange={(e) =>
                          setValue("dob", e.value, { shouldValidate: true })
                        }
                        dateFormat="dd/mm/yy"
                        mask="99/99/9999"
                        className={`w-100 ${errors.dob ? "p-invalid block" : ""}`}
                      />
                      {calculatedAge ? (
                        <span
                          className="position-absolute end-0 top-50 translate-middle-y me-2 px-3 py-1 rounded fs-14"
                          style={{ backgroundColor: "aliceblue" }}>
                          {calculatedAge}
                        </span>
                      ) : null}
                    </div>
                    <span className="p-error text-xs">
                      {errors.dob?.message}
                    </span>
                  </div>
                  <div>
                    <div className={`${styles.form_row}`}>
                      <label
                        className="mb-2 position-absolute translate-middle-y ms-3 bg-white px-2 fs-14"
                        htmlFor="Mobile Number"
                        style={{ zIndex: "1" }}
                      >
                        Mobile Number
                      </label>
                      <span className="p-input-icon-left w-100 ">
                        <i className="pi pi-mobile" />
                        <InputText

                          {...register("mobile_number")}
                          className={`mobile_input w-100 ${errors.mobile_number ? "p-invalid block" : ""
                            }`}
                        />
                      </span>

                      <span className="p-error text-xs">
                        {errors.mobile_number?.message}
                      </span>
                    </div>
                  </div>
                  <div>
                    <div className={`${styles.form_row}`}>
                      <label
                        className="mb-2 position-absolute translate-middle-y ms-3 bg-white px-2 fs-14"
                        style={{ zIndex: "1" }}
                      >
                        Course
                      </label>
                      <Dropdown
                        placeholder={"Select a course"}
                        className={`w-100 ${errors.course ? "p-invalid block" : ""
                          }`}
                        value={getValues("course")}
                        options={menudata}
                        onChange={(e) =>
                          setValue("course", e.value, { shouldValidate: true })
                        }
                        optionLabel="display_name"
                        optionValue="id"
                      />
                      <span className="p-error text-xs">
                        {errors.course?.message}
                      </span>
                    </div>
                  </div>
                  <Button
                    type="submit"
                    className={`${styles.bluebtn} w-100 mt-3 mb-4 d-flex justify-content-center`}
                    loading={sendingOTP}
                    label={"Send OTP"}
                  />
                </div>
                <p className="text-center mt-3 d-flex align-items-center justify-content-center">
                  Already a User?&nbsp;
                  <span
                    className="textprimary cursor-pointer"
                    onClick={() => props.setMode("login")}
                  >
                    Sign In
                  </span>
                </p>
              </Form>
            )}

            <div className="mt-3 text-center">
              <span className="badge fw-normal px-3">
                <FaLock className="lock-icon" /> Your personal information is
                secure with us
              </span>
            </div>
            <div className="text-center mt-3">
              <small>
                By clicking, you agree to our{" "}
                <Link href="/">
                  <a className="text-decoration-none">
                    Privacy Policy, Terms of Use
                  </a>
                </Link>{" "}
                &amp; Disclaimers
              </small>
              <small className="text-secondary">
                {" "}
                (Standard T&amp;C Apply)
              </small>
            </div>
          </div>
        </div>
      </Modal.Body>
    </>
  );
};

export default SignUpModal;
