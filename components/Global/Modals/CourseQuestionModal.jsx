import { useState } from "react";
import { Button, Form, Modal, Tab, Tabs } from "react-bootstrap";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { generate } from "shortid";
import { RadioButton } from "primereact/radiobutton";
import { useEffect } from "react";
import { Button as PRButton } from "primereact/button";
import { useAuth } from "../../../hooks/auth";
import { addLead } from "../../../service/LeadService";
import { useRouter } from "next/router";
import styles from "./CourseQuestionModal.module.scss";
import { BsChevronLeft, BsChevronRight, BsDot } from "react-icons/bs";

const CourseQuestionModal = ({
  from,
  userID,
  courseID,
  specializationID = "",
  courseQuestions,
}) => {
  const router = useRouter();
  const { user } = useAuth();

  const [addingLead, setAddingLead] = useState(false);
  const [currentQuestion, setCurrentQuestion] = useState(0);

  const [validationSchema, setValidationSchema] = useState(
    Yup.object().shape({})
  );

  const {
    register,
    handleSubmit,
    setValue,
    getValues,
    formState: { errors },
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  const createValidationSchema = (currentQuestion) => {
    let schema = {};

    if (currentQuestion.required && currentQuestion.type === "radio") {
      schema[currentQuestion.id.toString()] = Yup.string().required(
        "Please select an option"
      );
    }

    setValidationSchema(Yup.object().shape(schema));
  };

  useEffect(() => {
    if (currentQuestion <= courseQuestions.length) {
      createValidationSchema(courseQuestions[currentQuestion]);
    }
  }, [currentQuestion, courseQuestions]);

  const submitCourseQuestions = (data) => {
    setAddingLead(true);

    let formData = new FormData();
    formData.append("user_id", userID ? userID : user);
    formData.append("course", courseID);
    formData.append("specializations", specializationID);
    formData.append("question_answers", JSON.stringify(data));

    addLead(formData)
      .then(() => {
        setAddingLead(false);
        let newURL = "";
        if (from === "course")
          newURL = `/colleges-universities/${router.query.courseSlug}?uid=${userID ? userID : user
            }`;
        else
          newURL = `/colleges-universities/${router.query.courseSlug}/${router.query.specializationSlug
            }?uid=${userID ? userID : user}`;
        window.location.href = newURL;
      })
      .catch(() => {
        setAddingLead(false);
        iziToast.error({
          title: "Error",
          message: "Oops! Something went wrong",
          timeout: 2000,
          position: "topRight",
        });
      });
  };

  return (
    <Modal.Body className="pb-5">
      <div className="10px auto">
        <Form onSubmit={handleSubmit(submitCourseQuestions)}>
          <div
            style={{
              borderBottom: "1px dashed #ccc",
              paddingBottom: "10px",
              color: "#5d6068",
              fontSize: "13px",
              textAlign: "center",
            }}
          >
            Just Answer {courseQuestions.length - currentQuestion} Simple
            questions to get Best University for you!
          </div>

          <Tabs activeKey={currentQuestion}>
            {courseQuestions.map((question, index) => (
              <Tab key={question.id} eventKey={index}>
                <h1
                  style={{
                    textAlign: "center",
                    fontSize: "24px",
                    fontWeight: "800",
                    marginBottom: "35px",
                    marginTop: "30px",
                  }}
                >
                  {question.question}
                </h1>
                {question.type === "radio" ? (
                  <div>
                    <div
                      className={`${styles.course_ques} d-flex justify-content-center flex-wrap`}
                    >
                      {question.options.map((option) => {
                        const randomFor = generate();
                        return (
                          <span
                            key={generate()}
                            className={`${styles.course_ques_radiowrap}`}
                          >
                            <RadioButton
                              id={randomFor}
                              inputId={`${option.answer}-${randomFor}`}
                              value={option.answer}
                              {...register(question.id.toString())}
                              checked={
                                getValues(question.id.toString()) ===
                                option.answer
                              }
                              onChange={(e) => {
                                setValue(question.id.toString(), e.value, {
                                  shouldValidate: true,
                                });
                                if (
                                  currentQuestion !==
                                  courseQuestions.length - 1
                                ) {
                                  setCurrentQuestion((prev) => prev + 1);
                                }
                              }}
                            />
                            <label
                              onClick={() => {
                                document.getElementById(randomFor).click();
                              }}
                              htmlFor={`${option.answer}-${randomFor}`}
                            >
                              {option.answer}
                            </label>
                          </span>
                        );
                      })}
                    </div>
                    <div
                      className="p-error text-xs text-center my-4"
                      style={{ top: "-13px" }}
                    >
                      {errors[question.id.toString()]?.message}
                    </div>
                  </div>
                ) : null}
              </Tab>
            ))}
          </Tabs>
          <div className="my-2 flex align-items-center justify-content-center text-center">
            {courseQuestions &&
              courseQuestions.map((question, index) => {
                if (currentQuestion === index) {
                  return (
                    <div
                      className="d-inline-block mx-1 bgprimary"
                      key={question.id}
                      style={{
                        width: "6px",
                        height: "6px",
                        borderRadius: "50%",
                      }}
                    />
                  );
                } else {
                  return (
                    <div
                      className="d-inline-block mx-1 bg-secondary"
                      key={question.id}
                      style={{
                        width: "6px",
                        height: "6px",
                        borderRadius: "50%",
                      }}
                    />
                  );
                }
              })}
          </div>
          <div
            className={`${styles.quesform_modalfooter} d-flex align-items-center justify-content-between`}
          >
            {currentQuestion !== 0 ? (
              <p
                className="m-0 cursor-pointer textprimary fs-14"
                onClick={() => setCurrentQuestion((prev) => prev - 1)}
              >
                <BsChevronLeft /> Back
              </p>
            ) : null}
            {/* {currentQuestion !== 0 ? <Button onClick={() => setCurrentQuestion((prev) => prev - 1)}>Previous</Button> : null} */}
            {currentQuestion === courseQuestions.length - 1 ? (
              <p>
                <PRButton className="ms-2" loading={addingLead}>
                  Submit <BsChevronRight className="ms-1" />
                </PRButton>
              </p>
            ) : null}
          </div>
        </Form>
      </div>
    </Modal.Body>
  );
};

export default CourseQuestionModal;
