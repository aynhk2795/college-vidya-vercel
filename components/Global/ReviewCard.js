import moment from "moment/moment";
import React from "react";
import { BsPersonCircle } from "react-icons/bs";
import Rating from "/components/universityListingPage/Rating/Rating";

const ReviewCard = ({
  reviewerName,
  reviewDate,
  reviewContent,
  reviewRating,
}) => {
  return (
    <>
      <div className="mb-3 bg-white p-3 rounded card">
        <p className="d-flex align-items-center justify-content-between">
          <span className="fw-bold">
            <BsPersonCircle fontSize={30} /> {reviewerName}
          </span>
          <span className="fs-12">{moment(reviewDate).format("lll")}</span>
        </p>
        <p className="fs-14">{reviewContent}</p>
        <Rating reviewRating={reviewRating} />
      </div>
    </>
  );
};

export default ReviewCard;
