import React from "react";
import styles from "/styles/AnimatedPodcast.module.scss";
import { BsFillMicFill } from "react-icons/bs";

const AnimatedPodcast = () => {
  return (
    <>
      <BsFillMicFill className="fs-4" />
      <div className={`${styles.boxContainer}`}>
        <div className={`${styles.box1} ${styles.box}`}></div>
        <div className={`${styles.box2} ${styles.box}`}></div>
        <div className={`${styles.box3} ${styles.box}`}></div>
        <div className={`${styles.box4} ${styles.box}`}></div>
        <div className={`${styles.box5} ${styles.box}`}></div>
      </div>
    </>
  );
};

export default AnimatedPodcast;
