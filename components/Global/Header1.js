import Link from "next/link";
import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Logo from "/components/Global/Logo";
import HeaderButton from "/components/Global/Buttons/HeaderButton";
import { FcIdea } from "react-icons/fc";
import SuggestMeMObile from "./SuggestMeMObile";
import Edit from "../universityListingPage/Modal/Edit";
import ImportantFacts from "/components/Global/Modals/ImportantFacts";

const Header1 = ({
  screenSize,
  showEdit = false,
  leadData = [],
  userData = {},
  courseID = "",
  specializationID = "",
}) => {
  return (
    <>
      <Container className="py-3">
        <Row>
          <Col>
            <Logo />
          </Col>
          <Col className="d-flex align-items-center justify-content-end">
            {showEdit ? (
              <Edit
                leadData={leadData}
                userData={userData}
                courseID={courseID}
                specializationID={specializationID}
                screenSize={screenSize}
              />
            ) : null}
          </Col>
          {screenSize && screenSize.width < 992 ? (
            <Col
              className="d-flex justify-content-end align-items-center"
              style={{ minWidth: "202px" }}>
              <SuggestMeMObile />
            </Col>
          ) : (
            <Col className="d-flex align-items-center gap-2 justify-content-end ms-auto">
              <ImportantFacts />

              {/* <Link href="/">
                <a
                  className="px-3 py-2 rounded text-white bgprimary fs-13 text-center"
                  style={{ minWidth: "230px" }}>
                  Know Your University in 2 mins
                </a>
              </Link> */}
            </Col>
          )}
        </Row>
      </Container>
    </>
  );
};

export default Header1;
