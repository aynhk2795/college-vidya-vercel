import React from "react";
import { BsX } from "react-icons/bs";

const MobileMenuHeader = ({ menu }) => {
  return (
    <>
      <div className="d-flex align-items-center justify-content-between w-100 sticky-top">
        <div
          onClick={() => {
            if (menu.current) {
              menu.current.toggle(false);
            }
          }}>
          <BsX fontSize={32} className="shadow-sm rounded" />
        </div>

        <div>
          <span className="fs-13">#ChunoWahiJoHaiSahi </span>
        </div>
      </div>
    </>
  );
};

export default MobileMenuHeader;
