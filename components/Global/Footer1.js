import Link from "next/link";
import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { FcLike } from "react-icons/fc";

const Footer1 = () => {
  return (
    <>
      <Container>
        <Row>
          <Col md={10} className="mx-auto text-center fs-12 mb-5">
            <p>
              By clicking, you agree to our{" "}
              <Link href="/privacy-policy">
                <a className="textprimary">Privacy Policy</a>
              </Link>
              , &{" "}
              <Link href="/our-trust">
                <a>Our Trust</a>
              </Link>
            </p>
            <p>
              The intend of College Vidya is to provide unbiased precise
              information & comparative guidance on Universities and its
              Programs of Study to the Admission Aspirants. The contents of the
              Collegevidya Site, such as Texts, Graphics, Images, Blogs, Videos,
              University Logos, and other materials contained on Collegevidya
              Site (collectively, “Content”) are for information purpose only.
              The content is not intended to be a substitute for in any form on
              offerings of its Academia Partner. Infringing on intellectual
              property or associated rights is not intended or deliberately
              acted upon. The information provided by College Vidya on
              www.collegevidya.com or any of its mobile or any other
              applications is for general information purposes only. All
              information on the site and our mobile application is provided in
              good faith with accuracy and to the best of our knowledge,
              however, we make nor representation or warranty of any kind,
              express or implied, regarding the accuracy, adequacy, validity,
              reliability, completeness of any information on the Site or our
              mobile application. Collegevidya & its fraternity will not be
              liable for any errors or omissions and damages or losses resultant
              if any from the usage of its information.
            </p>
            <p className="d-flex align-items-center justify-content-center">
              Build with <FcLike className="ms-1" />. Made in India.
            </p>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Footer1;
