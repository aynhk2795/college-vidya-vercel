import React from "react";
import { Button } from "primereact/button";
import Link from "next/link";

const PrimaryButton = (props) => {
  return (
    <>
      <div className="primary-button-wrap">
        <Button className="primary-button">
          <span>{props.buttonText}</span>
        </Button>
      </div>
    </>
  );
};

export default PrimaryButton;
