import React from "react";
import Link from "next/link";
import styles from "/styles/HeaderButton.module.scss";

const HeaderButton = (props) => {
  return (
    <>
      <a className={`${styles.headerbtn} ms-2`}>
        {props.icon}
        {props.title}
      </a>
    </>
  );
};

export default HeaderButton;
