import React from "react";
import { Button } from "primereact/button";
import Link from "next/link";

const VideoCounselling = () => {
  return (
    <>
      <div className="primary-button-wrap">
        <Link href="/">
          <a>
            <Button className="secondary-button fs-10 px-0 text-center d-flex">
              <span className="px-3">Video Counselling</span>
            </Button>
          </a>
        </Link>
      </div>
    </>
  );
};

export default VideoCounselling;
