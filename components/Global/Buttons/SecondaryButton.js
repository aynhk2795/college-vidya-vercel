import React from "react";
import { Button } from "primereact/button";
import Link from "next/link";

const PrimaryButton = () => {
  return (
    <>
      <div className="primary-button-wrap">
        <Link href="/">
          <a>
            <Button className="primary-button fs-10 px-0 text-center d-flex">
              <span className="px-3">Proceed to university</span>
            </Button>
          </a>
        </Link>
      </div>
    </>
  );
};

export default PrimaryButton;
