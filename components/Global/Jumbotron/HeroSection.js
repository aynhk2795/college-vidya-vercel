import React from "react";
import { Container } from "react-bootstrap";

const HeroSection = (props) => {
  return (
    <>
      <Container fluid className="px-0">
        <div className="bg-grey py-5 text-center">
          <h1>{props.title}</h1>
          <p>{props.content}</p>
        </div>
      </Container>
    </>
  );
};

export default HeroSection;
