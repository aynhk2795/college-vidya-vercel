import Link from "next/link";
import React, { useState } from "react";
import { AiOutlineHome } from "react-icons/ai";
import {
  BsPerson,
  BsPersonPlus,
  BsSearch,
  BsShieldFillCheck,
} from "react-icons/bs";
import { MdOutlineHelpOutline } from "react-icons/md";
import Mentor from "../universityListingPage/Modal/Mentor";
import Search from "./Modals/Search";

const MobileFooter = ({ screenSize }) => {
  return (
    <>
      <div
        className="sticky-bottom bottom-0 bg-white d-flex align-items-center pt-3 pb-2 justify-content-around border-top"
        style={{ zIndex: "111" }}>
        <Link href="/">
          <a className="text-dark col">
            <div className="d-flex flex-column justify-content-center align-items-center">
              <AiOutlineHome fontSize={20} />
              <span className="fw-normal fs-11">Home</span>
            </div>
          </a>
        </Link>

        <div className="d-flex flex-column justify-content-center align-items-center col">
          <BsPerson fontSize={20} />
          <span className="fw-normal fs-11">Account</span>
        </div>
        <Link href="/suggest-me-an-university">
          <a className="col">
            <div className="d-flex flex-column justify-content-center align-items-center position-relative">
              <span
                className="badge bgsecondary position-absolute start-50 translate-middle shadow-sm"
                style={{ top: "-16px" }}>
                Suggest
              </span>
              <BsShieldFillCheck className="textprimary" fontSize={20} />
              <span className="fw-normal fs-11">University</span>
            </div>
          </a>
        </Link>

        <div className="d-flex flex-column justify-content-center align-items-center col">
          <span className="fw-normal fs-11">
            <Search screenSize={screenSize} />
          </span>
          <span className="fw-normal fs-11">Search</span>
        </div>
        <div className="d-flex flex-column justify-content-center align-items-center col position-relative">
          <span
            className="badge position-absolute start-50 translate-middle shadow-sm"
            style={{ top: "-16px", backgroundColor: "#11d479" }}>
            Video
          </span>
          <Mentor fromFooter={true} />
          <span className="fw-normal fs-11">Counseling</span>
        </div>
      </div>
    </>
  );
};

export default MobileFooter;
