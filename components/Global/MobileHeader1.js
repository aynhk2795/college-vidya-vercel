import Link from "next/link";
import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Logo from "/components/Global/Logo";
import HeaderButton from "/components/Global/Buttons/HeaderButton";
import { FcIdea } from "react-icons/fc";
import SuggestMeMObile from "./SuggestMeMObile";

const MobileHeader1 = () => {
  return (
    <>
      <Container className="py-3">
        <Row>
          <Col>
            <Logo />
          </Col>
          <Col
            className="d-flex justify-content-end align-items-center"
            style={{ minWidth: "192px" }}>
            <SuggestMeMObile />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default MobileHeader1;
