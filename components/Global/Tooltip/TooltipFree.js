import React from "react";
import { Tooltip } from "primereact/tooltip";
import { BsInfoCircle } from "react-icons/bs";

const TooltipFree = (props) => {
  return (
    <>
      <Tooltip target=".tooltipIcon" mouseTrack />
      <BsInfoCircle className={`tooltipIcon`} data-pr-tooltip={props.info} />
    </>
  );
};

export default TooltipFree;
