import React from "react";
import { Tooltip } from "primereact/tooltip";
import { BsInfoCircle } from "react-icons/bs";
import styles from "/styles/TooltipNotch.module.scss";

const TooltipNotch = (props) => {
  return (
    <>
      <Tooltip target=".tooltipIcon" mouseTrack />
      <BsInfoCircle
        className={`${styles.topnotch} tooltipIcon`}
        data-pr-tooltip={props.info}
      />
    </>
  );
};

export default TooltipNotch;
