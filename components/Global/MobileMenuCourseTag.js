import React from "react";
import { BsArrowLeft } from "react-icons/bs";

const MobileMenuCourseTag = ({ displayName, navigateBackward }) => {
  return (
    <>
      <div className="d-flex align-items-center justify-content-between">
        <div
          className="shadow-1 px-3 py-2 rounded"
          onClick={() => navigateBackward()}>
          <BsArrowLeft />
        </div>
        <span>{displayName}</span>
      </div>
    </>
  );
};

export default MobileMenuCourseTag;
