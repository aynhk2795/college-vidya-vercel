import React, { useState, useEffect } from "react";
import { Sidebar } from "primereact/sidebar";
import { Container } from "react-bootstrap";
import Col from "react-bootstrap/Col";
import Nav from "react-bootstrap/Nav";
import Row from "react-bootstrap/Row";
import Tab from "react-bootstrap/Tab";
import { Card } from "primereact/card";
import Image from "next/image";
import Link from "next/link";
import {
  BsChevronBarRight,
  BsChevronDown,
  BsChevronRight,
} from "react-icons/bs";
import { generate } from "shortid";
import { TiTick } from "react-icons/ti";
import SearchBar from "../Search/SearchBar";
import { HiOutlineArrowRight } from "react-icons/hi";
import Aos from "aos";
import { AiOutlineRight } from "react-icons/ai";
import { InputText } from "primereact/inputtext";
import CourseModal from "../Homepage/Modal/CourseModal";
import styles from "./Megamenu.module.scss";

const Megamenu = ({
  menuData = [],
  alertVisible,
  megamenuVisible,
  setMegamenuVisible,
  setAbroadModalVisible,
}) => {
  const [selectedCourse, setSelectedCourse] = useState(null);
  const [searchedSpecializations, setSearchedSpecializations] = useState([]);
  const [maskClass, setMaskClass] = useState(null);

  useEffect(function () {
    Aos.init({ duration: 1000 });
  }, []);

  useEffect(() => {
    setMaskClass(
      `top-side-bar-wrap ${alertVisible ? "top-header-visible" : ""}`
    );
  }, [alertVisible]);

  const searchSpecialization = (searchValue) => {
    const allSpecializations = menuData
      .map((domain) => domain.courses)
      .flat()
      .filter((course) => course.id === selectedCourse)[0].specializations;

    if (searchValue !== "") {
      let remainingSpecs = allSpecializations.filter((specialization) =>
        specialization.name.toLowerCase().includes(searchValue.toLowerCase())
      );
      setSearchedSpecializations(remainingSpecs);
    } else {
      setSearchedSpecializations(allSpecializations);
    }
  };

  useEffect(() => {
    if (selectedCourse) {
      setSearchedSpecializations([]);
      const allSpecializations = menuData
        .map((domain) => domain.courses)
        .flat()
        .filter((course) => course.id === selectedCourse)[0].specializations;

      setSearchedSpecializations(allSpecializations);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedCourse]);

  return (
    <>
      <Sidebar
        visible={megamenuVisible}
        position="top"
        onHide={() => setMegamenuVisible(false)}
        maskClassName={maskClass}
        className="h-75 pt-4"
        blockScroll={true}
        transitionOptions={{
          onExiting: () => {
            setMaskClass("");
          },
          onEntering: () => {
            setMaskClass(
              `top-side-bar-wrap ${alertVisible ? "top-header-visible" : ""}`
            );
          },
        }}>
        <Container>
          <Tab.Container defaultActiveKey={0}>
            <Row>
              <Col sm={3}>
                <div className="sticky-top">
                  <h4 className="textprimary">Browse by Domains</h4>

                  <Nav
                    variant="pills"
                    className={`megamenu_navwrap flex-column`}>
                    {menuData.map((domain, index) => (
                      <Nav.Item
                        key={domain.id}
                        className="mb-2"
                        onClick={() => setSelectedCourse(null)}>
                        <Nav.Link
                          eventKey={index}
                          className="d-flex justify-content-between align-items-center text-dark">
                          {domain.name} <BsChevronRight />
                        </Nav.Link>
                      </Nav.Item>
                    ))}
                    <Nav.Item
                      className="mb-2"
                      onClick={() => {
                        setMegamenuVisible(false);
                        setAbroadModalVisible(true);
                      }}>
                      <Nav.Link
                        eventKey={"abroad-online"}
                        className="d-flex justify-content-between align-items-center text-dark">
                        Study Abroad (Online) <BsChevronRight />
                      </Nav.Link>
                    </Nav.Item>
                    <Nav.Item
                      className="mb-2"
                      onClick={() => {
                        setMegamenuVisible(false);
                        setAbroadModalVisible(true);
                      }}>
                      <Nav.Link
                        eventKey={"abroad-campus"}
                        className="d-flex justify-content-between align-items-center text-dark">
                        Study Abroad (On-Campus) <BsChevronRight />
                      </Nav.Link>
                    </Nav.Item>
                  </Nav>
                  <div
                    className="text-center px-3 py-3 rounded"
                    style={{ backgroundColor: "#f0e68c" }}>
                    Interest Free EMI Available
                  </div>
                </div>
              </Col>
              <Col sm={9}>
                {!selectedCourse ? (
                  <div>
                    <h4 className="text-dark" data-aos="fade-left">
                      Popular Programs
                    </h4>
                    <Tab.Content data-aos="fade-left">
                      {menuData.map((domain, index) => (
                        <Tab.Pane key={domain.id} eventKey={index}>
                          <Row className="row-cols-4 g-3">
                            {menuData
                              .filter((type) => type.id === domain.id)[0]
                              .courses.map((course) => (
                                <Col key={course.id}>
                                  <span className="cursor-pointer">
                                    <Card className="position-relative">
                                      <Link href={`/courses/${course.slug}/`}>
                                        <a className="d-block">
                                          {course.icon ? (
                                            <Image
                                              src={course.icon}
                                              width={35}
                                              height={35}
                                              alt=""
                                            />
                                          ) : (
                                            <Image
                                              src={
                                                "/images/course_icons/dummy.png"
                                              }
                                              width={35}
                                              height={35}
                                              alt=""
                                            />
                                          )}

                                          <p
                                            className="mt-2 mb-2 text-dark"
                                            style={{
                                              height: "42px",
                                              overflow: "hidden",
                                            }}>
                                            {course.name}
                                          </p>
                                          <p
                                            className="fs-12 d-inline-block px-3 py-1 mb-2"
                                            style={{
                                              backgroundColor: "aliceblue",
                                              borderRadius: "20px",
                                            }}>
                                            {course.duration}
                                          </p>
                                          <p className="fs-13 text-secondary mb-2 ps-1 fw-bold text-dark">
                                            <span className="blink"></span>{" "}
                                            Compare{" "}
                                            <span className="textprimary">
                                              {" "}
                                              {course.university_count}{" "}
                                            </span>
                                            Universities
                                          </p>
                                        </a>
                                      </Link>

                                      <p
                                        className="textprimary fs-14"
                                        onClick={() => {
                                          setSelectedCourse(course.id);
                                          Aos.refresh();
                                        }}>
                                        View Specialisations{" "}
                                        <HiOutlineArrowRight className="ms-1" />{" "}
                                      </p>
                                      {selectedCourse === course.id ? (
                                        <TiTick
                                          fontSize={24}
                                          className="text-white rounded-circle position-absolute end-0 top-0 bgprimary me-2 mt-2  "
                                        />
                                      ) : null}
                                    </Card>
                                  </span>
                                </Col>
                              ))}
                          </Row>
                        </Tab.Pane>
                      ))}
                    </Tab.Content>
                  </div>
                ) : (
                  <div data-aos="fade-left">
                    <div className="d-flex justify-content-between align-items-center mb-4 sticky-top bg-white py-3">
                      <h4 className="text-dark fs-14 m-0" data-aos="fade-left">
                        <span
                          className="textprimary cursor-pointer"
                          onClick={() => setSelectedCourse(null)}>
                          {
                            menuData
                              .map((menu) => menu.courses)
                              .flat()
                              .filter(
                                (course) => course.id === selectedCourse
                              )[0].name
                          }{" "}
                          <AiOutlineRight className="me-1" />
                        </span>
                        Specialisations
                      </h4>
                      <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <InputText
                          placeholder="Search Specialisation"
                          className="p-inputtext-sm"
                          onChange={(e) => searchSpecialization(e.target.value)}
                        />
                      </span>
                    </div>
                    <Row className="row-cols-4 g-3">
                      {searchedSpecializations.map((specialization) => (
                        <Link
                          key={specialization.id}
                          href={`/courses/${
                            menuData
                              .map((domain) => domain.courses)
                              .flat()
                              .filter(
                                (course) => course.id === selectedCourse
                              )[0].slug
                          }/${specialization.slug}/`}>
                          <a>
                            <Col key={specialization.id}>
                              <span className="cursor-pointer">
                                <Card className="position-relative">
                                  <Image
                                    src={"/images/course_icons/dummy.png"}
                                    width={35}
                                    height={35}
                                    alt=""
                                  />
                                  <p
                                    className="mt-2 mb-2 text-dark"
                                    style={{
                                      height: "45px",
                                      overflow: "hidden",
                                    }}>
                                    {specialization.name}
                                  </p>

                                  <p className="fs-12 text-secondary mb-2 myshine">
                                    <span className="blink"></span> Compare{" "}
                                    {specialization.university_count}{" "}
                                    Universities
                                  </p>

                                  <p className="m-0 textprimary fs-14">
                                    {" "}
                                    View Info{" "}
                                    <HiOutlineArrowRight className="ms-1" />{" "}
                                  </p>
                                </Card>
                              </span>
                            </Col>
                          </a>
                        </Link>
                      ))}
                    </Row>
                  </div>
                )}
              </Col>
              {/* <Col sm={3} className={`${selectedCourse ? 'visible ms-0' : 'invisible ms-n3'}`} style={{ transition: "all 5s" }}>

                {selectedCourse ? (<div
                  className="h-75 pe-2 py-3 rounded"
                  style={{ overflowY: "scroll" }}

                >
                  <h4 className="textprimary">Specialisation</h4>
                  <div className="mb-3 px-1">
                    <SearchBar />
                  </div>
                  {menuData.map(domain => domain.courses).flat().filter(course => course.id === selectedCourse)[0].specializations.map(specialization =>
                    <Link href={`/courses/${menuData.map(domain => domain.courses).flat().filter(course => course.id === selectedCourse)[0].slug}/${specialization.slug}/`} key={generate()}>
                      <a
                        className="text-dark d-block mb-2 p-2 rounded d-flex align-items-start justify-content-between"
                        style={{ backgroundColor: "aliceblue" }}
                      >
                        {specialization.name} <BsChevronRight />
                      </a>
                    </Link>
                  )}

                </div>) : null}
              </Col> */}
            </Row>
          </Tab.Container>
        </Container>
      </Sidebar>

      <p
        onClick={() => setMegamenuVisible(!megamenuVisible)}
        className="bgprimary rounded text-white"
        style={{ padding: "11px 15px" }}>
        Explore All Programs <BsChevronDown />{" "}
      </p>
    </>
  );
};

export default Megamenu;
