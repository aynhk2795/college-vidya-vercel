import Link from "next/link";
import React from "react";
import { useState } from "react";
import { Alert, Container } from "react-bootstrap";

const TopHeader = (props) => {
  if (props.show) {
    return (
      <>
        <style>
          {`
          .alert-dismissible .btn-close {
            top: 50%;
    transform: translateY(-50%);
    right: 0;
    
}
button:focus:not(:focus-visible) {
box-shadow: none;
}
`}
        </style>

        <Alert
          className={`text-center m-0 py-2 d-flex align-items-center justify-content-center border-0 rounded-0`}
          variant="primary"
          onClose={() => props.setShow(false)}
          dismissible>
          <p className="me-0 fs-14">
            {props.taglineData && props.taglineData.title}{" "}
            <Link href={props.taglineData ? props.taglineData.link : "#"}>
              <a className="bgprimary text-white fs-12 px-2 rounded py-1 fw-normal text-decoration-none">
                {props.taglineData.button_text}
              </a>
            </Link>{" "}
          </p>
        </Alert>
      </>
    );
  }
  return null;
};

export default TopHeader;
