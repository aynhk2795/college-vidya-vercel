import React from "react";
import { FcLike } from "react-icons/fc";
import { TiTick } from "react-icons/ti";
import { FcVideoCall } from "react-icons/fc";
import { BiCheckShield } from "react-icons/bi";
import { IoIosChatbubbles } from "react-icons/io";
import Link from "next/link";
import styles from "/styles/Footer.module.scss";

const footermenu = [
  {
    id: 1,
    title: "About us",
    url: "/about-us",
  },
  {
    id: 2,
    title: "Blog",
    url: "/blog",
  },
  {
    id: 3,
    title: "Our Policy",
    url: "/privacy-policy",
  },
  {
    id: 4,
    title: "Terms & Conditions",
    url: "/term-and-conditions",
  },
  {
    id: 5,
    title: "Sitemap",
    url: "/sitemap",
  },
  {
    id: 6,
    title: "Contact us",
    url: "/contact-us",
  },
  {
    id: 7,
    title: "CV Checklist",
    url: "/checklist",
    icon: <TiTick className="text-success fs-3" />,
  },
  {
    id: 8,
    title: "Important Videos(CV TV)",
    url: "/collection",
    icon: <FcVideoCall className="fs-3" />,
  },
  {
    id: 9,
    title: "Our Trust",
    url: "/our-trust",
    icon: <BiCheckShield className="fs-3" />,
  },
  {
    id: 10,
    title: "Feedback",
    url: "/contact-us",
    icon: <IoIosChatbubbles className="fs-3" />,
  },
];

const Disclamer = () => {
  return (
    <>
      <div className="row mt-4">
        <ul className="d-flex list-unstyled justify-content-between flex-wrap">
          {footermenu.map((list) => (
            <li key={list.id}>
              <Link href={list.url}>
                <a className={` ${styles.link_text} text-decoration-none`}>
                  {list.icon} {list.title}
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </div>

      <p className={`text-center border-top pt-4 fs-10`}>
        <span className="d-block mb-3">
          <Link href="/terms">
            <a className={` ${styles.link_text} text-decoration-none`}>
              {" "}
              Terms & Conditions
            </a>
          </Link>
          <Link href="/refund">
            <a className={` ${styles.link_text} text-decoration-none`}>
              {" "}
              / Refund Policy
            </a>
          </Link>
        </span>
        The intend of College Vidya is to provide unbiased precise information
        &amp; comparative guidance on Universities and its Programs of Study to
        the Admission Aspirants. The contents of the Collegevidya Site, such as
        Texts, Graphics, Images, Blogs, Videos, University Logos, and other
        materials contained on Collegevidya Site (collectively, “Content”) are
        for information purpose only. The content is not intended to be a
        substitute for in any form on offerings of its Academia Partner.
        Infringing on intellectual property or associated rights is not intended
        or deliberately acted upon. The information provided by College Vidya on
        www.collegevidya.com or any of its mobile or any other applications is
        for general information purposes only. All information on the site and
        our mobile application is provided in good faith with accuracy and to
        the best of our knowledge, however, we make nor representation or
        warranty of any kind, express or implied, regarding the accuracy,
        adequacy, validity, reliability, completeness of any information on the
        Site or our mobile application. Collegevidya &amp; its fraternity will
        not be liable for any errors or omissions and damages or losses
        resultant if any from the usage of its information.
      </p>
      <p className={`text-center fs-14`}>
        © 2022 CollegeVidya, Inc. All Rights Reserved.
      </p>
      <p className={`text-center`}>
        Build with <FcLike /> Made in India.
      </p>
    </>
  );
};

export default Disclamer;
