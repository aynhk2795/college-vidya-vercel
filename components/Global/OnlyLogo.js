import Image from "next/image";
import React from "react";
import logo from "/public/images/logo.svg";

const OnlyLogo = () => {
  return (
    <>
      <Image src={logo} alt="" />
    </>
  );
};

export default OnlyLogo;
