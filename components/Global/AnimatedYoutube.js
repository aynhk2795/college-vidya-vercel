import React from "react";
import styles from "/styles/AnimatedYoutube.module.scss";
import { BsCaretRightFill, BsPlayFill } from "react-icons/bs";

const AnimatedYoutube = () => {
  return (
    <>
      <div
        className={`${styles.pulse} d-flex justify-content-center align-items-center`}>
        <BsPlayFill className="text-white fs-16" />
      </div>

      {/* <div className="circle pulse orange d-flex align-items-center justify-content-center"><BsCaretRightFill fontSize={14} className="text-white" /></div> */}
    </>
  );
};

export default AnimatedYoutube;
