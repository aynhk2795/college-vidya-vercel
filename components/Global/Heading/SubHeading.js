import React from "react";

const SubHeading = (props) => {
  return (
    <>
      <h6 className="sub-heading">{props.title}</h6>
    </>
  );
};

export default SubHeading;
