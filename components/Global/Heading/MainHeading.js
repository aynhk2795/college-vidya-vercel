import React from "react";

const MainHeading = (props) => {
  return (
    <>
      <h2
        className={`main-heading m-0 ${
          props.useextraClass ? props.useextraClass : "text-dark"
        }`}>
        {props.title}
      </h2>
    </>
  );
};

export default MainHeading;
