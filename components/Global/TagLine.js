import React from "react";

const TagLine = (props) => {
  return (
    <>
      <h2
        className={` m-0 textprimary ${
          props.extraClass ? "h4 ms-2 textprimary" : ""
        }`}>
        #ChunoWahiJoHaiSahi{" "}
      </h2>
    </>
  );
};

export default TagLine;
