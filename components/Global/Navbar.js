import React, { useEffect } from "react";
import { Container } from "react-bootstrap";
import Link from "next/link";
import SearchPanel from "/components/Search/SearchPanel";
import {
  BsHandThumbsUp,
  BsJournalCheck,
  BsArrow90DegRight,
  BsCameraVideo,
  BsChatLeftDots,
  BsSortAlphaUpAlt,
  BsJournalText,
  BsPeople,
  BsQuestionCircle,
  BsCollectionPlay,
} from "react-icons/bs";
import { Dropdown, Menu } from "semantic-ui-react";
import Logo from "./Logo.js";
import { useState } from "@hookstate/core";
import store from "../../utils/store.js";
import Image from "next/image";
import badge_img from "/public/images/badge.png";

const Navbar = () => {
  const globalStore = useState(store);

  return (
    <>
      <style>
        {`
          .ui.secondary.menu .dropdown.item>.menu, .ui.text.menu .dropdown.item>.menu {
    margin-top: 0em;
}
.ui.secondary.menu .item {
    padding: 0.78571429em 0.01em;
}
.ui.menu .item>i.dropdown.icon {
    margin: 0 0 0 0.2em;
}
`}
      </style>

      <Container>
        <Menu secondary>
          <Menu.Item header>
            <Logo />
          </Menu.Item>

          <div className="ms-auto d-flex">
            <Dropdown item simple text="PG course">
              <Dropdown.Menu>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <i className="dropdown icon" />
                  <span className="text">Submenu</span>
                  <Dropdown.Menu>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown item simple text="UG course">
              <Dropdown.Menu>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <i className="dropdown icon" />
                  <span className="text">Submenu</span>
                  <Dropdown.Menu>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown item simple text="Diploma">
              <Dropdown.Menu>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <i className="dropdown icon" />
                  <span className="text">Submenu</span>
                  <Dropdown.Menu>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown item simple text="Study Abroad">
              <Dropdown.Menu>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <i className="dropdown icon" />
                  <span className="text">Submenu</span>
                  <Dropdown.Menu>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                    <Dropdown.Item>
                      <Link href="/">
                        <a className="text-dark text-decoration-none">
                          Link Item
                        </a>
                      </Link>
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown item simple text="More">
              <Dropdown.Menu>
                <Dropdown.Item>
                  <Link href="/ourtrust">
                    <a className="text-dark text-decoration-none">
                      <BsHandThumbsUp /> CollegeVidya Commitment
                    </a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/checklist">
                    <a className="text-dark text-decoration-none">
                      <BsJournalCheck /> Important Facts
                    </a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="https://verify.collegevidya.com/">
                    <a className="text-dark text-decoration-none">
                      <BsArrow90DegRight /> Verify Your University
                    </a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">
                      <BsCameraVideo /> Video Counselling{" "}
                    </a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/whycollegevidya">
                    <a className="text-dark text-decoration-none">
                      <BsQuestionCircle /> Why CollegeVidya
                    </a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/media">
                    <a className="text-dark text-decoration-none">
                      <BsCollectionPlay /> Media
                    </a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/contactus">
                    <a className="text-dark text-decoration-none">
                      <BsChatLeftDots /> Contact Us
                    </a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/aboutus">
                    <a className="text-dark text-decoration-none">
                      <BsSortAlphaUpAlt /> About Us{" "}
                    </a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/ourpolicy">
                    <a className="text-dark text-decoration-none">
                      <BsJournalText /> Our Policy{" "}
                    </a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">
                      <BsPeople /> Meet the Team{" "}
                    </a>
                  </Link>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown.Item>
              <Link href="/suggest">
                <a className="text-white py-3 px-3 rounded bgprimary text-decoration-none shadow-1 d-flex align-items-center position-relative">
                  <Image src={badge_img} width={20} height={20} alt="" />{" "}
                  <span className="ms-1">Suggest me an university</span>{" "}
                  <span className="bgsecondary fs-11 px-2 py-1 rounded position-absolute top-0 start-0 translate-middle">
                    New
                  </span>{" "}
                </a>
              </Link>
            </Dropdown.Item>
            <Dropdown item simple text="Top Universities">
              <Dropdown.Menu>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                  <Link href="/">
                    <a className="text-dark text-decoration-none">Link Item</a>
                  </Link>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown.Item>
              <SearchPanel />{" "}
            </Dropdown.Item>
          </div>
        </Menu>
      </Container>
    </>
  );
};

export default Navbar;
