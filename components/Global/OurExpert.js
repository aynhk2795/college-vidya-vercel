import Link from "next/link";
import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Image from "next/image";
import expert1 from "/public/images/experts/expert-team1.png";
import expert2 from "/public/images/experts/expert-team2.jpeg";
import expert3 from "/public/images/experts/expert-team3.jpeg";
import expert4 from "/public/images/experts/expert-team4.jpeg";
import expert5 from "/public/images/experts/expert-team5.jpeg";
import expert6 from "/public/images/experts/expert-team6.jpeg";
import { BsChevronRight } from "react-icons/bs";
import MainHeading from "./Heading/MainHeading";
import { generate } from "shortid";

const expertlist = [
  {
    image: expert1,
  },
  {
    image: expert2,
  },
  {
    image: expert3,
  },
  {
    image: expert4,
  },
  {
    image: expert6,
  },
  {
    image: expert5,
  },
];

const OurExpert = () => {
  return (
    <>
      <Container>
        <Row className="my-5">
          <Col md={7}>
            <MainHeading title="Talk to our Experts" />
            <Link href="tel:1800-420-5757" passHref>
              <a className="textsecondary text-decoration-none h3 d-block mb-3">
                1800-420-5757
              </a>
            </Link>
            <p>Got a question? call us!</p>
            <p>
              We are here for you round the clock with our assistance for your
              driving career!
            </p>

            <p className="button-black mb-4">
              Book Your 30 Mins Expert Career Advice <BsChevronRight />
            </p>
          </Col>
          <Col md={5}>
            <Row>
              {expertlist.map((list) => (
                <Col key={generate()} md={4} xs={4}>
                  <Image src={list.image} width={170} height={170} alt="" />
                </Col>
              ))}
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default OurExpert;
