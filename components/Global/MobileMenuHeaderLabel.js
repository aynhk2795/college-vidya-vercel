import React from "react";
import { Badge } from "react-bootstrap";

const MobileMenuHeaderLabel = (props) => {
  return (
    <>
      <div className="d-flex align-items-center">
        {props.icon ? (
          <>
            <span className="me-2">{props.icon}</span>{" "}
          </>
        ) : null}{" "}
        <span className="me-1">{props.title}</span>{" "}
        {props.tag ? (
          <>
            <span className="badge bgsecondary shadow-sm">{props.tag}</span>
          </>
        ) : null}{" "}
        {props.imageicon ? (
          <>
            <span>{props.imageicon}</span>
          </>
        ) : null}
      </div>
    </>
  );
};

export default MobileMenuHeaderLabel;
