import React from "react";
import Universitypage from "../../pages/universitypage";

const UniversityPage = ({ menuData }) => {
  return (
    <>
      <Universitypage menuData={menuData} />
    </>
  );
};

export default UniversityPage;
