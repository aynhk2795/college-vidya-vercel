import React from "react";
import Link from "next/link";
import { BsArrowRight } from "react-icons/bs";
import styles from "../../components/UniversityPage/button.module.scss";

const Button = (props) => {
  return (
    <div>
      <Link href={props.link}>
        <a className={`${styles.animbtn} d-inline-block text-white`}>
          <span>
            {props.text} <BsArrowRight className={`${styles.rightIcon}`} />
          </span>
        </a>
      </Link>
    </div>
  );
};

export default Button;
