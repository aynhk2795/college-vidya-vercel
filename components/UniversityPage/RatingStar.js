import React from "react";
import { Rating } from "primereact/rating";

const RatingStar = (props) => {
  return (
    <>
      <Rating value={props.value} readOnly stars={5} cancel={false} />
    </>
  );
};

export default RatingStar;
