import React from "react";
import { Accordion, AccordionTab } from "primereact/accordion";
import Heading from "./Heading/Heading";
import { generate } from "shortid";

const universitypagefaq = [
  {
    heading: "How is College Vidya different from others ?",
    description:
      "We at Collegevidya aspire to be your one-stop-solution for anything related to Higher Education Needs. We really believe that Selecting Right University matters for your better career.This means.We at Collegevidya aspire to be your one-stop-solution for anything related to Higher Education Needs. We really believe that Selecting Right University matters for your better career.This means.",
  },
  {
    heading:
      "Do payments made by students go directly into the university account?",
    description:
      "We at Collegevidya aspire to be your one-stop-solution for anything related to Higher Education Needs. We really believe that Selecting Right University matters for your better career.This means.We at Collegevidya aspire to be your one-stop-solution for anything related to Higher Education Needs. We really believe that Selecting Right University matters for your better career.This means.",
  },
  {
    heading: "What are College Vidya Advantages?",
    description:
      "We at Collegevidya aspire to be your one-stop-solution for anything related to Higher Education Needs. We really believe that Selecting Right University matters for your better career.This means.We at Collegevidya aspire to be your one-stop-solution for anything related to Higher Education Needs. We really believe that Selecting Right University matters for your better career.This means.",
  },
  {
    heading: "Can there be errors on College Vidya?",
    description:
      "We at Collegevidya aspire to be your one-stop-solution for anything related to Higher Education Needs. We really believe that Selecting Right University matters for your better career.This means.We at Collegevidya aspire to be your one-stop-solution for anything related to Higher Education Needs. We really believe that Selecting Right University matters for your better career.This means.",
  },
];

const Faq = () => {
  return (
    <>
      <Heading title="Frequently Asked Questions?" />
      <Accordion activeIndex={0}>
        {universitypagefaq.map((list) => (
          <AccordionTab header={list.heading} key={generate()}>
            <p>{list.description}</p>
          </AccordionTab>
        ))}
      </Accordion>
    </>
  );
};

export default Faq;
