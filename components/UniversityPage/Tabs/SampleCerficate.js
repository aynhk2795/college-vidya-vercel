import React from "react";
import { Row, Col } from "react-bootstrap";
import Certificate from "../../../components/TopUniversity/Certificate";
import Heading from "./Heading/Heading";
import styles from "./SampleCertificate.module.scss";

const SampleCerficate = () => {
  return (
    <>
      <Heading title="Sample Certificate from NMIMS Distance Learning" />
      <Row>
        <Col md={9}>
          <p>
            Complete all the research phases and your dissertation to receive a
            Global Doctor of Business Administration in any specialisation of
            your interest from SSBM Geneva
          </p>
          <ul className={`${styles.list}`}>
            <li className="mb-3">
              <p className="fw-bold h6 m-0">Top Standalone Institution</p>
              <p className="fs-12 text-secondary">By Outlook India</p>
            </li>
            <li className="mb-3">
              <p className="fw-bold h6 m-0">One Year Programs</p>
              <p className="fs-12 text-secondary">By Business World</p>
            </li>
            <li className="mb-3">
              <p className="fw-bold h6 m-0">Top B-Schools</p>
              <p className="fs-12 text-secondary">By Business India</p>
            </li>
            <li className="mb-3">
              <p className="fw-bold h6 m-0">Top Indian B-Schools</p>
              <p className="fs-12 text-secondary">By NIRF</p>
            </li>
          </ul>
        </Col>
        <Col md={3}>
          <Certificate />
        </Col>
      </Row>
    </>
  );
};

export default SampleCerficate;
