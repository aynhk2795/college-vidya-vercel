import React from "react";

const Heading = (props) => {
  return (
    <>
      <h3 className="mb-3 pt-3">{props.title}</h3>
    </>
  );
};

export default Heading;
