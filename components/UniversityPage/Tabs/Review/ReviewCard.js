import React from "react";
import Image from "next/image";
import review from "../../../../public/images/testimonial.jpeg";
import RatingStar from "../../../../components/UniversityPage/RatingStar.js";
import { generate } from "shortid";

const review_detail = [
  {
    name: "saumya",
    heading: "Best University for MBA",
    date: "11 September 2020",
    auth: "verified",
    message:
      "College Vidya is India’s first education portal that provides all the information related to online and distance education. You can find all the nitty-gritty related to distance and online universities and the courses offered by them. College Vidya helps you in comparing all the distance and online universities and their courses in India online via its compare feature.",
  },
  {
    name: "pawan",
    heading: "Best University for MBA",
    date: "11 September 2020",
    auth: "unverified",
    message:
      "College Vidya is India’s first education portal that provides all the information related to online and distance education. You can find all the nitty-gritty related to distance and online universities and the courses offered by them. College Vidya helps you in comparing all the distance and online universities and their courses in India online via its compare feature.",
  },
  {
    name: "rohit",
    heading: "Best University for MBA",
    date: "11 September 2020",
    auth: "verified",
    message:
      "College Vidya is India’s first education portal that provides all the information related to online and distance education. You can find all the nitty-gritty related to distance and online universities and the courses offered by them. College Vidya helps you in comparing all the distance and online universities and their courses in India online via its compare feature.",
  },
];
function ReviewCard() {
  return (
    <>
      {review_detail.map((list) => (
        <div className="review_card mb-5" key={generate()}>
          <div className="d-flex align-items-center mt-4 mb-2">
            <Image
              src={review}
              width={40}
              height={40}
              className="rounded-circle"
              alt=""
            />
            <p className="m-0 ms-2 text-capitalize">{list.name}</p>
          </div>

          <RatingStar value={3} />
          <p className="m-0 fw-bold">{list.heading}</p>

          <p className="fs-12 mt-2 mb-1 text-secondary">
            Reviewed in India on {list.date}
          </p>
          <p className="text-primary fw-bold fs-14 mb-1 text-capitalize">
            {list.auth}
          </p>
          <p>{list.message}</p>
        </div>
      ))}
    </>
  );
}

export default ReviewCard;
