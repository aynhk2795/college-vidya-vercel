import React from "react";
import Heading from "./Heading/Heading";
import { Row, Col } from "react-bootstrap";
import styles from "/components/UniversityPage/Tabs/About.module.scss";
import { BsChevronDoubleRight } from "react-icons/bs";

const About = () => {
  return (
    <>
      <Heading title="About University" />
      <p>
        NMIMS Global Access School for Continuing Education (NMIMS Distance
        Learning) is one of the largest distance education universities
        established in 1981 and awarded by the DEEMED-TO-BE university status in
        2003. NMIMS Distance University for Working Professionals is approved by
        the University Grant Admission and AICTE and provides UGC-DEB approved
        courses to the students.
      </p>
      <p>
        NMIMS Global Access School for Continuing Education (NMIMS Distance
        Learning) is one of the largest distance education universities
        established in 1981 and awarded by the DEEMED-TO-BE university status in
        2003. NMIMS Distance University for Working Professionals is approved by
        the University Grant Admission and AICTE and provides UGC-DEB approved
        courses to the students.
      </p>

      <p>
        NMIMS Global Access School for Continuing Education (NMIMS Distance
        Learning) is one of the largest distance education universities
        established in 1981 and awarded by the DEEMED-TO-BE university status in
        2003. NMIMS Distance University for Working Professionals is approved by
        the University Grant Admission and AICTE and provides UGC-DEB approved
        courses to the students.
      </p>
      <p>
        NMIMS Global Access School for Continuing Education (NMIMS Distance
        Learning) is one of the largest distance education universities
        established in 1981 and awarded by the DEEMED-TO-BE university status in
        2003. NMIMS Distance University for Working Professionals is approved by
        the University Grant Admission and AICTE and provides UGC-DEB approved
        courses to the students.
      </p>

      <p>
        NMIMS Global Access School for Continuing Education (NMIMS Distance
        Learning) is one of the largest distance education universities
        established in 1981 and awarded by the DEEMED-TO-BE university status in
        2003. NMIMS Distance University for Working Professionals is approved by
        the University Grant Admission and AICTE and provides UGC-DEB approved
        courses to the students.
      </p>

      <Row className={`${styles.about_cards_wrap}`}>
        <Col md={4} className={`${styles.about_cards}`}>
          <div>
            <p className="fw-bold">
              Comprehensive Curriculum <BsChevronDoubleRight />
            </p>
            <p>Choose one among 4 future-ready MBA Degree</p>
          </div>
        </Col>
        <Col md={4} className={`${styles.about_cards}`}>
          <div>
            <p className="fw-bold">
              In-person Residencies & Local Meetups <BsChevronDoubleRight />
            </p>
            <p>
              Learn from top faculty with live online interactive and recorded
              sessions
            </p>
          </div>
        </Col>
        <Col md={4} className={`${styles.about_cards}`}>
          <div>
            <p className="fw-bold">
              Dedicated Career Assistance <BsChevronDoubleRight />
            </p>
            <p>Job opportunities shared by 12000+ companies & Industry</p>
          </div>
        </Col>
      </Row>
    </>
  );
};

export default About;
