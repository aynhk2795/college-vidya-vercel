import React from "react";
import Image from "next/image";
import placement1 from "../../../public/images/placement/placement1.png";
import placement2 from "../../../public/images/placement/placement2.webp";
import placement3 from "../../../public/images/placement/placement3.png";
import placement4 from "../../../public/images/placement/placement4.webp";
import placement5 from "../../../public/images/placement/placement5.png";
import placement6 from "../../../public/images/placement/placement6.png";
import Heading from "./Heading/Heading";

const Placement = () => {
  return (
    <>
      <Heading title="Placement Partners" />
      <p>&quot;Your career needs to grow with you&quot;</p>
      <p
        style={{ backgroundColor: "aliceblue" }}
        className="d-inline-block px-4 py-2 rounded">
        Our students work at
      </p>
      <ul className="list-unstyled d-flex">
        <li
          className="me-3 px-4 py-3 rounded"
          style={{ backgroundColor: "aliceblue" }}>
          <p className="m-0 textprimary h2">50%</p>
          <p>Average Salary Hike</p>
        </li>
        <li
          className="me-3 px-4 py-3 rounded"
          style={{ backgroundColor: "aliceblue" }}>
          <p className="m-0 textprimary h2">300+</p>
          <p>Hiring Partners</p>
        </li>
        <li
          className="me-3 px-4 py-3 rounded"
          style={{ backgroundColor: "aliceblue" }}>
          <p className="m-0 textprimary h2">3X</p>
          <p>Increase in Interview Opportunities</p>
        </li>
      </ul>
      <ul className="list-unstyled d-flex mt-4 align-items-center">
        <li className="me-4">
          <Image src={placement1} width={80} height={65} alt="placement1" />
        </li>
        <li className="me-4">
          <Image src={placement2} width={80} height={25} alt="placement2" />
        </li>
        <li className="me-4">
          <Image src={placement3} width={90} height={40} alt="placement3" />
        </li>
        <li className="me-4">
          <Image src={placement4} width={60} height={23} alt="placement4" />
        </li>
        <li className="me-4">
          <Image src={placement5} width={65} height={35} alt="placement5" />
        </li>
        <li className="me-4">
          <Image src={placement6} width={30} height={30} alt="placement6" />
        </li>
      </ul>
    </>
  );
};

export default Placement;
