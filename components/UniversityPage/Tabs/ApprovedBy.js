import React from "react";
import Image from "next/image";
import appoval1 from "../../../public/images/approvals/approval_1.png";
import appoval2 from "../../../public/images/approvals/approval_2.png";
import appoval3 from "../../../public/images/approvals/approval_3.png";
import appoval4 from "../../../public/images/approvals/approval_4.png";
import appoval5 from "../../../public/images/approvals/approval_5.png";
import Heading from "./Heading/Heading";

const ApprovedBy = () => {
  return (
    <>
      <Heading title="University Approved By" />
      <ul className="list-unstyled d-flex mt-4">
        <li className="me-4">
          <Image src={appoval1} width={90} height={85} alt="approval" />
        </li>
        <li className="me-4">
          <Image src={appoval2} width={90} height={85} alt="approval" />
        </li>
        <li className="me-4">
          <Image src={appoval3} width={90} height={85} alt="approval" />
        </li>
        <li className="me-4">
          <Image src={appoval4} width={90} height={85} alt="approval" />
        </li>
        <li className="me-4">
          <Image src={appoval5} width={90} height={85} alt="approval" />
        </li>
      </ul>
    </>
  );
};

export default ApprovedBy;
