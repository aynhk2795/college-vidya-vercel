import React from "react";
import Modal from "react-bootstrap/Modal";
import { BsChevronRight } from "react-icons/bs";
import ReviewBar from "../Tabs/Review/ReviewBar";
import ReviewCard from "../Tabs/Review/ReviewCard";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          See All Students Review
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ReviewBar />
        <ReviewCard />
      </Modal.Body>
    </Modal>
  );
}

function AllReview() {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
      <span
        onClick={() => setModalShow(true)}
        className="textsecondary cursor-pointer d-block my-4">
        See all reviews <BsChevronRight />
      </span>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
}

export default AllReview;
