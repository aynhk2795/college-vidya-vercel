import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import { BsPencil } from "react-icons/bs";
import { Rating } from "primereact/rating";
import { InputText } from "primereact/inputtext";
import { InputTextarea } from "primereact/inputtextarea";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import { InputSwitch } from "primereact/inputswitch";

function MyVerticallyCenteredModal(props) {
  const [val1, setVal1] = useState(null);
  const [value1, setValue1] = useState("");
  const [checked1, setChecked1] = useState(false);
  return (
    <Modal
      {...props}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Create Review
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form>
          {/* <Image className="rounded" src={university_image} width={140} height={70} />
        <p className="fw-bold mt-2 h5">NMIMS Distance Learning Education</p> */}
          <p className="fw-bold">Overall Rating</p>
          <Rating
            cancel={false}
            value={val1}
            onChange={(e) => setVal1(e.value)}
          />
          <p className="fw-bold mb-2 mt-3">Add a heading</p>
          <InputText
            value={value1}
            onChange={(e) => setValue1(e.target.value)}
            className="w-100 p-inputtext-sm"
          />
          <p className="fw-bold mb-2 mt-3">Show as a anonymous user</p>
          <InputSwitch
            checked={checked1}
            onChange={(e) => setChecked1(e.value)}
          />

          <p className="fw-bold mb-2 mt-3">Add a written review</p>

          <InputTextarea
            className="w-100"
            value={value1}
            onChange={(e) => setValue1(e.target.value)}
            rows={5}
            cols={30}
          />

          <Alert variant="success" style={{ fontSize: "14px" }}>
            College Vidya is India’s first education portal that provides all
            the information related to online and distance education.
          </Alert>

          <div className="text-end">
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </div>
        </form>
      </Modal.Body>
    </Modal>
  );
}

function Review() {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
      {/* <BsSearch variant="primary" onClick={() => setModalShow(true)} /> */}
      <span
        onClick={() => setModalShow(true)}
        className="textsecondary cursor-pointer d-block my-4">
        <BsPencil fontSize={13} /> Write a review
      </span>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
}

export default Review;
