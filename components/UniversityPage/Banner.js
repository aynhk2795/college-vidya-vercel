import React from "react";
import Image from "next/image";
import { Container } from "react-bootstrap";
import Carousel from "react-bootstrap/Carousel";
import logo from "../../public/images/NMIMS.jpeg";
import Button from "/components/UniversityPage/Button";
import AnimatedYoutube from "../../components/Global/AnimatedYoutube";
import RatingStar from "/components/UniversityPage/RatingStar";
import Link from "next/link";
import styles from "/components/UniversityPage/banner.module.scss";
import banner1 from "/public/images/chandigarhffff.webp";
import banner2 from "/public/images/jamia-test-img.jpg";
import banner3 from "/public/images/chandigarhffff.webp";
// import banner1 from "/public/images/full-wide.png";
import { AiOutlineCloudDownload } from "react-icons/ai";

const Banner = (props) => {
  return (
    <>
      <div className="position-relative">
        <Container fluid className="px-0">
          <Carousel>
            <Carousel.Item>
              <Image
                className="w-100"
                src={banner1}
                alt="First slide"
                quality={100}
                height={384}
              />
            </Carousel.Item>
            <Carousel.Item>
              <Image
                className="w-100"
                src={banner2}
                alt="First slide"
                height={384}
              />
            </Carousel.Item>
            <Carousel.Item>
              <Image
                className="w-100"
                src={banner3}
                alt="First slide"
                quality={100}
                height={384}
              />
            </Carousel.Item>
          </Carousel>
        </Container>
        <Container className="">
          <div
            className="text-white px-4 py-4 rounded shadow-1 position-absolute top-50 translate-middle-y"
            style={{ backgroundColor: "rgba(0,0,0,0.5)" }}>
            <Image
              src={logo}
              className="rounded"
              width={150}
              height={50}
              alt="banner"
            />
            <h2 className="text-white mt-3">{props.title}</h2>
            <p className="text-white my-3">{props.tagline}</p>
            <p
              className="d-inline-block px-3 py-2 rounded"
              style={{ backgroundColor: "aliceblue" }}>
              <Link href="/">
                <a>
                  Download Prospectus <AiOutlineCloudDownload fontSize={20} />
                </a>
              </Link>{" "}
            </p>
            <div className="d-inline-block mb-3 d-flex ">
              <span className="text-white me-1">Student Rating : </span>{" "}
              <RatingStar value={4} />
            </div>

            <div
              className={`${styles.banner_button_wrapper} d-flex align-items-center`}>
              <div className={`${styles.proceed_btn_wrap} me-3`}>
                <Button link="/" text="Proceed to University" />
              </div>
              <div className={`${styles.youtube_btn_wrap}`}>
                <Link href="/">
                  <a className="d-flex align-items-center">
                    <AnimatedYoutube />
                    <span className="text-white ps-2">Watch Video</span>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </Container>
      </div>
    </>
  );
};

export default Banner;
