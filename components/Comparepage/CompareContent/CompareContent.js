import React, { useEffect } from "react";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import CompareShortHead from "/components/Comparepage/CompareHeader/CompareShortHead";
import { Container } from "react-bootstrap";
import AddToCompareBox from "../../universityListingPage/Modal/AddToCompareBox";
import {
  getUniversityListingByCourse,
  getUniversityListingBySpecialization,
} from "../../../service/UniversityListingService";
import { useState, Downgraded } from "@hookstate/core";
import store from "../../../utils/store";
import { Image } from "primereact/image";
import styles from "./CompareContent.module.scss";
import { AiFillStar } from "react-icons/ai";
import UniversityDetail from "../../universityListingPage/Modal/UniversityDetail";
import { FiChevronLeft } from "react-icons/fi";
import { useRouter } from "next/router";
import { useAuth } from "../../../hooks/auth";

const CompareContent = ({
  screenSize,
  compareTitles,
  courseID,
  courseSlug,
  specializationID,
  specializationSlug,
  userID,
}) => {
  const router = useRouter();
  const { user } = useAuth();
  const [allUnivs, setAllUnivs] = React.useState([]);
  const [compareData, setCompareData] = React.useState([]);

  const { universitiesToCompare } = useState(store);

  useEffect(() => {
    if (specializationID) {
      getUniversityListingBySpecialization(
        userID,
        courseID,
        specializationID
      ).then((response) => {
        setAllUnivs(response.data.data);
      });
    } else {
      getUniversityListingByCourse(userID, courseID).then((response) => {
        setAllUnivs(response.data.data);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (universitiesToCompare.attach(Downgraded).get().length > 0) {
      prepareData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [universitiesToCompare.attach(Downgraded).get()]);

  const prepareData = () => {
    const fixedParams = [
      { title: "Fee", value: "fee", styling: "fee", data: "about" },
      {
        title: "Sample Certificate",
        value: "sample_certificate",
        styling: "sample_certificate",
      },
      {
        title: "Approvals",
        value: "university_approvals",
        styling: "approvals",
      },
      { title: "Student Rating", value: "avg_rating", styling: "cv_rating" },
    ];
    let data = [];

    fixedParams.map((param) => {
      let dataPoint = {};
      dataPoint["parameter"] = param.title;
      dataPoint["styling"] = param.styling;

      universitiesToCompare
        .attach(Downgraded)
        .get()
        .map((uni, index) => {
          dataPoint[`about-${index.toString()}`] = uni.about_university;
          dataPoint[`logo-${index.toString()}`] = uni.logo;
          dataPoint[`name-${index.toString()}`] = uni.name;
          dataPoint[`prospectus-${index.toString()}`] = uni.prospectus_link;
          dataPoint[`facts-${index.toString()}`] = uni.university_facts;
          dataPoint[`university_approvals-${index.toString()}`] =
            uni.university_approvals;
          dataPoint[`sample_certificate-${index.toString()}`] =
            uni.sample_certificate;
          dataPoint[`books_lms-${index.toString()}`] = uni.books_lms;
          dataPoint[`reviews-${index.toString()}`] = uni.university_reviews;
          dataPoint[`faqs-${index.toString()}`] = uni.university_faqs;
          dataPoint[`fee-details-${index.toString()}`] = uni.fee_details;
          dataPoint[`loan-facility-${index.toString()}`] = uni.loan_facility;

          const uniValue =
            uni[param.value] === 0
              ? "0"
              : uni[param.value]
              ? uni[param.value]
              : "NA";
          dataPoint[index.toString()] = uniValue;
        });

      data.push(dataPoint);
    });

    compareTitles.map((cTitle) => {
      let DataPoint = {};

      const title = cTitle.title;
      DataPoint["parameter"] = title;

      if (title.toLowerCase().includes("enrolled")) {
        DataPoint["styling"] = "student_enrolled";
      }

      universitiesToCompare
        .attach(Downgraded)
        .get()
        .map((uni, index) => {
          const uniValue =
            uni.compare && uni.compare[0]
              ? uni.compare[0].compare_details.some(
                  (cDetail) => cDetail.comparetitle.title === title
                )
                ? uni.compare[0].compare_details.filter(
                    (cDetail) => cDetail.comparetitle.title === title
                  )[0].description
                : "NA"
              : "NA";
          DataPoint[index.toString()] = uniValue;
        });

      data.push(DataPoint);
    });

    setCompareData(data);
  };

  const styleTemplateUno = (rowData) => {
    if (!rowData.styling) {
      return <div className="fs-14">{rowData["0"]}</div>
    } else {
      if (rowData.styling === "fee") {
        return (
          <div className={`${styles.fees_wrap} fs-14`}>
            <UniversityDetail
              screenSize={screenSize}
              universityFee={rowData["0"] && rowData["0"].toLocaleString()}
              universityFeeDetails={rowData["fee-details-0"]}
              aboutUniversity={rowData["about-0"]}
              universityLogo={rowData["logo-0"]}
              universityName={rowData["name-0"]}
              prospectusLink={rowData["prospectus-0"]}
              universityFacts={rowData["facts-0"]}
              universityApprovals={rowData["university_approvals-0"]}
              sampleCertificateLink={rowData["sample_certificate-0"]}
              LMSVideos={rowData["books_lms-0"]}
              universityReviews={rowData["reviews-0"]}
              universityFAQs={rowData["faqs-0"]}
              loanFacility={rowData["loan-facility-0"]}
            />
          </div>
        );
      }
      if (rowData.styling === "sample_certificate") {
        return (
          <Image
            alt=""
            src={rowData["0"]}
            width={100}
            className={`${styles.compare_certificate}`}
            height={150}
            preview
          />
        );
      }
      if (rowData.styling === "approvals") {
        return (
          <div className="fs-14"
            >
            {rowData["0"] &&
              rowData["0"].map((approval) => approval.title).join(" | ")}
          </div>
        );
      }
      if (rowData.styling === "cv_rating") {
        return (
          <div className="fs-14">
            <AiFillStar className="textsecondary" />{" "}
            {rowData["0"] && rowData["0"].toLocaleString()} / 5
          </div>
        );
      }
      if (rowData.styling === "student_enrolled") {
        return (
          <div className={`${styles.enrolled_wrap} fs-14`}>
            {rowData["0"] && rowData["0"].toLocaleString()}
          </div>
        );
      }
    }
  };

  const styleTemplateDos = (rowData) => {
    if (!rowData.styling) {
      return <div className="fs-14">{rowData["1"]}</div>
    } else {
      if (rowData.styling === "fee") {
        return (
          <div className={`${styles.fees_wrap} fs-14`}>
            <UniversityDetail
              screenSize={screenSize}
              universityFee={rowData["1"] && rowData["1"].toLocaleString()}
              universityFeeDetails={rowData["fee-details-1"]}
              aboutUniversity={rowData["about-1"]}
              universityLogo={rowData["logo-1"]}
              universityName={rowData["name-1"]}
              prospectusLink={rowData["prospectus-1"]}
              universityFacts={rowData["facts-1"]}
              universityApprovals={rowData["university_approvals-1"]}
              sampleCertificateLink={rowData["sample_certificate-1"]}
              LMSVideos={rowData["books_lms-1"]}
              universityReviews={rowData["reviews-1"]}
              universityFAQs={rowData["faqs-1"]}
              loanFacility={rowData["loan-facility-1"]}
            />
          </div>
        );
      }
      if (rowData.styling === "sample_certificate") {
        return (
          <Image
            alt=""
            src={rowData["1"]}
            className={`${styles.compare_certificate}`}
            width={100}
            height={150}
            preview
          />
        );
      }
      if (rowData.styling === "approvals") {
        return (
          <div
          className="fs-14"
            >
            {rowData["1"] &&
              rowData["1"].map((approval) => approval.title).join(" | ")}
          </div>
        );
      }
      if (rowData.styling === "cv_rating") {
        return (
          <div className="fs-14">
            <AiFillStar className="textsecondary" />{" "}
            {rowData["1"] && rowData["1"].toLocaleString()} / 5
          </div>
        );
      }
      if (rowData.styling === "student_enrolled") {
        return (
          <div className={`${styles.enrolled_wrap} fs-14`}>
            {rowData["1"] && rowData["1"].toLocaleString()}
          </div>
        );
      }
    }
  };

  const styleTemplateTres = (rowData) => {
    if (!rowData.styling) {
      return <div className="fs-14">{rowData["2"]}</div>
    } else {
      if (rowData.styling === "fee") {
        return (
          <div className={`${styles.fees_wrap} fs-14`}>
            <UniversityDetail
              screenSize={screenSize}
              universityFee={rowData["2"] && rowData["2"].toLocaleString()}
              universityFeeDetails={rowData["fee-details-2"]}
              aboutUniversity={rowData["about-2"]}
              universityLogo={rowData["logo-2"]}
              universityName={rowData["name-2"]}
              prospectusLink={rowData["prospectus-2"]}
              universityFacts={rowData["facts-2"]}
              universityApprovals={rowData["university_approvals-2"]}
              sampleCertificateLink={rowData["sample_certificate-2"]}
              LMSVideos={rowData["books_lms-2"]}
              universityReviews={rowData["reviews-2"]}
              universityFAQs={rowData["faqs-2"]}
              loanFacility={rowData["loan-facility-2"]}
            />
          </div>
        );
      }
      if (rowData.styling === "sample_certificate") {
        return (
          <Image
            alt=""
            src={rowData["2"]}
            className={`${styles.compare_certificate}`}
            width={100}
            height={150}
            preview
          />
        );
      }
      if (rowData.styling === "approvals") {
        return (
          <div className="fs-14"
            >
            {rowData["2"] &&
              rowData["2"].map((approval) => approval.title).join(" | ")}
          </div>
        );
      }
      if (rowData.styling === "cv_rating") {
        return (
          <div className="fs-14">
            <AiFillStar className="textsecondary" />{" "}
            {rowData["2"] && rowData["2"].toLocaleString()} / 5
          </div>
        );
      }
      if (rowData.styling === "student_enrolled") {
        return (
          <div className={`${styles.enrolled_wrap} fs-14`}>
            {rowData["2"] && rowData["2"].toLocaleString()}
          </div>
        );
      }
    }
  };

  const backHeaderTemplate = () => {
    return (
      <a
        className="text-dark flex items-center gap-2 fs-14"
        href={`/colleges-universities/${courseSlug}/${
          specializationSlug ? specializationSlug : ""
        }?uid=${user ? user : router.query.uid}`}>
        <FiChevronLeft /> Back
      </a>
    );
  };

  const newTemplate = (rowData) => {
    return (
      <div className="fs-14">
      {rowData.parameter}
      </div>
    )
  }

  return (
    <>
      <div>
        <Container className="mb-5">
          <div className="card">
            <DataTable
              className={`${styles.compare_table}`}
              value={compareData}
              stripedRows
              responsiveLayout="scroll"
              scrollable
              scrollHeight="auto">
              <Column
                field="parameter"
                headerClassName="text-primary"
                header={backHeaderTemplate}
                className="text-dark"
                body={newTemplate}
              />
              {universitiesToCompare.attach(Downgraded).get()[0] ? (
                <Column
                  field="0"
                  className="text-center justify-content-center text-dark"
                  header={
                    <CompareShortHead
                      id={universitiesToCompare.attach(Downgraded).get()[0].id}
                      name={
                        universitiesToCompare.attach(Downgraded).get()[0].name
                      }
                      prospectus_link={
                        universitiesToCompare.attach(Downgraded).get()[0]
                          .prospectus_link
                      }
                      logo={
                        universitiesToCompare.attach(Downgraded).get()[0].logo
                      }
                      about_university={
                        universitiesToCompare.attach(Downgraded).get()[0]
                          .about_university
                      }
                      university_facts={
                        universitiesToCompare.attach(Downgraded).get()[0]
                          .university_facts
                      }
                      university_approvals={
                        universitiesToCompare.attach(Downgraded).get()[0]
                          .university_approvals
                      }
                      books_lms={
                        universitiesToCompare.attach(Downgraded).get()[0]
                          .books_lms
                      }
                      university_reviews={
                        universitiesToCompare.attach(Downgraded).get()[0]
                          .university_reviews
                      }
                      university_faqs={
                        universitiesToCompare.attach(Downgraded).get()[0]
                          .university_faqs
                      }
                      fromComparePage={true}
                      courseSlug={courseSlug}
                      specializationSlug={specializationSlug}
                      userID={userID}
                    />
                  }
                  body={styleTemplateUno}
                />
              ) : (
                <Column
                  header={
                    <AddToCompareBox
                      allUniversities={allUnivs}
                      fromComparePage={true}
                      courseSlug={courseSlug}
                      specializationSlug={specializationSlug}
                      userID={userID}
                    />
                  }
                />
              )}
              {universitiesToCompare.attach(Downgraded).get()[1] ? (
                <Column
                  field="1"
                  className="text-center justify-content-center text-dark"
                  header={
                    <CompareShortHead
                      id={universitiesToCompare.attach(Downgraded).get()[1].id}
                      name={
                        universitiesToCompare.attach(Downgraded).get()[1].name
                      }
                      prospectus_link={
                        universitiesToCompare.attach(Downgraded).get()[1]
                          .prospectus_link
                      }
                      logo={
                        universitiesToCompare.attach(Downgraded).get()[1].logo
                      }
                      about_university={
                        universitiesToCompare.attach(Downgraded).get()[1]
                          .about_university
                      }
                      university_facts={
                        universitiesToCompare.attach(Downgraded).get()[1]
                          .university_facts
                      }
                      university_approvals={
                        universitiesToCompare.attach(Downgraded).get()[1]
                          .university_approvals
                      }
                      books_lms={
                        universitiesToCompare.attach(Downgraded).get()[1]
                          .books_lms
                      }
                      university_reviews={
                        universitiesToCompare.attach(Downgraded).get()[1]
                          .university_reviews
                      }
                      university_faqs={
                        universitiesToCompare.attach(Downgraded).get()[1]
                          .university_faqs
                      }
                      fromComparePage={true}
                      courseSlug={courseSlug}
                      specializationSlug={specializationSlug}
                      userID={userID}
                    />
                  }
                  body={styleTemplateDos}
                />
              ) : (
                <Column
                  header={
                    <AddToCompareBox
                      allUniversities={allUnivs}
                      fromComparePage={true}
                      courseSlug={courseSlug}
                      specializationSlug={specializationSlug}
                      userID={userID}
                    />
                  }
                />
              )}
              {screenSize && screenSize.width > 991 ? (
                universitiesToCompare.attach(Downgraded).get()[2] ? (
                  <Column
                    field="2"
                    className="text-center justify-content-center text-dark"
                    header={
                      <CompareShortHead
                        id={
                          universitiesToCompare.attach(Downgraded).get()[2].id
                        }
                        name={
                          universitiesToCompare.attach(Downgraded).get()[2].name
                        }
                        prospectus_link={
                          universitiesToCompare.attach(Downgraded).get()[2]
                            .prospectus_link
                        }
                        logo={
                          universitiesToCompare.attach(Downgraded).get()[2].logo
                        }
                        about_university={
                          universitiesToCompare.attach(Downgraded).get()[2]
                            .about_university
                        }
                        university_facts={
                          universitiesToCompare.attach(Downgraded).get()[2]
                            .university_facts
                        }
                        university_approvals={
                          universitiesToCompare.attach(Downgraded).get()[2]
                            .university_approvals
                        }
                        books_lms={
                          universitiesToCompare.attach(Downgraded).get()[2]
                            .books_lms
                        }
                        university_reviews={
                          universitiesToCompare.attach(Downgraded).get()[2]
                            .university_reviews
                        }
                        university_faqs={
                          universitiesToCompare.attach(Downgraded).get()[2]
                            .university_faqs
                        }
                        fromComparePage={true}
                        courseSlug={courseSlug}
                        specializationSlug={specializationSlug}
                        userID={userID}
                      />
                    }
                    body={styleTemplateTres}
                  />
                ) : (
                  <Column
                    header={
                      <AddToCompareBox
                        allUniversities={allUnivs}
                        fromComparePage={true}
                        courseSlug={courseSlug}
                        specializationSlug={specializationSlug}
                        userID={userID}
                      />
                    }
                  />
                )
              ) : null}
            </DataTable>
          </div>
        </Container>
      </div>
    </>
  );
};

export default CompareContent;
