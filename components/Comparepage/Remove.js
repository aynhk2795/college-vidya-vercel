import React from "react";
import { AiFillCloseCircle } from "react-icons/ai";

const Remove = () => {
  return (
    <>
      <AiFillCloseCircle className="h5 position-absolute top-0 start-100 translate-middle" />
    </>
  );
};

export default Remove;
