import Image from "next/image";
import React from "react";
import { Card } from "react-bootstrap";
import logo from "/public/images/university/01nmims-University.png";
import styles from "/styles/CompareShortHead.module.scss";
import KnowInTwoMIns from "/components/universityListingPage/Modal/KnowInTwoMIns";
import Remove from "/components/Comparepage/Remove";
import { Downgraded, useState } from "@hookstate/core";
import store from "../../../utils/store";
import { useEffect } from "react";
import { useRouter } from "next/router";

const CompareShortHead = (props) => {
  const router = useRouter();

  const { universitiesToCompare } = useState(store);
  const [showClose] = React.useState(true);

  const removeUniv = () => {
    if (props.fromComparePage) {
      let newURL = "";
      if (props.specializationSlug) {
        newURL = `/compare/${props.courseSlug}/${
          props.specializationSlug
        }?${universitiesToCompare
          .attach(Downgraded)
          .get()
          .filter((univ) => univ.id !== props.id)
          .map(function (university) {
            return `check=${university.slug}&`;
          })}`;

        newURL = newURL.replaceAll(",", "");
        newURL = newURL + `uid=${props.userID}`;

        window.location.href = newURL;
      } else {
        newURL = `/compare/${props.courseSlug}/?${universitiesToCompare
          .attach(Downgraded)
          .get()
          .filter((univ) => univ.id !== props.id)
          .map(function (university) {
            return `check=${university.slug}&`;
          })}`;

        newURL = newURL.replaceAll(",", "");
        newURL = newURL + `uid=${props.userID}`;

        window.location.href = newURL;
      }
    } else {
      let allUnivs = [...universitiesToCompare.attach(Downgraded).get()];

      if (allUnivs.length > 1) {
        allUnivs = allUnivs.filter((univ) => univ.id !== props.id);

        universitiesToCompare.set(allUnivs);
      }
    }
  };

  return (
    <>
      <div className="position-relative mb-3">
        <Card className={`${styles.cardhead} p-2 shadow-1`}>
          <div className="">
            <Image src={props.logo} width={130} height={66} alt="" />
          </div>

          <div className={`${styles.cardUniversityName} text-uppercase`}>
            {props.name}
          </div>
        </Card>
        <div
          className={`${styles.twominsbar} position-absolute start-50  translate-middle-x fw-normal text-truncate`}>
          <KnowInTwoMIns
            smallTitle={true}
            aboutUniversity={props.about_university}
            universityFacts={props.university_facts}
            universityApprovals={props.university_approvals}
            LMSVideos={props.books_lms}
            universityReviews={props.university_reviews}
          />
          {/* Know University in 2 mins */}
        </div>
        {showClose ? (
          <div className="remove cursor-pointer" onClick={() => removeUniv()}>
            <Remove />
          </div>
        ) : null}
      </div>
    </>
  );
};

export default CompareShortHead;
