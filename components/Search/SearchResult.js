import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import SearchBar from "/components/Search/SearchBar";
import Logo from "/components/Global/Logo";
import AnimatedBird from "/components/Global/AnimatedBird";
import SearchTab from "/components/Search/SearchTab";

const SearchResult = () => {
  return (
    <>
      <Container fluid className="my-3">
        <Row>
          <Col md={2}>
            <Logo />
          </Col>
          <Col md={6}>
            <div>
              <SearchBar />
            </div>
          </Col>
          <Col md={4} className="d-flex justify-content-end align-items-center">
            <p className="m-0">
              #ChunoWahiJoHaiSahi <AnimatedBird />
            </p>
          </Col>
        </Row>
        <Row>
          <Col md={2}></Col>
          <Col md={7}>
            <SearchTab />
          </Col>
          <Col md={3}></Col>
        </Row>
      </Container>
    </>
  );
};

export default SearchResult;
