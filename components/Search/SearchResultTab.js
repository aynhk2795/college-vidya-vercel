import Link from "next/link";
import React from "react";

const SearchResultTab = () => {
  return (
    <>
      <div className="mb-4">
        <Link href="/">
          <a className="fw-bold">
            Compare 50+ Online College - Compare Online University Now
          </a>
        </Link>
        <p className="m-0">
          Compare and Select Top UGC and DEB Approved Degree From Online and
          Distance Universities. Compare University & Select the Best One. PG
          Course UG Course Diploma & Certificate.
        </p>
      </div>
      <div className="mb-4">
        <Link href="/">
          <a className="fw-bold">Online & Distance MBA</a>
        </Link>
        <p className="m-0">
          Compare and Select Top UGC and DEB Approved Degree From Online and
          Distance Universities. Compare University & Select the Best One. PG
          Course UG Course Diploma & Certificate.
        </p>
      </div>
      <div className="mb-4">
        <Link href="/">
          <a className="fw-bold">
            Compare 50+ Online College - Compare Online University Now
          </a>
        </Link>
        <p className="m-0">
          Compare and Select Top UGC and DEB Approved Degree From Online and
          Distance Universities. Compare University & Select the Best One. PG
          Course UG Course Diploma & Certificate.
        </p>
      </div>
      <div className="mb-4">
        <Link href="/">
          <a className="fw-bold">
            Compare 50+ Online College - Compare Online University Now
          </a>
        </Link>
        <p className="m-0">
          Compare and Select Top UGC and DEB Approved Degree From Online and
          Distance Universities. Compare University & Select the Best One. PG
          Course UG Course Diploma & Certificate.
        </p>
      </div>
      <div className="mb-4">
        <Link href="/">
          <a className="fw-bold">Online & Distance MBA</a>
        </Link>
        <p className="m-0">
          Compare and Select Top UGC and DEB Approved Degree From Online and
          Distance Universities. Compare University & Select the Best One. PG
          Course UG Course Diploma & Certificate.
        </p>
      </div>
    </>
  );
};

export default SearchResultTab;
