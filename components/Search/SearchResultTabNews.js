import React from "react";
import SearchResultTabCard from "/components/Search/SearchResultTabCard";

const SearchResultTabNews = () => {
  return (
    <>
      <SearchResultTabCard />
    </>
  );
};

export default SearchResultTabNews;
