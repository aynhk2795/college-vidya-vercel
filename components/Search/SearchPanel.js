import React, { useEffect, useState } from "react";
import { Sidebar } from "primereact/sidebar";
import { Button } from "primereact/button";
import { Container, Row, Col } from "react-bootstrap";
import AnimatedBird from "/components/Global/AnimatedBird";
import SearchBar from "/components/Search/SearchBar";
import SearchSuggestion from "/components/Search/Searchsuggestion";
import Login from "../Otta/Modal/Login";
import { useAuth } from "../../hooks/auth";
import { useAxiosJWT } from "../../hooks/axios";
import { UserService } from "../../service/UserService";
import LoginProfile from "../Global/LoginProfile";
import SignUp from "../Otta/Modal/SignUp";
import Search from "../Global/Modals/Search";

const SearchPanel = ({ menuData, screenSize }) => {
  const { user, logout } = useAuth();
  const { axiosJWT } = useAxiosJWT();

  const [username, setUsername] = useState(null);
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    if (user) {
      const userService = new UserService(axiosJWT);
      userService
        .getUserDetails()
        .then((response) => {
          setLoggedIn(true);
          setUsername(response.data.name);
        })
        .catch(() => {
          logout();
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return (
    <div>
      <div className="d-flex align-items-center gap-2">
        {loggedIn ? (
          <LoginProfile username={username} />
        ) : (
          <SignUp menuData={menuData} />
        )}
        <Search screenSize={screenSize} />
      </div>
    </div>
  );
};

export default SearchPanel;
