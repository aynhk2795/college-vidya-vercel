import Image from "next/image";
import React from "react";
import { Card } from "react-bootstrap";
import news from "/public/images/Top-University-Lovely-Professional-University.jpeg";

const SearchResultTabCard = () => {
  return (
    <>
      <div className="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div className="col-md-9 p-4 d-flex flex-column position-static">
          <strong className="d-inline-block mb-2 text-primary">
            India Today
          </strong>
          <h5 className="mb-0">
            Hindu College Vidya Vistar scheme to partner with three Northeast
            colleges
          </h5>
          <div className="mb-1 text-muted">Nov 12</div>
          <p className="card-text mb-auto">
            This is a wider card with supporting text below as a natural lead-in
            to additional content.
          </p>
          <a href="#" className="stretched-link">
            Continue reading
          </a>
        </div>
        <div className="col-md-3 d-flex align-items-center justify-content-center px-4 rounded">
          <Image src={news} alt="" />
        </div>
      </div>

      <div className="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div className="col-md-9 p-4 d-flex flex-column position-static">
          <strong className="d-inline-block mb-2 text-primary">
            India Today
          </strong>
          <h5 className="mb-0">
            Hindu College Vidya Vistar scheme to partner with three Northeast
            colleges
          </h5>
          <div className="mb-1 text-muted">Nov 12</div>
          <p className="card-text mb-auto">
            This is a wider card with supporting text below as a natural lead-in
            to additional content.
          </p>
          <a href="#" className="stretched-link">
            Continue reading
          </a>
        </div>
        <div className="col-md-3 d-flex align-items-center justify-content-center px-4 rounded">
          <Image src={news} alt="" />
        </div>
      </div>
    </>
  );
};

export default SearchResultTabCard;
