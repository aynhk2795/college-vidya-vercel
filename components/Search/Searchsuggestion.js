import React from "react";
import { Row, Col, Card } from "react-bootstrap";
import { AiFillYoutube } from "react-icons/ai";
import Link from "next/link";

const SearchSuggestion = () => {
  return (
    <>
      <p className="mt-4 mb-2 fs-12 text-secondary">Trending Searches...</p>
      <Row className="row-cols-3">
        <Col md={2}>
          <Card className="text-center fs-13 px-2 py-3 mb-3">MBA Courses</Card>
        </Col>
        <Col md={2}>
          <Card className="text-center fs-13 px-2 py-3 mb-3">MBA Courses</Card>
        </Col>
        <Col md={2}>
          <Card className="text-center fs-13 px-2 py-3 mb-3">MBA Courses</Card>
        </Col>
        <Col md={2}>
          <Card className="text-center fs-13 px-2 py-3 mb-3">MBA Courses</Card>
        </Col>
        <Col md={2}>
          <Card className="text-center fs-13 px-2 py-3 mb-3">MBA Courses</Card>
        </Col>
        <Col md={2}>
          <Card className="text-center fs-13 px-2 py-3 mb-3">MBA Courses</Card>
        </Col>
        <Col md={2}>
          <Card className="text-center fs-13 px-2 py-3 mb-3">MBA Courses</Card>
        </Col>
        <Col md={2}>
          <Card className="text-center fs-13 px-2 py-3 mb-3">MBA Courses</Card>
        </Col>
      </Row>
      <p className="text-center fs-13 mt-5">
        <Link href="/">
          <a>
            <AiFillYoutube fontSize={25} style={{ color: "red" }} />{" "}
            <span className="text-dark">learning journey on YouTube</span>
          </a>
        </Link>
      </p>
    </>
  );
};

export default SearchSuggestion;
