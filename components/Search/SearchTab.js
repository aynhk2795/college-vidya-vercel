import { TabView, TabPanel } from "primereact/tabview";
import SearchResultTab from "/components/Search/SearchResultTab";
import SearchResultTabImage from "/components/Search/SearchResultTabImage";
import SearchResultTabVideo from "/components/Search/SearchResultTabVideo";
import SearchResultTabNews from "/components/Search/SearchResultTabNews";

const SearchTabList = [
  {
    id: 1,
    icon: "pi pi-search",
    tabname: "All",
    content: <SearchResultTab />,
  },
  {
    id: 2,
    icon: "pi pi-book",
    tabname: "News",
    content: <SearchResultTabNews />,
  },
  {
    id: 3,
    icon: "pi pi-image",
    tabname: "Image",
    content: <SearchResultTabImage />,
  },
  {
    id: 4,
    icon: "pi pi-video",
    tabname: "Videos",
    content: <SearchResultTabVideo />,
  },
];

const SearchTab = () => {
  return (
    <>
      <style>{`
        .pi {
          margin-right: 7px;
        }
      `}</style>
      <TabView>
        {SearchTabList.map((list) => (
          <TabPanel key={list.id} header={list.tabname} leftIcon={list.icon}>
            <p>{list.content}</p>
          </TabPanel>
        ))}
      </TabView>
    </>
  );
};

export default SearchTab;
