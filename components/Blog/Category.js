import React from "react";
import Link from "next/link";
import styles from "./Category.module.scss";
import SidebarHeading from "./SidebarHeading";

const Category = ({ allCategories }) => {
  return (
    <>
      <div className={`${styles.blogsidebar} card mb-4`}>
        <SidebarHeading title="Categories" />
        {allCategories.map((category) => (
          <Link key={category.id} href={`/blog/category/${category.slug}`}>
            <a>
              {category.name} <span>({category.blog_count})</span>
            </a>
          </Link>
        ))}
      </div>
    </>
  );
};

export default Category;
