import React from "react";
import styles from "./SidebarHeading.module.scss";

const SidebarHeading = (props) => {
  return (
    <>
      <h3 className={`${styles.sidebar_heading} fw-bold mb-4 text-capitalize`}>
        {props.title}
      </h3>
    </>
  );
};

export default SidebarHeading;
