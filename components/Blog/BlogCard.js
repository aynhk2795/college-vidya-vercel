import React from "react";
import Image from "next/image";
import { Row, Col } from "react-bootstrap";
import blog_img from "/public/images/blog.jpeg";
import { BsCalendar } from "react-icons/bs";
import SocialShare from "../Global/SocialShare";
import Link from "next/link";
import styles from "./BlogCard.module.scss";

const BlogCard = ({ fromBlog, title, image, slug, content, created_date }) => {
  return (
    <>
      <div className="col mb-3">
        <Link href={`/blog/${slug}`}>
          <a>
            <div className={`${styles.blog_card} card shadow-1 border-0`}>
              {image ? (
                <Image src={image} width={421} height={174} alt="" />
              ) : (
                <Image src={blog_img} alt="" />
              )}

              <div className="card-body">
                <h3
                  className={`fw-bold text-dark`}
                  style={{
                    display: "-webkit-box",
                    WebkitLineClamp: "2",
                    WebkitBoxOrient: "vertical",
                    overflow: "hidden",
                    lineClamp: "2",
                  }}>
                  {title}
                </h3>
                <small className="text-muted mb-2 d-block">
                  <BsCalendar /> {created_date}
                </small>
                {!fromBlog ? (
                  <>
                    <p
                      className="card-text text-secondary fs-14 text-dark"
                      style={{
                        display: "-webkit-box",
                        WebkitLineClamp: "2",
                        WebkitBoxOrient: "vertical",
                        overflow: "hidden",
                        lineClamp: "2",
                      }}>
                      <div dangerouslySetInnerHTML={{ __html: content }} />
                    </p>
                    <Row>
                      <Col md={12} className="d-flex gap-2">
                        {" "}
                        <SocialShare
                          url={`https://collegevidya.com/blog/${slug}`}
                          title={title}
                        />
                      </Col>
                    </Row>
                  </>
                ) : null}
              </div>
            </div>
          </a>
        </Link>
      </div>
    </>
  );
};

export default BlogCard;
