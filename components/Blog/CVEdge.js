import React from "react";
import Link from "next/link";
import styles from "./Category.module.scss";
import SidebarHeading from "./SidebarHeading";
import { BiGitCompare } from "react-icons/bi";
import { FaGraduationCap } from "react-icons/fa";
import student from "/public/images/student.jpeg";
import Image from "next/image";
import { AiOutlineFieldTime, AiOutlineSafetyCertificate } from "react-icons/ai";
import { BsPeople } from "react-icons/bs";

const RecentPost = () => {
  return (
    <>
      <div className={`${styles.blogsidebar} card mb-4`}>
        <SidebarHeading title="College Vidya Edge" />

        <p className="d-flex align-items-start">
          <BiGitCompare className="mt-1 textsecondary me-2" fontSize={20} />{" "}
          Unbiased Counselling
        </p>

        <p className="d-flex align-items-start">
          <FaGraduationCap className="mt-1 textsecondary me-2" fontSize={20} />{" "}
          Only University with valid approvals
        </p>

        <p className="d-flex align-items-start">
          <AiOutlineFieldTime
            className="mt-1 textsecondary me-2"
            fontSize={20}
          />{" "}
          Full Time Degree Assistance{" "}
        </p>

        <p className="d-flex align-items-start">
          <BsPeople className="mt-1 textsecondary me-2" fontSize={20} /> Face to
          Face Counseling{" "}
        </p>

        <p className="d-flex align-items-start">
          <AiOutlineSafetyCertificate
            className="mt-1 textsecondary me-2"
            fontSize={20}
          />{" "}
          Certified Educational Counselors
        </p>
        <Image src={student} alt="" />
      </div>
    </>
  );
};

export default RecentPost;
