import React from "react";
import Link from "next/link";
import styles from "./Category.module.scss";
import SidebarHeading from "./SidebarHeading";

const RecentPost = ({ recentBlogs }) => {
  return (
    <>
      <div className={`${styles.blogsidebar} card mb-4`}>
        <SidebarHeading title="Recent posts" />
        {recentBlogs.map((recent) => (
          <Link key={recent.id} href={`/blog/${recent.slug}`}>
            <a>{recent.title}</a>
          </Link>
        ))}
      </div>
    </>
  );
};

export default RecentPost;
