import Link from "next/link";
import Image from "next/image";
import { Card } from "react-bootstrap";
import { Chip } from "primereact/chip";
import Head from "next/head";
import styles from "./CourseCard.module.scss";

const CourseCard = (props) => {
  return (
    <>
      <Link href={props.link}>
        <a className={`${styles.cardWrapper} text-decoration-none`}>
          <Card className="text-center pt-4 rounded-10 shadow-1 mb-3 mx-1 border-0">
            <div className="mt-2">
              <Image
                src={props.icon ? props.icon : "/images/course_icons/dummy.png"}
                height={18}
                width={18}
                alt="Course Icon"
              />
            </div>
            <Card.Body style={{ paddingTop: "0.2rem" }}>
              <Card.Title className="mb-lg-3 fw-normal fs-12 text-dark text-truncate">
                {props.title}
              </Card.Title>
              <Chip
                className="position-absolute top-0 end-0 me-1 mt-1 text-truncate"
                label={props.label}
              />
              <Card.Text
                className={`${styles.compare_number} position-absolute bottom-0 end-0 card-text translate-middle-x start-50 w-100 bgprimary text-white text-truncate`}
                style={{ lineHeight: "18px" }}>
                Compare {props.university_number} Now
              </Card.Text>
            </Card.Body>
          </Card>
        </a>
      </Link>
    </>
  );
};

export default CourseCard;
