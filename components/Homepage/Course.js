import { Container, Modal } from "react-bootstrap";
import { Tab, Row, Col, Nav } from "react-bootstrap";
import CourseCard from "/components/Homepage/CourseCard";
import { BsChevronRight } from "react-icons/bs";
import styles from "./CourseCard.module.scss";
import CourseModal from "./Modal/CourseModal";
import { useState } from "react";

const Course = ({ menuData }) => {
  const [abroadModalVisible, setAbroadModalVisible] = useState(false);

  return (
    <>
      <Container>
        <Tab.Container id="course" defaultActiveKey={1}>
          <Row>
            <Col sm={3} className={`${styles.course_domain}`}>
              <Nav
                variant="pills"
                className={`${styles.course_nav} flex-lg-column p-3`}>
                {menuData.map((courseType) => (
                  <Nav.Item key={courseType.id}>
                    <Nav.Link eventKey={courseType.id}>
                      {courseType.name}
                    </Nav.Link>
                  </Nav.Item>
                ))}
                <Nav.Item onClick={() => setAbroadModalVisible(true)}>
                  <Nav.Link>Study Abroad (Online)</Nav.Link>
                </Nav.Item>
                <Nav.Item onClick={() => setAbroadModalVisible(true)}>
                  <Nav.Link>Study Abroad (On-Campus)</Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
            <Col sm={9} className={`${styles.course_domain_content}`}>
              <Tab.Content className={`${styles.course_tabcontent}`}>
                {menuData.map((courseType) => (
                  <Tab.Pane key={courseType.id} eventKey={courseType.id}>
                    <div className="d-flex flex-wrap">
                      {courseType.courses
                        .filter((course) => course.status === 1)
                        .map((course) => (
                          <CourseCard
                            key={course.id}
                            title={course.display_name}
                            university_number="10"
                            icon={course.icon}
                            link={`/courses/${course.slug}/`}
                            label={course.duration}
                          />
                        ))}
                    </div>
                  </Tab.Pane>
                ))}
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
        <Modal
          show={abroadModalVisible}
          onHide={() => setAbroadModalVisible(false)}
          centered>
          <CourseModal closeModal={() => setAbroadModalVisible(false)} />
        </Modal>
      </Container>
    </>
  );
};

export default Course;
