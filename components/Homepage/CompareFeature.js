import { AiOutlineDoubleRight } from "react-icons/ai";
import Link from "next/link";
import MainHeading from "../Global/Heading/MainHeading";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import TagLine from "../Global/TagLine";
import { Card, Row, Col } from "react-bootstrap";
import Image from "next/image";
import image1 from "/public/images/more-videos.png";
import image2 from "/public/images/phone-call.png";
import image3 from "/public/images/conference.png";
import image4 from "/public/images/meeting.png";
import image5 from "/public/images/video-conference.png";
import styles from "./CompareFeature.module.scss";
import { generate } from "shortid";

const CompareFeature = ({ compareFeatureData }) => {
  const Feature = [
    {
      heading:
        "Best Suggestion from Unbiased & Certified Advisors through Compare",
      description:
        "A university will never tell you other options that are better for you but we will. We will give all the options of approved universities that you can choose from and help you in selecting one on your factors.",
    },
    {
      heading:
        "Best Suggestion from Unbiased & Certified Advisors through Compare",
      description:
        "A university will never tell you other options that are better for you but we will. We will give all the options of approved universities that you can choose from and help you in selecting one on your factors.",
    },
    {
      heading:
        "Best Suggestion from Unbiased & Certified Advisors through Compare",
      description:
        "A university will never tell you other options that are better for you but we will. We will give all the options of approved universities that you can choose from and help you in selecting one on your factors.",
    },
    {
      heading:
        "Best Suggestion from Unbiased & Certified Advisors through Compare",
      description:
        "A university will never tell you other options that are better for you but we will. We will give all the options of approved universities that you can choose from and help you in selecting one on your factors.",
    },
    {
      heading:
        "Best Suggestion from Unbiased & Certified Advisors through Compare",
      description:
        "A university will never tell you other options that are better for you but we will. We will give all the options of approved universities that you can choose from and help you in selecting one on your factors.",
    },
    {
      heading:
        "Best Suggestion from Unbiased & Certified Advisors through Compare",
      description:
        "A university will never tell you other options that are better for you but we will. We will give all the options of approved universities that you can choose from and help you in selecting one on your factors.",
    },
    {
      heading:
        "Best Suggestion from Unbiased & Certified Advisors through Compare",
      description:
        "A university will never tell you other options that are better for you but we will. We will give all the options of approved universities that you can choose from and help you in selecting one on your factors.",
    },
    {
      heading:
        "Best Suggestion from Unbiased & Certified Advisors through Compare",
      description:
        "A university will never tell you other options that are better for you but we will. We will give all the options of approved universities that you can choose from and help you in selecting one on your factors.",
    },
  ];

  return (
    <>
      <div className={`${styles.compare_feature_wrap} container`}>
        <div className="row mb-sm-5">
          <div className={`${styles.compare_feature_col} col-md-8 me-auto`}>
            <div className="d-flex align-items-start align-items-sm-center flex-column flex-sm-row mb-2">
              <MainHeading
                useextraClass={true}
                title={compareFeatureData.heading}
              />
              <TagLine className="ps-2" extraClass={true} />
            </div>
            <p>{compareFeatureData.description}</p>
            <Link href="/contactus">
              <a className={`text-decoration-none h5 textprimary`}>
                Know More <AiOutlineDoubleRight />
              </a>
            </Link>
          </div>

          <Swiper
            slidesPerView={4}
            spaceBetween={20}
            loop={false}
            navigation={true}
            // pagination={{
            //   clickable: true,
            // }}
            pagination={false}
            breakpoints={{
              "@0.00": {
                slidesPerView: 1,
                // spaceBetween: 20,
              },
              500: {
                slidesPerView: 2,
                // spaceBetween: 20,
              },
              1024: {
                slidesPerView: 3,
                // spaceBetween: 20,
              },
              1336: {
                slidesPerView: 4,
                // spaceBetween: 20,
              },
            }}
            modules={[Navigation, Pagination]}
            className={`${styles.featureSlider}`}>
            {Feature.map((list, index) => (
              <SwiperSlide className="mt-3 pb-4" key={generate()}>
                <Card className="px-3 py-4 text-center">
                  <p
                    className={`${styles.number} h3 bg-white rounded-circle d-flex align-items-center justify-content-center mx-auto`}>
                    {index + 1}
                  </p>
                  <p className="h5 my-4">{list.heading} </p>
                  <p className="text-secondary">{list.description}</p>
                </Card>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </>
  );
};

export default CompareFeature;
