import { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import MainHeading from "../Global/Heading/MainHeading";

const AllUniversity = ({ universityData }) => {
  const [status, setStatus] = useState(true);

  return (
    <>
      <div className="container text-center pb-5">
        <MainHeading title="Approved Online &amp; Distance Education Universities" />
        <div className="row d-flex mt-4 justify-content-center">
          {universityData
            .slice(0, status ? 14 : universityData.length)
            .map((university) => (
              <div
                key={university.id}
                className="cols border border-1 d-flex align-items-center justify-content-center">
                <Link href={`/university/${university.slug}`}>
                  <a className="w-100" style={{ marginTop: "4px" }}>
                    <Image
                      src={university.logo}
                      width={110}
                      height={56}
                      objectFit="contain"
                      alt=""
                      quality={100}
                    />
                  </a>
                </Link>
              </div>
            ))}
        </div>
        <p
          className="mt-3 mb-4 textprimary cursor-pointer text-uppercase d-inline-block rounded py-2 px-3"
          style={{ backgroundColor: "aliceblue" }}
          onClick={() => setStatus(!status)}>
          {!status ? "Show Less" : "View more universities"}
        </p>
      </div>
    </>
  );
};

export default AllUniversity;
