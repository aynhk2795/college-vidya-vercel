import React, { useState, useRef } from "react";
import Carousel from "react-bootstrap/Carousel";
import { Container, Row, Col } from "react-bootstrap";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation } from "swiper";
import { Pagination } from "swiper";
import imageframe from "/public/images/mobileFrame2.png";
import image1 from "/public/images/screen1.png";
import image2 from "/public/images/screen2.png";
import image3 from "/public/images/screen3.png";
import image4 from "/public/images/screen4.png";
import image5 from "/public/images/1R.png";
import {
  FcAssistant,
  FcUp,
  FcSurvey,
  FcTodoList,
  FcRefresh,
} from "react-icons/fc";
import styles from "/styles/OneStop.module.scss";
import Image from "next/image";
import MainHeading from "../../Global/Heading/MainHeading";
import OneStopOther from "./OneStopOther";

const OneStop = ({ oneStopData, oneStopImages }) => {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex) => {
    setIndex(selectedIndex);
  };

  const oneStopIcons = [
    {
      component: <FcUp className={`${styles.appicon} rounded p-2`} />,
    },
    {
      component: <FcSurvey className={`${styles.appicon} rounded p-2`} />,
    },
    {
      component: <FcTodoList className={`${styles.appicon} rounded p-2`} />,
    },
    {
      component: <FcRefresh className={`${styles.appicon} rounded p-2`} />,
    },
    {
      component: <FcAssistant className={`${styles.appicon} rounded p-2`} />,
    },
  ];

  return (
    <>
      <style>{`

.mobile_slider {
    z-index: -1;
    padding-right: 2px;
    padding-left: 3px;
    border-radius: 52px;
}
        .mobile_slider .carousel-inner {
          border-radius: 60px;
          height:500px;
        }
        .mobile_slider .carousel-indicators {
    bottom: -75px;
}
.carousel-indicators [data-bs-target] {
    background-color: #bbb;
}
.carousel-indicators .active {
    background-color: #0074d7 !important;
}
.onestop_text_wrap {
    background-color: #fff;
    padding: 15px 15px 15px 15px;
    border-radius: 6px;
   
}
.onestop_text .head {
  color: #0074d7;
}
.onestop_text {
  
 
    transition: .3s;
    box-shadow:0 2px 4px 2px rgb(0 0 0 / 7%);
}

      
      `}</style>
      <section className="bg-grey pt-5 pb-3">
        <Container>
          <Row className="mb-4">
            <Col md={12}>
              <div className="d-flex align-items-start align-items-sm-center  flex-column flex-sm-row mb-2">
                <MainHeading title={oneStopData.heading} />
                <h2 className="m-0 ms-1 textprimary">
                  {" "}
                  Your One Stop Solution
                </h2>
              </div>
              <p>{oneStopData.description}</p>
            </Col>
          </Row>
          <Row className="row mb-5">
            <Col md={6}>
              {oneStopData.data.map((oneStop, indexL) => (
                <div
                  key={oneStop.id}
                  className={`col d-flex align-items-start mb-4 cursor-pointer onestop_text_wrap ${
                    index === indexL ? "onestop_text" : ""
                  }`}
                  onClick={() => setIndex(indexL)}>
                  <div className="icon-square bg-light text-dark flex-shrink-0 me-3 shadow-1 rounded">
                    {oneStopIcons[indexL].component}
                  </div>
                  <div>
                    <p className="fw-bold m-0 head">{oneStop.title}</p>
                    <p>{oneStop.description}</p>
                  </div>
                </div>
              ))}
            </Col>
            <Col
              md={6}
              className="d-flex justify-content-center align-items-center">
              {/* <Swiper
                slidesPerView={1}
                spaceBetween={20}
                centeredSlides={true}
                loop={true}
                navigation={false}
                pagination={{
                  clickable: true,
                }}
                modules={[Navigation, Pagination]}
                className="onestopSwiper"
              >
                {oneStopImages.map((image) => (
                  <SwiperSlide key={image.id}>
                    <div className="text-center mb-4">
                      <Image
                        className="rounded"
                        width={350}
                        height={620}
                        src={image.image}
                        alt=""
                      />
                    </div>
                  </SwiperSlide>
                ))}
              </Swiper> */}

              <div
                className="position-relative"
                style={{
                  width: "250px",
                  height: "550px",
                  overflow: "hidden",
                  borderRadius: "52px",
                  zIndex: "1",
                }}>
                <Image src={imageframe} width={250} height={520} alt="" />
                <Carousel
                  activeIndex={index}
                  onSelect={handleSelect}
                  controls={true}
                  slide={true}
                  touch={true}
                  indicators={true}
                  keyboard={true}
                  interval={2000}
                  className="position-absolute start-0 top-0 mobile_slider">
                  <Carousel.Item style={{ borderRadius: "52px" }}>
                    <Image src={image1} width={250} height={550} alt="" />
                  </Carousel.Item>
                  <Carousel.Item style={{ borderRadius: "52px" }}>
                    <Image src={image2} width={250} height={550} alt="" />
                  </Carousel.Item>
                  <Carousel.Item style={{ borderRadius: "52px" }}>
                    <Image src={image3} width={250} height={550} alt="" />
                  </Carousel.Item>
                  <Carousel.Item style={{ borderRadius: "52px" }}>
                    <Image src={image4} width={250} height={550} alt="" />
                  </Carousel.Item>
                  <Carousel.Item style={{ borderRadius: "52px" }}>
                    <Image src={image5} width={250} height={550} alt="" />
                  </Carousel.Item>
                </Carousel>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};

export default OneStop;
