import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import screen1 from "/public/images/media/ASIAN-COMMUNITY-NEWS-.png";
import screen2 from "/public/images/media/Hindustan-Tim.webp";
import screen3 from "/public/images/media/forbes.png";
import screen4 from "/public/images/media/edugraph-logo.svg";
import screen5 from "/public/images/media/et-brandequity.png";
import screen6 from "/public/images/media/the_hindu.png";
import { Card } from "primereact/card";
import MainHeading from "../../Global/Heading/MainHeading";
import SubHeading from "../../Global/Heading/SubHeading";

import Link from "next/link";
import { BsChevronRight } from "react-icons/bs";
import { RiArrowRightUpLine } from "react-icons/ri";

const Media = () => {
  const Mediacard = [
    {
      id: 1,
      logo: screen6,
      name: "Education summit: Reimagining Indian Education by Global India",
      date: "21 Mar 2022",
    },
    {
      id: 2,
      logo: screen5,
      name: "I wanted to create a platform that is free and unbiased so that it can",
      date: "21 Mar 2022",
    },
    {
      id: 3,
      logo: screen1,
      name: "College Vidya is enabling information access to students",
      date: "21 Mar 2022",
    },
    {
      id: 4,
      logo: screen4,
      name: "College Vidya via its recent campaign #ChunoWahiJoHaiSahi",
      date: "21 Mar 2022",
    },
    {
      id: 5,
      logo: screen2,
      name: "Post-Covid work environment: Upskilling for impact in 2022",
      date: "21 Mar 2022",
    },
    {
      id: 6,
      logo: screen3,
      name: "Post-Covid work environment: Upskilling for impact in 2022",
      date: "21 Mar 2022",
    },
  ];
  return (
    <>
      <div className="container my-sm-5 mb-3">
        <div className="row">
          <div className="col-md-12">
            <div className="mb-2">
              <SubHeading title="College Vidya in Media" />
            </div>
            <div className="mb-sm-4">
              <MainHeading title="Media Spotlight" />
            </div>
          </div>
        </div>
        <Swiper
          slidesPerView={4}
          spaceBetween={20}
          loop={true}
          navigation={true}
          // pagination={{
          //   clickable: true,
          // }}
          pagination={false}
          breakpoints={{
            "@0.00": {
              slidesPerView: 1,
              // spaceBetween: 20,
            },
            500: {
              slidesPerView: 2,
              // spaceBetween: 20,
            },
            1024: {
              slidesPerView: 3,
              // spaceBetween: 20,
            },
            1336: {
              slidesPerView: 4,
              // spaceBetween: 20,
            },
          }}
          modules={[Navigation, Pagination]}
          className="mediaSwiper">
          {Mediacard.map((list) => (
            <SwiperSlide className="mt-3 pb-4" key={list.id}>
              <Link href="/">
                <a className="text-decoration-none">
                  <Card>
                    <Image
                      src={list.logo}
                      className="rounded"
                      width={146}
                      height={43}
                      objectFit="contain"
                      alt=""
                    />
                    <p className="my-lg-4 text-dark fw-bolder">{list.name}</p>
                    <div className="d-flex align-items-center justify-content-between">
                      <p className="mb-0 fs-12">{list.date}</p>
                      <p className="fs-13">
                        <Link href="/">
                          <a className="textprimary">
                            Read More <BsChevronRight />{" "}
                          </a>
                        </Link>
                      </p>
                    </div>
                  </Card>
                </a>
              </Link>
            </SwiperSlide>
          ))}
        </Swiper>
        <p className="text-end pe-3">
          <Link href="/media">
            <a className="textsecondary">
              Explore Media <RiArrowRightUpLine />{" "}
            </a>
          </Link>
        </p>
      </div>
    </>
  );
};

export default Media;
