import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import styles from "./MobileBanner.module.scss";
import { Container, Row, Col } from "react-bootstrap";
import { useAuth } from "../../../hooks/auth";
import { useAxiosJWT } from "../../../hooks/axios";
import { useState, useEffect } from "react";
import { UserService } from "../../../service/UserService";

const MobileBanner = ({ bannerData }) => {
  const { user, logout } = useAuth();
  const { axiosJWT } = useAxiosJWT();

  const [username, setUsername] = useState(null);
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    if (user) {
      const userService = new UserService(axiosJWT);
      userService
        .getUserDetails()
        .then((response) => {
          setLoggedIn(true);
          setUsername(response.data.name);
        })
        .catch(() => {
          logout();
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return (
    <div>
      {loggedIn ? (
        <Container>
          <Row>
            <Col className="ps-4">
              <p className="mt-4 mb-1 h6 text-capitalize">
                Namaste, <span>{username && username.split(" ")[0]}</span> 🙏
              </p>
              <p className="fs-10 text-secondary">Great to have you back.</p>
            </Col>
          </Row>
        </Container>
      ) : null}

      <Swiper
        slidesPerView={1}
        spaceBetween={15}
        loop={true}
        navigation={false}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          500: {
            slidesPerView: 2,
          },
          600: {
            slidesPerView: 2,
          },
        }}
        modules={[Navigation, Pagination]}
        className={`${styles.mobile_banner} mt-3`}>
        {bannerData.map((banner) => (
          <SwiperSlide key={banner.id} className="text-center">
            <Image
              alt="Full Logo"
              className="rounded"
              objectFit="contain"
              src={banner.image}
              width={280}
              height={150}
              layout="responsive"
            />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default MobileBanner;
