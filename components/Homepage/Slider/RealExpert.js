import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Autoplay } from "swiper";
import Image from "next/image";
import { Chip } from "primereact/chip";
import MainHeading from "../../Global/Heading/MainHeading";

import Link from "next/link";
import { generate } from "shortid";
import { Card } from "react-bootstrap";
import verify from "/public/images/badge.png";
import { BsFillStarFill } from "react-icons/bs";

export default function App({ counsellors }) {
  return (
    <>
      <div style={{ backgroundColor: "#030a21" }} className="py-sm-5 py-1 mb-5">
        <div className="container my-5">
          <div className="row">
            <div className="col-md-8 mx-auto">
              <div className="text-center">
                <MainHeading
                  useextraClass="text-white bottom_line"
                  title="Real Advice from Real Experts &#128526;"
                />
              </div>
              <p className="text-center text-white mb-sm-5 mb-3 mt-3">
                College Vidya has more than 2500+ mentors who work closely with
                you to give a realistic viewpoint about your career goals and
                enable you to build a successful career.
              </p>
            </div>
          </div>
          <Swiper
            slidesPerView={4}
            spaceBetween={20}
            // centeredSlides={false}
            loop={true}
            navigation={true}
            // autoplay={{
            //   "delay": 2500,
            //   "disableOnInteraction": false
            // }}

            slidesPerGroup={1}
            breakpoints={{
              "@0.00": {
                slidesPerView: 1,
                // spaceBetween: 20,
              },
              500: {
                slidesPerView: 2,
                // spaceBetween: 20,
              },
              1024: {
                slidesPerView: 3,
                // spaceBetween: 20,
              },
              1336: {
                slidesPerView: 4,
                // spaceBetween: 20,
              },
            }}
            modules={[Navigation, Autoplay]}
            className="expertSwiper">
            {counsellors.map((counsellor) => (
              <SwiperSlide key={generate()}>
                <Card
                  className="text-center border-0 shadow-2 position-relative"
                  style={{ borderRadius: "20px" }}>
                  <span
                    className="position-absolute end-0 top-0 me-2 mt-2"
                    style={{ zIndex: "1" }}>
                    <Image
                      src={verify}
                      width={25}
                      height={25}
                      objectFit="contain"
                      alt=""
                    />
                  </span>

                  <Image
                    src={counsellor.profile_image}
                    width={300}
                    height={360}
                    objectFit="cover"
                    alt=""
                    className="rounded"
                  />

                  {counsellor.rating ? (
                    <p className="m-0 fs-12 d-flex align-items-center position-absolute top-0 start-0 ms-3 mt-2 bg-white px-1 rounded shadow-2">
                      <BsFillStarFill color="orange" className="me-1" />{" "}
                      {counsellor.rating}
                    </p>
                  ) : (
                    <p className="m-0 fs-12 d-flex align-items-center position-absolute top-0 start-0 ms-3 mt-2 bg-white px-1 rounded shadow-2">
                      <BsFillStarFill color="orange" className="me-1" /> 0
                    </p>
                  )}

                  <div className="px-3">
                    <div
                      className="content position-absolute bottom-0 mb-2 rounded pb-2 start-50 translate-middle-x bg-white shadow-1"
                      style={{ width: "85%" }}>
                      <p className="fw-bold mt-3 mb-0">{counsellor.name}</p>
                      <p className="fw-normal mb-0 textprimary">
                        {counsellor.designation}
                      </p>
                      <p className="fw-normal text-secondary fs-14 mb-1">
                        {counsellor.experience} years experience
                      </p>

                      {/* <Link href={process.env.NEXT_PUBLIC_CV_COUNSELLOR_URL+"counsellor/"+counsellor.slug} passHref>
                      <a target="_blank" className="text-decoration-none">
                        <Chip label="Consult Now" />
                      </a>
                    </Link> */}
                      <Link
                        href={
                          process.env.NEXT_PUBLIC_CV_COUNSELLOR_URL +
                          "counsellor/" +
                          counsellor.slug
                        }
                        passHref>
                        <a target="_blank" className="text-decoration-none">
                          {/* <Chip
                        className="shadow-1 mb-3 text-dark px-4 py-1 rounded-pill"
                        label="Consult Now"
                      /> */}

                          <p
                            className="d-inline-block rounded px-3 py-1 fs-14"
                            style={{ backgroundColor: "aliceblue" }}>
                            Consult Now
                          </p>
                        </a>
                      </Link>
                    </div>
                  </div>
                </Card>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </>
  );
}
