import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import Link from "next/link";
import styles from "./Colorcard.module.scss";

export default function App({ infoBanners }) {
  return (
    <>
      <div className={`${styles.colorcard_container} container my-md-5`}>
        <Swiper
          slidesPerView={3}
          spaceBetween={20}
          loop={false}
          navigation={false}
          pagination={{
            clickable: true,
          }}
          breakpoints={{
            "@0.00": {
              slidesPerView: 1,
              spaceBetween: 20,
            },
            "@0.75": {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            "@1.00": {
              slidesPerView: 3,
              spaceBetween: 20,
            },
          }}
          modules={[Navigation, Pagination]}
          className={`${styles.colorcard_slider}`}>
          {infoBanners.map((banner) => (
            <SwiperSlide className="mb-3 mt-2 mt-sm-0 pb-2" key={banner.id}>
              {banner.url && banner.url !== "" ? (
                <Link href={banner.url} passHref>
                  <a target="_blank">
                    <div className="item position-relative">
                      <Image
                        className="rounded"
                        src={banner.image}
                        width={420}
                        height={200}
                        alt=""
                        objectFit="cover"
                      />
                      <p className="m-0 w-40 ps-4 pt-3 position-absolute top-0 start-0 color-card-content">
                        {banner.title}
                        <br />
                        {banner.number !== "" ? (
                          <Link href={`tel:${banner.number}`} passHref>
                            <a style={{ color: "#fd4705", fontWeight: "bold" }}>
                              {banner.number}
                            </a>
                          </Link>
                        ) : null}
                      </p>
                    </div>
                  </a>
                </Link>
              ) : (
                <div className="item position-relative">
                  <Image
                    className="rounded"
                    src={banner.image}
                    width={420}
                    height={200}
                    alt=""
                  />
                  <p className="m-0 w-40 ps-4 pt-3 position-absolute top-0 start-0 color-card-content">
                    {banner.title}
                    <br />
                    {banner.number !== "" ? (
                      <Link href={`tel:${banner.number}`} passHref>
                        <a style={{ color: "#fd4705", fontWeight: "bold" }}>
                          {banner.number}
                        </a>
                      </Link>
                    ) : null}
                  </p>
                </div>
              )}
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </>
  );
}
