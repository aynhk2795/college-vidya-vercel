import React, { useState, useRef } from "react";
import Carousel from "react-bootstrap/Carousel";
import Image from "next/image";
import imageframe from "/public/images/mobileFrame2.png";
import image1 from "/public/images/2n.png";
import image2 from "/public/images/1R.png";
import image3 from "/public/images/3R.png";
import { Container, Row, Col } from "react-bootstrap";

const OneStopOther = () => {
  const [index, setIndex] = useState(0);
  const hello = useRef(null);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };
  return (
    <>
      <style>{`

.mobile_slider {
    z-index: -1;
    padding-right: 2px;
    padding-left: 3px;
    border-radius: 52px;
}
        .mobile_slider .carousel-inner {
          border-radius: 60px;
          height:500px;
        }
        .mobile_slider .carousel-indicators {
    bottom: -60px;
}
.carousel-indicators [data-bs-target] {
    background-color: #ccc;
}
      
      `}</style>
      <Container className="my-5 py-5">
        <Row>
          <Col md={6}>
            <p onClick={() => setIndex(0)}>First</p>
            <p onClick={() => setIndex(1)}>Second</p>
            <p onClick={() => setIndex(2)}>Third</p>
            <p onClick={() => setIndex(3)}>Fourth</p>
            <p onClick={() => setIndex(4)}>Fifth</p>
          </Col>
          <Col md={6} className="d-flex justify-content-center">
            <div
              className="position-relative"
              style={{
                width: "250px",
                height: "550px",
                overflow: "hidden",
                borderRadius: "52px",
                zIndex: "1",
              }}>
              <Image
                src={imageframe}
                width={250}
                height={520}
                style={{ borderRadius: "52px" }}
                alt=""
              />
              <Carousel
                activeIndex={index}
                onSelect={handleSelect}
                controls={true}
                slide={true}
                touch={true}
                indicators={true}
                keyboard={true}
                interval={1000}
                className="position-absolute start-0 top-0 mobile_slider">
                <Carousel.Item style={{ borderRadius: "52px" }}>
                  <Image src={image1} width={250} height={550} alt="" />
                </Carousel.Item>
                <Carousel.Item style={{ borderRadius: "52px" }}>
                  <Image src={image2} width={250} height={550} alt="" />
                </Carousel.Item>
                <Carousel.Item style={{ borderRadius: "52px" }}>
                  <Image src={image3} width={250} height={550} alt="" />
                </Carousel.Item>
                <Carousel.Item style={{ borderRadius: "52px" }}>
                  <Image src={image1} width={250} height={550} alt="" />
                </Carousel.Item>
                <Carousel.Item style={{ borderRadius: "52px" }}>
                  <Image src={image2} width={250} height={550} alt="" />
                </Carousel.Item>
              </Carousel>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default OneStopOther;
