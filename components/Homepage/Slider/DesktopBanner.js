import React from "react";
import Carousel from "react-bootstrap/Carousel";
import { Container } from "react-bootstrap";
import Link from "next/link";
import Image from "next/image";

const DesktopBanner = ({ bannerData }) => {
  return (
    <>
      <Container fluid className="px-0">
        <Carousel>
          {bannerData.map((banner) => (
            <Carousel.Item key={banner.id}>
              <Link href={banner.banner_image_link} passHref>
                <a>
                  <Image
                    src={banner.image}
                    width={1920}
                    height={420}
                    alt="banner"
                    priority
                    objectFit="cover"
                    layout="responsive"
                  />
                </a>
              </Link>
            </Carousel.Item>
          ))}
        </Carousel>
      </Container>
    </>
  );
};

export default DesktopBanner;
