import Link from "next/link";
import React from "react";
import { Container } from "react-bootstrap";
import { FaGrinHearts } from "react-icons/fa";
import { FcGoogle } from "react-icons/fc";
import { FcOnlineSupport } from "react-icons/fc";

const keypointlist = [
  {
    id: 1,
    icon: <FaGrinHearts className="icon icon1" />,
    heading: "14 Lac+",
    desc: "Happy Students",
    link: "",
  },
  {
    id: 2,
    icon: <FcGoogle className="icon" />,
    heading: "4.9",
    desc: "On Google",
    link: "https://www.google.com/search?q=collegevidya&rlz=1C5CHFA_enIN956IN956&oq=collegevidya&aqs=chrome..69i57j69i60l3j69i65j69i60.2106j0j1&sourceid=chrome&ie=UTF-8#lrd=0x390ce91a83de9ab3:0xd1ca78f64f373b2,1",
  },
  {
    id: 3,
    icon: <FcOnlineSupport className="icon" />,
    heading: "50K+",
    desc: "Admission",
    link: "",
  },
];

const KeyPoint = () => {
  return (
    <>
      <Container>
        <div className="key-wrapper row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-3 g-4 pt-sm-3 mb-0 mb-sm-5 mt-3">
          {keypointlist.map((list) => (
            <div
              key={list.id}
              className="key-wrap col d-flex align-items-start justify-content-center mt-0">
              {list.icon}

              {list.link !== "" ? (
                <Link href={list.link} passHref>
                  <a className="text-dark" target={"_blank"}>
                    <h2 className="fw-bold mb-0 key-title"> {list.heading} </h2>
                    <p className="m-0 key-content">{list.desc}</p>
                  </a>
                </Link>
              ) : (
                <div>
                  <h2 className="fw-bold mb-0 key-title"> {list.heading} </h2>
                  <p className="m-0 key-content">{list.desc}</p>
                </div>
              )}
            </div>
          ))}
        </div>
      </Container>
    </>
  );
};

export default KeyPoint;
