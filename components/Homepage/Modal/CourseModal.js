import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Image from "next/image";
import vidya from "/public/images/vidya.png";
import Link from "next/link";
import { BsX } from "react-icons/bs";

const CourseModal = ({ closeModal }) => {
  return (
    <Modal.Body className="text-center rounded">
      <p
        onClick={() => closeModal()}
        className="position-absolute end-0 top-0 me-2 mt-2 cursor-pointer">
        <BsX fontSize={30} />{" "}
      </p>
      <Image src={vidya} className="bg-white rounded-circle p-3" alt="" />
      <p className="fs-14 my-4 px-5">
        Let me take you to a dedicated portal which we prepared to address all
        your queries related to studying abroad in detail.
      </p>
      <Link href="https://collegevidyaabroad.com/" passHref>
        <a
          className="bgprimary px-3 py-2 rounded text-white my-3 d-inline-block shadow-1"
          target="_blank"
          style={{ zIndex: "9999" }}
          onClick={() =>
            window.open("https://collegevidyaabroad.com/", "_blank")
          }>
          Lets&quot;s Go
        </a>
      </Link>
    </Modal.Body>
  );
};

export default CourseModal;
