import { Accordion, AccordionTab } from "primereact/accordion";

const FaqSlim = ({ universityFAQs }) => {
  return (
    <>
      <Accordion activeIndex={0}>
        {universityFAQs.map((faq) => (
          <AccordionTab key={faq.id} header={faq.title}>
            <div dangerouslySetInnerHTML={{ __html: faq.content }}></div>
          </AccordionTab>
        ))}
      </Accordion>
    </>
  );
};

export default FaqSlim;
