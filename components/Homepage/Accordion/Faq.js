import { Accordion, Container, Row, Col } from "react-bootstrap";
import MainHeading from "/components/Global/Heading/MainHeading";
import styles from "./Faq.module.scss";

const Faq = ({ faqData }) => {
  return (
    <>
      <style>{`

          .faq .accordion-button {
            font-size: 20px;
            padding: 25px 30px 20px 30px;
          }
          .faq .accordion-button:not(.collapsed) {
            background-color: #fff;
            border-bottom: 1px solid #eee;
          }
          
      
      `}</style>
      <section
        className="py-5 mb-5 px-0 faq"
        style={{ backgroundColor: "#f7f8ff" }}>
        <Container>
          <Row>
            <Col md={10} className={`mx-auto`}>
              <div className="text-center mb-5">
                <MainHeading title="Ask us anything, we’d love to answer!" />
              </div>
              <Accordion
                className={`${styles.custom_faq}`}
                defaultActiveKey={0}>
                {faqData &&
                  faqData
                    .filter((faq) => faq.status === 1)
                    .map((faq, index) => (
                      <Accordion.Item
                        eventKey={faq.id}
                        className="border"
                        key={faq.id}>
                        <Accordion.Header>
                          <span
                            className="bgprimary text-white rounded-circle d-flex align-items-center justify-content-center me-2 fs-12"
                            style={{
                              width: "25px",
                              height: "25px",
                              minWidth: "24px",
                            }}>
                            {index + 1}
                          </span>
                          {faq.title}
                        </Accordion.Header>
                        <Accordion.Body
                          dangerouslySetInnerHTML={{ __html: faq.content }}
                        />
                      </Accordion.Item>
                    ))}
              </Accordion>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};

export default Faq;
