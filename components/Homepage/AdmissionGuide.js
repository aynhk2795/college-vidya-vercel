import { Row, Col } from "react-bootstrap";
import AdmissionGuidecard from "/components/Homepage/AdmissionGuidecard";
import styles from "/components/Homepage/Slider/AdmissionGuide.module.scss";

const AdmissionGuide = ({ featureData }) => {
  return (
    <>
      <section className={`${styles.bg_wrap} position-relative`}>
        <div className="container">
          <div className="row mb-5">
            <Col md={5} className="d-flex align-items-center me-auto">
              <h1 className={`${styles.heading} my-4 my-sm-0`}>
                {featureData.heading}
              </h1>
            </Col>
            <Col md={6} className="card-wrapper d-flex flex-wrap">
              <Row>
                {featureData.data.map((feature, index) => (
                  <Col xs={6} key={feature.id}>
                    <div
                      className={` ${styles.card_wrap} card-wrap ${
                        index % 2 === 0 ? "" : "mt-4"
                      }`}>
                      <AdmissionGuidecard
                        icon={feature.icon}
                        title={feature.title}
                        para={feature.description}
                      />
                    </div>
                  </Col>
                ))}
              </Row>
            </Col>
          </div>
        </div>
      </section>
    </>
  );
};

export default AdmissionGuide;
