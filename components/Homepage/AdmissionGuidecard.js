import React from "react";
import Image from "next/image";
import styles from "/components/Homepage/Slider/AdmissionGuide.module.scss";

const AdmissionGuidecard = (props) => {
  return (
    <>
      <div
        className={` ${styles.mycard} card  px-2 px-sm-3 py-3 py-sm-5 shadow-1 rounded-10 border-top-0 position-relative mb-3 mb-sm-5`}>
        <span className="position-absolute top-0 start-0 translate-middle ms-5 bg-white">
          <Image src={props.icon} width={60} height={60} alt="" />
        </span>
        <div className="card-title fw-bold mt-2 text-truncate">
          {props.title}
        </div>
        <div className="card-body px-0">{props.para}</div>
      </div>
    </>
  );
};

export default AdmissionGuidecard;
