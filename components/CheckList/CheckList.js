import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import styles from "/styles/Checklist.module.scss";
import AnimatedYoutube from "/components/Global/AnimatedYoutube";
import Link from "next/link";
import Image from "next/image";
import checklist1 from "/public/images/checklist/checklist-verified.png";
import checklist2 from "/public/images/checklist/degree.png";
import checklist3 from "/public/images/checklist/data-science.png";
import checklist4 from "/public/images/checklist/registration-form.png";
import checklist5 from "/public/images/checklist/registration-process.png";
import checklist6 from "/public/images/checklist/onlinetest.png";
import mobile from "/public/images/mobile-phone.png";
import { TiTick } from "react-icons/ti";

const CheckList = () => {
  return (
    <>
      <Container>
        <Row className="text-center py-5">
          <Col md={8} className="mx-auto">
            <h1>College Vidya Checklist</h1>
            <p className="textprimary">#ChunoWahiJoHaiSahi</p>
            <p>
              Questions and Myths often puzzle students before taking admission.
              We have answered the 8 most frequently asked questions related to
              online universities, just for you.
            </p>
          </Col>
        </Row>
        <div className={`${styles.checklistWrap} p-3 p-sm-5`}>
          <h1 className="text-center mb-3 mb-sm-5">Did You Know ?</h1>
          <Row>
            <Col md={4} className={`${styles.checklistPanel}`}>
              <div className={`${styles.checklistDiv}`}>
                <h2 className="d-flex align-items-center">
                  <span className={`${styles.checklistCount}`}>1</span>
                  <span>Value of Online University Degree?</span>
                </h2>
                <p>
                  Before applying to online universities individuals often find
                  themselves in a dilemma, whether their online university
                  degree will hold the same value as a regular one in the long
                  run? The answer that holds the key to their dilemma is simple,
                  yes it does, but only if the online university holds the right
                  approvals. An online university degree holds the same value as
                  a regular one
                </p>
                <Link href="/">
                  <a className="d-flex align-items-center text-dark">
                    <span className="me-2">
                      To know more students can watch
                    </span>{" "}
                    <AnimatedYoutube />
                  </a>
                </Link>
              </div>
            </Col>
            <Col md={8} className={`${styles.checklistPanel}`}>
              <div className={`${styles.checklistDiv}`}>
                <h2 className="d-flex align-items-center">
                  <span className={`${styles.checklistCount}`}>2</span>
                  <span>Importance of Approvals & what are they?</span>
                </h2>
                <p>
                  Approvals hold a significant value when it comes to online
                  education. A university without the proper approvals (UGC-DEB,
                  NAAC, etc) is a fake university with no value provided as it
                  won&apos;t get students the quality education, valued and
                  accepted online degree, proper skills, and benefits to achieve
                  great heights in their careers.
                </p>
                <Link href="/">
                  <a className="d-flex align-items-center text-dark">
                    <span className="me-2">
                      To know more students can watch
                    </span>{" "}
                    <AnimatedYoutube />
                  </a>
                </Link>
                <Image
                  className="pt-3"
                  src={checklist1}
                  width={95}
                  height={110}
                  alt=""
                />
              </div>
            </Col>
            <Col md={12} className={`${styles.checklistPanel}`}>
              <div className={`${styles.checklistDiv}`}>
                <h2 className="d-flex align-items-center">
                  <span className={`${styles.checklistCount}`}>3</span>
                  <span>
                    Online university degree Valid for Government &amp; Private
                    Jobs?
                  </span>
                </h2>
                <p>
                  Any individual with an online university degree can secure a
                  good job in both Government and Private organizations. An
                  online university degree holds the same value as a regular
                  degree and hence makes individuals eligible for high profile
                  jobs with handsome salaries from startups to MNCs.
                </p>
                <Link href="/">
                  <a className="d-flex align-items-center text-dark">
                    <span className="me-2">
                      To know more students can watch
                    </span>{" "}
                    <AnimatedYoutube />
                  </a>
                </Link>
                <Image
                  className="pt-3"
                  src={checklist2}
                  width={90}
                  height={110}
                  alt=""
                />
              </div>
            </Col>
            <Col md={8} className={`${styles.checklistPanel}`}>
              <div className={`${styles.checklistDiv}`}>
                <h2 className="d-flex align-items-center">
                  <span className={`${styles.checklistCount}`}>4</span>
                  <span>Myths on Online education</span>
                </h2>
                <p>
                  There are certain myths that revolve around Online
                  Universities, some state that online universities have no
                  placement cells, and others that they are not worth the time
                  and effort. Statements as such are nothing but myths and false
                  information. Online universities have the same and more
                  benefits than regular universities like dedicated placement
                  cells, highly educated and trained professors, LMS, quality
                  education, alumni connections, recorded lectures, libraries,
                  value degrees, popper approvals, and much more. Online
                  education is like regular education the only difference is the
                  mode through which the student receives their knowledge.
                </p>
                <Link href="/">
                  <a className="d-flex align-items-center text-dark">
                    <span className="me-2">
                      To know more students can watch
                    </span>{" "}
                    <AnimatedYoutube />
                  </a>
                </Link>
                <Image
                  className="pt-3"
                  src={checklist3}
                  width={90}
                  height={110}
                  alt=""
                />
              </div>
            </Col>
            <Col md={4} className={`${styles.checklistPanel}`}>
              <div className={`${styles.checklistDiv}`}>
                <h2 className="d-flex align-items-center">
                  <span className={`${styles.checklistCount}`}>5</span>
                  <span>&quot;Online&quot; mentioned on the Degree?</span>
                </h2>
                <p>
                  For those who are wondering if the word Online will be
                  Mentioned on their Degrees the answer is yes it will be
                  mentioned. The word Online being mentioned on the degree makes
                  no difference and the value of the online degree remains the
                  same as any other degree.
                </p>
                <Link href="/">
                  <a className="d-flex align-items-center text-dark">
                    <span className="me-2">
                      To know more students can watch
                    </span>{" "}
                    <AnimatedYoutube />
                  </a>
                </Link>
              </div>
            </Col>
            <Col md={4} className={`${styles.checklistPanel}`}>
              <div className={`${styles.checklistDiv}`}>
                <h2 className="d-flex align-items-center">
                  <span className={`${styles.checklistCount}`}>6</span>
                  <span>
                    Approvals needed for Foreign jobs or higher education?
                  </span>
                </h2>
                <p>
                  Individuals who wish to complete their higher education from
                  foreign or secure a job there need not worry as there are no
                  extra approvals needed for the same. If their university is
                  UGC-DEB approved with AIU approval and is giving them a
                  recommendation letter they are good to go.
                </p>
                <Link href="/">
                  <a className="d-flex align-items-center text-dark">
                    <span className="me-2">
                      To know more students can watch
                    </span>{" "}
                    <AnimatedYoutube />
                  </a>
                </Link>
              </div>
            </Col>
            <Col md={8} className={`${styles.checklistPanel}`}>
              <div className={`${styles.checklistDiv}`}>
                <h2 className="d-flex align-items-center">
                  <span className={`${styles.checklistCount}`}>7</span>
                  <span>
                    3 Major Differences Between Online Education & Distance
                    Education
                  </span>
                </h2>
                <p>
                  In the Era of &quot;New Normal&quot;, this mode of education
                  is taking new hype So every new way comes up with a lot of
                  confusion and misconceptions. Therefore today, we wanted to
                  clear this one doubt about the difference between online
                  education & distance education. Here are the 3 major
                  differences between online & distance education.
                </p>
                <ol>
                  <li>
                    <span className="fw-bold">Difference of Approvals :</span>{" "}
                    In online education, the university requires NAAC
                    accreditation and NIRF ranking whereas in distance education
                    the university requires UGC-DEB approval.
                  </li>
                  <li>
                    <span className="fw-bold">Examination Mode :</span> In
                    Online Education both studies and examinations held online
                    and in distance examinations will be in the university
                    center.
                  </li>
                  <li>
                    <span className="fw-bold">Difference of LMS :</span> In
                    distance education, most of the university except some top
                    universities does not offer live lectures and other
                    facilities but online education inculcates all the
                    advantages including live lectures & assignments.
                  </li>
                </ol>
                <Link href="/">
                  <a className="d-flex align-items-center text-dark">
                    <span className="me-2">
                      To know more students can watch
                    </span>{" "}
                    <AnimatedYoutube />
                  </a>
                </Link>
                <Image
                  className="pt-3"
                  src={checklist4}
                  width={95}
                  height={110}
                  alt=""
                />
              </div>
            </Col>
            <Col md={8} className={`${styles.checklistPanel}`}>
              <div className={`${styles.checklistDiv}`}>
                <h2 className="d-flex align-items-center">
                  <span className={`${styles.checklistCount}`}>8</span>
                  <span>Registration Process and how to pay?</span>
                </h2>
                <p>
                  The registration process of online universities consists of
                  simple steps that require students to visit the official
                  website/portal of the university, fill and submit the
                  registration form provided with correct information, and pay
                  the registration fee. One thing that students should keep in
                  mind is to pay the registration fee only on the University
                  official Website/Portal, services provided by College Vidya
                  are absolutely free and students don&apos;t need to pay any
                  fee or charge.
                </p>
                <Link href="/">
                  <a className="d-flex align-items-center text-dark">
                    <span className="me-2">
                      To know more students can watch
                    </span>{" "}
                    <AnimatedYoutube />
                  </a>
                </Link>
                <Image
                  className="pt-3"
                  src={checklist5}
                  width={95}
                  height={110}
                  alt=""
                />
              </div>
            </Col>
            <Col md={4} className={`${styles.checklistPanel}`}>
              <div className={`${styles.checklistDiv}`}>
                <h2 className="d-flex align-items-center">
                  <span className={`${styles.checklistCount}`}>9</span>
                  <span>Mode of examination?</span>
                </h2>
                <p>
                  The mode of examination is online, the same as the mode of
                  education. Students can attend their examinations from their
                  homes as Proctored Examinations and are not required to visit
                  their university.
                </p>
                <Link href="/">
                  <a className="d-flex align-items-center text-dark">
                    <span className="me-2">
                      To know more students can watch
                    </span>{" "}
                    <AnimatedYoutube />
                  </a>
                </Link>
                <Image
                  className="pt-3"
                  src={checklist6}
                  width={95}
                  height={110}
                  alt=""
                />
              </div>
            </Col>
            <Col md={4} className={`${styles.checklistPanel} text-center`}>
              <div className={`${styles.checklistDiv}`}>
                <Image src={mobile} width={120} height={120} alt="" />
                <p>#ChunoWahiJoHaiVerified</p>
              </div>
            </Col>
            <Col md={8} className={`${styles.checklistPanel}`}>
              <div className={`${styles.checklistDiv}`}>
                <h2 className="d-flex align-items-center">
                  <span className={`${styles.checklistCount}`}>10</span>
                  <span>Verify Your University</span>
                </h2>
                <p>
                  <TiTick className="text-success fs-2" /> Don&apos;t make a
                  decision that you might regret!
                </p>
                <p>
                  <TiTick className="text-success fs-2" /> Pick UGC-DEB stamped
                  universities only!
                </p>
                <Link href="/">
                  <a className="d-flex align-items-center text-dark">
                    <span className="me-2">
                      To know more students can watch
                    </span>{" "}
                    <AnimatedYoutube />
                  </a>
                </Link>
              </div>
            </Col>
          </Row>
        </div>
      </Container>
    </>
  );
};

export default CheckList;
