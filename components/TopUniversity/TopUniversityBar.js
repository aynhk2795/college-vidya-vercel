import React from "react";
import { Card, Row, Col } from "react-bootstrap";
import Link from "next/link";
import Image from "next/image";
import UniversitySlider from "/components/TopUniversity/Slider/UniversitySlider";
import Certificate from "/components/TopUniversity/Certificate";
import styles from "/styles/UniversitySlider.module.scss";
import Universitydetail from "/components/TopUniversity/Universitydetail";
import TopUniversityAccordion from "/components/TopUniversity/TopUniversityAccordion";
import logo from "/public/images/logo-univ.jpeg";
import { generate } from "shortid";

const topuniversitylist = [
  {
    university_name: "Lovely Professional University Online",
    university_logo: logo,
    university_slider: <UniversitySlider />,
    university_detail: <Universitydetail />,
    university_certificate: <Certificate />,
    university_faq: <TopUniversityAccordion />,
  },
  {
    university_name: "Lovely Professional University Online",
    university_logo: logo,
    university_slider: <UniversitySlider />,
    university_detail: <Universitydetail />,
    university_certificate: <Certificate />,
    university_faq: <TopUniversityAccordion />,
  },
  {
    university_name: "Lovely Professional University Online",
    university_logo: logo,
    university_slider: <UniversitySlider />,
    university_detail: <Universitydetail />,
    university_certificate: <Certificate />,
    university_faq: <TopUniversityAccordion />,
  },
];

const TopUniversityBar = ({ topUniversityList }) => {
  return (
    <>
      {topUniversityList.map((list) => (
        <Card className="p-3 mb-3" key={generate()}>
          <p>{list.university_name}</p>
          <Row>
            <Col md={4} className="position-relative">
              <div
                className={`${styles.logoWrapper} position-absolute`}
                style={{ zIndex: "1" }}>
                <Image
                  src={list.university.logo}
                  width={100}
                  height={35}
                  className="rounded"
                  alt=""
                />
              </div>
              <UniversitySlider />
              <Link href="">
                <a
                  className={`${styles.addtocompare} shadow-1 w-100 d-flex justify-content-center text-white`}>
                  Add to Compare
                </a>
              </Link>
            </Col>
            <Col md={6} className="d-flex align-items-center">
              <Universitydetail
                courseFee={list.fee}
                approvals={list.university.approval_details}
                rating={list.university.avg_rating}
                pros={list.university.universities_pros_cons.filter(
                  (pro_con) => pro_con.universities_pros_cons_title === 1
                )}
                universityName={list.university.name}
              />
            </Col>
            <Col md={2}>
              <Certificate
                sampleCertificateLink={list.university.sample_certificate}
              />
            </Col>
          </Row>
          {list.university_faq}
        </Card>
      ))}
    </>
  );
};

export default TopUniversityBar;
