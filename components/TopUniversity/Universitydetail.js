import React from "react";
import Youtube from "./Modal/Youtube";
import Podcast from "/components/TopUniversity/Modal/Podcast";

const Universitydetail = ({
  courseFee,
  approvals,
  rating,
  pros,
  universityName,
}) => {
  return (
    <>
      <ul className="list-unstyled">
        <li className="mb-2 border-bottom py-1">
          <span className="fw-bold">Fee : </span>
          <span>₹{courseFee.toLocaleString()}</span>
        </li>
        <li className="mb-2 border-bottom py-1">
          <span className="fw-bold">Approvals : </span>
          <span>{approvals.map((approval) => approval.title).join(", ")}</span>
        </li>
        {/* <li className="mb-2 border-bottom py-1">
          <span className="fw-bold">College Vidya Score : </span>
          <span>9.9</span>
        </li> */}
        <li className="mb-2 border-bottom py-1">
          <span className="fw-bold">University Rating : </span>
          <span>{rating ? rating : 0}/5</span>
        </li>
        <li className="mb-2 border-bottom py-1">
          <span className="fw-bold">Pros : </span>
          <span>{pros.map((pro) => pro.content).join(", ")}</span>
        </li>
        <li className="mb-2 border-bottom py-1 d-flex align-items-center">
          <span className="fw-bold me-2">Listen Podcast </span>

          <span>
            <Podcast universityName={universityName} />
          </span>
        </li>
        <li className="mb-2 py-1 d-flex align-items-center">
          <span className="fw-bold me-2">Watch video </span>
          <span>
            <Youtube universityName={universityName} />
          </span>
        </li>
      </ul>
    </>
  );
};

export default Universitydetail;
