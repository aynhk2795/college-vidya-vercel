import React from "react";
import { AiFillEye } from "react-icons/ai";
import { Image } from "primereact/image";
import styles from "/styles/Certificate.module.scss";

const Certificate = ({ sampleCertificateLink }) => {
  return (
    <>
      <Image
        className={`${styles.certificate}`}
        src={sampleCertificateLink}
        alt=""
        downloadable
        
        preview
      />
      <p className="fs-12 text-center">
        <AiFillEye /> View Sample Certificate
      </p>
    </>
  );
};

export default Certificate;
