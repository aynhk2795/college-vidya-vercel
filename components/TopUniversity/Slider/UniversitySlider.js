import React from "react";
import Image from "next/image";
import { Carousel } from "react-bootstrap";
import slide1 from "/public/images/Top-University-NMIMS.jpeg";
import slide2 from "/public/images/Top-University-Lovely-Professional-University.jpeg";
import logo from "/public/images/logo-univ.jpeg";
import styles from "/styles/UniversitySlider.module.scss";
import Link from "next/link";

const UniversitySlider = () => {
  return (
    <>
      <Carousel indicators={false} touch>
        <Carousel.Item className="rounded">
          <Image src={slide1} className="d-block w-100 rounded" alt="" />
        </Carousel.Item>
        <Carousel.Item className="rounded">
          <Image src={slide2} className="d-block w-100 rounded" alt="" />
        </Carousel.Item>
        <Carousel.Item className="rounded">
          <Image src={slide1} className="d-block w-100 rounded" alt="" />
        </Carousel.Item>
      </Carousel>
    </>
  );
};

export default UniversitySlider;
