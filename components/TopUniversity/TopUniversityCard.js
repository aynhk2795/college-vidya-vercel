import Image from 'next/image';
import React from 'react'
import { Container , Row , Col } from 'react-bootstrap'
import RatingStar from '/components/CoursePage/RatingStar';
import UniversitySlider from "/components/TopUniversity/Slider/UniversitySlider";
import logo from "/public/images/NMIMS.jpeg"

import { AiOutlineVideoCamera ,AiOutlineEye } from "react-icons/ai"
import AddToCompare from "/components/universityListingPage/Modal/AddToCompare";
import Youtube from './Modal/Youtube';
import Podcast from './Modal/Podcast';
import Certificate from "/components/TopUniversity/Certificate";
import More from './Modal/More';



const TopUniversityCard = ({ screenSize,  courseFee,
   approvals,
   rating,
   pros,
   universityName, }) => {
  return (
    <>
          <Col style={{maxWidth:"420px"}}>
                <div className={`top_card shadow-2 rounded mb-4 bg-white`}>
                   <div className='position-relative rounded overflow-hidden'>
                   <div
                className={`position-absolute ms-2 mt-2 bg-white rounded shadow-1`}
                style={{ zIndex: "1"}}>
                <Image
                  src={logo}
                  width={100}
                  height={35}
                  className="rounded"
                  
                  objectFit='contain'
                  alt=""
                />
              </div>
                      <UniversitySlider />
                   </div>
                   <div className='px-3 py-2'>
                   
                   <p className='mt-1 mb-2 fw-bold'>Lovely Professional University Online</p>
                <div className='d-flex align-items-center justify-content-between'>
                <RatingStar value={3} />
                   {/* <p className='fs-12 text-end'> Sample Certificate <AiOutlineEye/></p> */}
                   <Certificate/>
                </div>
                      <div className='d-flex justify-content-between align-items-center gap-2 mt-3'>
                          {/* <Col className='d-flex justify-content-center align-items-center col fs-12 text-center py-2 rounded' style={{backgroundColor:"aliceblue"}}>60,000</Col> */}
                          <Col className='d-flex justify-content-center align-items-center col fs-12 text-center py-2 rounded' style={{backgroundColor:"aliceblue"}}> <Podcast /> </Col>
                          <Col className='d-flex justify-content-center align-items-center col fs-12 text-center py-2 rounded' style={{backgroundColor:"aliceblue"}}> <Youtube universityName={universityName} /></Col>
                          
                      </div>
                      
                         <div className='mt-3'>
                         <p className='ps-2 h6 textprimary'>₹11,224</p> 
                         <p className='fs-14 text-secondary ps-2 d-flex align-items-center'>AIU, AICTE, NAAC+, UGC-DEB 
                          {/* <span className='fs-10 px-2 py-0 ms-2' style={{backgroundColor:"#f5f5dc",borderRadius:"25px"}}>Approvals</span> */}
                          </p>
                         </div>
                          <hr/>
                         <div className='d-flex align-items-center justify-content-between ps-1'>
                         <AddToCompare screenSize={screenSize} />
                       
                         <More/>
                         </div>
                         
                   </div>
                     
                      </div>
                </Col>
    </>
  )
}

export default TopUniversityCard