import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { BsSearch } from "react-icons/bs";
import Form from "react-bootstrap/Form";
import AnimatedPodcast from "/components/Global/AnimatedPodcast";
import { MdPodcasts } from "react-icons/md"

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {props.universityName}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="d-flex flex-wrap justify-content-center">
        Podcast here...
      </Modal.Body>
    </Modal>
  );
}

function Podcast({ universityName }) {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
      <span
        onClick={() => setModalShow(true)}
        className="fw-bold me-2 d-flex align-items-center cursor-pointer">
        {/* <AnimatedPodcast /> */}
         <MdPodcasts className='me-1' />Listen Podcast
      </span>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        universityName={universityName}
      />
    </>
  );
}

export default Podcast;
