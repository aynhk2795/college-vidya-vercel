import Image from 'next/image';
import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Offcanvas from 'react-bootstrap/Offcanvas';
import UniversityDetailTabHeading from '../../universityListingPage/Modal/UniversityDetailTabHeading';
import logo from "/public/images/NMIMS.jpeg"



function More() {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

  return (
    <>
      <span
        onClick={handleShow}
        className='fs-12 m-0 textsecondary cursor-pointer' >
       More+
      </span>

      <Offcanvas show={show} onHide={handleClose} placement='end'>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>
          <div
                className={`bg-white rounded border d-flex align-items-center p-1`}
                style={{ zIndex: "1"}}>
                <Image
                  src={logo}
                  width={100}
                  height={35}
                  className="rounded"
                  
                  objectFit='contain'
                  alt=""
                />
              </div>
          </Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
        <h4 className='fw-bold'>Lovely Professional University Online</h4>
      <div className='mb-2 px-2 rounded'>
      <UniversityDetailTabHeading title="College Details" />
      
      
      <ol className='ps-3'>
      <li className='mb-3'>Approval: UGC-DEB, NAAC A+</li>
<li className='mb-3'>Year of Establishment: 1981</li>
<li className='mb-3'>Mode of Learning: Distance &amp; Online</li>
      </ol>
      </div>
<div className='px-2 rounded'>

<UniversityDetailTabHeading title="Our Take" />
<ol className='ps-3'>
<li className='mb-3'>The quality of education at NMIMS Distance Education is top-notch </li>
<li className='mb-3'>NMIMS Distance has better placement assistance </li>
<li className='mb-3'>Good Hiring Partners at high salary package </li>
<li className='mb-3'>Upgrade Learning Management System</li>
</ol>
</div>
<div className='position-fixed bottom-0'>
  add to compare
</div>
        </Offcanvas.Body>
      
      </Offcanvas>
    </>
  );
}

export default More;
