import React from "react";
import Modal from "react-bootstrap/Modal";
import { AiOutlineVideoCamera } from "react-icons/ai";
import AnimatedYoutube from "/components/Global/AnimatedYoutube";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {props.universityName}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="d-flex flex-wrap justify-content-center">
        Youtube here...
      </Modal.Body>
    </Modal>
  );
}

function Youtube({ universityName }) {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
      {/* <BsSearch variant="primary" onClick={() => setModalShow(true)} /> */}
      <span
        onClick={() => setModalShow(true)}
        className="cursor-pointer">
        {/* <AnimatedYoutube /> */}
        <AiOutlineVideoCamera className='me-1' /> Watch Video
      </span>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        universityName={universityName}
      />
    </>
  );
}

export default Youtube;
