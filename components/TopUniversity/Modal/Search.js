import React from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { BsSearch } from "react-icons/bs";
import Form from "react-bootstrap/Form";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Select Specialisation
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="d-flex flex-wrap justify-content-center">
        <Form.Group
          className="mb-2 me-2 bg-grey px-3 rounded py-1"
          controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Marketing And HR Management " />
        </Form.Group>
        <Form.Group
          className="mb-2 me-2 bg-grey px-3 rounded py-3"
          controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Marketing And HR Management " />
        </Form.Group>
        <Form.Group
          className="mb-2 me-2 bg-grey px-3 rounded py-3"
          controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Marketing And HR Management " />
        </Form.Group>
        <Form.Group
          className="mb-2 me-2 bg-grey px-3 rounded py-3"
          controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Marketing And HR Management " />
        </Form.Group>
        <Form.Group
          className="mb-2 me-2 bg-grey px-3 rounded py-3"
          controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Marketing And HR Management " />
        </Form.Group>
        <Form.Group
          className="mb-2 me-2 bg-grey px-3 rounded py-3"
          controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Marketing And HR Management " />
        </Form.Group>
        <Form.Group
          className="mb-2 me-2 bg-grey px-3 rounded py-3"
          controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Marketing And HR Management " />
        </Form.Group>
        <Form.Group
          className="mb-2 me-2 bg-grey px-3 rounded py-3"
          controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Marketing And HR Management " />
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary">Search</Button>
      </Modal.Footer>
    </Modal>
  );
}

function Search() {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <>
      <BsSearch variant="primary" onClick={() => setModalShow(true)} />

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
}

export default Search;
