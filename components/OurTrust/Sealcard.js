import React from "react";
import { Col } from "react-bootstrap";
import { Card } from "primereact/card";
import { AiOutlineSafetyCertificate, AiOutlineSmile } from "react-icons/ai";
import { BiRupee } from "react-icons/bi";
import { FaUniversity } from "react-icons/fa";

const sealcardlist = [
  {
    id: 1,
    icon: <AiOutlineSafetyCertificate fontSize={50} />,
    title: "secured",
    content: "We Don’t Share or Sell your personal Information.",
  },
  {
    id: 2,
    icon: <BiRupee fontSize={50} />,
    title: "No Hidden Fees",
    content:
      "Comparing Distance & Online Universities is absolutely free. No fee is charged at any stage.",
  },
  {
    id: 3,
    icon: <AiOutlineSmile fontSize={50} />,
    title: "Ease of Use",
    content: "The Interface of College Vidya Compare is hassle free and fast.",
  },
  {
    id: 4,
    icon: <FaUniversity fontSize={50} />,
    title: "Seal of Trust",
    content:
      "Information on College Vidya Compare is trusted by all listed universities.",
  },
];

const Sealcard = () => {
  return (
    <>
      {sealcardlist.map((list) => (
        <Col key={list.id} md={3}>
          <Card className="text-center my-2">
            {list.icon}
            <p className="fw-bold">{list.title}</p>
            <p>{list.content}</p>
          </Card>
        </Col>
      ))}
    </>
  );
};

export default Sealcard;
