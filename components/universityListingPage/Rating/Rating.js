import { Rating } from "primereact/rating";

const Ratings = ({ reviewRating }) => {
  return (
    <>
      <Rating value={reviewRating} readOnly stars={5} cancel={false} />
    </>
  );
};

export default Ratings;
