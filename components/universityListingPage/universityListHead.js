import React from "react";
import FeeDropdown from "/components/universityListingPage/Dropdown/FeeDropdown";
import CollegeVidyaScore from "/components/universityListingPage/Dropdown/collegeVidyaScore";
import StudentAvgRating from "/components/universityListingPage/Dropdown/StudentAvgRating";
import styles from "./universityListing.module.scss";

const UniversityListHead = ({ screenSize }) => {
  return (
    <>
      <div
        className={`${styles.universitylist_heads} row row-cols-6 sticky-top mb-3`}>
        <div
          className={`${styles.universityListHead} col text-center py-2 rounded border fs-12 text-truncate`}>
          Unversity
        </div>
        <div
          className={`${styles.universityListHead} col text-center py-2 rounded border fs-12 text-truncate`}>
          Approvals
        </div>
        <div
          className={`${styles.universityListHead} col text-center py-2 rounded border fs-12 text-truncate`}>
          {/* <FeeDropdown /> */}
          NAAC Score
        </div>

        <div
          className={`${styles.universityListHead} col text-center py-2 rounded border fs-12 text-truncate`}>
          {/* <StudentAvgRating screenSize={screenSize} /> */}
          Student Rating
        </div>
        <div
          className={`${styles.universityListHead} col text-center py-2 rounded border fs-12 text-truncate`}>
          {screenSize.width < 600 ? "Proceed" : "Proceed to University"}
        </div>
        <div
          className={`${styles.universityListHead} col text-center py-2 rounded border fs-12 text-truncate`}>
          {/* <CollegeVidyaScore /> */}
          CV Advisor
        </div>
      </div>
    </>
  );
};

export default UniversityListHead;
