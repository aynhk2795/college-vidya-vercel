import React from "react";
import { Card, Button } from "react-bootstrap";
import SidebarHeading from "/components/universityListingPage/Sidebar/SidebarHeading";
import { FaBalanceScale, FaGraduationCap } from "react-icons/fa";
import {
  AiFillClockCircle,
  AiFillSafetyCertificate,
  AiOutlineUsergroupAdd,
} from "react-icons/ai";

import Image from "next/image";
import student from "/public/images/student.jpeg";
import WhyCv from "/components/universityListingPage/Modal/WhyCv";
import Link from "next/link";
import Checklist from "../Modal/Checklist";

const CVAdvantage = () => {
  return (
    <>
      <Card className="p-3 shadow-1 mb-3">
        <SidebarHeading heading="College Vidya Advantages" />
        <ul className="list-unstyled">
          <li className="d-flex mb-2 fs-14 align-items-center">
            <FaBalanceScale className="me-2" />
            Unbiased Counselling
          </li>
          <li className="d-flex mb-2 fs-14 align-items-center">
            <FaGraduationCap className="me-2" />
            Only University with valid approvals
          </li>
          <li className="d-flex mb-2 fs-14 align-items-center">
            <AiFillClockCircle className="me-2" />
            Full Time Degree Assistance{" "}
          </li>
          <li className="d-flex mb-2 fs-14 align-items-center">
            <AiOutlineUsergroupAdd className="me-2" />
            Face to Face Counseling{" "}
          </li>
          <li className="d-flex mb-2 fs-14 align-items-center">
            <AiFillSafetyCertificate className="me-2" />
            Certified Educational Counselors
          </li>
        </ul>

        <Checklist fromChecklist={true} />

        <Image src={student} alt="" objectFit="contain" />
      </Card>
    </>
  );
};

export default CVAdvantage;
