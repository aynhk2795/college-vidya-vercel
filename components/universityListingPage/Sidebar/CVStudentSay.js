import React from "react";
import { Card, Row, Col } from "react-bootstrap";
import Image from "next/image";
import student from "/public/images/experts/expert-team1.png";
import { RiDoubleQuotesL, RiDoubleQuotesR } from "react-icons/ri";
import linkedin from "/public/images/linkdin.svg";
import styles from "/styles/cvstudentsay.module.scss";
import { FcRightUp2 } from "react-icons/fc";
import StudentSay from "/components/universityListingPage/Modal/StudentSay";
// import UniversityDetail from "/components/universityListingPage/Modal/UniversityDetail";

const CVStudentSay = () => {
  return (
    <>
      <Card className="p-3 shadow-1 mb-3">
        <Row>
          <Col md={6} className="pe-0 col">
            <p>
              <sup>
                <RiDoubleQuotesL fontSize={20} />
              </sup>
              <span className="fw-normal fs-14"> See what our</span> <br />
              <span className="fs-20 fw-bold bg-grey"> Students Say </span>
              <sub>
                <RiDoubleQuotesR fontSize={20} />
              </sub>
            </p>
            <div className="mb-3">
              <StudentSay />
            </div>

            {/* <UniversityDetail /> */}
          </Col>
          <Col md={6} className="ps-0 col">
            <Card className={`${styles.studentsayCard} rounded`}>
              <Image
                className="position-absolute top-0 start-50 translate-middle"
                src={linkedin}
                alt=""
              />
              <div className="p-2 pt-0">
                <p className="mb-1 fs-10 fw-bold d-flex align-items-center">
                  <Image
                    src={student}
                    width={25}
                    height={25}
                    className="rounded-circle"
                    alt=""
                  />{" "}
                  <span className="ms-2"> Harish D V N </span>
                </p>
                <p className="m-0 fs-10">
                  Probably the best thing I have done in recent times is to.{" "}
                  <FcRightUp2 fontSize={12} />
                </p>
              </div>
            </Card>
          </Col>
        </Row>
      </Card>
    </>
  );
};

export default CVStudentSay;
