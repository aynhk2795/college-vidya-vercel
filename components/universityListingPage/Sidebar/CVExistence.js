import React from "react";
import { Card, Row, Col } from "react-bootstrap";
import SidebarHeading from "/components/universityListingPage/Sidebar/SidebarHeading";
import Image from "next/image";
import Assistance from "/components/universityListingPage/Modal/Assistance";
import student from "/public/images/experts/expert-team1.png";
import student1 from "/public/images/experts/expert-team2.jpeg";
import student2 from "/public/images/experts/expert-team4.jpeg";
import { BsFillTelephoneFill } from "react-icons/bs";
import { GoLocation } from "react-icons/go";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "swiper/css/autoplay";
import { EffectFade, Navigation, Autoplay } from "swiper";

const Appcard = [
  {
    id: 1,
    location: "Bhopal",
    image: student,
    name1: "Mr.Manish",
    content1: "claim",
    desc1: " Beneficiary",
    name2: "Mr.kumars",
    content2: "claims",
    desc2: " Beneficiarys",
  },
  {
    id: 2,
    location: "Mumbai",
    image: student1,
    name1: "Mr.Sarthak",
    content1: "claim",
    desc1: " Beneficiary",
    name2: "Mrs.kumars",
    content2: "claims",
    desc2: " Beneficiarys",
  },
  {
    id: 3,
    location: "Delhi",
    image: student2,
    name1: "Mr.Sankalp",
    content1: "claim",
    desc1: " Beneficiary",
    name2: "Mrs.kumars",
    content2: "claims",
    desc2: " Beneficiarys",
  },
];
const CVExistence = () => {
  return (
    <>
      <Card className="p-3 shadow-1 mb-3">
        <SidebarHeading heading="Free Dedicated Assistance" />

        <Row>
          <Col md={12}>
            <Swiper
              slidesPerView={3}
              effect={"fade"}
              spaceBetween={20}
              autoplay={{
                delay: 1500,
                disableOnInteraction: false,
              }}
              loop={true}
              navigation={false}
              breakpoints={{
                "@0.00": {
                  slidesPerView: 1,
                },
                "@0.75": {
                  slidesPerView: 1,
                },
                "@1.00": {
                  slidesPerView: 1,
                },
              }}
              modules={[Navigation, EffectFade, Autoplay]}
              className="mediaSwiper">
              {Appcard.map((list) => (
                <SwiperSlide key={list.id}>
                  <Card className="p-2 mb-3">
                    <Row className="align-items-center">
                      <Col md={4} className="col">
                        <div className="rounded">
                          <Image src={list.image} alt="" />
                        </div>
                      </Col>
                      <Col md={4} className="col">
                        <p className="fw-bold m-0 fs-10">{list.name1}</p>
                        <p className="m-0 fs-10">{list.content1}</p>
                        <p className="m-0 fs-10">{list.desc1}</p>
                      </Col>
                      <Col md={4}>
                        <p className="fw-bold m-0 fs-10">{list.name2}</p>
                        <p className="m-0 fs-10">{list.content2}</p>
                        <p className="m-0 fs-10">{list.desc2}</p>
                        <p className="m-0 fs-10 position-absolute end-0 top-0 bg-grey px-2 py-1">
                          <GoLocation className="me-2" /> {list.location}
                        </p>
                      </Col>
                    </Row>
                  </Card>
                </SwiperSlide>
              ))}
            </Swiper>
          </Col>
        </Row>

        <Row className="align-items-center">
          <Col md={5}>
            <div className="mb-3">
              <Assistance />
            </div>
          </Col>
          <Col md={7}>
            <p className="fs-10 m-0 bg-grey rounded text-center py-2">
              {" "}
              <BsFillTelephoneFill fontSize={10} /> Helpline: 1800-420-5757
            </p>
          </Col>
        </Row>

        {/* <Image src={cvedge} /> */}
      </Card>
    </>
  );
};

export default CVExistence;
