import React from "react";

const SidebarHeading = (props) => {
  return (
    <>
      <h6 className="mb-3">{props.heading}</h6>
    </>
  );
};

export default SidebarHeading;
