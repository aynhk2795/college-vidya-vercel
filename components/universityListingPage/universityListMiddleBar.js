import Image from "next/image";
import React from "react";
import { Row, Col, Button } from "react-bootstrap";
import pablo from "/public/images/pablo.png";

const UniversityListMiddleBar = () => {
  return (
    <>
      <Row
        className="d-flex align-items-center rounded my-4 py-2"
        style={{ backgroundColor: "aliceblue" }}>
        <Col md={2}>
          <Image src={pablo} alt="" />
        </Col>
        <Col md={8}>
          <h3>What is College Vidya ⭐️ Rating?</h3>
          <p>
            College Vidya has more than 2500+ mentors who work closely with you
            to give a realistic viewpoint about your career goals and enable you
            to build a successful career.
          </p>
        </Col>
        <Col md={2} className="text-center">
          <Button className="bgprimary">Know More </Button>
        </Col>
      </Row>
    </>
  );
};

export default UniversityListMiddleBar;
