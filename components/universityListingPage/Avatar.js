import React from "react";
import Image from "next/image";
import avatar from "/public/images/poojasingh.jpeg";
import { GoPrimitiveDot } from "react-icons/go";

const Avatar = () => {
  return (
    <>
      <div
        className="position-relative rounded-circle border-1"
        style={{ width: "58px", height: "58px" }}>
        <Image
          src={avatar}
          className="rounded-circle border border-light"
          width={55}
          height={56}
          alt=""></Image>
        <GoPrimitiveDot
          fontSize={20}
          style={{ color: "#00d8a1", bottom: "0px", right: "3px" }}
          className="position-absolute"
        />
      </div>
    </>
  );
};

export default Avatar;
