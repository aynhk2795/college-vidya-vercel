import React from "react";
import { Card } from "primereact/card";
import { Row, Col } from "react-bootstrap";
import Image from "next/image";
import myimage from "/public/images/experts/expert-team1.png";
import { GoLocation } from "react-icons/go";

const TruthCard = () => {
  return (
    <>
      <Card className="my-3">
        <Row>
          <Col md={3} className="text-center">
            <Image src={myimage} alt="" />
            <p className="fw-bold fs-14 m-0">Mrs. Chouhan</p>
            <p className="fs-12 m-0">Claim</p>
            <p className="fs-12 m-0 bg-grey rounded text-dark">
              {" "}
              <GoLocation />
              Bhopal
            </p>
          </Col>
          <Col md={9}>
            <p className="fs-12 mb-2">
              Mr. Chouhan was working with BHEL in Bhopal.
            </p>
            <p className="fs-12 mb-2">
              To take care of the financial future of his wife and 2 sons, he
              purchased an Edelweiss Tokio Term insurance plan from Policybazaar
              in May 2019.
            </p>
            <p className="fs-12 mb-2">
              In May 2021, during the 2nd wave of Covid, he turned Covid
              positive and passed away eventually.
            </p>
            <p className="fs-12 mb-2">
              Mr. Chouhan is survived by his wife and 2 sons. Mrs. Chouhan
              reached out to Policybazaar regarding assistance with the claim.
              Policybazaar team immediately connected with Edelweiss Tokio Life
              and assisted her with the documents, process and was continuously
              in touch with her till the claim amount of Rs. 1.25 Crore got
              disbursed to them.
            </p>
            <p className="fs-12 mb-2">
              In May 2021, during the 2nd wave of Covid, he turned Covid
              positive and passed away eventually.
            </p>
          </Col>
        </Row>
      </Card>
      <Card className="my-3">
        <Row>
          <Col md={3} className="text-center">
            <Image src={myimage} alt="" />
            <p className="fw-bold fs-14 m-0">Mrs. Chouhan</p>
            <p className="fs-12 m-0">Claim</p>
            <p className="fs-12 m-0 bg-grey rounded text-dark">
              {" "}
              <GoLocation />
              Bhopal
            </p>
          </Col>
          <Col md={9}>
            <p className="fs-12 mb-2">
              Mr. Chouhan was working with BHEL in Bhopal.
            </p>
            <p className="fs-12 mb-2">
              To take care of the financial future of his wife and 2 sons, he
              purchased an Edelweiss Tokio Term insurance plan from Policybazaar
              in May 2019.
            </p>
            <p className="fs-12 mb-2">
              In May 2021, during the 2nd wave of Covid, he turned Covid
              positive and passed away eventually.
            </p>
            <p className="fs-12 mb-2">
              Mr. Chouhan is survived by his wife and 2 sons. Mrs. Chouhan
              reached out to Policybazaar regarding assistance with the claim.
              Policybazaar team immediately connected with Edelweiss Tokio Life
              and assisted her with the documents, process and was continuously
              in touch with her till the claim amount of Rs. 1.25 Crore got
              disbursed to them.
            </p>
            <p className="fs-12 mb-2">
              In May 2021, during the 2nd wave of Covid, he turned Covid
              positive and passed away eventually.
            </p>
          </Col>
        </Row>
      </Card>
    </>
  );
};

export default TruthCard;
