import React from "react";
import { AiOutlineRight } from "react-icons/ai";
import { Modal } from "react-bootstrap";
import styles from "/styles/ProsCons.module.scss";
import Right from "/components/Global/Icon/Right";
import Wrong from "/components/Global/Icon/Wrong";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      show={props.show}
      onHide={() => props.onHide()}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Pros &amp; Cons
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={`${styles.prosconsModal}`}>
        {props.pros_cons.some(
          (both) => both.universities_pros_cons_title === 1
        ) ? (
          <div className={`${styles.prosdiv}`}>
            <h3>
              <Right />
              Pros
            </h3>
            {props.pros_cons
              .filter((both) => both.universities_pros_cons_title === 1)
              .map((pro) => (
                <p key={pro.id}>{pro.content}</p>
              ))}
          </div>
        ) : null}
        {props.pros_cons.some(
          (both) => both.universities_pros_cons_title === 2
        ) ? (
          <div className={`${styles.consdiv} mt-4`}>
            <h3>
              <Wrong />
              Cons
            </h3>
            {props.pros_cons
              .filter((both) => both.universities_pros_cons_title === 2)
              .map((con) => (
                <p key={con.id}>{con.content}</p>
              ))}
          </div>
        ) : null}
      </Modal.Body>
    </Modal>
  );
}
const ProsCons = ({ pros_cons }) => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <p className="m-0 cursor-pointer" onClick={() => setModalShow(true)}>
        Pros &amp; Cons <AiOutlineRight />
      </p>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        pros_cons={pros_cons}
      />
    </>
  );
};

export default ProsCons;
