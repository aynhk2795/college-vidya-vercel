import React from "react";
import { AiOutlineRight } from "react-icons/ai";
import { Modal } from "react-bootstrap";
import styles from "./approval.module.scss";
import AnimatedYoutube from "/components/Global/AnimatedYoutube";
import Link from "next/link";
import Image from "next/image";
import ugc from "/public/images/deb_logo.webp";
import naac from "/public/images/approvals/approval_3.png";
import nirf from "/public/images/approvals/nirf_logo.webp";
import aictc from "/public/images/approvals/approval_4.png";
import aiu from "/public/images/approvals/approval_1.png";
import verify from "/public/images/approvals/mobile-phone.png";
import OnlyLogo from "../../Global/OnlyLogo";
import TagLine from "/components/Global/TagLine";
import AnimatedBird from "../../Global/AnimatedBird";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      className={`${styles.approvalModal}`}
      centered>
      <Modal.Header closeButton>
        <Modal.Title
          id="contained-modal-title-vcenter"
          className="d-flex justify-content-center align-items-center fs-16 mx-auto">
          <OnlyLogo />
          #ChunoWahiJoHaiSahi <AnimatedBird />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={`${styles.approvalModal}`}>
        {/* <p className='text-center'>Check out these must-have approvals to secure your future</p> */}
        <div className="d-flex align-items-center justify-content-center mb-4 mt-2">
          <span className="me-2">
            {" "}
            Check out these must-have approvals to secure your future{" "}
          </span>
          <Link href="/">
            <a>
              <AnimatedYoutube />
            </a>
          </Link>
        </div>
        <div className={`${styles.approvalPanel} card p-4 mb-3`}>
          <h5 className="d-flex align-items-center">
            <Image src={ugc} alt="" /> UGC (University Grants Commission)
          </h5>
          <ul>
            <li>
              UGC is responsible to maintain the standards of Higher Education
              in India
            </li>
            <li>
              UGC approval helps in identifying fake and authentic universities.
            </li>
            <li>
              UGC approval is a must. Without this, your degree is not valid
            </li>
          </ul>
          <p>DEB (Distance Education Bureau)</p>
          <ul>
            <li>
              DEB under UGC is the regulatory body that regulates all the
              Distance &amp; Online Education Programs.
            </li>
            <li>
              The university needs UGC-DEB approval to provide distance learning
              courses.
            </li>
            <li>
              Only 10% of the total universities in India, is allowed by DEB to
              provide Online Degree Courses
            </li>
          </ul>
        </div>
        <div className={`${styles.approvalPanel} card p-4 mb-3`}>
          <h5 className="d-flex align-items-center">
            <Image src={naac} alt="" width={60} height={60} /> NAAC(National
            Assessment and Accreditation Council)
          </h5>
          <ul>
            <li>
              NAAC’s prime agenda is to assess the quality of education in
              Higher Education Institutes and then provide accreditation.
            </li>
            <li>
              NAAC provides students with information about the quality of
              institutes.
            </li>
            <li>
              NAAC provides full-fledged reviews to universities about
              strengths, weaknesses, and opportunities that help institutions to
              improve.
            </li>
            <li>
              NAAC accreditation ranges from A++ to C. A++ is the maximum grade
            </li>
          </ul>
        </div>
        <div className={`${styles.approvalPanel} card p-4 mb-3`}>
          <h5 className="d-flex align-items-center">
            <Image src={nirf} alt="" /> NIRF Ranking (National Institutional
            Ranking Framework)
          </h5>
          <ul>
            <li>
              NIRF assess the teaching methodology of institutions and then
              provides them ranking accordingly.
            </li>
            <li>
              NIRF ranking helps students identify the best university amongst
              thousands.
            </li>
            <li>
              NIRF provides ranking as per the 11 different categories and the
              ranking helps students know where the university stands in
              comparison to other universities in the same category.
            </li>
          </ul>
        </div>
        <div className={`${styles.approvalPanel} card p-4 mb-3`}>
          <h5 className="d-flex align-items-center">
            <Image src={aictc} width={60} height={60} alt="" /> AICTE (All India
            Council of Technical Education)
          </h5>
          <ul>
            <li>
              AICTE approval is mandatory for technical institutes to offer
              courses that come under AICTE.
            </li>
            <li>
              The degrees of engineering institutes (except IITs, NITs, etc.)
              are valid only if they have AICTE approval.
            </li>
          </ul>
        </div>
        <div className={`${styles.approvalPanel} card p-4 mb-3`}>
          <h5 className="d-flex align-items-center">
            <Image src={aiu} width={60} height={60} alt="" /> AIU (Association
            of Indian Universities)
          </h5>
          <ul>
            <li>
              AIU approval means your University Degree is valid across the
              Globe (Internationally accepted)
            </li>
          </ul>
        </div>
        <div className={`${styles.approvalPanel} card p-4 mb-3`}>
          <h5 className="d-flex align-items-center">
            <Image src={verify} width={60} height={60} alt="" /> Verify Your
            University
          </h5>
          <ul>
            <li>Don&apos;t make a decision that you might regret!</li>
            <li>Pick UGC-DEB stamped universities only!</li>
          </ul>
        </div>
      </Modal.Body>
    </Modal>
  );
}
const Approvals = () => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <p className="m-0" onClick={() => setModalShow(true)}>
        Click to know <AiOutlineRight />
      </p>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
};

export default Approvals;
