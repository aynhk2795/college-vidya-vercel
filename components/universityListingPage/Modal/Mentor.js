import Link from "next/link";
import React from "react";
import { Modal } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import Image from "next/image";
import {
  AiOutlineRight,
  AiOutlineSafety,
  AiOutlineSchedule,
} from "react-icons/ai";
import {
  FcMoneyTransfer,
  FcCustomerSupport,
  FcConferenceCall,
  FcInspection,
  FcBusinessman,
  FcAdvance,
} from "react-icons/fc";
import CircularIcon from "/components/Global/Icon/CircularIcon";
import mentor from "/public/images/mentor-modal.jpg";
import multi from "/public/images/multi.png";
import { BsArrowRight, BsPersonPlus } from "react-icons/bs";
import { Swiper, SwiperSlide } from "swiper/react";
import styles from "./Mentor.module.scss";
import { Autoplay } from "swiper";
import mentor1 from "/public/images/mentor-1.png";
import mentor2 from "/public/images/mentor-2.png";
import mentor3 from "/public/images/mentor-3.png";
import mentor4 from "/public/images/mentor-4.png";
import mentor5 from "/public/images/mentor-5.png";
import mentor6 from "/public/images/mentor-6.png";
import mentor7 from "/public/images/mentor-7.png";

// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-cards";
import { EffectCards } from "swiper";
import RatingStar from "../../CoursePage/RatingStar";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="md"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header
        closeButton
        className="position-absolute top-0 end-0 border-0"
        style={{ zIndex: "1111" }}></Modal.Header>
      <Modal.Body>
        <Row>
          <Col md={12} className="text-center">
            <Swiper
              loop={true}
              effect={"cards"}
              grabCursor={true}
              modules={[Autoplay, EffectCards]}
              autoplay={{
                delay: 1000,
                disableOnInteraction: false,
              }}
              className={`${styles.mentor_slider}`}>
              <SwiperSlide className="d-flex flex-column px-3">
                <Image src={mentor1} alt="" />
                <div className="mt-3">
                  <RatingStar value={3} />
                </div>
              </SwiperSlide>
              <SwiperSlide className="d-flex flex-column px-3">
                <Image src={mentor2} alt="" />
                <div className="mt-3">
                  <RatingStar value={4} />
                </div>
              </SwiperSlide>
              <SwiperSlide className="d-flex flex-column px-3">
                <Image src={mentor3} alt="" />
                <div className="mt-3">
                  <RatingStar value={4} />
                </div>
              </SwiperSlide>
              <SwiperSlide className="d-flex flex-column px-3">
                <Image src={mentor4} alt="" />
                <div className="mt-3">
                  <RatingStar value={3} />
                </div>
              </SwiperSlide>
              <SwiperSlide className="d-flex flex-column px-3">
                <Image src={mentor5} alt="" />
                <div className="mt-3">
                  <RatingStar value={3} />
                </div>
              </SwiperSlide>
              <SwiperSlide className="d-flex flex-column px-3">
                <Image src={mentor6} alt="" />
                <div className="mt-3">
                  <RatingStar value={4} />
                </div>
              </SwiperSlide>
              <SwiperSlide className="d-flex flex-column px-3">
                <Image src={mentor7} alt="" />
                <div className="mt-3">
                  <RatingStar value={3} />
                </div>
              </SwiperSlide>
            </Swiper>

            <p className="mt-4 textprimary fs-14 mb-2">
              Join 30,000+ Happy students!
            </p>
            <p>Make College decisions like a pro 😎</p>
            <Link href="">
              <a className="bgprimary p-4 text-center rounded d-block w-100 text-white myshine">
                Connect with Best Mentors <BsArrowRight className="ms-2" />
              </a>
            </Link>
          </Col>
          {/* <Col md={8}>
           <Link href=""><a>Connect with mentors</a></Link>
       </Col> */}
        </Row>
      </Modal.Body>
    </Modal>
  );
}
const Mentor = ({ fromFooter }) => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      {/* <p className="m-0 cursor-pointer" onClick={() => setModalShow(true)}>
        <Button variant="outline-primary fs-12">Why College Vidya ?</Button>
      </p> */}

      {/* <p
        className="textprimary fs-12 cursor-pointer"
        onClick={() => setModalShow(true)}>
        mentor <AiOutlineRight />
      </p> */}

      <div
        onClick={() => setModalShow(true)}
        className="cursor-pointer d-flex h-100 flex-column justify-content-center mentor_dialog">
        {fromFooter ? (
          <BsPersonPlus fontSize={20} />
        ) : (
          <>
            <FcBusinessman fontSize={30} className="mx-auto" />{" "}
            <p className="m-0 fs-12 textprimary">
              {" "}
              Discuss with <br />
              India&apos;s best mentors <FcAdvance />
            </p>
          </>
        )}
      </div>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
};

export default Mentor;
