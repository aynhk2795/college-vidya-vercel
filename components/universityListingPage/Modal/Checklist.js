import React from "react";
import { Modal } from "react-bootstrap";
import { Button } from "react-bootstrap";
import {
  AiOutlineRight,
  AiOutlineSafety,
  AiOutlineSchedule,
} from "react-icons/ai";
import {
  FcMoneyTransfer,
  FcCustomerSupport,
  FcConferenceCall,
  FcInspection,
} from "react-icons/fc";
import OnlyLogo from "../../Global/OnlyLogo";
import WhyTakeAdmission from "../../WhyCollegevidya/WhyTakeAdmission";
import CircularIcon from "/components/Global/Icon/CircularIcon";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter" className="fw-normal">
          <OnlyLogo />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <WhyTakeAdmission />
      </Modal.Body>
    </Modal>
  );
}
const Checklist = ({ fromChecklist }) => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      {/* <p className="m-0 cursor-pointer" onClick={() => setModalShow(true)}>
        <Button variant="outline-primary fs-12">Why College Vidya ?</Button>
      </p> */}

      {fromChecklist ? (
        <p
          className="btn btn-outline-primary fs-12 py-2"
          onClick={() => setModalShow(true)}>
          Why take admission from CV ?
        </p>
      ) : (
        <p className="fs-12 cursor-pointer" onClick={() => setModalShow(true)}>
          #ChunoWahiJoHaiSahi
        </p>
      )}

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
};

export default Checklist;
