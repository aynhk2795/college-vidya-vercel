import React from "react";
import { Modal } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import { MdBolt } from "react-icons/md";
import FilledCompareBox from "/components/universityListingPage/Modal/FilledCompareBox";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Proceed to University
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          <Col md={4} className="mb-4">
            <FilledCompareBox />
          </Col>
          <Col md={4} className="mb-4">
            <FilledCompareBox />
          </Col>
          <Col md={4} className="mb-4">
            <FilledCompareBox />
          </Col>
          <Col md={4} className="mb-4">
            <FilledCompareBox />
          </Col>
          <Col md={4} className="mb-4">
            <FilledCompareBox />
          </Col>
          <Col md={4} className="mb-4">
            <FilledCompareBox />
          </Col>
          <Col md={4} className="mb-4">
            <FilledCompareBox />
          </Col>
          <Col md={4} className="mb-4">
            <FilledCompareBox />
          </Col>
        </Row>
      </Modal.Body>
    </Modal>
  );
}
const ProceedToUniversity = () => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <p
        className="m-0 cursor-pointer textprimary"
        onClick={() => setModalShow(true)}>
        <MdBolt /> Proceed to University
      </p>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
};

export default ProceedToUniversity;
