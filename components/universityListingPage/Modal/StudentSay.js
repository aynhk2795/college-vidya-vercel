import React from "react";
import { Row, Modal } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { TabView, TabPanel } from "primereact/tabview";
import SocialMediaCard from "./SocialMediaCard";
import StudentVideo from "/components/universityListingPage/Modal/StudentVideo";

const Videolist = [{}];

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter" className="fw-normal">
          Our Students Says
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="studentsay-tabview">
          <TabView>
            <TabPanel header="Social Media">
              <SocialMediaCard />
            </TabPanel>
            <TabPanel header="video">
              <Row>
                <StudentVideo />
              </Row>
            </TabPanel>
          </TabView>
        </div>
      </Modal.Body>
    </Modal>
  );
}
const StudentSay = () => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <p className="m-0 cursor-pointer" onClick={() => setModalShow(true)}>
        <Button variant="outline-primary fs-12">Know More</Button>
      </p>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
};

export default StudentSay;
