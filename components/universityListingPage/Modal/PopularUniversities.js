import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import screen1 from "/public/images/media/hindustan-times.png";
import { Card } from "primereact/card";
import MainHeading from "../../Global/Heading/MainHeading";
import SubHeading from "../../Global/Heading/SubHeading";
import testimage1 from "/public/images/ucompare1.jpeg";
import testimage2 from "/public/images/ucompare2.jpeg";

import Link from "next/link";
import { generate } from "shortid";
import { useRouter } from "next/router";

export default function App({
  universityCombinations,
  courseSlug,
  specializationSlug,
}) {
  const router = useRouter();

  return (
    <>
      <div className="container my-5">
        <div className="row">
          <div className="col-md-12">
            <div className="mb-2">
              <SubHeading title="College Vidya" />
            </div>
            <div className="mb-4">
              <MainHeading title="Popular Universities Comparison" />
            </div>
          </div>
        </div>
        <Swiper
          slidesPerView={4}
          spaceBetween={20}
          loop={false}
          navigation={true}
          // pagination={{
          //   clickable: false,
          // }}
          pagination={false}
          breakpoints={{
            "@0.00": {
              slidesPerView: 1,
              // spaceBetween: 20,
            },
            500: {
              slidesPerView: 2,
              // spaceBetween: 20,
            },
            1024: {
              slidesPerView: 3,
              // spaceBetween: 20,
            },
            1336: {
              slidesPerView: 4,
              // spaceBetween: 20,
            },
          }}
          modules={[Navigation, Pagination]}
          className="university_comparison_swiper">
          {universityCombinations &&
            universityCombinations.map((combination) => (
              <SwiperSlide className="card position-relative" key={generate}>
                <Card className="p-0">
                  <div className="d-flex text-center position-relative">
                    <div
                      className="position-absolute bg-dark text-white rounded-circle p-2 start-50 translate-middle top-50 shadow-1"
                      style={{ zIndex: "1" }}>
                      V/S
                    </div>
                    <div className="w-50">
                      <Image
                        width={150}
                        height={127}
                        src={combination[0].university.logo}
                        alt=""
                      />
                      <p className="px-2 fs-12">
                        {combination[0].university.name}
                      </p>
                    </div>
                    <div className="w-50">
                      <Image
                        width={150}
                        height={127}
                        src={combination[1].university.logo}
                        alt=""
                      />
                      <p className="px-2 fs-12">
                        {combination[1].university.name}
                      </p>
                    </div>
                  </div>
                  {specializationSlug ? (
                    <a
                      href={`/compare/${courseSlug}/${specializationSlug}?check=${combination[0].university.slug}&check=${combination[1].university.slug}&uid=${router.query.uid}`}
                      className="bgprimary text-white text-center py-2 rounded shadow-1 mx-3 mt-3 d-block mb-3">
                      Compare
                    </a>
                  ) : (
                    <a
                      href={`/compare/${courseSlug}?check=${combination[0].university.slug}&check=${combination[1].university.slug}&uid=${router.query.uid}`}
                      className="bgprimary text-white text-center py-2 rounded shadow-1 mx-3 mt-3 d-block mb-3">
                      Compare
                    </a>
                  )}
                </Card>
              </SwiperSlide>
            ))}
        </Swiper>
      </div>
    </>
  );
}
