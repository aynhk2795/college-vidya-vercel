import React, { useState } from "react";
import { Dialog } from "primereact/dialog";
import { Button } from "primereact/button";
import logo from "/public/images/NMIMS.jpeg";
import UniversityDetailLogo from "/components/universityListingPage/Modal/UniversityDetailLogo";
import UniversityDetailTab from "/components/universityListingPage/Modal/UniversityDetailTab";
import { BsChevronRight } from "react-icons/bs";
import styles from "./UniversityDetail.module.scss";

const UniversityDetail = ({
  screenSize,
  aboutUniversity,
  universityLogo,
  universityName,
  prospectusLink,
  universityFacts = [],
  universityApprovals = [],
  sampleCertificateLink,
  universityFee,
  universityFeeDetails,
  LMSVideos = [],
  universityReviews = [],
  universityFAQs = [],
  loanFacility,
}) => {
  const [displayPosition, setDisplayPosition] = useState(false);

  const [position, setPosition] = useState("center");

  const dialogFuncMap = {
    displayPosition: setDisplayPosition,
  };

  const onClick = (name, position) => {
    dialogFuncMap[`${name}`](true);

    if (position) {
      setPosition(position);
    }
  };

  const onHide = (name) => {
    dialogFuncMap[`${name}`](false);
  };
  const renderFooter = (name) => {
    return (
      <div
        className={`${styles.universityDetail_footer} d-flex justify-content-between shadow-top py-3`}>
        <Button
          label={
            <div>
              <span className="h4 text-dark">₹{universityFee}</span>
              {/* <span className="text-dark">/Semester</span> */}
            </div>
          }
          className="p-button-text"
        />
        <Button
          label="Proceed to University"
          icon="pi pi-chevron-right"
          onClick={() => onHide(name)}
          autoFocus
          className={`${styles.proceed_btn} d-flex bgsecondary flex-row-reverse shadow-none border-0 text-truncate`}
        />
      </div>
    );
  };

  return (
    <>
      <Button
        label={`₹${universityFee}`}
        style={{ fontSize: "12px", fontWeight: "normal", maxWidth: "95%" }}
        onClick={() => onClick("displayPosition", "right")}
        className={`${styles.proceed_btn} fw-normal bgsecondary border-0 p-2 py-2 text-truncate w-100`}>
        <BsChevronRight />
      </Button>

      <Dialog
        header={
          <UniversityDetailLogo
            screenSize={screenSize}
            universityLogo={universityLogo}
            universityName={universityName}
            prospectusLink={prospectusLink}
          />
        }
        visible={displayPosition}
        position={position}
        blockScroll={true}
        modal
        style={{
          width: screenSize && screenSize.width < 991 ? "100vw" : "65vw",
          maxHeight: "100%",
        }}
        footer={renderFooter("displayPosition")}
        onHide={() => onHide("displayPosition")}
        draggable={false}
        resizable={false}
        className={`${styles.universityDetail_wrap} m-0 h-100 p-0`}>
        <UniversityDetailTab
          screenSize={screenSize}
          universityName={universityName}
          aboutUniversity={aboutUniversity}
          universityFacts={universityFacts}
          universityApprovals={universityApprovals}
          sampleCertificateLink={sampleCertificateLink}
          universityFeeDetails={universityFeeDetails}
          LMSVideos={LMSVideos}
          universityReviews={universityReviews}
          universityFAQs={universityFAQs}
          loanFacility={loanFacility}
        />
      </Dialog>
    </>
  );
};

export default UniversityDetail;
