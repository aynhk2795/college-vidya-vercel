import React from "react";
import { AiOutlineCloudDownload } from "react-icons/ai";
import Link from "next/link";

const UniversityDetailProspectus = ({ prospectusLink }) => {
  return (
    <>
      <div className="px-0 px-sm-3 py-1 ms-0 ms-sm-3 rounded">
        <Link href={prospectusLink} passHref>
          <a target={"_blank"}>
            <AiOutlineCloudDownload fontSize={20} />
            <span className="ms-2">Prospectus</span>
          </a>
        </Link>
      </div>
    </>
  );
};

export default UniversityDetailProspectus;
