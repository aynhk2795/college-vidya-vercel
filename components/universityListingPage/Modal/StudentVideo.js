import React from "react";
import { Row, Col, Card } from "react-bootstrap";
import Link from "next/link";
import Image from "next/image";

import videoimage from "/public/images/Top-University-NMIMS.jpeg";
import AnimatedYoutube from "/components/Global/AnimatedYoutube";

const Videolist = [
  {
    id: 1,
    url: "link1",
    title: "Will definitely assist you in selecting.",
    content:
      " University of Petroleum and Energy Studies is UGC-DEB approved university that has come into existence",
    image: videoimage,
  },
  {
    id: 2,
    url: "link2",
    title: "You will definitely assist you in selecting.",
    content:
      " University of Petroleum and Energy Studies is UGC-DEB approved university that has come into existence",
    image: videoimage,
  },
  {
    id: 3,
    url: "link3",
    title: "We will definitely assist you in selecting.",
    content:
      " University of Petroleum and Energy Studies is UGC-DEB approved university that has come into existence",
    image: videoimage,
  },
];
const StudentVideo = () => {
  return (
    <>
      {Videolist.map((list) => (
        <Col key={list.id} md={6}>
          <Card className="p-3 mb-4">
            <Link href={list.url}>
              <a className="text-dark">
                <Row>
                  <Col md={8}>
                    <p className="fs-14 fw-bold mb-2">{list.title}</p>
                    <p className="fs-10">{list.content}</p>
                    <p className="fs-14 d-flex align-items-center">
                      <span className="me-2">Watch video</span>{" "}
                      <AnimatedYoutube />
                    </p>
                  </Col>
                  <Col md={4}>
                    <Image src={list.image} alt="" />
                  </Col>
                </Row>
              </a>
            </Link>
          </Card>
        </Col>
      ))}
    </>
  );
};

export default StudentVideo;
