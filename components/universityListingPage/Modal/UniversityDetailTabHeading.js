import React from "react";
import styles from "/styles/UniversityDetailTabHeading.module.scss";

const UniversityDetailTabHeading = (props) => {
  return (
    <>
      <p
        className={`${styles.heading} fw-bold position-relative bg-white my-2 my-sm-4`}>
        <span>{props.title}</span>
      </p>
    </>
  );
};

export default UniversityDetailTabHeading;
