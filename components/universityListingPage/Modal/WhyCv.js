import React from "react";
import { Modal } from "react-bootstrap";
import { Button } from "react-bootstrap";
import {
  AiOutlineRight,
  AiOutlineSafety,
  AiOutlineSchedule,
} from "react-icons/ai";
import {
  FcMoneyTransfer,
  FcCustomerSupport,
  FcConferenceCall,
  FcInspection,
} from "react-icons/fc";
import CircularIcon from "/components/Global/Icon/CircularIcon";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter" className="fw-normal">
          CV Advantages
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="d-flex mb-4">
          <div>
            <CircularIcon icon={<FcMoneyTransfer fontSize={30} />} />
          </div>
          <div className="ps-3">
            <p className="fw-bold m-0 fs-16">Best Prices</p>
            <p className="fs-14">
              Get an online discount of up to 10% when you buy online. You will
              not get a better price anywhere else.
            </p>
          </div>
        </div>
        <div className="d-flex mb-4">
          <div>
            <CircularIcon icon={<FcCustomerSupport fontSize={30} />} />
          </div>
          <div className="ps-3">
            <p className="fw-bold m-0 fs-16">Recommendations</p>
            <p className="fs-14">1-to-1 Dedicated Support</p>
          </div>
        </div>
        <div className="d-flex mb-4">
          <div>
            <CircularIcon icon={<FcConferenceCall fontSize={30} />} />
          </div>
          <div className="ps-3">
            <p className="fw-bold m-0 fs-16">Top Mentors</p>
            <p className="fs-14">Talk to Top 5% advisors in India</p>
          </div>
        </div>
        <div className="d-flex mb-4">
          <div>
            <CircularIcon icon={<FcInspection fontSize={30} />} />
          </div>
          <div className="ps-3">
            <p className="fw-bold m-0 fs-16">Dedicated Claim Assistance</p>
            <p className="fs-14">
              CollegeVidya will assist your family at every step of their claim
              settlement journey including all the paperwork, provide document
              pickup service & access to grief support programs for free. We
              also have on ground claim staff in 40+ cities and we are expanding
              further.
            </p>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
}
const WhyCv = () => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      {/* <p className="m-0 cursor-pointer" onClick={() => setModalShow(true)}>
        <Button variant="outline-primary fs-12">Why College Vidya ?</Button>
      </p> */}

      <p
        className="textprimary fs-12 cursor-pointer"
        onClick={() => setModalShow(true)}>
        CV Advantages <AiOutlineRight />
      </p>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
};

export default WhyCv;
