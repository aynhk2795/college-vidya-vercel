import React, { useState } from "react";
import { Dialog } from "primereact/dialog";
import { Button } from "primereact/button";
import logo from "/public/images/NMIMS.jpeg";
import UniversityDetailLogo from "/components/universityListingPage/Modal/UniversityDetailLogo";
import UniversityDetailTab from "/components/universityListingPage/Modal/UniversityDetailTab";
// import styles from "./UniversityDetail.module.scss";
import moment from "moment";
import { Col, Form, Row } from "react-bootstrap";

import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { InputText } from "primereact/inputtext";
import { useEffect } from "react";
import { RadioButton } from "primereact/radiobutton";
import { Dropdown } from "primereact/dropdown";
import { Calendar } from "primereact/calendar";
import { generate } from "shortid";
import Image from "next/image";
import male from "/public/images/male.png";
import female from "/public/images/female.png";
import styles from "./Edit.module.scss";
import { addLead } from "../../../service/LeadService";
import { useRouter } from "next/router";
import { BsFillPencilFill } from "react-icons/bs";

const Edit = ({
  leadData,
  userData,
  courseID,
  specializationID,
  screenSize,
}) => {
  const router = useRouter();

  const [addingLead, setAddingLead] = useState(false);
  const [displayPosition, setDisplayPosition] = useState(false);
  const [position, setPosition] = useState("center");

  const dialogFuncMap = {
    displayPosition: setDisplayPosition,
  };

  const onClick = (name, position) => {
    dialogFuncMap[`${name}`](true);

    if (position) {
      setPosition(position);
    }
  };

  const onHide = (name) => {
    dialogFuncMap[`${name}`](false);
  };

  const renderFooter = () => {
    return (
      <div className="d-flex justify-content-between shadow-top py-3 px-4 px-sm-5">
        <Button
          label="Save"
          icon="pi pi-chevron-right"
          loading={addingLead}
          onClick={() => handleSubmit(editLead)()}
          autoFocus
          className={`w-100 d-flex m-0 bgsecondary flex-row-reverse shadow-none border-0 py-3`}
        />
      </div>
    );
  };

  const validationSchema = Yup.object().shape({});

  const {
    register,
    handleSubmit,
    setValue,
    getValues,
    formState: { errors },
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  const editLead = (data) => {
    delete data.gender;
    delete data.name;
    delete data.dob;

    let formData = new FormData();
    formData.append("user_id", userData.id);
    formData.append("course", courseID);
    formData.append(
      "specializations",
      specializationID !== "" ? specializationID : ""
    );
    formData.append("question_answers", JSON.stringify(data));

    addLead(formData)
      .then(() => {
        setAddingLead(false);
        router.reload();
      })
      .catch(() => {
        setAddingLead(false);
        iziToast.error({
          title: "Error",
          message: "Oops! Something went wrong",
          timeout: 2000,
          position: "topRight",
        });
      });
  };

  useEffect(() => {
    if (userData) {
      if (userData.name) {
        setValue("name", userData.name, { shouldValidate: true });
      }
      if (userData.gender) {
        setValue("gender", userData.gender, { shouldValidate: true });
      }
      if (userData.dob) {
        setValue("dob", new Date(userData.dob), { shouldValidate: true });
      }
    }

    if (leadData) {
      leadData.forEach((question) => {
        setValue(question.questions.id.toString(), question.answer, {
          shouldValidate: true,
        });
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData, leadData]);

  return (
    <>
      {screenSize && screenSize.width < 992 ? null : (
        <p className="fs-12 m-0">
          {userData && userData.name ? (
            <span
              className="border-end px-2"
              style={{ textTransform: "capitalize" }}>
              {userData.name.split(" ")[0]}
            </span>
          ) : null}
          {userData && userData.gender ? (
            <span
              className="border-end px-2"
              style={{ textTransform: "capitalize" }}>
              {userData.gender}
            </span>
          ) : null}
          {userData && userData.dob ? (
            <span className="border-end px-2">
              {moment.duration(new Date() - new Date(userData.dob)).years() +
                " Yrs"}
            </span>
          ) : null}
        </p>
      )}

      <p
        className="m-0 cursor-pointer textprimary text-uppercase fs-12 px-2"
        onClick={() => onClick("displayPosition", "right")}>
        Edit <BsFillPencilFill fontSize={10} />
      </p>

      <Dialog
        header="Edit Preferences"
        blockScroll={true}
        visible={displayPosition}
        position={position}
        modal
        // style={{ width: "35vw", maxHeight: "100%" }}
        footer={renderFooter("displayPosition")}
        onHide={() => onHide("displayPosition")}
        draggable={false}
        resizable={false}
        className={`${styles.edit_dialog} m-0 h-100 p-0`}>
        <Form
          onSubmit={handleSubmit(editLead)}
          className={` ${styles.editform}  px-2 px-sm-5`}>
          <Row>
            <Col className="position-relative">
              <RadioButton
                inputId="male"
                disabled={true}
                checked={getValues("gender") === "male"}
                onChange={() =>
                  setValue("gender", "male", { shouldValidate: true })
                }
              />
              <label htmlFor="male">
                <Image
                  className="bg-white rounded-circle"
                  src={male}
                  width={30}
                  height={30}
                  alt=""
                />{" "}
                <span className="ms-2"> Male</span>
              </label>
            </Col>
            <Col className="position-relative">
              <RadioButton
                inputId="female"
                disabled={true}
                checked={getValues("gender") === "female"}
                onChange={() =>
                  setValue("gender", "female", { shouldValidate: true })
                }
              />
              <label htmlFor="female">
                <Image
                  className="bg-white rounded-circle"
                  src={female}
                  width={30}
                  height={30}
                  alt=""
                />
                <span className="ms-2"> Female</span>
              </label>
            </Col>
            <span className="p-error text-xs d-block">
              {errors["gender"]?.message}
            </span>
          </Row>
          <Row className="mt-5">
            <Col className="position-relative">
              <div
                className={`${styles.custom_label} position-absolute top-0 start-0 translate-middle-y ms-4 bg-white px-2 fs-14`}
                style={{ zIndex: "1" }}>
                Full Name
                <span className="d-none" style={{ color: "red" }}>
                  *
                </span>
              </div>
              <InputText
                {...register("name")}
                disabled={true}
                className={`w-100 ${errors.name ? "p-invalid block" : ""}`}
              />
              <span className="p-error text-xs d-block">
                {errors["name"]?.message}
              </span>
            </Col>
          </Row>
          <Row className="mt-5">
            <Col className={`position-relative`}>
              <div
                className={`${styles.custom_label} position-absolute top-0 start-0 translate-middle-y ms-4 bg-white px-2 fs-14`}
                style={{ zIndex: "1" }}>
                Date of Birth (DD/MM/YYY)
                <span className="d-none" style={{ color: "red" }}>
                  *
                </span>
              </div>
              <div className="position-relative">
                <Calendar
                  value={getValues("dob")}
                  inputMode={"numeric"}
                  disabled={true}
                  onChange={(e) =>
                    setValue("dob", e.value, { shouldValidate: true })
                  }
                  dateFormat="dd/mm/yy"
                  mask="99/99/9999"
                  className={`w-100 ${errors.dob ? "p-invalid block" : ""}`}
                />
                <span
                  className="position-absolute end-0 top-50 translate-middle-y me-2 px-3 py-2 rounded fs-14"
                  style={{ backgroundColor: "aliceblue" }}>
                  {moment.duration(new Date() - getValues("dob")).years() +
                    " Years"}
                </span>
              </div>
              <span className="p-error text-xs d-block">
                {errors["dob"]?.message}
              </span>
            </Col>
          </Row>
          {leadData &&
            leadData.map((question) => (
              <Row className="mt-4" key={question.id}>
                <Col className="position-relative">
                  <div className={`${styles.custom_label} pb-1 fs-14`}>
                    {question.questions.question}
                    <span className="d-none" style={{ color: "red" }}>
                      *
                    </span>
                  </div>

                  {question.questions.type === "radio" ? (
                    <div>
                      <span key={generate()}>
                        <Dropdown
                          className="w-100"
                          value={getValues(question.questions.id.toString())}
                          options={question.questions.options}
                          onChange={(e) =>
                            setValue(
                              question.questions.id.toString(),
                              e.value,
                              {
                                shouldValidate: true,
                              }
                            )
                          }
                          optionValue={"answer"}
                          optionLabel={"answer"}
                        />
                      </span>
                    </div>
                  ) : null}
                </Col>
              </Row>
            ))}
        </Form>
      </Dialog>
    </>
  );
};

export default Edit;
