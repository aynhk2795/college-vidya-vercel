import React from "react";
// import { Card } from "primereact/card";
import { Row, Col, Card } from "react-bootstrap";
import Image from "next/image";
import myimage from "/public/images/experts/expert-team1.png";
import linkedin from "/public/images/linkdin.svg";

const SocialMediaCard = () => {
  return (
    <>
      <Image src={linkedin} width={100} height={50} alt="" />
      <Card className="my-3 py-4">
        <Row>
          <Col md={2} className="text-center">
            <Image
              src={myimage}
              width={60}
              height={60}
              className="rounded-circle"
              alt=""
            />
          </Col>
          <Col md={10}>
            <p className="fs-14 mb-1 fw-bold">Harish D V N</p>
            <p className="m-0 fs-12 mb-2 bg-grey rounded px-2 py-1 d-inline-block">
              Senior Software Engineer, Java, J2EE, Spring, Microservices, AWS
            </p>
            <p className="fs-12 mb-2">
              To take care of the financial future of his wife and 2 sons, he
              purchased an Edelweiss Tokio Term insurance plan from Policybazaar
              in May 2019.
            </p>
            <p className="fs-12 mb-2">
              In May 2021, during the 2nd wave of Covid, he turned Covid
              positive and passed away eventually.
            </p>
            <p className="fs-12 mb-2">
              In May 2021, during the 2nd wave of Covid, he turned Covid
              positive and passed away eventually.
            </p>
          </Col>
        </Row>
      </Card>
      <Card className="my-3 py-4">
        <Row>
          <Col md={2} className="text-center">
            <Image
              src={myimage}
              width={60}
              height={60}
              className="rounded-circle"
              alt=""
            />
          </Col>
          <Col md={10}>
            <p className="fs-14 mb-1 fw-bold">Harish D V N</p>
            <p className="m-0 fs-12 mb-2 bg-grey rounded px-2 py-1 d-inline-block">
              Senior Software Engineer, Java, J2EE, Spring, Microservices, AWS
            </p>
            <p className="fs-12 mb-2">
              To take care of the financial future of his wife and 2 sons, he
              purchased an Edelweiss Tokio Term insurance plan from Policybazaar
              in May 2019.
            </p>
            <p className="fs-12 mb-2">
              In May 2021, during the 2nd wave of Covid, he turned Covid
              positive and passed away eventually.
            </p>
            <p className="fs-12 mb-2">
              In May 2021, during the 2nd wave of Covid, he turned Covid
              positive and passed away eventually.
            </p>
          </Col>
        </Row>
      </Card>
    </>
  );
};

export default SocialMediaCard;
