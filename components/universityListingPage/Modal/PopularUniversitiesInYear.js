import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";
import { Card } from "primereact/card";
import MainHeading from "../../Global/Heading/MainHeading";
import SubHeading from "../../Global/Heading/SubHeading";
import testimage1 from "/public/images/NMIMS.jpeg";

import Link from "next/link";

const popuplar_university_in_year = [
  {
    id: 1,
    university_logo: testimage1,
    university_name: "Chandigarh University",
    url: "compare1",
  },
  {
    id: 2,
    university_logo: testimage1,
    university_name: "Chandigarh University",
    url: "compare1",
  },
  {
    id: 3,
    university_logo: testimage1,
    university_name: "Chandigarh University",
    url: "compare1",
  },
  {
    id: 4,
    university_logo: testimage1,
    university_name: "Chandigarh University",
    url: "compare1",
  },
  {
    id: 5,
    university_logo: testimage1,
    university_name: "Chandigarh University",
    url: "compare1",
  },
];

export default function App() {
  return (
    <>
      <div className="container my-5">
        <div className="row">
          <div className="col-md-12">
            <div className="mb-2">
              <SubHeading title="College Vidya" />
            </div>
            <div className="mb-4">
              <MainHeading title="Popular Universities in 2022" />
            </div>
          </div>
        </div>
        <Swiper
          slidesPerView={4}
          spaceBetween={20}
          loop={false}
          navigation={true}
          // pagination={{
          //   clickable: false,
          // }}
          pagination={false}
          breakpoints={{
            "@0.00": {
              slidesPerView: 1,
              // spaceBetween: 20,
            },
            500: {
              slidesPerView: 2,
              // spaceBetween: 20,
            },
            1024: {
              slidesPerView: 3,
              // spaceBetween: 20,
            },
            1336: {
              slidesPerView: 4,
              // spaceBetween: 20,
            },
          }}
          modules={[Navigation, Pagination]}
          className="university_comparison_swiper">
          {popuplar_university_in_year.map((list) => (
            <SwiperSlide className="card position-relative" key={list.id}>
              <Card className="text-center pt-5">
                <Image
                  src={list.university_logo}
                  width={120}
                  height={45}
                  alt=""
                />

                <Link href={list.url}>
                  <a className="bgprimary text-white text-center py-2 rounded shadow-1 mx-3 mt-3 d-block mb-3 mt-4">
                    {list.university_name}
                  </a>
                </Link>
              </Card>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </>
  );
}
