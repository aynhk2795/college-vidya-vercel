import React from "react";
import { Dialog } from "primereact/dialog";
import { Row, Col, Modal } from "react-bootstrap";
import AddToCompareBox from "/components/universityListingPage/Modal/AddToCompareBox";
import FilledCompareBox from "/components/universityListingPage/Modal/FilledCompareBox";
import { Form } from "react-bootstrap";
import styles from "./AddToCompare.module.scss";
import { useAuth } from "../../../hooks/auth";
import AuthModal from "../../Global/Modals/AuthModal";
import { useState, Downgraded } from "@hookstate/core";
import store from "../../../utils/store";
import { Button } from "primereact/button";
import { useRouter } from "next/router";

const AddToCompare = ({
  courseSlug,
  specializationSlug,
  menuData,
  university,
  allUniversities,
  screenSize,
}) => {
  const router = useRouter();
  const { user } = useAuth();

  const { universitiesToCompare } = useState(store);

  const [showCompareBox, setShowCompareBox] = React.useState(false);
  const [modalShow, setModalShow] = React.useState(false);
  const [comparing, setComparing] = React.useState(false);

  const moveToCompare = () => {
    setComparing(true);

    let url = "";

    if (specializationSlug) {
      url = `/compare/${courseSlug}/${specializationSlug}?${universitiesToCompare
        .attach(Downgraded)
        .get()
        .map(function (university) {
          return `check=${university.slug}&`;
        })}`;

      url = url.replaceAll(",", "");
      url = url + `uid=${user ? user : router.query.uid}`;

      setComparing(false);

      window.location.href = url;
    } else {
      url = `/compare/${courseSlug}?${universitiesToCompare
        .attach(Downgraded)
        .get()
        .map(function (university) {
          return `check=${university.slug}&`;
        })}`;

      url = url.replaceAll(",", "");
      url = url + `uid=${user ? user : router.query.uid}`;

      setComparing(false);

      window.location.href = url;
    }
  };

  const validateUser = () => {
    if (user || router.query.uid) {
      if (
        !universitiesToCompare
          .attach(Downgraded)
          .get()
          .some(
            (includedUniversity) =>
              includedUniversity.id === university.university.id
          ) &&
        universitiesToCompare.length < 3
      ) {
        let newUniversityToCompare = {
          id: university.university.id,
          name: university.university.name,
          slug: university.university.slug,
          logo: university.university.logo,
          fee: university.fee,
          sample_certificate: university.sample_certificate,
          cv_rating: university.university.cv_rating,
          about_university: university.university.about,
        };

        universitiesToCompare.set((prev) => [...prev, newUniversityToCompare]);
      }
      setShowCompareBox(true);
    } else {
      setModalShow(true);
    }
  };

  return (
    <div className={`${styles.addtocompare_modal}`}>
      <Form.Group controlId="formBasicCheckbox" className="cursor-pointer">
        <Form.Check
          checked={universitiesToCompare
            .attach(Downgraded)
            .get()
            .some(
              (includedUniversity) =>
                includedUniversity.id === university.university.id
            )}
          className={`${styles.compareCheck} d-inline-flex textprimary align-items-center`}
          style={{ minWidth: "120px" }}
          onClick={() => validateUser()}
          type="checkbox"
          label={<span className="cursor-pointer">Add to compare</span>}
        />
      </Form.Group>

      <Dialog
        className={`${styles.add_to_compare_wrap} compareBottomModal`}
        header={
          universitiesToCompare.attach(Downgraded).get().length < 3
            ? `Select ${
                screenSize.width < 991
                  ? 2 - universitiesToCompare.length
                  : 3 - universitiesToCompare.length
              } more universities to compare`
            : `Click Compare to proceed!`
        }
        visible={showCompareBox}
        position={"bottom"}
        modal
        style={{ width: "100vw", margin: "0", padding: "0" }}
        onHide={() => setShowCompareBox(false)}
        draggable={false}
        resizable={false}>
        <Row className={`${styles.compare_bottom_modal}`}>
          <Col md={8} className={`mx-auto pt-3`}>
            <Row className={`row-cols-${screenSize.width < 991 ? 3 : 4}`}>
              <Col
                className={`d-flex align-items-center justify-content-center ${
                  screenSize.width < 991 ? "col-12 mb-3" : null
                }`}>
                {universitiesToCompare.attach(Downgraded).get()[0] ? (
                  <FilledCompareBox
                    id={universitiesToCompare.attach(Downgraded).get()[0].id}
                    name={
                      universitiesToCompare.attach(Downgraded).get()[0].name
                    }
                    logo={
                      universitiesToCompare.attach(Downgraded).get()[0].logo
                    }
                    fee={universitiesToCompare.attach(Downgraded).get()[0].fee}
                    compare={
                      universitiesToCompare.attach(Downgraded).get()[0].compare
                    }
                    sample_certificate={
                      universitiesToCompare.attach(Downgraded).get()[0]
                        .sample_certificate
                    }
                    cv_rating={
                      universitiesToCompare.attach(Downgraded).get()[0]
                        .cv_rating
                    }
                    about_university={
                      universitiesToCompare.attach(Downgraded).get()[0]
                        .about_university
                    }
                  />
                ) : (
                  <AddToCompareBox
                    universitiesToCompare={universitiesToCompare
                      .attach(Downgraded)
                      .get()}
                    allUniversities={allUniversities}
                  />
                )}
              </Col>
              <Col
                className={`d-flex align-items-center justify-content-center ${
                  screenSize.width < 991 ? "col-12 mb-3" : null
                }`}>
                {universitiesToCompare.attach(Downgraded).get()[1] ? (
                  <FilledCompareBox
                    id={universitiesToCompare.attach(Downgraded).get()[1].id}
                    name={
                      universitiesToCompare.attach(Downgraded).get()[1].name
                    }
                    logo={
                      universitiesToCompare.attach(Downgraded).get()[1].logo
                    }
                    fee={universitiesToCompare.attach(Downgraded).get()[1].fee}
                    compare={
                      universitiesToCompare.attach(Downgraded).get()[1].compare
                    }
                    sample_certificate={
                      universitiesToCompare.attach(Downgraded).get()[1]
                        .sample_certificate
                    }
                    cv_rating={
                      universitiesToCompare.attach(Downgraded).get()[1]
                        .cv_rating
                    }
                    about_university={
                      universitiesToCompare.attach(Downgraded).get()[1]
                        .about_university
                    }
                  />
                ) : (
                  <AddToCompareBox
                    universitiesToCompare={universitiesToCompare
                      .attach(Downgraded)
                      .get()}
                    allUniversities={allUniversities}
                  />
                )}
              </Col>
              {screenSize.width < 991 ? null : (
                <Col
                  className={`d-flex align-items-center justify-content-center ${
                    screenSize.width < 991 ? "col-12 mb-3" : null
                  }`}>
                  {universitiesToCompare.attach(Downgraded).get()[2] ? (
                    <FilledCompareBox
                      id={universitiesToCompare.attach(Downgraded).get()[2].id}
                      name={
                        universitiesToCompare.attach(Downgraded).get()[2].name
                      }
                      logo={
                        universitiesToCompare.attach(Downgraded).get()[2].logo
                      }
                      fee={
                        universitiesToCompare.attach(Downgraded).get()[2].fee
                      }
                      compare={
                        universitiesToCompare.attach(Downgraded).get()[2]
                          .compare
                      }
                      sample_certificate={
                        universitiesToCompare.attach(Downgraded).get()[2]
                          .sample_certificate
                      }
                      cv_rating={
                        universitiesToCompare.attach(Downgraded).get()[2]
                          .cv_rating
                      }
                      about_university={
                        universitiesToCompare.attach(Downgraded).get()[2]
                          .about_university
                      }
                    />
                  ) : (
                    <AddToCompareBox
                      universitiesToCompare={universitiesToCompare
                        .attach(Downgraded)
                        .get()}
                      allUniversities={allUniversities}
                    />
                  )}
                </Col>
              )}

              <Col
                className={`d-flex align-items-center justify-content-center BottomModalButton ${
                  screenSize.width < 991 ? "col-12 mb-3" : null
                }`}>
                <div className="primary-button-wrap">
                  <Button
                    disabled={
                      universitiesToCompare.attach(Downgraded).length === 0
                    }
                    className="primary-button py-4 h1"
                    label="Compare Now"
                    loading={comparing}
                    onClick={() => moveToCompare()}
                  />
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Dialog>
      <Modal
        show={modalShow}
        onHide={() => setModalShow(false)}
        backdrop="static"
        size={"md"}
        centered>
        <AuthModal
          menuData={menuData}
          initialMode={"login"}
          closeModal={() => setModalShow(false)}
        />
      </Modal>
    </div>
  );
};

export default AddToCompare;
