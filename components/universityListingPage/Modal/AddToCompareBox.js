import React from "react";
import { Modal } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import { AiOutlinePlusCircle } from "react-icons/ai";
import styles from "/styles/AddToCompareBox.module.scss";
import FilledCompareBox from "/components/universityListingPage/Modal/FilledCompareBox";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter" className="fw-normal">
          Choose 3 University to compare
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row className="row-cols-2">
          {props.allUniversities.map((university) => (
            <Col
              key={university.university.id}
              md={4}
              className="mb-2 mt-2 mb-sm-3 mt-sm-3 col">
              <FilledCompareBox
                id={university.university.id}
                name={university.university.name}
                prospectus_link={university.university.prospectus_link}
                slug={university.university.slug}
                logo={university.university.logo}
                fee={university.fee}
                fee_details={university.courses_fee_details}
                compare={university.compare}
                sample_certificate={university.university.sample_certificate}
                cv_rating={university.university.cv_rating}
                about_university={university.university.about}
                university_facts={university.university.universities_facts}
                university_approvals={university.university.approval_details}
                book_lms={university.university.book_lms}
                university_reviews={university.university.reviews}
                university_faqs={university.university.universities_faqs}
                fromComparePage={props.fromComparePage}
                courseSlug={props.courseSlug}
                specializationSlug={props.specializationSlug}
                userID={props.userID}
              />
            </Col>
          ))}
        </Row>
      </Modal.Body>
    </Modal>
  );
}
const AddToCompareBox = ({
  allUniversities,
  fromComparePage,
  courseSlug,
  specializationSlug,
  userID,
}) => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <div
        onClick={() => setModalShow(true)}
        className={`${styles.dottedbox} p-2 rounded position-relative h-100 d-flex align-items-center justify-content-center w-100 cursor-pointer`}>
        <div className="py-2 text-center">
          <p className="m-0 fs-13 text-dark">
            Add to Compare <AiOutlinePlusCircle fontSize={20} />
          </p>
        </div>
      </div>

      <MyVerticallyCenteredModal
        allUniversities={allUniversities}
        show={modalShow}
        onHide={() => setModalShow(false)}
        fromComparePage={fromComparePage}
        courseSlug={courseSlug}
        specializationSlug={specializationSlug}
        userID={userID}
      />
    </>
  );
};

export default AddToCompareBox;
