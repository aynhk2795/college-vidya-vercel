import React from "react";
import styles from "/styles/FilledCompareBox.module.scss";
import Image from "next/image";
import { AiFillCloseCircle, AiFillCheckCircle } from "react-icons/ai";
import { RiCheckboxBlankCircleFill } from "react-icons/ri";

import store from "../../../utils/store";
import { useState, Downgraded } from "@hookstate/core";
import { useRouter } from "next/router";

const FilledCompareBox = ({
  id,
  name,
  prospectus_link,
  slug,
  logo,
  fee,
  fee_details,
  compare,
  sample_certificate,
  about_university,
  university_facts,
  university_approvals,
  book_lms,
  university_reviews,
  university_faqs,
  fromComparePage,
  userID,
  courseSlug,
  specializationSlug,
}) => {
  const { universitiesToCompare } = useState(store);

  const removeUniversity = () => {
    if (fromComparePage) {
      let newURL = "";
      if (specializationSlug) {
        newURL = `/compare/${courseSlug}/${specializationSlug}?${universitiesToCompare
          .attach(Downgraded)
          .get()
          .filter((univ) => univ.id !== id)
          .map(function (university) {
            return `check=${university.slug}&`;
          })}`;

        newURL = newURL.replaceAll(",", "");
        newURL = newURL + `uid=${userID}`;
      } else {
        newURL = `/compare/${courseSlug}/?${universitiesToCompare
          .attach(Downgraded)
          .get()
          .filter((univ) => univ.id !== id)
          .map(function (university) {
            return `check=${university.slug}&`;
          })}`;

        newURL = newURL.replaceAll(",", "");
        newURL = newURL + `uid=${userID}`;

        window.location.href = newURL;
      }
    } else {
      let allUnivs = [...universitiesToCompare.attach(Downgraded).get()];

      allUnivs = allUnivs.filter((univ) => univ.id !== id);

      universitiesToCompare.set(allUnivs);
    }
  };

  const addUniversity = () => {
    if (fromComparePage) {
      let newURL = "";
      if (specializationSlug) {
        newURL = `/compare/${courseSlug}/${specializationSlug}?${universitiesToCompare
          .attach(Downgraded)
          .get()
          .map(function (university) {
            return `check=${university.slug}&`;
          })}`;

        newURL = newURL.replaceAll(",", "");
        newURL = newURL + `check=${slug}&`;
        newURL = newURL + `uid=${userID}`;

        window.location.href = newURL;
      } else {
        newURL = `/compare/${courseSlug}/?${universitiesToCompare
          .attach(Downgraded)
          .get()
          .map(function (university) {
            return `check=${university.slug}&`;
          })}`;

        newURL = newURL.replaceAll(",", "");
        newURL = newURL + `check=${slug}&`;
        newURL = newURL + `uid=${userID}`;

        window.location.href = newURL;
      }
    } else {
      let newUniversityToCompare = {
        id: id,
        name: name,
        prospectus_link: prospectus_link,
        slug: slug,
        logo: logo,
        fee: fee,
        fee_details: fee_details,
        compare: compare,
        sample_certificate: sample_certificate,
        about_university: about_university,
        university_facts: university_facts,
        university_approvals: university_approvals,
        university_reviews: university_reviews,
        university_faqs: university_faqs,
        book_lms: book_lms,
      };

      universitiesToCompare.set((prev) => [...prev, newUniversityToCompare]);
    }
  };

  return (
    <>
      <div
        className={`${styles.solidbox} p-1 p-sm-2 rounded position-relative w-100 cursor-pointer`}>
        {universitiesToCompare
          .attach(Downgraded)
          .get()
          .some((university) => university.id === id) ? (
          <AiFillCloseCircle
            fontSize={25}
            className="position-absolute top-0 start-100 translate-middle text-dark"
            onClick={() => removeUniversity()}
          />
        ) : (
          <RiCheckboxBlankCircleFill
            fontSize={25}
            style={{ zIndex: "1" }}
            className="position-absolute top-0 start-100 translate-middle text-dark"
            onClick={() => addUniversity()}
          />
        )}

        <div className="text-center mt-1">
          <Image
            src={logo}
            width={140}
            height={50}
            quality={100}
            objectFit="contain"
            alt=""
          />
        </div>
        {/* <p
          className={`${styles.solidboxTitle} m-0 fs-12 py-1 position-absolute top-0 start-0 px-3 text-truncate`}>
          {name}
        </p> */}
        <p className={`m-0 fs-10 py-1 px-3 text-truncate text-center`}>
          {name}
        </p>

        {/* <p
          className={`${styles.solidboxFees} m-0 text-center position-absolute bottom-0 fs-16 w-100 start-0 my-1`}>
          ₹{fee && fee.toLocaleString()}
        </p> */}
        <p className={`m-0 text-center fs-16 w-100 text-dark fw-bold`}>
          ₹{fee && fee.toLocaleString()}
        </p>
      </div>
    </>
  );
};

export default FilledCompareBox;
