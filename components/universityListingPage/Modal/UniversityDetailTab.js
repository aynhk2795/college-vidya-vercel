import React from "react";
import { TabView, TabPanel } from "primereact/tabview";
import UniversityDetailTabHeading from "/components/universityListingPage/Modal/UniversityDetailTabHeading";
import { Card, Col, Row, Table } from "react-bootstrap";
import { Timeline } from "primereact/timeline";
import FaqSlim from "/components/Homepage/Accordion/FaqSlim";
import Image from "next/image";
import appoved1 from "/public/images/deb_logo.webp";
import appoved2 from "/public/images/naac_logo.webp";
import Certificate from "/components/TopUniversity/Certificate";
import { AiFillBank } from "react-icons/ai";
import learning from "/public/images/Top-University-Lovely-Professional-University.jpeg";
import Link from "next/link";
import AnimatedYoutube from "/components/Global/AnimatedYoutube";
import ReviewCard from "/components/Global/ReviewCard";
import styles from "./UniversityDetailTab.module.scss";

function UniversityDetailTab({
  screenSize,
  universityName,
  aboutUniversity,
  universityFacts,
  universityApprovals,
  sampleCertificateLink,
  universityFeeDetails,
  LMSVideos,
  universityReviews,
  universityFAQs,
  loanFacility,
}) {
  const events1 = [
    { status: "Ordered" },
    { status: "Processing" },
    { status: "Shipped" },
    { status: "Delivered" },
    { status: "Processing" },
    { status: "Ordered" },
  ];
  return (
    <div className={`${styles.UniversityDetailTab}`}>
      <TabView>
        <TabPanel header="Know about University">
          <UniversityDetailTabHeading title="About University" />
          <div dangerouslySetInnerHTML={{ __html: aboutUniversity }} />
          {/* <UniversityDetailTabHeading title="How Does it Work?" />
          <Timeline
            value={events1}
            align="alternate"
            content={(item) => item.status}
          /> */}
          <UniversityDetailTabHeading title="Facts" />
          {universityFacts.map((fact) => (
            <Card className="p-3 mb-3 positivediv" key={fact.id}>
              {fact.content}
            </Card>
          ))}
          <UniversityDetailTabHeading title="Approved By" />
          <p>Approvals To Look For Before Selecting A University</p>
          <div className="d-flex gap-3">
            {universityApprovals.map((approval) => (
              <Image
                src={approval.logo}
                width={70}
                height={60}
                objectFit="contain"
                alt={approval.title}
                key={approval.id}
              />
            ))}
          </div>
          <UniversityDetailTabHeading title="Sample Certificate" />
          <div style={{ width: "14rem" }}>
            <Certificate sampleCertificateLink={sampleCertificateLink} />
          </div>
        </TabPanel>
        <TabPanel header="Fees Breakup ">
          <UniversityDetailTabHeading title={`${universityName} Fees`} />
          {loanFacility ? (
            <p className="m-0 bg-grey d-inline-block px-3 py-2 rounded me-2 mb-2">
              <AiFillBank fontSize={20} /> Loan Facility Available
            </p>
          ) : null}
          <p className="m-0 bg-grey d-inline-block px-3 py-2 rounded me-2 mb-2">
            <AiFillBank fontSize={20} /> Easy to Pay
          </p>
          <p className="m-0 bg-grey d-inline-block px-3 py-2 rounded me-2 mb-2">
            <AiFillBank fontSize={20} /> Direct Pay to University
          </p>
          <p className="m-0 bg-grey d-inline-block px-3 py-2 rounded me-2 mb-2">
            <AiFillBank fontSize={20} /> No Hidden Charges
          </p>
          <div className="feestable my-4">
            <Table borderless responsive>
              <thead>
                <tr>
                  <th>Fee Type</th>
                  <th>Total Fee</th>
                </tr>
              </thead>
              <tbody>
                {universityFeeDetails &&
                  universityFeeDetails.map((feeType) => (
                    <tr key={feeType.id}>
                      <td>{feeType.fee_type.fee_type}</td>
                      <td>₹{parseInt(feeType.fee).toLocaleString()}</td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </div>
        </TabPanel>
        <TabPanel header="E-learning Experience ">
          <UniversityDetailTabHeading title="E-learning Experience" />
          <div className="d-flex gap-2 align-items-center">
            {LMSVideos.map((video) => (
              <Link key={video.id} href={video.link} passHref>
                <a target={"_blank"}>
                  {" "}
                  <Card style={{ width: "15rem" }}>
                    <Card.Body style={{ padding: "0" }}>
                      <Image
                        src={video.thumbnail}
                        width={250}
                        height={170}
                        objectFit="contain"
                        alt=""
                      />
                      <div className="position-absolute top-50 start-50 translate-middle">
                        <AnimatedYoutube />
                      </div>
                    </Card.Body>
                  </Card>
                </a>
              </Link>
            ))}
          </div>
        </TabPanel>
        <TabPanel header="Alumni Talk & Reviews">
          <UniversityDetailTabHeading title="Alumni Talk & Reviews" />
          {universityReviews.map((review) => (
            <ReviewCard
              key={review.id}
              reviewerName={review.register.name}
              reviewDate={review.created_date_time}
              reviewContent={review.content}
              reviewRating={review.rating}
            />
          ))}
        </TabPanel>
        <TabPanel header="FAQs">
          <UniversityDetailTabHeading title="Ask us anything, we’d love to answer!" />
          <FaqSlim universityFAQs={universityFAQs} />
        </TabPanel>
      </TabView>
    </div>
  );
}

export default UniversityDetailTab;
