import React from "react";
import { AiOutlineRight } from "react-icons/ai";
import { Modal, Accordion, Badge, Card } from "react-bootstrap";
import styles from "/styles/knowintwomins.module.scss";
import AnimatedYoutube from "/components/Global/AnimatedYoutube";
import Link from "next/link";
import Image from "next/image";
import approval1 from "/public/images/approvals/approval_1.png";
import approval2 from "/public/images/approvals/approval_2.png";
import approval3 from "/public/images/approvals/approval_3.png";
import approval4 from "/public/images/approvals/approval_4.png";
import approval5 from "/public/images/approvals/approval_5.png";
import ReviewCard from "/components/Global/ReviewCard";
import {
  FcAcceptDatabase,
  FcBriefcase,
  FcDocument,
  FcGraduationCap,
  FcVoicePresentation,
} from "react-icons/fc";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      className={`${styles.know_modal}`}
      show={props.show}
      onHide={() => props.onHide()}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title
          id="contained-modal-title-vcenter"
          className="d-flex justify-content-center">
          <span className="me-2"> Know University in 2 mins </span>
          {props.videoLink ? (
            <Link href={props.videoLink} passHref>
              <a target={"_blank"}>
                <AnimatedYoutube />
              </a>
            </Link>
          ) : null}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={`${styles.knowintwoModal}`}>
        <Accordion>
          <Accordion.Item
            eventKey="0"
            className={`${styles.knowintwoHeaderWrap}`}>
            <Accordion.Header className={`${styles.knowintwoHeader}`}>
              <FcDocument fontSize={30} className="me-2" /> About University
            </Accordion.Header>
            <Accordion.Body>
              <div
                dangerouslySetInnerHTML={{
                  __html: props.aboutUniversity,
                }}></div>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item
            eventKey="1"
            className={`${styles.knowintwoHeaderWrap}`}>
            <Accordion.Header className={`${styles.knowintwoHeader}`}>
              <FcAcceptDatabase fontSize={30} className="me-2" /> University
              Facts
            </Accordion.Header>
            <Accordion.Body>
              <ul className="list-unstyled">
                {props.universityFacts.map((fact) => (
                  <li
                    className="mb-3 d-flex rounded px-3 py-3"
                    style={{ backgroundColor: "aliceblue" }}
                    key={fact.id}>
                    <span>-</span>
                    {fact.content}
                  </li>
                ))}
              </ul>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item
            eventKey="2"
            className={`${styles.knowintwoHeaderWrap}`}>
            <Accordion.Header className={`${styles.knowintwoHeader}`}>
              <FcGraduationCap fontSize={30} className="me-2" /> Approvals
            </Accordion.Header>
            <Accordion.Body>
              <p>Approvals To Look For Before Selecting A University</p>
              <ul className="list-unstyled d-flex gap-4">
                {props.universityApprovals.map((approval) => (
                  <li key={approval.id}>
                    <Image
                      src={approval.logo}
                      width={75}
                      height={60}
                      objectFit="contain"
                      alt=""
                    />{" "}
                  </li>
                ))}
              </ul>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item
            eventKey="3"
            className={`${styles.knowintwoHeaderWrap}`}>
            <Accordion.Header className={`${styles.knowintwoHeader}`}>
              <span className="me-2">
                <FcBriefcase fontSize={30} className="me-2" /> Book & LMS
                Experience{" "}
              </span>{" "}
              <Badge pill className={`${styles.videoBadge}`}>
                Videos
              </Badge>
            </Accordion.Header>
            <Accordion.Body className="d-flex gap-2">
              {props.LMSVideos.map((video) => (
                <Link key={video.id} href={video.link} passHref>
                  <a target={"_blank"}>
                    {" "}
                    <Card>
                      <Card.Body style={{ padding: "0" }}>
                        <Image
                          src={video.thumbnail}
                          width={200}
                          height={170}
                          alt=""
                          objectFit="cover"
                        />
                        <div className="position-absolute top-50 start-50 translate-middle">
                          <AnimatedYoutube />
                        </div>
                      </Card.Body>
                    </Card>
                  </a>
                </Link>
              ))}
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item
            eventKey="4"
            className={`${styles.knowintwoHeaderWrap}`}>
            <Accordion.Header className={`${styles.knowintwoHeader}`}>
              <FcVoicePresentation fontSize={30} className="me-2" /> Reviews
            </Accordion.Header>
            <Accordion.Body>
              {props.universityReviews.map((review) => (
                <ReviewCard
                  key={review.id}
                  reviewerName={review.register.name}
                  reviewDate={review.created_date_time}
                  reviewContent={review.content}
                  reviewRating={review.rating}
                />
              ))}
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </Modal.Body>
    </Modal>
  );
}
const KnowInTwoMIns = ({
  aboutUniversity,
  universityFacts,
  universityApprovals,
  LMSVideos,
  universityReviews,
  videoLink,
  smallTitle,
}) => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <p
        className={`m-0 cursor-pointer comparepage_title ${
          smallTitle ? styles.small_title : null
        }`}
        onClick={() => setModalShow(true)}>
        Know University in 2 mins
        <AiOutlineRight />
      </p>

      <MyVerticallyCenteredModal
        aboutUniversity={aboutUniversity}
        universityFacts={universityFacts}
        universityApprovals={universityApprovals}
        LMSVideos={LMSVideos}
        universityReviews={universityReviews}
        videoLink={videoLink}
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
};

export default KnowInTwoMIns;
