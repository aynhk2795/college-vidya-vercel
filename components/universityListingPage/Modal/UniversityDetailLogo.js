import Image from "next/image";
import React from "react";
import { Card } from "react-bootstrap";
import UniversityDetailProspectus from "/components/universityListingPage/Modal/UniversityDetailProspectus";

const UniversityDetailLogo = ({
  screenSize,
  universityLogo,
  universityName,
  prospectusLink,
}) => {
  return (
    <>
      <div className="d-flex align-items-center">
        <Card className="p-1">
          <Image src={universityLogo} width={120} height={40} alt="" />
        </Card>{" "}
        {screenSize.width > 991 ? (
          <p className="m-0 text-uppercase ms-3 text-truncate">
            {universityName}
          </p>
        ) : null}
        {screenSize.width > 991 ? (
          <UniversityDetailProspectus prospectusLink={prospectusLink} />
        ) : null}
      </div>
      {screenSize.width < 991 ? (
        <p className="m-0 mt-2 text-uppercase fs-13 d-flex align-items-start flex-column">
          <span className="text-truncate">{universityName}</span>
          <span>
            <UniversityDetailProspectus prospectusLink={prospectusLink} />
          </span>
        </p>
      ) : null}
    </>
  );
};

export default UniversityDetailLogo;
