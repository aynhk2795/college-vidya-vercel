import React from "react";
import { Modal } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { AiOutlineSafety, AiOutlineSchedule } from "react-icons/ai";
import CircularIcon from "/components/Global/Icon/CircularIcon";
import Assistanceslider from "/components/universityListingPage/Slider/Assistanceslider";
import TruthCard from "/components/universityListingPage/Modal/TruthCard";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter" className="fw-normal">
          Dedicated Claim Assistance
        </Modal.Title>
      </Modal.Header>

      <Modal.Body className="bg-grey">
        <Assistanceslider />

        <h4 className="text-center mt-3">Moment Of Truth</h4>
        <p className="text-center">
          Policybazaar ensures your family gets what’s rightfully theirs, even
          in your absence
        </p>
        <TruthCard />
      </Modal.Body>
    </Modal>
  );
}
const Assistance = () => {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <>
      <p className="m-0 cursor-pointer" onClick={() => setModalShow(true)}>
        <Button variant="outline-primary fs-12">Know More</Button>
      </p>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </>
  );
};

export default Assistance;
