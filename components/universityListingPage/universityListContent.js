import React, { useState, useEffect } from "react";
import { AiTwotoneStar } from "react-icons/ai";
import Image from "next/image";
import {
  BsCircleFill,
  BsFillInfoCircleFill,
  BsInfoCircle,
  BsInfoCircleFill,
} from "react-icons/bs";
import ProsCons from "/components/universityListingPage/Modal/ProsCons";
import Approvals from "/components/universityListingPage/Modal/Approvals";
import KnowInTwoMIns from "/components/universityListingPage/Modal/KnowInTwoMIns";
import AddToCompare from "/components/universityListingPage/Modal/AddToCompare";
import UniversityDetail from "/components/universityListingPage/Modal/UniversityDetail";
import UniversityListMiddleBar from "./universityListMiddleBar";
import WhyCv from "/components/universityListingPage/Modal/WhyCv";
import Tippy from "@tippyjs/react";
import styles from "./universityListing.module.scss";
import Mentor from "./Modal/Mentor";
import { TiInfo } from "react-icons/ti";

const middleBar_position = 3;
const UniversityListContent = ({
  courseSlug,
  specializationSlug,
  menuData,
  screenSize,
  universityList,
  showSmall,
}) => {
  return (
    <>
      {/* <div className="mt-3 py-2 rounded-top text-center" style={{ backgroundColor: "#d7ebfd" }}> <ProceedToUniversity /></div> */}

      <div className={`${styles.universitylist_grid}`}>
        {universityList.map((list, index) => (
          <div key={index} className={`${styles.universityOuterBar}`}>
            {index === middleBar_position ||
            index === middleBar_position + 4 ? (
              <UniversityListMiddleBar />
            ) : null}

            <div
              className={`${styles.universityListBbar}  row ${
                showSmall ? "row-cols-4" : "row-cols-6"
              } rounded`}>
              <div
                className={`${styles.universityListContent} py-4 col border-end d-flex justify-content-center align-items-center position-relative flex-column`}>
                <Image
                  src={list.university.logo}
                  width={130}
                  height={60}
                  alt=""
                />

                <div
                  className={`${styles.universityName} text-uppercase position-absolute bottom-0 w-100 text-center text-truncate px-2`}>
                  {" "}
                  {list.university.name}
                </div>
                <div
                  className={`${styles.universityListTopTag} position-relative text-truncate position-absolute top-0 w-100 text-center fs-10 d-flex justify-content-center align-items-center`}
                  style={{
                    backgroundColor: `${
                      list.university_dynamic_tag_color
                        ? list.university_dynamic_tag_color
                        : list.university.university_dynamic_tag_color
                    }`,
                  }}>
                  {/* <BsCircleFill fontSize={7} className="me-1 position-absolute start-0 ms-1" /> */}

                  <span
                    className="ps-2 text-truncate"
                    style={{ maxWidth: "80%", margin: "0 auto" }}>
                    {list.custom_tags
                      ? list.custom_tags
                      : list.university.custom_tags}{" "}
                  </span>
                  <Tippy
                    className="me-3"
                    content={
                      <span className="fs-12 text-center d-flex">
                        {list.custom_tags
                          ? list.custom_tags
                          : list.university.custom_tags}
                      </span>
                    }
                    animation="scale"
                    allowHTML={true}>
                    <span>
                      <BsInfoCircleFill fontSize={12} className="me-2" />{" "}
                    </span>
                  </Tippy>
                </div>
              </div>

              {showSmall ? null : (
                <>
                  <div
                    className={`${styles.universityListContent} col border-end d-flex justify-content-center align-items-center position-relative text-center`}>
                    <div
                      className={`${styles.approvalTag} position-absolute end-0`}>
                      <Approvals />
                    </div>
                    <span className="fs-12">
                      {list.university.approval_details
                        .map((approval) => approval.title)
                        .join(" | ")}
                    </span>
                  </div>

                  <div
                    className={`${styles.universityListContent} col border-end d-flex justify-content-center align-items-center position-relative`}>
                    <div
                      className={`${styles.universityLoanTag} text-uppercase position-absolute bottom-0 text-center text-truncate`}>
                      wes approval{" "}
                      <Tippy
                        content={
                          <span className="fs-12 text-center d-flex">
                            Degree Internationally Accepted &#9989;
                          </span>
                        }
                        animation="scale"
                        allowHTML={true}>
                        <span>
                          <BsInfoCircle className="ms-1" />{" "}
                        </span>
                      </Tippy>
                    </div>
                    3.28/4
                  </div>
                </>
              )}

              <div
                className={`${styles.universityListContent} col border-end d-flex flex-column justify-content-center align-items-center position-relative`}>
                <p className="m-0 text-center">
                  <AiTwotoneStar className={`${styles.starIcon}`} />
                  {list.university.avg_rating ? list.university.avg_rating : 0}
                  /5
                </p>

                <p className="textprimary fs-12">
                  (
                  {list.reviews_universities_specializations
                    ? list.reviews_universities_specializations.length
                    : list.review_universities_courses.length}{" "}
                  reviews)
                </p>
                <span className="position-absolute top-0 end-0 me-1 mt-1">
                  <Tippy
                    content={
                      <div className="d-flex">
                        <span className="fs-14 text-center d-flex fw-normal">
                          To calculate the overall star rating and percentage
                          breakdown by star, we don’t use a simple average.The
                          student was enrolled in the university. It also
                          analyses reviews to verify trustworthiness.
                        </span>
                      </div>
                    }
                    animation="scale"
                    allowHTML={true}>
                    <span>
                      <BsInfoCircle className="ms-1" />{" "}
                    </span>
                  </Tippy>
                </span>
              </div>
              <div
                className={`${styles.universityListContent} col border-end d-flex justify-content-center align-items-center position-relative px-1`}>
                <UniversityDetail
                  screenSize={screenSize}
                  universityName={list.university.name}
                  universityLogo={list.university.logo}
                  prospectusLink={list.university.prospectus_link}
                  aboutUniversity={list.university.about}
                  universityFacts={list.university.universities_facts}
                  universityApprovals={list.university.approval_details}
                  sampleCertificateLink={list.university.sample_certificate}
                  universityFee={list.fee.toLocaleString()}
                  universityFeeDetails={list.courses_fee_details}
                  LMSVideos={list.university.books_lms}
                  universityReviews={
                    list.reviews_universities_specializations
                      ? list.reviews_universities_specializations
                      : list.review_universities_courses
                  }
                  universityFAQs={
                    list.universities_faqs_specializations_all_faqs
                      ? list.universities_faqs_specializations_all_faqs
                      : list.universities_faqs_courses_all_faqs
                  }
                  loanFacility={list.university.loan_facility}
                />
                <span className="position-absolute top-0 end-0 me-1 mt-1">
                  <Tippy
                    content={
                      <div className="d-flex">
                        <span className="fs-14 text-center d-flex">
                          Fees Info
                        </span>
                      </div>
                    }
                    animation="scale"
                    allowHTML={true}>
                    <span>
                      <BsInfoCircle className="ms-1" />{" "}
                    </span>
                  </Tippy>
                </span>
                {list.university.loan_facility ? (
                  <div
                    className={`${styles.universityWesTag} text-uppercase position-absolute bottom-0 text-center text-truncate`}>
                    <span>&#8377;</span> Interest Free EMI
                    <Tippy
                      content={
                        <span className="fs-12 text-center d-flex">
                          &#8377; Interest Free EMI{" "}
                        </span>
                      }
                      animation="scale"
                      allowHTML={true}>
                      <span>
                        <BsInfoCircle className="ms-1" />{" "}
                      </span>
                    </Tippy>
                  </div>
                ) : null}
                <div
                  className={`${styles.universityWesTag} text-uppercase position-absolute bottom-0 text-center text-truncate`}>
                  <span>&#8377;</span> Interest Free EMI
                  <Tippy
                    content={
                      <span className="fs-12 text-center d-flex">
                        &#8377; Interest Free EMI{" "}
                      </span>
                    }
                    animation="scale"
                    allowHTML={true}>
                    <span>
                      <BsInfoCircle className="ms-1" />{" "}
                    </span>
                  </Tippy>
                </div>

                {/* ₹{list.fee.toLocaleString()}/Semester */}
              </div>

              <div
                className={`${styles.universityListContent} ${styles.universityAdvice} col border-end position-relative text-center p-0`}>
                <Mentor />
              </div>
            </div>

            <div
              className={`${styles.bottomBar} row row-cols-${
                screenSize.width < 1200 ? "2" : null
              } mb-4`}>
              <div
                className={`${styles.col} col-${
                  screenSize.width < 1200 ? "6" : "4"
                } d-flex align-items-center`}>
                <KnowInTwoMIns
                  aboutUniversity={list.university.about}
                  universityFacts={list.university.universities_facts}
                  universityApprovals={list.university.approval_details}
                  LMSVideos={list.university.books_lms}
                  universityReviews={list.university.reviews}
                  videoLink={list.university.video_link}
                />
              </div>

              {screenSize.width < 1200 ? null : (
                <>
                  <div
                    className={`${styles.col} textprimary fw-bold col text-center d-flex justify-content-center align-items-center`}>
                    {list.university.universities_pros_cons.length > 0 ? (
                      <ProsCons
                        pros_cons={list.university.universities_pros_cons}
                      />
                    ) : null}
                  </div>

                  <div
                    className={`${styles.col} ms-auto col text-center d-flex justify-content-center align-items-center`}>
                    <WhyCv />
                  </div>
                </>
              )}

              <div
                className={`${styles.col} ms-auto col text-center d-flex justify-content-end align-items-center`}>
                {" "}
                <AddToCompare
                  courseSlug={courseSlug}
                  specializationSlug={specializationSlug}
                  menuData={menuData}
                  university={list}
                  allUniversities={universityList}
                  screenSize={screenSize}
                />
              </div>
            </div>

            <div className={`${styles.mob_mentor}`}>
              <Mentor />
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default UniversityListContent;
