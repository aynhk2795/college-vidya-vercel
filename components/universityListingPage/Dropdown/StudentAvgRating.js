import React, { useState } from "react";
import { Dropdown } from "primereact/dropdown";

const StudentAvgRating = ({ screenSize }) => {
  const [selectedCity1, setSelectedCity1] = useState(null);
  const cities = [
    { name: " student Avg Rating ", code: "NY" },
    { name: " Recommended   ", code: "RM" },
    { name: "Low to high ", code: "LDN" },
    { name: "High to low", code: "IST" },
  ];
  const onCityChange = (e) => {
    setSelectedCity1(e.value);
  };
  return (
    <>
      <div className="dropdownCustom">
        <Dropdown
          value={selectedCity1}
          options={cities}
          onChange={onCityChange}
          optionLabel="name"
          // placeholder=" student Avg Rating "
          placeholder={
            screenSize.width < 600 ? "Rating" : " student Avg Rating"
          }
        />
      </div>
    </>
  );
};

export default StudentAvgRating;
