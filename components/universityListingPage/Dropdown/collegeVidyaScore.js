import React, { useState } from "react";
import { Dropdown } from "primereact/dropdown";

const CollegeVidyaScore = () => {
  const [selectedCity1, setSelectedCity1] = useState(null);
  const cities = [
    { name: "  College Vidya Score ", code: "NY" },
    { name: " Recommended   ", code: "RM" },
    { name: "Low to high ", code: "LDN" },
    { name: "High to low", code: "IST" },
  ];
  const onCityChange = (e) => {
    setSelectedCity1(e.value);
  };
  return (
    <>
      <div className="dropdownCustom">
        <Dropdown
          value={selectedCity1}
          options={cities}
          onChange={onCityChange}
          optionLabel="name"
          placeholder="  College Vidya Score "
        />
      </div>
    </>
  );
};

export default CollegeVidyaScore;
