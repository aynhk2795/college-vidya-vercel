import Link from "next/link";
import React from "react";
import styles from "/styles/VideoButton.module.scss";

const VideoButton = () => {
  return (
    <>
      <Link href="/">
        <a className={`${styles.videobtn} bgprimary text-white`}>
          Video Counselling
        </a>
      </Link>
    </>
  );
};

export default VideoButton;
