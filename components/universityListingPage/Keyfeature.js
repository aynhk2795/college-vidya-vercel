import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { BsPatchCheck, BsShift, BsAward } from "react-icons/bs";
import { FaBalanceScale } from "react-icons/fa";

const keyfeaturelist = [
  {
    id: 1,
    icon: <BsPatchCheck className="h1" />,
    heading: "India’s #1",
    desc: "Larget Online Education Portal",
  },
  {
    id: 2,
    icon: <BsShift className="h1" />,
    heading: "500 +",
    desc: "Admissions every 1 hour",
  },
  {
    id: 3,
    icon: <FaBalanceScale className="h1" />,
    heading: "Compare",
    desc: "(Decode the right University for you)",
  },
  {
    id: 4,
    icon: <BsAward className="h1" />,
    heading: "Scholarship",
    desc: "Stay updated",
  },
];

const Keyfeature = () => {
  return (
    <>
      <Container className="mb-5">
        <Row>
          {keyfeaturelist.map((list) => (
            <Col key={list.id}>
              <div className="text-center">
                {list.icon}
                <p className="fw-bold mb-0">{list.heading}</p>
                <p>{list.desc}</p>
              </div>
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
};

export default Keyfeature;
