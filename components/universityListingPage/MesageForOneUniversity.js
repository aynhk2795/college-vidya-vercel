import React from "react";
import { FcAbout } from "react-icons/fc";

const MesageForOneUniversity = () => {
  return (
    <>
      <div
        style={{ backgroundColor: "aliceblue" }}
        className="p-4 rounded my-3">
        <div>
          <FcAbout fontSize={30} />
        </div>
        <div>
          <p className="fs-14">
          Hey there, looks like the course in which you are interested is so unique that only a single university provides it. If in future any university starts providing the course and matches our strict parameters of Approvals, Learning Management System, Placement Assistance & Faculty we will notify you. College Vidya strictly inspects all the factors before listing any universities.
          </p>
        </div>
      </div>
    </>
  );
};

export default MesageForOneUniversity;
