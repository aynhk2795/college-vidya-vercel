import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation } from "swiper";
import { Container } from "react-bootstrap";
import { Card } from "primereact/card";
import { FcCustomerSupport } from "react-icons/fc";

const Appcard = [
  {
    id: 1,
    heading: "Personal ClaimHandler",
    content:
      "Your family will be receiving your personal claim handler from our team of claim experts to handle your end-to-end claim related queries and provide assistance for a hassle-free claims experience",
  },
  {
    id: 2,
    heading: "Personal ClaimHandler",
    content:
      "Your family will be receiving your personal claim handler from our team of claim experts to handle your end-to-end claim related queries and provide assistance for a hassle-free claims experience",
  },
  {
    id: 3,
    heading: "Personal ClaimHandler",
    content:
      "Your family will be receiving your personal claim handler from our team of claim experts to handle your end-to-end claim related queries and provide assistance for a hassle-free claims experience",
  },
];

export default function App() {
  return (
    <>
      <Container fluid className="px-0">
        <Swiper
          slidesPerView={3}
          spaceBetween={20}
          loop={true}
          navigation={true}
          breakpoints={{
            "@0.00": {
              slidesPerView: 1,
            },
            "@0.75": {
              slidesPerView: 1,
            },
            "@1.00": {
              slidesPerView: 3,
            },
          }}
          modules={[Navigation]}
          className="mediaSwiper">
          {Appcard.map((list) => (
            <SwiperSlide key={list.id}>
              <Card className="my-2">
                <FcCustomerSupport
                  fontSize={70}
                  className="mb-3 bg-grey p-2 rounded-circle"
                />
                <p className="fw-bold">{list.heading}</p>
                <p className="fs-12">{list.content}</p>
              </Card>
            </SwiperSlide>
          ))}
        </Swiper>
      </Container>
    </>
  );
}
