import Image from "next/image";
import React from "react";
import { Card } from "primereact/card";
import { Container, Row, Col } from "react-bootstrap";
import confused from "/public/images/confused.png";
import Link from "next/link";
import styles from "./Cta.module.scss";

const Cta = () => {
  return (
    <>
      <Container className={`${styles.cta_wrap} mx-auto`}>
        <Card>
          <Row>
            <Col md={2} className="text-center">
              <Image
                src={confused}
                width={200}
                height={200}
                className={`${styles.ctaimg} position-absolute bottom-0 start-0`}
                alt=""
                objectFit="contain"
              />
            </Col>
            <Col
              md={7}
              className="text-center d-flex align-items-center justify-content-center flex-column">
              <p className="m-0">College Vidya Recommender</p>
              <h1 className="cta-heading">
                Not Sure, Which University to choose?
              </h1>
              <p className="m-0">Let us help you find the dream University</p>
            </Col>
            <Col
              md={3}
              className="d-flex align-items-center justify-content-center">
              <Link href="/">
                <a className="bgsecondary text-white py-3 px-4 rounded shadow-1 w-100 text-center my-2">
                  Schedule a Appointment
                </a>
              </Link>
            </Col>
          </Row>
        </Card>
      </Container>
    </>
  );
};

export default Cta;
