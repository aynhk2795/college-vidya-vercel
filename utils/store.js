import { createState } from "@hookstate/core";

const store = createState({
  universitiesToCompare: [],
});

export default store;
