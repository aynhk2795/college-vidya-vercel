export const getSearchSchema = () => {
  return {
    "@context": "http://schema.org",
    "@type": "WebSite",
    url: "https://www.collegevidya.com/",
    potentialAction: [
      {
        "@type": "SearchAction",
        target: "https://www.collegevidya.com/search?q={search_term_string}",
        "query-input": "required name=search_term_string",
      },
      {
        "@type": "SearchAction",
        target:
          "android-app://com.collegevidya.android/collegevidya/de_sq_seg_-search.collegevidya.com-_{search_term_string}",
        "query-input": "required name=search_term_string",
      },
    ],
  };
};

export const getGenericCourseSpecializationTitleSchema = (
  courseName,
  specializationName
) => {
  if (specializationName) {
    return {
      title: `Online ${courseName} In ${specializationName} (${courseName} ${specializationName} Distance Learning 2022-23)`,
      description: `Discover the benefits of earning ${courseName} ${specializationName} degree online and distance. Find and compare top colleges, subjects, fees, approvals, eligibility, careers, more.`,
    };
  } else {
    return {
      title: `Online ${courseName} Programs (${courseName} Distance Courses 2022-23)`,
      description: `Discover the benefits of earning ${courseName} degree online and distance. Find and compare top colleges, subjects, fees, approvals, eligibility, careers, more.`,
    };
  }
};

export const getFAQSchema = (faqs) => {
  let mainEntity = [];

  if (faqs.length) {
    mainEntity.push(
      faqs.map((faq) => {
        return {
          "@type": "Question",
          name: faq.title,
          acceptedAnswer: {
            "@type": "Answer",
            text: faq.content,
          },
        };
      })
    );
  }

  const faqSchema = {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    mainEntity: mainEntity[0] ? mainEntity[0] : mainEntity,
  };

  return faqSchema;
};
