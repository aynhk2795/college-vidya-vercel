import { getVideoData } from "../service/YouTubeService";

export const generateVideoSchema = async (videoURL) => {
  let videoData = await getVideoData(youtubeParser(videoURL))
    .then((response) => response.data.items[0])
    .catch(() => {
      return {};
    });

  let schema = {
    "@context": "http://schema.org",
    "@type": "VideoObject",
    name: `${videoData.snippet.localized.title}`,
    description: `${videoData.snippet.localized.description}`,
    thumbnailUrl: `${videoData.snippet.thumbnails.default.url}`,
    uploadDate: `${videoData.snippet.publishedAt}`,
    duration: `${videoData.contentDetails.duration}`,
    embedUrl: `https://www.youtube.com/embed/${youtubeParser(videoURL)}`,
    interactionCount: `${videoData.statistics.viewCount}`,
  };

  return schema;
};

function youtubeParser(url) {
  var regExp =
    /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
  var match = url.match(regExp);
  return match && match[7].length == 11 ? match[7] : false;
}
