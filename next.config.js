/** @type {import('next').NextConfig} */

const withVideos = require("next-videos");
const nextConfig = {
  reactStrictMode: true,
  cssModules: true,
};
const withCss = require("@zeit/next-css");
const withPurgeCss = require("next-purgecss");

module.exports = withCss(withPurgeCss());
module.exports = nextConfig;
module.exports = withVideos();

module.exports = {
  async headers() {
    return [
      {
        source: "/:all*(svg|jpg|png)",
        locale: false,
        headers: [
          {
            key: "Cache-Control",
            value: "public, max-age=9999999999, must-revalidate",
          },
        ],
      },
    ];
  },
  sassOptions: {
    prependData: `@import "/styles/scss/imports/_variables.scss";`,
  },
  images: {
    domains: [
      "collegevidyanew.s3.amazonaws.com",
      "cv-counsellors.s3.ap-south-1.amazonaws.com",
      "collegevidya.com",
    ],
  },
  trailingSlash: true,
};
