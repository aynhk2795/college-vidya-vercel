import Image from "next/image";
import React from "react";
import { Container, Row, Col, Card } from "react-bootstrap";
import help from "/public/images/how-mentors-helps.png";
import { AiFillStar, AiOutlineMail } from "react-icons/ai";
import { BsPhone } from "react-icons/bs";
import { GoLocation } from "react-icons/go";
import Header from "../components/Global/Header";
import Footer from "../components/Global/Footer";
import { getMenu } from "../service/MiscService";
import ContactForm from "../components/Contact/ContactForm";
import HeaderDecider from "../components/Global/HeaderDecider";
import Head from "next/head";
import { getSearchSchema } from "../utils/genericSchemas";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

const contactus = ({ screenSize, menuData }) => {
  return (
    <>
      <Head>
        <title>Contact US (Get In Touch with Us) - College Vidya</title>
        <meta
          name="description"
          content="What Is Our Approch? Get all your questions answered over a detailed session with College Vidya experienced distance education counselors."
        />
        <meta name="robots" content="index, follow" />
        <link href="https://collegevidya.com/contact-us/" rel="canonical" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Contact US (Get In Touch with Us) - College Vidya"
        />
        <meta
          property="og:description"
          content="What Is Our Approch? Get all your questions answered over a detailed session with College Vidya experienced distance education counselors."
        />
        <meta
          property="og:url"
          content="https://collegevidya.com/contact-us/"
        />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="Contact US (Get In Touch with Us) - College Vidya"
        />
        <meta
          property="twitter:description"
          content="What Is Our Approch? Get all your questions answered over a detailed session with College Vidya experienced distance education counselors."
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/contact-us/"
        />
        <meta property="twitter:image" content="" />
        <meta
          name="keywords"
          content="contact college vidya, collegevidya contact, collegevidya, top online and distance education compare portal, top online education portal"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/contact-us/",
              name: "Contact US (Get In Touch with Us) - College Vidya",
              description:
                "What Is Our Approch? Get all your questions answered over a detailed session with College Vidya experienced distance education counselors.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "Contact US (Get In Touch with Us) - College Vidya",
              description:
                "What Is Our Approch? Get all your questions answered over a detailed session with College Vidya experienced distance education counselors.",
              url: "https://collegevidya.com/contact-us/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo.svg",
              address: "India",
            }),
          }}
        />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <div className="bgprimary py-5">
        <Container>
          <p className="text-white text-center">
            What Is Our Approch? Get all your questions answered over a detailed
            session with College Vidya experienced distance education
            counselors.
          </p>
          <Card className="bg-white py-3">
            <Row>
              <Col md={2}>
                <Image src={help} alt="" />
              </Col>
              <Col
                md={7}
                className="px-4 d-flex flex-column justify-content-center">
                <h3>How College Vidya Mentors Help</h3>
                <Row>
                  <Col sm={6} className="mb-2">
                    <AiFillStar /> Which university to select?
                  </Col>
                  <Col sm={6} className="mb-2">
                    <AiFillStar /> What is the value of the distance degree?
                  </Col>
                  <Col sm={6} className="mb-2">
                    <AiFillStar /> Which specialisation to select?
                  </Col>
                  <Col sm={6} className="mb-2">
                    <AiFillStar /> How much salary hike you can expect?
                  </Col>
                </Row>
              </Col>
              <Col md={3}></Col>
            </Row>
          </Card>
        </Container>
      </div>
      <Container className="my-5">
        <h2 className="text-center">Get in Touch</h2>
        <p className="text-center">
          Call us, send us a message, or come visit us.
        </p>
        <Row>
          <Col md={6} className="d-flex flex-column justify-content-center">
            <p className="mb-4">
              Getting confused? Don’t know what’s the best university & program
              for you? ‘Don’t you worry!’ We at College Vidya have the best
              counselors to guide you in your journey!
            </p>
            <div className="d-flex mb-4 py-2 py-sm-4 px-3 rounded">
              <div>
                <AiOutlineMail className="h3 me-2" />
              </div>
              <div>
                <p className="m-0">Email</p>
                <p className="m-0 fw-bold">info@collegevidya.com</p>
              </div>
            </div>
            <div className="d-flex mb-4 py-2 py-sm-4 px-3 rounded">
              <div>
                <BsPhone className="h3 me-2" />
              </div>
              <div>
                <p className="m-0">Phone</p>
                <p className="m-0 fw-bold">18004205757</p>
              </div>
            </div>
            <div className="d-flex mb-4 py-2 py-sm-4 px-3 rounded">
              <div>
                <GoLocation className="h3 me-2" />
              </div>
              <div>
                <p className="m-0">Address</p>
                <p className="m-0 fw-bold">
                  B-136, B Block, Sector 2, Noida, Uttar Pradesh 201301
                </p>
              </div>
            </div>
          </Col>
          <Col md={6} className="px-4 px-sm-0">
            <div
              style={{ backgroundColor: "aliceblue" }}
              className="rounded  p-3 p-sm-5">
              <ContactForm />
            </div>
          </Col>
        </Row>
      </Container>
      <Footer menuData={menuData} />
    </>
  );
};

export default contactus;
