import Head from "next/head";
import Link from "next/link";
import { Col, Container, Row } from "react-bootstrap";
import Footer from "../components/Global/Footer";
import HeaderDecider from "../components/Global/HeaderDecider";
import OurExpert from "../components/Global/OurExpert";
import { getMenu } from "../service/MiscService";
import { getSearchSchema } from "../utils/genericSchemas";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

const Sitemap = ({ screenSize, menuData }) => {
  return (
    <>
      <Head>
        <title>Sitemap | College Vidya</title>
        <meta
          name="description"
          content="College Vidya's sitemap provides users with an easy way to navigate our site and find what they need. Look at the different sections of College Vidya's website."
        />
        <meta name="robots" content="index, follow" />
        <link href="https://collegevidya.com/sitemap/" rel="canonical" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Sitemap | College Vidya" />
        <meta
          property="og:description"
          content="College Vidya's sitemap provides users with an easy way to navigate our site and find what they need. Look at the different sections of College Vidya's website."
        />
        <meta property="og:url" content="https://collegevidya.com/sitemap/" />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="College Vidya's sitemap provides users with an easy way to navigate our site and find what they need. Look at the different sections of College Vidya's website."
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/sitemap/"
        />
        <meta property="twitter:image" content="" />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/sitemap/",
              name: "Sitemap | College Vidya",
              description:
                "College Vidya's sitemap provides users with an easy way to navigate our site and find what they need. Look at the different sections of College Vidya's website.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "Sitemap | College Vidya",
              description:
                "College Vidya's sitemap provides users with an easy way to navigate our site and find what they need. Look at the different sections of College Vidya's website.",
              url: "https://collegevidya.com/sitemap/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo.svg",
              address: "India",
            }),
          }}
        />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <div style={{ background: "#f3f4f5", padding: "20px 5px 20px 5px" }}>
        <Container>
          <Row>
            <Col md={12}>
              <div
                style={{
                  display: "flex",
                  flexWrap: "wrap",
                  backgroundColor: "#fff",
                  padding: "30px",
                }}>
                {menuData
                  .map((domain) => domain.courses)
                  .flat()
                  .map((course) => (
                    <li key={course.id} className="list-unstyled">
                      <span
                        className="fw-bold me-2 mt-4 d-block mb-2"
                        style={{ listStyle: "none" }}>
                        <Link href={`/courses/${course.slug}`}>
                          <a style={{ color: "#fd4705" }}>
                            {course.display_name}
                          </a>
                        </Link>
                      </span>
                      {course.specializations.map((specialization) => (
                        <span
                          key={specialization.id}
                          style={{ listStyle: "none" }}>
                          <Link
                            href={`/courses/${course.slug}/${specialization.slug}`}>
                            <a
                              className="me-2 mb-2 py-2 bg-light p-3 d-inline-block rounded"
                              style={{ border: "1px solid #eee" }}>
                              {specialization.display_name}
                            </a>
                          </Link>
                        </span>
                      ))}
                    </li>
                  ))}
              </div>
              <div
                style={{
                  display: "flex",
                  flexWrap: "wrap",
                  backgroundColor: "#fff",
                  padding: "30px",
                  marginTop: "20px",
                }}>
                <li
                  className="d-block w-100 mb-3"
                  style={{ listStyle: "none", fontWeight: "bold" }}>
                  Top University
                </li>
                {menuData
                  .map((domain) => domain.courses)
                  .flat()
                  .map((course) => (
                    <span key={course.id}>
                      <li style={{ listStyle: "none" }}>
                        <Link
                          href={`/top-universities-colleges/${course.slug}`}>
                          <a
                            className="me-2 mb-2 py-2 bg-light p-3 d-inline-block rounded"
                            style={{ border: "1px solid #eee" }}>
                            {course.name}
                          </a>
                        </Link>
                      </li>
                    </span>
                  ))}
              </div>
              <div
                style={{
                  display: "flex",
                  flexWrap: "wrap",
                  backgroundColor: "#fff",
                  padding: "30px",
                  marginTop: "20px",
                }}>
                <li
                  className="d-block w-100 mb-3"
                  style={{ listStyle: "none", fontWeight: "bold" }}>
                  Others
                </li>
                <li style={{ listStyle: "none" }}>
                  <Link href={"/about-us"}>
                    <a
                      className="me-2 mb-2 py-2 bg-light p-3 d-inline-block rounded"
                      style={{ border: "1px solid #eee" }}>
                      About Us
                    </a>
                  </Link>
                </li>
                <li style={{ listStyle: "none" }}>
                  <Link href={"/contact-us"}>
                    <a
                      className="me-2 mb-2 py-2 bg-light p-3 d-inline-block rounded"
                      style={{ border: "1px solid #eee" }}>
                      Contact Us
                    </a>
                  </Link>
                </li>
                <li style={{ listStyle: "none" }}>
                  <Link href={"/our-trust"}>
                    <a
                      className="me-2 mb-2 py-2 bg-light p-3 d-inline-block rounded"
                      style={{ border: "1px solid #eee" }}>
                      Our Trust
                    </a>
                  </Link>
                </li>
                <li style={{ listStyle: "none" }}>
                  <Link href={"/blog"}>
                    <a
                      className="me-2 mb-2 py-2 bg-light p-3 d-inline-block rounded"
                      style={{ border: "1px solid #eee" }}>
                      Blog
                    </a>
                  </Link>
                </li>
                <li style={{ listStyle: "none" }}>
                  <Link href={"/meet-team"}>
                    <a
                      className="me-2 mb-2 py-2 bg-light p-3 d-inline-block rounded"
                      style={{ border: "1px solid #eee" }}>
                      Meet Team
                    </a>
                  </Link>
                </li>
                <li style={{ listStyle: "none" }}>
                  <Link href={"/suggest-me-an-university"}>
                    <a
                      className="me-2 mb-2 py-2 bg-light p-3 d-inline-block rounded"
                      style={{ border: "1px solid #eee" }}>
                      Suggest me a university
                    </a>
                  </Link>
                </li>
                <li style={{ listStyle: "none" }}>
                  <Link href={"/privacy-policy"}>
                    <a
                      className="me-2 mb-2 py-2 bg-light p-3 d-inline-block rounded"
                      style={{ border: "1px solid #eee" }}>
                      Our Policy
                    </a>
                  </Link>
                </li>
                <li style={{ listStyle: "none" }}>
                  <Link href={"/collection"}>
                    <a
                      className="me-2 mb-2 py-2 bg-light p-3 d-inline-block rounded"
                      style={{ border: "1px solid #eee" }}>
                      College Vidya TV
                    </a>
                  </Link>
                </li>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <OurExpert />
      <Footer menuData={menuData} />
    </>
  );
};

export default Sitemap;
