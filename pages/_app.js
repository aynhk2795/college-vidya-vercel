import Head from "next/head";
import React from "react";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "semantic-ui-css/semantic.min.css";
import "../node_modules/react-owl-carousel2/src/owl.carousel.css";
import "../node_modules/react-owl-carousel2/src/owl.theme.default.css";
import "../styles/Globals.scss";
import { SSRProvider } from "react-bootstrap";
import useWindowSize from "/components/Global/use-window-size";
import "aos/dist/aos.css";
import "izitoast/dist/css/iziToast.min.css";
import "tippy.js/dist/tippy.css";
import "tippy.js/animations/scale.css";
import MobileFooter from "../components/Global/MobileFooter";
import { useRouter } from "next/router";
import NextNProgress from "nextjs-progressbar";

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const size = useWindowSize();

  return (
    <SSRProvider>
      <NextNProgress color="#0074d7" options={{ showSpinner: false }} />
      <div className="container-fluid mx-auto px-0">
        <Head>
          <title>
            Compare (Online &amp; Distance) Colleges and Universities 2022
          </title>
          <link rel="icon" href="/favicon.ico" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <meta
            name="description"
            content="Compare distance and online colleges universities on their fees, courses, approvals, e-learning facility, emi option and other details only at College Vidya compare portal."
          />
          <meta name="robots" content="index, follow" />
          <link href="https://collegevidya.com/" rel="canonical" />
          <meta property="og:locale" content="en_US" />
          <meta property="og:type" content="website" />
          <meta
            property="og:title"
            content="Compare (Online &amp; Distance) Colleges and Universities 2022"
          />
          <meta
            property="og:description"
            content="Compare distance and online colleges universities on their fees, courses, approvals, e-learning facility, emi option and other details only at College Vidya compare portal."
          />
          <meta property="og:url" content="https://collegevidya.com/" />
          <meta property="og:site_name" content="College Vidya" />
          <meta
            property="og:article:publisher"
            content="https://www.facebook.com/collegevidya"
          />
          <meta property="og:image" content="" />
          <meta property="twitter:card" content="summary" />
          <meta property="twitter:site" content="@CollegeVidya" />
          <meta
            property="twitter:title"
            content="Compare (Online &amp; Distance) Colleges and Universities 2022"
          />
          <meta
            property="twitter:description"
            content="Compare distance and online colleges universities on their fees, courses, approvals, e-learning facility, emi option and other details only at College Vidya compare portal."
          />
          <meta property="twitter:url" content="https://collegevidya.com/" />
          <meta property="twitter:image" content="" />
        </Head>

        <Component screenSize={size} {...pageProps} />

        {router &&
        router.pathname.includes(
          "suggest-me-an-university"
        ) ? null : size.width > 600 ? null : (
          <MobileFooter screenSize={size} />
        )}
      </div>
    </SSRProvider>
  );
}

export default MyApp;
