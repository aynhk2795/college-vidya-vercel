import Head from "next/head";
import DesktopBanner from "../components/Homepage/Slider/DesktopBanner";
import MobileBanner from "../components/Homepage/Slider/MobileBanner";
import KeyPoint from "/components/Homepage/KeyPoint";
import Course from "/components/Homepage/Course";
import Colorcard from "/components/Homepage/Slider/Colorcard";
import AdmissionGuide from "/components/Homepage/AdmissionGuide";
import CompareFeature from "/components/Homepage/CompareFeature";
import OneStop from "/components/Homepage/Slider/OneStop";
import AllUniversity from "/components/Homepage/AllUniversity";
import Faq from "/components/Homepage/Accordion/Faq";
import Media from "/components/Homepage/Slider/Media";
import Awards from "/components/Homepage/Slider/Awards";
import OurExpert from "/components/Global/OurExpert";
import RealExpert from "/components/Homepage/Slider/RealExpert";
import MobileMenu from "/components/Global/MobileMenu";
// import useWindowSize from "/components/Global/use-window-size";
import {
  getBanners,
  getCompareFeatureData,
  getCounsellorData,
  getFaqData,
  getFeatureData,
  getInfoBanners,
  getMenu,
  getOneStopData,
  getOneStopImages,
  getTagLine,
  getUniversityData,
} from "../service/MiscService";
import Header from "../components/Global/Header";
import Footer from "../components/Global/Footer";
import ThreeSlider from "../components/WhyCollegevidya/ThreeSlider";
import MobileMenu1 from "../components/Global/MobileMenu1";
import HeaderDecider from "../components/Global/HeaderDecider";

export async function getStaticProps() {
  // const size = useWindowSize();
  const menuData = await getMenu().then((response) => response.data);
  const infoBanners = await getInfoBanners().then((response) => response.data);
  const featureData = await getFeatureData().then((response) => response.data);
  const compareFeatureData = await getCompareFeatureData().then(
    (response) => response.data
  );
  const oneStopData = await getOneStopData().then((response) => response.data);
  const oneStopImages = await getOneStopImages().then(
    (response) => response.data
  );
  const counsellors = await getCounsellorData().then(
    (response) => response.data
  );
  const universityData = await getUniversityData().then(
    (response) => response.data
  );
  const faqData = await getFaqData(1).then((response) => response.data.data);
  const bannerData = await getBanners().then((response) => response.data);
  const taglineData = await getTagLine().then((response) => response.data[0]);

  return {
    props: {
      menuData: menuData,
      infoBanners: infoBanners,
      featureData: featureData,
      compareFeatureData: compareFeatureData,
      oneStopData: oneStopData,
      oneStopImages: oneStopImages,
      counsellors: counsellors,
      universityData: universityData,
      faqData: faqData,
      bannerData: bannerData,
      taglineData: taglineData,
    },
    revalidate: 10,
  };
}

export default function Home({
  menuData,
  infoBanners,
  featureData,
  compareFeatureData,
  oneStopData,
  oneStopImages,
  counsellors,
  universityData,
  faqData,
  screenSize,
  bannerData,
  taglineData,
}) {
  return (
    <div>
      <Head>
        <title>
          Compare (Online &amp; Distance) Colleges and Universities 2022
        </title>
      </Head>

      {/* {screenSize.width < 1200 ? (
        <MobileMenu1 screenSize={screenSize} menuData={menuData} />
      ) : (
        <Header menuData={menuData} />
      )} */}
      <HeaderDecider
        screenSize={screenSize}
        menuData={menuData}
        taglineData={taglineData}
      />

      {screenSize.width < 768 ? (
        <MobileBanner
          bannerData={bannerData.filter((banner) => banner.type === "mobile")}
        />
      ) : (
        <DesktopBanner
          bannerData={bannerData.filter((banner) => banner.type === "desktop")}
        />
      )}

      <KeyPoint />
      <Course menuData={menuData} />
      <Colorcard infoBanners={infoBanners} />
      <AdmissionGuide featureData={featureData} />
      <CompareFeature compareFeatureData={compareFeatureData} />
      <OneStop oneStopData={oneStopData} oneStopImages={oneStopImages} />

      <RealExpert counsellors={counsellors} />
      <AllUniversity universityData={universityData} />
      <Media />
      <ThreeSlider universityData={universityData} />
      <Faq faqData={faqData} />

      <OurExpert />

      <Footer menuData={menuData} />
    </div>
  );
}
