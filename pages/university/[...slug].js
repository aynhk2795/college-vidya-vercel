import React, { useEffect } from "react";
import UniversityPage from "../../components/UniversityPage/UniversityPage";
import { useRouter } from "next/router";
import { Downgraded, useState } from "@hookstate/core";
// import UniversityCoursePage from '../../components/UniversityPage/UniversityCoursePage';
// import UniversityCourseSpecializationPage from '../../components/UniversityPage/UniversityCourseSpecializationPage';
import FourOhFour from "../404";
import { getMenu } from "../../service/MiscService";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

export async function getStaticPaths() {
  const paths = [];

  return { paths, fallback: "blocking" };
}

const UniversityIndex = ({ menuData }) => {
  const router = useRouter();
  const pageToLoad = useState(null);

  useEffect(() => {
    if (router.query.slug) {
      // if(router.query.slug.length === 3) {
      //   pageToLoad.set(<UniversityCourseSpecializationPage universitySlug={router.query.slug[0]} courseSlug={router.query.slug[1]} specializationSlug={router.query.slug[2]} />);
      // }
      // else if(router.query.slug.length === 2) {
      //   pageToLoad.set(<UniversityCoursePage universitySlug={router.query.slug[0]} courseSlug={router.query.slug[1]} />);
      // }
      if (router.query.slug.length === 1) {
        pageToLoad.set(
          <UniversityPage
            menuData={menuData}
            universitySlug={router.query.slug[0]}
          />
        );
      } else {
        pageToLoad.set(<FourOhFour />);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  return <>{pageToLoad.attach(Downgraded).get()}</>;
};

export default UniversityIndex;
