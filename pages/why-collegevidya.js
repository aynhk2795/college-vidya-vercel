import React from "react";
import { Card } from "primereact/card";
import Image from "next/image";
import { Container, Row, Col } from "react-bootstrap";
import Banner from "../components/WhyCollegevidya/Banner";
import card1 from "/public/images/whycv/researcher.png";
import card2 from "/public/images/whycv/admission.png";
import card3 from "/public/images/whycv/advisor.png";
import card4 from "/public/images/whycv/department.png";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import ThreeSlider from "../components/WhyCollegevidya/ThreeSlider";
import CvProvide from "../components/WhyCollegevidya/CvProvide";
import Feedbacks from "../components/WhyCollegevidya/Feedbacks";
import FeedbackTextSlider from "../components/WhyCollegevidya/FeedbackTextSlider";
import FeedbackVideoSlider from "../components/WhyCollegevidya/FeedbackVideoSlider";
import WhyTakeAdmission from "../components/WhyCollegevidya/WhyTakeAdmission";
import { TabView, TabPanel } from "primereact/tabview";
import CVInNews from "../components/WhyCollegevidya/CVInNews";
import CVInNewsSlider from "../components/WhyCollegevidya/CVInNewsSlider";
import Header from "../components/Global/Header";
import Footer from "../components/Global/Footer";
import { getMenu, getUniversityData } from "../service/MiscService";
import HeaderDecider from "../components/Global/HeaderDecider";
import styles from "./whycollegevidya.module.scss";
import WhyCollegevidyaHeading from "../components/WhyCollegevidya/WhyCollegevidyaHeading";
import Head from "next/head";
import { getSearchSchema } from "../utils/genericSchemas";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);
  const universityData = await getUniversityData().then(
    (response) => response.data
  );

  return {
    props: {
      menuData: menuData,
      universityData: universityData,
    },
    revalidate: 10,
  };
}
const whycollegevidya = ({ screenSize, menuData, universityData }) => {
  return (
    <>
      <Head>
        <title>Why Choose College Vidya? - Online University Platform</title>
        <meta
          name="description"
          content="Choose the online course you want to pursue and get full details of all the approved online universities/colleges providing the course within 2 minutes!"
        />
        <meta name="robots" content="index, follow" />
        <link
          href="https://collegevidya.com/why-collegevidya/"
          rel="canonical"
        />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Why Choose College Vidya? - Online University Platform"
        />
        <meta
          property="og:description"
          content="Choose the online course you want to pursue and get full details of all the approved online universities/colleges providing the course within 2 minutes!"
        />
        <meta
          property="og:url"
          content="https://collegevidya.com/why-collegevidya/"
        />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="Why Choose College Vidya? - Online University Platform"
        />
        <meta
          property="twitter:description"
          content="Choose the online course you want to pursue and get full details of all the approved online universities/colleges providing the course within 2 minutes!"
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/why-collegevidya/"
        />
        <meta property="twitter:image" content="" />
        <meta
          name="keywords"
          content="why college vidya, why choose college vidya, collegevidya, top online and distance education compare portal"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/why-collegevidya/",
              name: "Why Choose College Vidya? - Online University Platform",
              description:
                "Choose the online course you want to pursue and get full details of all the approved online universities/colleges providing the course within 2 minutes!",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "Why Choose College Vidya? - Online University Platform",
              description:
                "Choose the online course you want to pursue and get full details of all the approved online universities/colleges providing the course within 2 minutes!",
              url: "https://collegevidya.com/why-collegevidya/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo2.png",
              address: "India",
            }),
          }}
        />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <Banner />
      <Container
        className={`${styles.content_wrapper} my-2 my-sm-5 py-2 py-sm-5`}>
        <WhyCollegevidyaHeading title="College Vidya Strengths" />
        <p className="text-center">
          College Vidya Family India’s Biggest Platform for Online Universities
        </p>

        <Row>
          <Col md={3} className="mb-3 col-6" sm={3}>
            <Card className="shadow-1">
              <Image src={card1} width={55} height={55} alt="" />
              <p className="h2 text-dark m-0 mt-3">4,00,000+</p>
              <p className="text-dark">
                Unique Monthly Visitors from multiple channels
              </p>
              <p className="fs-12 text-secondary">#ChunoWahiJoHaiSahi </p>
            </Card>
          </Col>
          <Col md={3} className="mb-3 col-6" sm={3}>
            <Card className="shadow-1">
              <Image src={card2} width={55} height={55} alt="" />
              <p className="h2 text-dark m-0 mt-3">1,00,000+</p>
              <p className="text-dark">
                Online Admissions with College Vidya Guidance{" "}
              </p>
              <p className="fs-12 text-secondary">#ChunoWahiJoHaiSahi </p>
            </Card>
          </Col>
          <Col md={3} className="mb-3 col-6" sm={3}>
            <Card className="shadow-1">
              <Image src={card3} width={55} height={55} alt="" />
              <p className="h2 text-dark m-0 mt-3">9000+</p>
              <p className="text-dark">
                {" "}
                Free Telephonic Career Counselling Sessions
              </p>
              <p className="fs-12 text-secondary">#ChunoWahiJoHaiSahi </p>
            </Card>
          </Col>
          <Col md={3} className="mb-3 col-6" sm={3}>
            <Card className="shadow-1">
              <Image src={card4} width={55} height={55} alt="" />
              <p className="h2 text-dark m-0 mt-3">3500+</p>
              <p className="text-dark">
                Free Face-to-Face Career Counselling Sessions
              </p>
              <p className="fs-12 text-secondary">#ChunoWahiJoHaiSahi </p>
            </Card>
          </Col>
        </Row>
      </Container>

      <ThreeSlider universityData={universityData} />
      <CvProvide />
      <Feedbacks />

      <Container className={`${styles.feedback_wrapper}`}>
        <TabView style={{ maxWidth: "240px", margin: "0 auto" }}>
          <TabPanel header="Text View">
            <FeedbackTextSlider />
          </TabPanel>
          <TabPanel header="Video View">
            <FeedbackVideoSlider />
          </TabPanel>
        </TabView>
        <WhyTakeAdmission />
        <CVInNews />
        <CVInNewsSlider />
      </Container>
      <Footer menuData={menuData} />
    </>
  );
};

export default whycollegevidya;
