import React from "react";
import QuestionForm from "/components/Otta/QuestionForm";
import Ottaheader from "/components/Otta/Ottaheader";

import { getMenu } from "../service/MiscService";
import Head from "next/head";
import { getSearchSchema } from "../utils/genericSchemas";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData
        .map((menu) => menu.courses)
        .flat()
        .map(({ id, display_name }) => ({ id, display_name })),
      completeMenuData: menuData,
    },
    revalidate: 10,
  };
}

const suggest = ({ menuData, completeMenuData }) => {
  return (
    <>
      <Head>
        <title>Suggest Me the Best University: Where to Study?</title>
        <meta
          name="description"
          content="Find your best matching university in a few steps. Choosing where to study, and to which university you should go, is one of the most important decisions you will make."
        />
        <meta name="robots" content="index, follow" />
        <link
          href="https://collegevidya.com/suggest-me-an-university/"
          rel="canonical"
        />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Suggest Me the Best University: Where to Study?"
        />
        <meta
          property="og:description"
          content="Find your best matching university in a few steps. Choosing where to study, and to which university you should go, is one of the most important decisions you will make."
        />
        <meta
          property="og:url"
          content="https://collegevidya.com/suggest-me-an-university/"
        />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="Find your best matching university in a few steps. Choosing where to study, and to which university you should go, is one of the most important decisions you will make."
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/suggest-me-an-university/"
        />
        <meta property="twitter:image" content="" />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/suggest-me-an-university/",
              name: "Suggest Me the Best University: Where to Study?",
              description:
                "Find your best matching university in a few steps. Choosing where to study, and to which university you should go, is one of the most important decisions you will make.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "Suggest Me the Best University: Where to Study?",
              description:
                "Find your best matching university in a few steps. Choosing where to study, and to which university you should go, is one of the most important decisions you will make.",
              url: "https://collegevidya.com/suggest-me-an-university/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo2.png",
              address: "India",
            }),
          }}
        />
      </Head>
      <Ottaheader menuData={menuData} />
      <QuestionForm completeMenuData={completeMenuData} />
    </>
  );
};

export default suggest;
