import Image from "next/image";
import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import OurExpert from "../components/Global/OurExpert";
import HeroSection from "/components/Global/Jumbotron/HeroSection";
import linkedin from "/public/images/linkedin.png";
import Header from "../components/Global/Header";
import Footer from "../components/Global/Footer";
import { getMenu, getTeam } from "../service/MiscService";
import Link from "next/link";
import HeaderDecider from "../components/Global/HeaderDecider";
import Head from "next/head";
import { getSearchSchema } from "../utils/genericSchemas";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);
  const meetTeam = await getTeam().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
      meetTeam: meetTeam,
    },
    revalidate: 10,
  };
}

const team = ({ screenSize, menuData, meetTeam }) => {
  return (
    <>
      <Head>
        <title>
          Meet Our Team - College Vidya Online & Distance Education Compare
          Portal
        </title>
        <meta
          name="description"
          content="When you partner with College Vidya, you’re partnering with our people. Ever Enthusiastic and Supercharged team at College Vidya."
        />
        <meta name="robots" content="index, follow" />
        <link href="https://collegevidya.com/meet-team/" rel="canonical" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Meet Our Team - College Vidya Online & Distance Education Compare Portal"
        />
        <meta
          property="og:description"
          content="When you partner with College Vidya, you’re partnering with our people. Ever Enthusiastic and Supercharged team at College Vidya."
        />
        <meta property="og:url" content="https://collegevidya.com/meet-team/" />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="Meet Our Team - College Vidya Online & Distance Education Compare Portal"
        />
        <meta
          property="twitter:description"
          content="When you partner with College Vidya, you’re partnering with our people. Ever Enthusiastic and Supercharged team at College Vidya."
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/meet-team/"
        />
        <meta property="twitter:image" content="" />
        <meta
          name="keywords"
          content="meet team college vidya, distance online education compare portal, top online and distance education compare portal, team of college vidya"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/meet-team/",
              name: "Meet Our Team - College Vidya Online & Distance Education Compare Portal",
              description:
                "When you partner with College Vidya, you’re partnering with our people. Ever Enthusiastic and Supercharged team at College Vidya.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "Meet Our Team - College Vidya Online & Distance Education Compare Portal",
              description:
                "When you partner with College Vidya, you’re partnering with our people. Ever Enthusiastic and Supercharged team at College Vidya.",
              url: "https://collegevidya.com/meet-team/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo2.png",
              address: "India",
            }),
          }}
        />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <HeroSection title="Meet The Team" />

      <Container className="my-5">
        <Row>
          <Col md={12} className="mx-auto">
            <Row className="row-cols-2 row-cols-sm-3 row-cols-md-4">
              {meetTeam.map((player) => (
                <Col className="mb-4" key={player.id}>
                  <div
                    className="team_card position-relative text-center p-3"
                    style={{ border: "1px solid #ddd", padding: "40px 0" }}>
                    <Image
                      src={player.thumbnail}
                      width={169}
                      height={169}
                      className="rounded-circle"
                      alt=""
                      objectFit="cover"
                    />
                    <p className="m-0 mt-3 fs-16 text-truncate">
                      {player.name}
                    </p>
                    <p className="mb-2 textprimary fs-14  text-truncate">
                      {player.destination}
                    </p>
                    <Link href={player.link}>
                      <a>
                        <Image src={linkedin} width={23} height={23} alt="" />
                      </a>
                    </Link>
                  </div>
                </Col>
              ))}
            </Row>
          </Col>
        </Row>

        <OurExpert />
      </Container>
      <Footer menuData={menuData} />
    </>
  );
};

export default team;
