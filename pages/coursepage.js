import React, { useState } from "react";
import Banner from "../components/CoursePage/Banner";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { Container } from "react-bootstrap";
import { Link } from "react-scroll";
import Specialisation from "../components/CoursePage/Slider/Specialisation";
import OtherUniversities from "../components/CoursePage/Slider/OtherUniversities";
import Faq from "../components/CoursePage/Tabs/Faq.js";
import Testimonial from "../components/CoursePage/Slider/Testimonial";
import Blog from "../components/CoursePage/Slider/Blog";
import Video from "../components/CoursePage/Slider/Video";
import { BsChevronRight } from "react-icons/bs";
import WriteReview from "../components/CoursePage/Modal/WriteReview.js";
import AllReview from "../components/CoursePage/Modal/AllReview";
import About from "../components/CoursePage/Tabs/About";
import CourseDetail from "../components/CoursePage/Tabs/CourseDetail";
import ApprovedBy from "../components/CoursePage/Tabs/ApprovedBy";
import SampleCerficate from "../components/CoursePage/Tabs/SampleCerficate";
import ReviewBar from "../components/CoursePage/Tabs/Review/ReviewBar";
import ReviewCard from "../components/CoursePage/Tabs/Review/ReviewCard";
import { TabView, TabPanel } from "primereact/tabview";
import Footer from "../components/Global/Footer";
import Header from "../components/Global/Header";
import { getMenu } from "../service/MiscService";
// import styles from '/styles/CoursePage.module.scss';

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

const Coursepage = ({ menuData }) => {
  const [activeIndex1, setActiveIndex1] = useState(0);
  const [isActive, setActive] = useState(false);
  return (
    <>
      <Header menuData={menuData} />
      <div>
        <Banner
          title="Online & Distance MBA"
          tagline="Ranked Among Top 10 Universities in India"
        />

        <Container className="my-5">
          <Row>
            <Col sm="3" className="d-none d-lg-block">
              <ul
                className={`university_page_list_items list-unstyled sticky-top shadow-sm p-4`}>
                <li className="mb-3 cursor-pointer">
                  <Link
                    duration={10}
                    activeClass="active"
                    to="tab1"
                    spy={true}
                    smooth={true}>
                    <span>About</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>

                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab2" spy={true} smooth={true}>
                    <span>Course Detail</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab3" spy={true} smooth={true}>
                    <span>Approved By</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab4" spy={true} smooth={true}>
                    <span>Specialisation</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab5" spy={true} smooth={true}>
                    <span>Sample Certificate</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab6" spy={true} smooth={true}>
                    <span>Other Universities</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab7" spy={true} smooth={true}>
                    <span>Review</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab8" spy={true} smooth={true}>
                    <span>Faq</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab9" spy={true} smooth={true}>
                    <span>Blog / Video</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab10" spy={true} smooth={true}>
                    <span>Testimonial</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
              </ul>
            </Col>
            <Col sm="12" md={12} lg={9} xl={9}>
              <div className="shadow-sm p-4">
                <div id="tab1" className="mb-4">
                  <About />
                </div>
                <div id="tab2" className="mb-4">
                  <CourseDetail />
                </div>
                <div id="tab3" className="mb-4">
                  <ApprovedBy />
                </div>
                <div id="tab4" className="mb-4">
                  <Specialisation />
                </div>
                <div id="tab5" className="mb-4">
                  <SampleCerficate />
                </div>
                <div id="tab6" className="mb-4">
                  <OtherUniversities />
                </div>
                <div id="tab7" className="mb-4">
                  <ReviewBar />

                  <WriteReview />
                  <h6>Top reviews from India</h6>

                  <ReviewCard />
                  <AllReview />
                </div>
                <div id="tab8" className="mb-4">
                  <Faq />
                </div>
                <div id="tab9" className={`mb-4 blog_video_tab`}>
                  <TabView
                    activeIndex={activeIndex1}
                    onTabChange={(e) => setActiveIndex1(e.index)}>
                    <TabPanel header="Blog">
                      <Blog />
                    </TabPanel>
                    <TabPanel header="Video">
                      <Video />
                    </TabPanel>
                  </TabView>
                </div>
                <div id="tab10" className="mb-4">
                  <Testimonial />
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <Footer menuData={menuData} />
    </>
  );
};

export default Coursepage;
