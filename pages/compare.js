import React from "react";
import Header1 from "/components/Global/Header1";
import CompareContent from "/components/Comparepage/CompareContent/CompareContent";
import RealExpert from "/components/Homepage/Slider/RealExpert";
import OurExpert from "/components/Global/OurExpert";
import { getCounsellorData, getMenu } from "../service/MiscService";
import Footer from "../components/Global/Footer";

export async function getStaticProps() {
  const counsellors = await getCounsellorData().then(
    (response) => response.data
  );
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData,
      menuData,
      counsellors: counsellors,
    },
    revalidate: 10,
  };
}

const compare = ({ counsellors, menuData, screenSize }) => {
  return (
    <>
      <Header1 screenSize={screenSize} />
      <CompareContent />
      <RealExpert counsellors={counsellors} />
      <OurExpert />
      <Footer menuData={menuData} />
    </>
  );
};

export default compare;
