import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Header from "../../components/Global/Header";
import Image from "next/image";
import { getMenu } from "../../service/MiscService";
import { BreadCrumb } from "primereact/breadcrumb";
import blog_banner from "/public/images/blogdetail.jpeg";
import { BsCalendar, BsChevronLeft, BsChevronRight } from "react-icons/bs";
import SocialShare from "../../components/Global/SocialShare";
import Category from "../../components/Blog/Category";
import RecentPost from "../../components/Blog/RecentPost";
import CVEdge from "../../components/Blog/CVEdge";
import blogside from "/public/images/compare-collegevidya-sidebar.jpeg";
import ContactForm from "../../components/Contact/ContactForm";
import Link from "next/link";
import BlogCard from "/components/Blog/BlogCard";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import styles from "./blog.module.scss";
import {
  getBlogDetails,
  getBlogSlugs,
  getCategorySlugs,
} from "../../service/BlogService";
import { useRouter } from "next/router";
import HeaderDecider from "../../components/Global/HeaderDecider";

export async function getStaticPaths() {
  const allSlugs = await getBlogSlugs().then((response) => response.data.data);

  const paths = allSlugs.map((blogSlug) => {
    return {
      params: {
        blogSlug: blogSlug.slug,
      },
    };
  });

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const blogSlug = context.params.blogSlug;
  const menuData = await getMenu().then((response) => response.data);
  const allCategories = await getCategorySlugs().then(
    (response) => response.data
  );

  const blogData = await getBlogDetails(blogSlug).then(
    (response) => response.data.data[0]
  );

  const recentBlogs = await getBlogDetails(blogSlug).then(
    (response) => response.data.recent_posts
  );

  if (!blogData) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      menuData: menuData,
      blogData: blogData,
      recentBlogs: recentBlogs,
      allCategories: allCategories,
    },
    revalidate: 10,
  };
}
const BlogDetail = ({
  screenSize,
  menuData,
  blogData,
  recentBlogs,
  allCategories,
}) => {
  const router = useRouter();

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <Container className={`${styles.detail_page}`}>
        <Row>
          <Col md={9} className="pe-0 pe-sm-5">
            <Image src={blogData.image} width={930} height={384} alt="" />
            <div className={`${styles.breadcrumb} fs-14 ps-0 mt-4`}>
              <Link href={"/blog"}>
                <a style={{ color: "#f75d34" }}>Home</a>
              </Link>{" "}
              &gt;{" "}
              <Link href={`/blog/category/${blogData.categories[0].slug}`}>
                <a style={{ color: "#f75d34" }}>
                  {blogData.categories[0].name}
                </a>
              </Link>{" "}
              &gt; <span dangerouslySetInnerHTML={{ __html: blogData.title }} />
            </div>
            <h2 className="fw-bold my-3 blogdetail_title">
              <div dangerouslySetInnerHTML={{ __html: blogData.title }} />
            </h2>
            <p className="fs-14 d-flex align-items-center">
              <BsCalendar className="me-2" /> {blogData.created_date}
            </p>
            <div className="d-flex gap-2">
              <SocialShare
                url={`https://collegevidya.com/blog/${blogData.slug}`}
                title={blogData.title}
              />
            </div>
            <p className="fs-12 mt-4">Listen us</p>
            <audio src="/" controls className="w-100 mb-3"></audio>
            <div
              className={`${styles.blog_detail_content}`}
              dangerouslySetInnerHTML={{ __html: blogData.content }}
            />
            <div className="d-flex justify-content-center my-3 my-sm-5">
              <Link href="/suggest-me-an-university">
                <a className={`${styles.suggest_link}`}>
                  Suggest me a University
                </a>
              </Link>
            </div>

            <div className="d-flex justify-content-between gap-4">
              <Link href={`/blog/${recentBlogs[1].slug}`}>
                <a className="col d-flex align-items-center gap-2">
                  <BsChevronLeft fontSize={35} /> {recentBlogs[1].title}
                </a>
              </Link>
              <Link href={`/blog/${recentBlogs[0].slug}`}>
                <a className="col d-flex align-items-center text-end gap-2">
                  {recentBlogs[0].title} <BsChevronRight fontSize={25} />{" "}
                </a>
              </Link>
            </div>

            <h3 className="fw-bold">Read Next</h3>

            <Swiper
              slidesPerView={4}
              spaceBetween={20}
              loop={false}
              navigation={true}
              // pagination={{
              //   clickable: true,
              // }}
              pagination={false}
              breakpoints={{
                "@0.00": {
                  slidesPerView: 1,
                  // spaceBetween: 20,
                },
                500: {
                  slidesPerView: 2,
                  // spaceBetween: 20,
                },
                1024: {
                  slidesPerView: 2,
                  // spaceBetween: 20,
                },
                1336: {
                  slidesPerView: 3,
                  // spaceBetween: 20,
                },
              }}
              modules={[Navigation, Pagination]}
              className="mediaSwiper">
              {recentBlogs.map((recent) => (
                <SwiperSlide key={recent.id} className="mt-3 pb-4">
                  <BlogCard
                    fromBlog={true}
                    title={recent.title}
                    image={recent.image}
                    slug={recent.slug}
                    content={recent.content}
                    created_date={recent.created_date}
                  />
                </SwiperSlide>
              ))}
            </Swiper>
          </Col>
          <Col md={3}>
            <RecentPost recentBlogs={recentBlogs} />
            <Category allCategories={allCategories} />

            <CVEdge />

            <div>
              <Image src={blogside} className="rounded" alt="" />
            </div>
            <div
              className="mt-3 sticky-top px-4 pt-4 pb-4 mb-5"
              style={{ backgroundColor: "#faebd7" }}>
              <ContactForm />
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default BlogDetail;
