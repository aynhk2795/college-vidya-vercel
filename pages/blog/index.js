import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { Container } from "react-bootstrap";
import { flushSync } from "react-dom";
import BlogCard from "../../components/Blog/BlogCard";
import Footer from "../../components/Global/Footer";
import HeaderDecider from "../../components/Global/HeaderDecider";
import { getBlogs } from "../../service/BlogService";
import { getMenu } from "../../service/MiscService";
import HeroSection from "/components/Global/Jumbotron/HeroSection";
import { Button } from "primereact/button";
import Head from "next/head";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

const Blog = ({ screenSize, menuData }) => {
  const [blogs, setBlogs] = useState([]);
  const [totalBlogs, setTotalBlogs] = useState(0);

  const [minBlogs, setMinBlogs] = useState(1);
  const [maxBlogs, setMaxBlogs] = useState(40);

  const [loadingBlogs, setLoadingBlogs] = useState(false);

  useEffect(() => {
    getBlogs(minBlogs, maxBlogs).then((response) => {
      setBlogs((prev) => prev.concat(response.data.data));
      setTotalBlogs(response.data.total_blogs);
    });
  }, [minBlogs, maxBlogs]);

  function loadMore() {
    if (totalBlogs >= maxBlogs) {
      setLoadingBlogs(true);
      flushSync(() => {
        setMinBlogs((minBlogs) => minBlogs + 40);
        setMaxBlogs((maxBlogs) => maxBlogs + 40);
      });
      setLoadingBlogs(false);
    } else {
      setLoadingBlogs(false);
    }
  }

  return (
    <>
      <Head>
        <title>
          Blog - College Vidya (Distance Education Colleges Admissions, Courses,
          Fees)
        </title>
        <link rel="canonical" href="https://collegevidya.com/blog/" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Blog - College Vidya (Distance Education Colleges Admissions, Courses, Fees)"
        />
        <meta property="og:url" content="https://collegevidya.com/blog/" />
        <meta
          property="og:site_name"
          content="Blog - College Vidya (Distance Education Colleges Admissions, Courses, Fees)"
        />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <HeroSection title="CollegeVidya Blog" />
      <div className="album py-5">
        <Container>
          <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-4">
            {blogs.map((blog) => (
              <BlogCard
                key={blog.id}
                title={blog.title}
                image={blog.image}
                slug={blog.slug}
                content={blog.content}
                created_date={blog.created_date}
              />
            ))}
          </div>
          {totalBlogs >= maxBlogs ? (
            <div className="mt-2 d-flex align-items-center justify-content-center">
              <Button
                className="bgprimary border-0 mt-3 myshine"
                label="View More"
                onClick={() => loadMore()}
                loading={loadingBlogs}
              />
            </div>
          ) : null}
        </Container>
      </div>
      <Footer menuData={menuData} />
    </>
  );
};

export default Blog;
