import { useRouter } from "next/router";
import { useEffect } from "react";
import { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import styles from "../blog.module.scss";
import {
  getCategoryBlogs,
  getCategorySlugs,
} from "../../../service/BlogService";
import HeaderDecider from "../../../components/Global/HeaderDecider";
import { getMenu } from "../../../service/MiscService";
import Link from "next/link";
import { Button } from "primereact/button";
import BlogCard from "../../../components/Blog/BlogCard";
import { flushSync } from "react-dom";

export async function getStaticPaths() {
  const allSlugs = await getCategorySlugs().then((response) => response.data);
  const paths = allSlugs.map((categorySlug) => {
    return {
      params: {
        categorySlug: categorySlug.slug,
      },
    };
  });

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const categorySlug = context.params.categorySlug;
  const menuData = await getMenu().then((response) => response.data);

  if (!categorySlug) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      menuData: menuData,
      categorySlug: categorySlug,
    },
    revalidate: 10,
  };
}

const BlogCategory = ({ screenSize, menuData, categorySlug }) => {
  const router = useRouter();

  const [blogs, setBlogs] = useState([]);
  const [totalBlogs, setTotalBlogs] = useState(0);

  const [minBlogs, setMinBlogs] = useState(1);
  const [maxBlogs, setMaxBlogs] = useState(40);

  const [loadingBlogs, setLoadingBlogs] = useState(false);

  useEffect(() => {
    getCategoryBlogs(categorySlug, minBlogs, maxBlogs).then((response) => {
      console.log(response.data.data);
      setBlogs((prev) => prev.concat(response.data.data));
      setTotalBlogs(response.data.blog_count);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [minBlogs, maxBlogs]);

  function loadMore() {
    if (totalBlogs >= maxBlogs) {
      setLoadingBlogs(true);
      flushSync(() => {
        setMinBlogs((minBlogs) => minBlogs + 40);
        setMaxBlogs((maxBlogs) => maxBlogs + 40);
      });
      setLoadingBlogs(false);
    } else {
      setLoadingBlogs(false);
    }
  }

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <Container className={`${styles.detail_page}`}>
        <Row>
          <Col>
            <div className="mt-4">
              <h2 className="m-0 fw-bold">
                {blogs[0] && blogs[0].blogs.categories[0].name}
              </h2>
              <div className={`${styles.breadcrumb} fs-14 ps-0`}>
                <Link href={"/blog"}>
                  <a style={{ color: "#f75d34" }}>Home</a>
                </Link>{" "}
                &gt;{" "}
                <span>{blogs[0] && blogs[0].blogs.categories[0].name}</span>
              </div>
            </div>
            <div className="album py-5">
              <Container>
                <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-4">
                  {blogs.map((blog) => (
                    <BlogCard
                      key={blog.blogs.id}
                      title={blog.blogs.title}
                      image={blog.blogs.image}
                      slug={blog.blogs.slug}
                      content={blog.blogs.content}
                      created_date={blog.blogs.created_date}
                    />
                  ))}
                </div>
                {totalBlogs >= maxBlogs ? (
                  <div className="mt-2 d-flex align-items-center justify-content-center">
                    <Button
                      className="bgprimary border-0 mt-3 myshine"
                      label="View More"
                      onClick={() => loadMore()}
                      loading={loadingBlogs}
                    />
                  </div>
                ) : null}
              </Container>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default BlogCategory;
