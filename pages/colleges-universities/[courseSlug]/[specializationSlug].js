import { useRouter } from "next/router";
import { useEffect } from "react";
import { useState } from "react";
import { useAuth } from "../../../hooks/auth";
import {
  getCourseDetail,
  getCourseSlugs,
  getQuestions,
  getSpecializationDetail,
} from "../../../service/CourseService";
import { getLeadDetailsBySpecializationID } from "../../../service/LeadService";
import { getCounsellorData, getMenu } from "../../../service/MiscService";
import Header1 from "/components/Global/Header1";
import Avatar from "/components/universityListingPage/Avatar";
import VideoButton from "/components/universityListingPage/Button/VideoButton";
import AnimatedBird from "/components/Global/AnimatedBird";
import UniversityListHead from "/components/universityListingPage/universityListHead";
import UniversityListContent from "/components/universityListingPage/universityListContent";
import CVAdvantage from "/components/universityListingPage/Sidebar/CVAdvantage";
import CVExistence from "/components/universityListingPage/Sidebar/CVExistence";
import CVStudentSay from "/components/universityListingPage/Sidebar/CVStudentSay";
import PopularUniversities from "/components/universityListingPage/Modal/PopularUniversities";
import PopularUniversitiesInYear from "/components/universityListingPage/Modal/PopularUniversitiesInYear";
import Cta from "/components/universityListingPage/Cta";
import RealExpert from "/components/Homepage/Slider/RealExpert";
import Keyfeature from "/components/universityListingPage/Keyfeature";
import Footer from "/components/Global/Footer";
import { Col, Container, Modal, Row, Spinner } from "react-bootstrap";
import { GoPrimitiveDot } from "react-icons/go";
import AuthModal from "../../../components/Global/Modals/AuthModal";
import CourseQuestionModal from "../../../components/Global/Modals/CourseQuestionModal";
import { getUniversityListingBySpecialization } from "../../../service/UniversityListingService";
import MesageForOneUniversity from "../../../components/universityListingPage/MesageForOneUniversity";
import Head from "next/head";

export async function getStaticPaths() {
  const allSlugs = await getCourseSlugs().then((response) => response.data);
  const paths = allSlugs
    .map((courseSlug) =>
      courseSlug.specializations.map((specializationSlug) => ({
        params: {
          courseSlug: courseSlug.slug,
          specializationSlug: specializationSlug.slug,
        },
      }))
    )
    .flat();

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const courseSlug = context.params.courseSlug;
  const specializationSlug = context.params.specializationSlug;
  const menuData = await getMenu().then((response) => response.data);
  const counsellors = await getCounsellorData().then(
    (response) => response.data
  );

  const courseData = await getCourseDetail(courseSlug).then(
    (response) => response.data.data[0]
  );

  const specializationData = await getSpecializationDetail(
    courseSlug,
    specializationSlug
  ).then((response) => response.data.data[0]);

  if (!specializationData) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      courseData: courseData,
      specializationData: specializationData,
      menuData: menuData,
      counsellors: counsellors,
    },
    revalidate: 10,
  };
}

const ListingSpecializationPage = ({
  courseData,
  specializationData,
  menuData,
  counsellors,
  screenSize,
}) => {
  const router = useRouter();
  const { user } = useAuth();
  const [leadData, setLeadData] = useState(null);
  const [userData, setUserData] = useState(null);
  const [universityList, setUniversityList] = useState(null);
  const [universityCombinations, setUniversityCombinations] = useState([]);

  const [modalSize, setModalSize] = useState("md");
  const [modalContent, setModalContent] = useState(null);
  const [showModal, setShowModal] = useState(false);

  const setupModal = (key, eKey = "", eKey2 = "", eKeyValues = []) => {
    if (key === "user-auth") {
      setModalSize("md");
      setModalContent(
        <AuthModal menuData={menuData} closeModal={() => router.push("/")} />
      );
      setShowModal(true);
    } else if (key === "course-questions") {
      setModalSize("md");
      setModalContent(
        <CourseQuestionModal
          from="specialization"
          userID={router.query.uid}
          courseID={eKey}
          specializationID={eKey2}
          courseQuestions={eKeyValues}
        />
      );
      setShowModal(true);
    }
  };

  useEffect(() => {
    if (router.isReady) {
      if (router.query.uid) {
        getLeadDetailsBySpecializationID(
          courseData.id,
          specializationData.id,
          router.query.uid
        ).then((response) => {
          if (response.data.data[0].question_answer.length === 0) {
            getQuestions(courseData.id, specializationData.id).then(
              (response) => {
                setupModal(
                  "course-questions",
                  courseData.id,
                  specializationData.id,
                  response.data.data[0].questions
                );
              }
            );
          } else if (response.data.data.length > 0) {
            setLeadData(response.data.data[0].question_answer);
            setUserData(response.data.data[0].register);

            getUniversityListingBySpecialization(
              router.query.uid,
              courseData.id,
              specializationData.id
            ).then((response) => {
              setUniversityList(response.data.data);
            });
          } else {
            if (user) {
              getQuestions(courseData.id, specializationData.id).then(
                (response) => {
                  setupModal(
                    "course-questions",
                    courseData.id,
                    specializationData.id,
                    response.data.data[0].questions
                  );
                }
              );
            } else {
              setupModal("user-auth");
            }
          }
        });
      } else {
        //NO UID ATTACHED
        if (user) {
          getQuestions(courseData.id, specializationData.id).then(
            (response) => {
              setupModal(
                "course-questions",
                courseData.id,
                specializationData.id,
                response.data.data[0].questions
              );
            }
          );
        } else {
          setupModal("user-auth");
        }
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user, router.isReady]);

  useEffect(() => {
    if (universityList && universityList.length > 1) {
      let mainArray = [];

      for (let i = 0; i < universityList.length; i++) {
        const currentUniversity = universityList[i];

        for (let j = i; j < universityList.length; j++) {
          if (i !== j) {
            mainArray.push([currentUniversity, universityList[j]]);
          }
        }
      }

      setUniversityCombinations(mainArray);
    }
  }, [universityList]);

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Head>
        <title>
          Best Colleges and Universities for {courseData.display_name} in{" "}
          {specializationData.name}
        </title>
      </Head>
      <Header1
        showEdit={true}
        leadData={leadData}
        userData={userData}
        courseID={courseData.id}
        specializationID={specializationData.id}
        screenSize={screenSize}
      />
      <Container>
        <Row className={`m-${screenSize.width < 568 ? 0 : null}`}>
          <Col lg={9}>
            <Row className="dark-shadow mb-4 d-flex align-items-center rounded py-2 bg-light">
              <Col className="col-sm-1 col-1">
                <Avatar />
              </Col>
              <Col md={8} className="text-center col-sm-8 col-11 ps-5 ps-sm-0">
                &#128522;{" "}
                <span className="textsecondary">
                  Hey {userData && userData.name},{" "}
                </span>{" "}
                I&apos;m Pooja , We will definitely assist you in selecting the
                best University for{" "}
                <b>
                  {courseData.display_name} {specializationData.name}
                </b>
              </Col>
              <Col
                md={3}
                className="text-center col-sm-3 col-12 d-none d-sm-block">
                <VideoButton />
              </Col>
            </Row>
          </Col>
          <Col
            lg={3}
            className="d-flex justify-content-center align-items-center d-none d-lg-flex">
            #ChunoWahiJoHaiSahi <AnimatedBird />
          </Col>
        </Row>

        <Row>
          <Col lg={9} className="pe-sm-4">
            {universityList ? (
              <>
                <UniversityListHead screenSize={screenSize} />
                <UniversityListContent
                  courseSlug={courseData.slug}
                  specializationSlug={specializationData.slug}
                  screenSize={screenSize}
                  universityList={universityList}
                />
                {universityList.length === 1 ? (
                  <MesageForOneUniversity />
                ) : null}
              </>
            ) : (
              <div className="d-flex align-items-center justify-content-center">
                <Spinner animation="border" />
              </div>
            )}
          </Col>
          <Col lg={3}>
            <CVAdvantage />
            <CVExistence />
            <CVStudentSay />
          </Col>
        </Row>
      </Container>
      {universityCombinations.length > 0 ? (
        <PopularUniversities
          universityCombinations={universityCombinations}
          courseSlug={courseData.slug}
          specializationSlug={specializationData.slug}
        />
      ) : null}
      <Cta />
      <PopularUniversitiesInYear />
      <RealExpert counsellors={counsellors} />
      <Keyfeature />
      <Footer menuData={menuData} />
      <Modal
        show={showModal}
        onHide={() => setShowModal(false)}
        backdrop="static"
        size={modalSize}
        centered>
        {modalContent}
      </Modal>
    </>
  );
};

export default ListingSpecializationPage;
