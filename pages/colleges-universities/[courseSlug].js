import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import { useAuth } from "../../hooks/auth";
import { GoPrimitiveDot } from "react-icons/go";
import { Col, Container, Modal, Row, Spinner } from "react-bootstrap";
import {
  getCourseDetail,
  getCourseSlugs,
  getQuestions,
} from "../../service/CourseService";
import { getLeadDetailsByCourseID } from "../../service/LeadService";
import { getCounsellorData, getMenu } from "../../service/MiscService";
import Header1 from "/components/Global/Header1";
import Avatar from "/components/universityListingPage/Avatar";
import VideoButton from "/components/universityListingPage/Button/VideoButton";
import AnimatedBird from "/components/Global/AnimatedBird";
import UniversityListHead from "/components/universityListingPage/universityListHead";
import UniversityListContent from "/components/universityListingPage/universityListContent";
import CVAdvantage from "/components/universityListingPage/Sidebar/CVAdvantage";
import CVExistence from "/components/universityListingPage/Sidebar/CVExistence";
import CVStudentSay from "/components/universityListingPage/Sidebar/CVStudentSay";
import PopularUniversities from "/components/universityListingPage/Modal/PopularUniversities";
import PopularUniversitiesInYear from "/components/universityListingPage/Modal/PopularUniversitiesInYear";
import Cta from "/components/universityListingPage/Cta";
import RealExpert from "/components/Homepage/Slider/RealExpert";
import Keyfeature from "/components/universityListingPage/Keyfeature";
import Footer from "/components/Global/Footer";
import AuthModal from "../../components/Global/Modals/AuthModal";
import CourseQuestionModal from "../../components/Global/Modals/CourseQuestionModal";
import { flushSync } from "react-dom";
import { getUniversityListingByCourse } from "../../service/UniversityListingService";
import MesageForOneUniversity from "../../components/universityListingPage/MesageForOneUniversity";
import Head from "next/head";

export async function getStaticPaths() {
  const allSlugs = await getCourseSlugs().then((response) => response.data);
  const paths = allSlugs.map((courseSlug) => {
    return {
      params: {
        courseSlug: courseSlug.slug,
      },
    };
  });

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const courseSlug = context.params.courseSlug;
  const menuData = await getMenu().then((response) => response.data);
  const counsellors = await getCounsellorData().then(
    (response) => response.data
  );

  const courseData = await getCourseDetail(courseSlug).then(
    (response) => response.data.data[0]
  );

  if (!courseData) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      courseData: courseData,
      menuData: menuData,
      counsellors: counsellors,
    },
    revalidate: 10,
  };
}

const ListingCoursePage = ({
  courseData,
  menuData,
  counsellors,
  screenSize,
}) => {
  const router = useRouter();
  const { user } = useAuth();
  const [leadData, setLeadData] = useState(null);
  const [userData, setUserData] = useState(null);
  const [universityList, setUniversityList] = useState(null);
  const [universityCombinations, setUniversityCombinations] = useState([]);

  const [modalSize, setModalSize] = useState("md");
  const [modalContent, setModalContent] = useState(null);
  const [showModal, setShowModal] = useState(false);

  const setupModal = (key, eKey = "", eKeyValues = []) => {
    if (key === "user-auth") {
      setModalSize("md");
      setModalContent(
        <AuthModal menuData={menuData} closeModal={() => router.push("/")} />
      );
      setShowModal(true);
    } else if (key === "course-questions") {
      setModalSize("md");
      setModalContent(
        <CourseQuestionModal
          from="course"
          userID={router.query.uid}
          courseID={eKey}
          courseQuestions={eKeyValues}
        />
      );
      setShowModal(true);
    }
  };

  useEffect(() => {
    if (router.isReady) {
      if (router.query.uid) {
        getLeadDetailsByCourseID(courseData.id, router.query.uid).then(
          (response) => {
            if (
              response.data.data[0] &&
              response.data.data[0].question_answer.length === 0
            ) {
              getQuestions(courseData.id, "").then((response) => {
                setupModal(
                  "course-questions",
                  courseData.id,
                  response.data.data[0].questions
                );
              });
            } else if (response.data.data.length > 0) {
              setLeadData(response.data.data[0].question_answer);
              setUserData(response.data.data[0].register);

              getUniversityListingByCourse(
                router.query.uid,
                courseData.id
              ).then((response) => {
                setUniversityList(response.data.data);
              });
            } else {
              //WRONG UID
              if (user) {
                //ASK ANSWERS AGAIN FROM USER
                getQuestions(courseData.id, "").then((response) => {
                  setupModal(
                    "course-questions",
                    courseData.id,
                    response.data.data[0].questions
                  );
                });
              } else {
                //ASK USER TO SIGN UP
                setupModal("user-auth");
              }
            }
          }
        );
      } else {
        //NO UID ATTACHED
        if (user) {
          getQuestions(courseData.id, "").then((response) => {
            setupModal(
              "course-questions",
              courseData.id,
              response.data.data[0].questions
            );
          });
        } else {
          setupModal("user-auth");
        }
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user, router.isReady]);

  useEffect(() => {
    if (universityList && universityList.length > 1) {
      let mainArray = [];

      for (let i = 0; i < universityList.length; i++) {
        const currentUniversity = universityList[i];

        for (let j = i; j < universityList.length; j++) {
          if (i !== j) {
            mainArray.push([currentUniversity, universityList[j]]);
          }
        }
      }

      setUniversityCombinations(mainArray);
    }
  }, [universityList]);

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Head>
        <title>Best Colleges and Universities for {courseData.name}</title>
      </Head>
      <Header1
        showEdit={true}
        leadData={leadData}
        userData={userData}
        courseID={courseData.id}
        specializationID={""}
        screenSize={screenSize}
      />
      <Container>
        <Row className={`m-${screenSize.width < 568 ? 0 : null}`}>
          <Col lg={9}>
            <Row className="dark-shadow mb-4 align-items-center rounded py-2 bg-light">
              <Col className="col-sm-1 col-1">
                <Avatar />
                {/* <GoPrimitiveDot
                    fontSize={20}
                    style={{ color: "#00d8a1", bottom: "5px", right: "3px" }}
                    className="position-absolute"
                  /> */}
              </Col>
              <Col className="text-center col-sm-8 col-11 ps-5 ps-sm-0">
                &#128522;{" "}
                <span className="textsecondary">
                  Hey {userData && userData.name},{" "}
                </span>{" "}
                I&apos;m Pooja , We will definitely assist you in selecting the
                best University for <b>{courseData.name}</b>
              </Col>
              <Col className="text-center col-sm-3 col-12 d-none d-sm-block">
                <VideoButton />
              </Col>
            </Row>
          </Col>
          <Col
            lg={3}
            className="d-flex justify-content-center align-items-center d-none d-lg-flex">
            #ChunoWahiJoHaiSahi <AnimatedBird />
          </Col>
        </Row>

        <Row>
          <Col lg={9} className="pe-sm-4">
            {universityList ? (
              <>
                <UniversityListHead screenSize={screenSize} />
                <UniversityListContent
                  courseSlug={courseData.slug}
                  menuData={menuData}
                  screenSize={screenSize}
                  universityList={universityList}
                />
                {universityList.length === 1 ? (
                  <MesageForOneUniversity />
                ) : null}
              </>
            ) : (
              <div className="d-flex align-items-center justify-content-center">
                <Spinner animation="border" />
              </div>
            )}
          </Col>
          <Col lg={3}>
            <CVAdvantage />
            <CVExistence />
            <CVStudentSay />
          </Col>
        </Row>
      </Container>
      {universityCombinations.length > 0 ? (
        <PopularUniversities
          universityCombinations={universityCombinations}
          courseSlug={courseData.slug}
        />
      ) : null}

      <Cta />
      <PopularUniversitiesInYear />
      <RealExpert counsellors={counsellors} />
      <Keyfeature />
      <Footer menuData={menuData} />
      <Modal
        show={showModal}
        onHide={() => setShowModal(false)}
        backdrop="static"
        size={modalSize}
        centered>
        {modalContent}
      </Modal>
    </>
  );
};

export default ListingCoursePage;
