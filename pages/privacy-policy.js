import Head from "next/head";
import React from "react";
import { Container, Row } from "react-bootstrap";
import Footer from "../components/Global/Footer";
import Header from "../components/Global/Header";
import HeaderDecider from "../components/Global/HeaderDecider";
import OurExpert from "../components/Global/OurExpert";
import { getMenu } from "../service/MiscService";
import { getSearchSchema } from "../utils/genericSchemas";
import HeroSection from "/components/Global/Jumbotron/HeroSection";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

const ourPolicy = ({ screenSize, menuData }) => {
  return (
    <>
      <Head>
        <title>Privacy Policy | College Vidya</title>
        <meta
          name="description"
          content="College Vidya respect your privacy and are committed to maintaining and using any information we collect through your use of our Services responsibly."
        />
        <meta name="robots" content="index, follow" />
        <link href="https://collegevidya.com/privacy-policy/" rel="canonical" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Privacy Policy | College Vidya" />
        <meta
          property="og:description"
          content="College Vidya respect your privacy and are committed to maintaining and using any information we collect through your use of our Services responsibly."
        />
        <meta
          property="og:url"
          content="https://collegevidya.com/privacy-policy/"
        />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="Privacy Policy | College Vidya"
        />
        <meta
          property="twitter:description"
          content="College Vidya respect your privacy and are committed to maintaining and using any information we collect through your use of our Services responsibly."
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/privacy-policy/"
        />
        <meta property="twitter:image" content="" />
        <meta
          name="keywords"
          content="privacy policy college vidya, college vidya policy, why college vidya, collegevidya, top online and distance education compare portal, top online education portal"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/privacy-policy/",
              name: "Privacy Policy | College Vidya",
              description:
                "College Vidya respect your privacy and are committed to maintaining and using any information we collect through your use of our Services responsibly.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "Privacy Policy | College Vidya",
              description:
                "College Vidya respect your privacy and are committed to maintaining and using any information we collect through your use of our Services responsibly.",
              url: "https://collegevidya.com/privacy-policy/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo.svg",
              address: "India",
            }),
          }}
        />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <HeroSection title="Our Policy" />
      <Container className="my-5">
        <Row>
          <div className="col-md-10 mx-auto">
            <h3>Introduction </h3>
            <p>
              College Vidya (together with its subsidiaries and affiliates,
              &quot;College Vidya,&quot; &quot;us,&quot; &quot;we,&quot; or
              &quot;our&quot; or &quot;the Company&quot;) is dedicated to the
              protection and handling of personal data in order to operate
              efficiently and effectively for the benefit of our stakeholders,
              customers, and the community. It is critical, however, that
              people&quot;s privacy be safeguarded by using authorised and
              suitable methods to handle personal data. As a result, we&quot;ve
              put in referred to as policy.{" "}
            </p>
            <h3>Aim</h3>
            <p>
              This policy seeks to safeguard the personal data of our different
              stakeholders. This policy is intended to inform persons of the
              fundamental principles in the treatment by the firm of personal
              information (&quot;personal data&quot;), of individuals who visit,
              use, handle and/or transact via the website, as well as of a guest
              user and a browser (hence &quot;you&quot;).
            </p>
            <h3>Purpose</h3>
            <p>
              The objective of this policy is to define how College Vidya
              collects, uses and communicates data about you through the online
              interfaces we own and control, including but not limited to
              website and mobile apps (hereinafter the &quot;website&quot;). The
              aim of this policy is also to offer information on how College
              Vidya protects data security, transmits data and processes data
              subject requests. The Policy Control applies to all information
              systems in the company, including board members, directors, staff
              and other third parties that have access to College Vidya&quot;s
              personal data.
            </p>
            <h3>Scope</h3>
            <p>
              The firm also undertakes to guarantee that its workers follow
              these and other related rules. If third parties process
              information on behalf of College Vidya, the company strives to
              ensure that your personal information is constantly protected by
              such third parties.
            </p>
            <p>
              College Vidya offers online and distance guidance
              (&quot;individual or collectively referred to as the
              &quot;project&quot;), selected and particularly created for higher
              education and the business. Unless otherwise indicated, this
              Privacy Policy applies to all of our services.
            </p>
            <h3>Data gathering sources</h3>
            <p>
              The firm collects data from the user or via the usage of our
              sites. Data is derived.
            </p>
            <p>Sources of Data Collected</p>
            <ul className="pl-5">
              <li>
                Register for different seminars, webinars or other outreach
                efforts provided by us
              </li>
              <li>
                Request a quotation for the many goods and services we provide
              </li>
              <li>
                Place your comments, complete any distributed customer surveys
                or engage online with our customer service
              </li>
              <li>View our services or visit our online pages</li>
              <li>Check out our website</li>
              <li>
                When you are going for online scholarships examinations or other
                evaluations.{" "}
              </li>
            </ul>
            <h3>Data protection principles</h3>
            <p>
              Where a third party processes data on behalf of College Vidya, we
              try to ensure that your personal data is constantly protected from
              such third parties. We recognise that any personal data kept in
              the form of manual records and computers are to be processed,
              managed and regulated, stored and retained.
            </p>
            <p>The Company will acquire and keep all personal data:</p>
            <ul className="pl-5">
              <li>
                Gathered properly, legally and transparently for specified,
                stated and justifiable objectives
              </li>
              <li>Be gathered for particular, clear, and lawful reasons</li>
              <li>
                Be sufficient, relevant, and restricted to what is required for
                processing purposes
              </li>
              <li>
                Keep up to date with precision. There will be every reasonable
                effort to ensure that incorrect information is immediately
                corrected or deleted
              </li>
              <li>
                Be handled in a manner that guarantees acceptable personal data
                safety including protection from unauthorised or illegal
                processing, by means of suitable technological or organisational
                measures, accidental loss, destruction or damage
              </li>
              <li>
                Comply with the appropriate laws and procedures for
                international data transfers of Personal Data.
              </li>
            </ul>
          </div>
        </Row>

        <OurExpert />
      </Container>
      <Footer menuData={menuData} />
    </>
  );
};

export default ourPolicy;
