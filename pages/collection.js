import { Container, Row, Col } from "react-bootstrap";
import React from "react";
import { TabView, TabPanel } from "primereact/tabview";
import CollectionCard from "/components/Collection/CollectionCard";
import HeroSection from "/components/Global/Jumbotron/HeroSection";
import Header from "../components/Global/Header";
import Footer from "../components/Global/Footer";
import { getCVTV, getMenu } from "../service/MiscService";
import styles from "/styles/scss/Collection.module.scss";
import HeaderDecider from "../components/Global/HeaderDecider";
import Head from "next/head";
import { getSearchSchema } from "../utils/genericSchemas";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);
  const cvTV = await getCVTV().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
      cvTV: cvTV,
    },
    revalidate: 10,
  };
}

const Collection = ({ screenSize, menuData, cvTV }) => {
  return (
    <>
      <Head>
        <title>
          College Vidya TV - Online & Distance Education Compare Portal
        </title>
        <meta
          name="description"
          content="Here is the list of video & podcast collections for online and distance education career guides. Click here to learn more about your career."
        />
        <meta name="robots" content="index, follow" />
        <link href="https://collegevidya.com/collection/" rel="canonical" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="College Vidya TV - Online & Distance Education Compare Portal"
        />
        <meta
          property="og:description"
          content="Here is the list of video & podcast collections for online and distance education career guides. Click here to learn more about your career."
        />
        <meta
          property="og:url"
          content="https://collegevidya.com/collection/"
        />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="College Vidya TV - Online & Distance Education Compare Portal"
        />
        <meta
          property="twitter:description"
          content="Here is the list of video & podcast collections for online and distance education career guides. Click here to learn more about your career."
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/collection/"
        />
        <meta property="twitter:image" content="" />
        <meta
          name="keywords"
          content="college vidya cv, college vidya collections, college vidya tv, college vidya video and podcast, distance online education compare portal"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/collection/",
              name: "College Vidya TV - Online & Distance Education Compare Portal",
              description:
                "Here is the list of video & podcast collections for online and distance education career guides. Click here to learn more about your career.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "College Vidya TV - Online & Distance Education Compare Portal",
              description:
                "Here is the list of video & podcast collections for online and distance education career guides. Click here to learn more about your career.",
              url: "https://collegevidya.com/collection/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo.svg",
              address: "India",
            }),
          }}
        />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <Container fluid className="px-0">
        <HeroSection title="College Vidya TV" />
      </Container>
      <Container className={`${styles.collection} my-5`}>
        <Row>
          <Col md={10} className="mx-auto">
            <TabView>
              <TabPanel header="Show All">
                <p>
                  <CollectionCard
                    content={cvTV.map((detail) => detail.cv_tv_detail).flat()}
                  />
                </p>
              </TabPanel>
              {cvTV.map((list) => (
                <TabPanel key={list.id} header={list.title}>
                  <p>
                    <CollectionCard content={list.cv_tv_detail} />
                  </p>
                </TabPanel>
              ))}
            </TabView>
          </Col>
        </Row>
      </Container>
      <Footer menuData={menuData} />
    </>
  );
};

export default Collection;
