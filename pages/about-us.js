import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import {
  AiOutlineRocket,
  AiOutlineSchedule,
  AiOutlineSplitCells,
} from "react-icons/ai";
import Image from "next/image";
import about1 from "/public/images/list-online-and-distance-university.png";
import about2 from "/public/images/Compiles-university-details.png";
import about3 from "/public/images/Gives-the-option-fo-in-person-counselling.png";
import OurExpert from "../components/Global/OurExpert";
import styles from "/styles/About.module.scss";
import HeroSection from "/components/Global/Jumbotron/HeroSection";
import Footer from "../components/Global/Footer";
import { getMenu } from "../service/MiscService";
import HeaderDecider from "../components/Global/HeaderDecider";
import Head from "next/head";
import { getSearchSchema } from "../utils/genericSchemas";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

const aboutus = ({ screenSize, menuData }) => {
  return (
    <>
      <Head>
        <title>
          About College Vidya - Online & Distance Education Compare Portal
        </title>
        <meta
          name="description"
          content="College Vidya is India's top online and distance education compare portal that helps students/working professionals choose the best university that is right for them."
        />
        <meta name="robots" content="index, follow" />
        <link href="https://collegevidya.com/about-us/" rel="canonical" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="About College Vidya- Online & Distance Education Compare Portal"
        />
        <meta
          property="og:description"
          content="College Vidya is India's top online and distance education compare portal that helps students/working professionals choose the best university that is right for them."
        />
        <meta property="og:url" content="https://collegevidya.com/about-us/" />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="About College Vidya- Online & Distance Education Compare Portal"
        />
        <meta
          property="twitter:description"
          content="College Vidya is India's top online and distance education compare portal that helps students/working professionals choose the best university that is right for them."
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/about-us/"
        />
        <meta property="twitter:image" content="" />
        <meta
          name="keywords"
          content="about college vidya, distance online education compare portal, top online and distance education compare portal, compare online distance mba universities"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/about-us/",
              name: "About College Vidya- Online & Distance Education Compare Portal",
              description:
                "College Vidya is India's top online and distance education compare portal that helps students/working professionals choose the best university that is right for them.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "About College Vidya- Online & Distance Education Compare Portal",
              description:
                "College Vidya is India's top online and distance education compare portal that helps students/working professionals choose the best university that is right for them.",
              url: "https://collegevidya.com/about-us/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo.svg",
              address: "India",
            }),
          }}
        />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <HeroSection
        title="About Us"
        content="Powering a Bright Career with Online & Distance Learning"
      />
      <Container className="mb-5">
        <Row className="my-5">
          <Col md={4} className="text-center">
            <AiOutlineRocket className={`${styles.icon}`} />
            <h2 className="my-2">Vision</h2>
            <p>
              Bringing the power of Distance & Online Education out in the open
              and make learning free from all the middlemen and vices.
            </p>
          </Col>
          <Col md={4} className="text-center">
            <AiOutlineSchedule className={`${styles.icon}`} />
            <h2 className="my-2">Mission</h2>
            <p>
              In this competitive global market, we are committed to providing
              professionals with high-quality and industry-related training
              programmes.
            </p>
          </Col>
          <Col md={4} className="text-center">
            <AiOutlineSplitCells className={`${styles.icon}`} />
            <h2 className="my-2">Objective</h2>
            <p>
              Providing students with the right knowledge so that they make
              informed decisions about their career.
            </p>
          </Col>
        </Row>
        <h2 className="text-center">How it Works</h2>
        <Row>
          <Col
            md={6}
            className="d-flex align-items-start justify-content-center flex-column">
            <h2>Lists best online and distance universities</h2>
            <p>
              You have to choose the course you want to pursue from the home
              page and then you will get the list of the best distance and
              online colleges/universities that are offering these courses along
              with the relevant details.
            </p>
          </Col>
          <Col
            md={6}
            className="d-flex align-items-start justify-content-center flex-column">
            <Image src={about1} alt="" />
          </Col>
          <Col md={6}>
            <Image src={about2} alt="" />
          </Col>
          <Col
            md={6}
            className="d-flex align-items-start justify-content-center flex-column">
            <h2>Compiles university details</h2>
            <p>
              You can find all the relevant details of any particular online and
              distance university on the website, including location, courses
              offered, placement support, fee structure, approvals, etc.
            </p>
          </Col>

          <Col
            md={6}
            className="d-flex align-items-start justify-content-center flex-column">
            <h2>Gives the option for in-person counselling</h2>
            <p>
              The website provides you with the option of career counselling by
              its experienced counsellors. You can reach out to them for any
              query with the help of the toll number 18004205757.
            </p>
          </Col>
          <Col
            md={6}
            className="d-flex align-items-start justify-content-center flex-column">
            <Image src={about3} alt="" />
          </Col>
        </Row>
        <Row className="mt-4">
          <p
            className="py-3 rounded text-center"
            style={{ backgroundColor: "aliceblue" }}>
            &ldquo;Edutra Consulting Services Pvt Ltd&ldquo; is parents
            organization of Collegevidya.com
          </p>
        </Row>
        <OurExpert />
      </Container>
      <Footer menuData={menuData} />
    </>
  );
};

export default aboutus;
