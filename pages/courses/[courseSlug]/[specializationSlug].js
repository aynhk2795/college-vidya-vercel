import { Col, Container, Row } from "react-bootstrap";
import Image from "next/image";
import { AiFillCheckCircle } from "react-icons/ai";

import styles from "/styles/LeadForm.module.scss";
import {
  getCourseDetail,
  getCourseSlugs,
  getSpecializationDetail,
} from "../../../service/CourseService";
import formimage from "/public/images/lead-form.png";
import Header1 from "../../../components/Global/Header1";
import Footer1 from "../../../components/Global/Footer1";
import Faq from "/components/Homepage/Accordion/Faq";
import ShortDescriptionImage from "/components/Leadpage/ShortDescriptionPanel/ShortDescriptionImage";
import ShortDescriptionContent from "/components/Leadpage/ShortDescriptionPanel/ShortDescriptionContent";
import LongDescription from "/components/Leadpage/LongDescriptionPanel/LongDescription";
import ChooseUniversity from "/components/Leadpage/ChooseUniversity";
import LeadForm from "/components/Leadpage/Form/LeadForm";
import VideoLeadpage from "../../../components/Leadpage/Modal/VideoLeadpage";
import AudioLeadpage from "../../../components/Leadpage/Modal/AudioLeadpage";

import { useEffect } from "react";
import MobileHeader1 from "../../../components/Global/MobileHeader1";
import { useRouter } from "next/router";
import { useAuth } from "../../../hooks/auth";
import {
  getUniversityListingBySpecialization,
  getUniversityPreviewBySpecialization,
} from "../../../service/UniversityListingService";
import UniversityListContent from "../../../components/universityListingPage/universityListContent";
import { useState } from "react";
import hero from "/public/images/hero-img.png";
import { UserService } from "../../../service/UserService";
import { useAxiosJWT } from "../../../hooks/axios";
import Link from "next/link";
import UniversityFormLogos from "../../../components/Leadpage/Slider/UniversityFormLogos";
import {
  getFAQSchema,
  getGenericCourseSpecializationTitleSchema,
  getSearchSchema,
} from "../../../utils/genericSchemas";
import { generateVideoSchema } from "../../../utils/schema";
import Head from "next/head";

export async function getStaticPaths() {
  const allSlugs = await getCourseSlugs().then((response) => response.data);
  const paths = allSlugs
    .map((courseSlug) =>
      courseSlug.specializations.map((specializationSlug) => ({
        params: {
          courseSlug: courseSlug.slug,
          specializationSlug: specializationSlug.slug,
        },
      }))
    )
    .flat();

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const courseSlug = context.params.courseSlug;
  const specializationSlug = context.params.specializationSlug;

  const courseData = await getCourseDetail(courseSlug).then(
    (response) => response.data.data[0]
  );

  const specializationData = await getSpecializationDetail(
    courseSlug,
    specializationSlug
  ).then((response) => response.data.data[0]);

  const videoSchema = await generateVideoSchema(specializationData.video_link);

  if (!specializationData || !courseData) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      courseData: courseData,
      specializationData: specializationData,
      videoSchema: videoSchema,
    },
    revalidate: 10,
  };
}

const CVSpecializationPage = ({
  courseData,
  specializationData,
  screenSize,
  videoSchema,
}) => {
  const router = useRouter();
  const { user } = useAuth();
  const { axiosJWT } = useAxiosJWT();

  const [userData, setUserData] = useState(null);
  const [universityList, setUniversityList] = useState(null);
  const [universityPreviewList, setUniversityPreviewList] = useState([]);

  useEffect(() => {
    getUniversityPreviewBySpecialization(
      courseData.id,
      specializationData.id
    ).then((response) => {
      setUniversityPreviewList(response.data.data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    let tables = document.getElementsByTagName("table");

    [].slice.call(tables).forEach((table) => {
      let wrapper = document.createElement("div");
      wrapper.className = "custom-table-wrap";
      table.parentNode.insertBefore(wrapper, table);
      wrapper.appendChild(table);
    });
  }, []);

  useEffect(() => {
    if (user) {
      getUniversityListingBySpecialization(
        user,
        courseData.id,
        specializationData.id
      ).then((response) => setUniversityList(response.data.data));

      const userService = new UserService(axiosJWT);
      userService
        .getUserDetails()
        .then((response) => setUserData(response.data));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Head>
        <title>
          {
            getGenericCourseSpecializationTitleSchema(
              courseData.display_name,
              specializationData.display_name
            ).title
          }
        </title>
        <meta
          name="description"
          content={
            getGenericCourseSpecializationTitleSchema(
              courseData.display_name,
              specializationData.display_name
            ).description
          }
        />
        <meta name="robots" content="index, follow" />
        <link
          href={`https://collegevidya.com/courses/${courseData.slug}/${specializationData.slug}/`}
          rel="canonical"
        />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content={
            getGenericCourseSpecializationTitleSchema(
              courseData.display_name,
              specializationData.display_name
            ).title
          }
        />
        <meta
          property="og:description"
          content={
            getGenericCourseSpecializationTitleSchema(
              courseData.display_name,
              specializationData.display_name
            ).description
          }
        />
        <meta
          property="og:url"
          content={`https://collegevidya.com/courses/${courseData.slug}/${specializationData.slug}/`}
        />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya/"
        />
        <meta property="og:image" content={specializationData.thumbnail} />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content={
            getGenericCourseSpecializationTitleSchema(
              courseData.display_name,
              specializationData.display_name
            ).title
          }
        />
        <meta
          property="twitter:description"
          content={
            getGenericCourseSpecializationTitleSchema(
              courseData.display_name,
              specializationData.display_name
            ).description
          }
        />
        <meta
          property="twitter:url"
          content={`https://collegevidya.com/courses/${courseData.slug}/${specializationData.slug}/`}
        />
        <meta property="twitter:image" content={specializationData.thumbnail} />

        {/* Search Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        {/* Schema One */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: `https://collegevidya.com/courses/${courseData.slug}/${specializationData.slug}/`,
              name: getGenericCourseSpecializationTitleSchema(
                courseData.display_name,
                specializationData.display_name
              ).title,
              description: getGenericCourseSpecializationTitleSchema(
                courseData.display_name,
                specializationData.display_name
              ).description,
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        {/* Schema Two */}
        <script
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        {/* Schema Three */}
        <script
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Course",
              "@id": `https://collegevidya.com/courses/${courseData.slug}/${specializationData.slug}/`,
              name: getGenericCourseSpecializationTitleSchema(
                courseData.display_name,
                specializationData.display_name
              ).title,
              description: getGenericCourseSpecializationTitleSchema(
                courseData.display_name,
                specializationData.display_name
              ).description,
              provider: {
                "@type": "Organization",
                name: "College Vidya",
                sameAs: "https://collegevidya.com/",
              },
              hasCourseInstance: {
                "@type": "CourseInstance",
                name: getGenericCourseSpecializationTitleSchema(
                  courseData.display_name,
                  specializationData.display_name
                ).title,
                url: `https://collegevidya.com/courses/${courseData.slug}/${specializationData.slug}/`,
                description: getGenericCourseSpecializationTitleSchema(
                  courseData.display_name,
                  specializationData.display_name
                ).description,
                eventStatus: "open",
                image: courseData.thumbnail,
                offers: {
                  "@type": "AggregateOffer",
                  url: `https://collegevidya.com/courses/${courseData.slug}/${specializationData.slug}/`,
                  availability: "In-stock",
                  availabilityStarts: "",
                  validFrom: "2020-01-12T00:00",
                  inventoryLevel: "no limit",
                  price: "50000.00",
                  priceCurrency: "Rs.",
                },
                location: {
                  "@type": "Place",
                  name: "Distance/Online/Part Time/Correspondence",
                  address: {
                    "@type": "PostalAddress",
                    name: getGenericCourseSpecializationTitleSchema(
                      courseData.display_name,
                      specializationData.display_name
                    ).title,
                    addressLocality: "",
                    addressRegion: "",
                  },
                  url: `https://collegevidya.com/courses/${courseData.slug}/${specializationData.slug}/`,
                },
                instructor: {
                  "@type": "Person",
                  name: `Leading Professional Experts in ${courseData.display_name}`,
                },
                inLanguage: "en",
                startDate: "2020-01-12T00:00",
                endDate: "",
                performer: `Leading Professional Experts in ${courseData.display_name}`,
              },
              image: courseData.thumbnail,
              audience: {
                "@type": "Audience",
                audienceType: ["Students", "Working Professionals"],
              },
              courseCode: "",
              timeRequired: "",
            }),
          }}
        />

        {/* VideoSchema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(videoSchema),
          }}
        />

        {/* FAQ Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(
              getFAQSchema(specializationData.specializations_all_faqs)
            ),
          }}
        />
      </Head>
      {screenSize.width < 992 ? <MobileHeader1 /> : <Header1 />}

      <Container>
        {universityList ? (
          <Row>
            <Col md={10} className="mx-auto">
              <Row>
                <Col md={4}>
                  <div className="p-4">
                    <Image src={hero} alt="" />
                  </div>
                  <p className="m-0 fs-10">Did you now ?</p>
                  <p className="m-0 fs-16 fw-bold">
                    Recently, UGC Declared 33 universities fake and its degrees
                    invalid.
                  </p>
                  <p className="m-0 fs-10">
                    Save Yourself from Falling for such traps!
                  </p>
                </Col>
                <Col md={8} className="ps-5">
                  <p className="textsecondary m-0">
                    Welcome back {userData ? userData.name : ""},
                  </p>
                  <p>
                    Checkout Some Most Searched Online and Distance Universities
                  </p>
                  <UniversityListContent
                    courseSlug={courseData.slug}
                    specializationSlug={specializationData.slug}
                    screenSize={screenSize}
                    showSmall={true}
                    universityList={universityList}
                  />
                  <div className="d-flex justify-content-between align-items-center gap-3">
                    <Link
                      href={`/colleges-universities/${courseData.slug}/${specializationData.slug}?uid=${user}`}>
                      <a className="bgprimary text-white w-100 p-2 rounded col text-center">
                        View More
                      </a>
                    </Link>
                    <span
                      className="d-flex justify-content-center align-items-center text-center text-secondary rounded-circle border fs-14"
                      style={{ width: "50px", height: "50px" }}>
                      OR
                    </span>
                    <Link href="/suggest-me-an-university">
                      <a className="bgprimary text-white w-100 p-2 rounded col text-center">
                        Suggest me a University
                      </a>
                    </Link>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        ) : (
          <Row>
            <Col md={10} className={`${styles.lead_form_wrap} mx-auto mt-3`}>
              <Row>
                <Col md={5} className={`${styles.lead_form_img} ps-0`}>
                  <div className={`${styles.lead_img}`}>
                    <Image
                      width={300}
                      height={390}
                      src={formimage}
                      alt="lead image"
                    />
                  </div>
                  <UniversityFormLogos
                    universityPreviewList={universityPreviewList}
                  />
                </Col>
                <Col md={7} className={`${styles.lead_form_box}`}>
                  <div className={`${styles.lead_form} pt-5 pb-3`}>
                    <h5 className="text-center">
                      <span className="textprimary">Compare &amp; Select</span>{" "}
                      Best University{" "}
                      <span className="d-block">
                        for your {specializationData.name} Course
                      </span>
                    </h5>
                    <ul className="d-flex justify-content-center list-unstyled mb-0">
                      <li
                        className="d-flex align-items-center mx-1"
                        style={{ color: "#16b1a2" }}>
                        <AiFillCheckCircle className="mx-1" /> Get Approved
                        University
                      </li>
                      <li
                        className="d-flex align-items-center mx-1"
                        style={{ color: "#16b1a2" }}>
                        <AiFillCheckCircle className="mx-1" /> 100% Placement
                        Assistance
                      </li>
                    </ul>
                  </div>
                  <LeadForm
                    courseData={courseData}
                    specializationData={specializationData}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        )}
        <Row className="mt-sm-5 m-0">
          <Col md={10} className="mx-auto border">
            <Row className="rounded">
              <Col md={4} className="ps-md-0 pt-3 pt-sm-0 text-center">
                <ShortDescriptionImage
                  thumbnail={specializationData.thumbnail}
                />
              </Col>
              <Col
                md={8}
                className={`${styles.lead_form_short_desc} d-flex align-items-center`}>
                <div>
                  <ShortDescriptionContent
                    short_description={specializationData.short_description}
                  />
                  <Row>
                    <Col md={7}>
                      <Row>
                        <Col>
                          <VideoLeadpage
                            video_link={specializationData.video_link}
                          />
                        </Col>
                        <Col>
                          <AudioLeadpage
                            audio_link={specializationData.audio_link}
                          />
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="mt-5">
          <Col md={10} className="mx-auto custom-course-wrap">
            <LongDescription
              approvals={specializationData.approvals}
              duration_range={specializationData.duration_range}
              eligibility={specializationData.eligibility}
              description={specializationData.description}
            />
          </Col>
        </Row>
      </Container>

      <ChooseUniversity universityPreviewList={universityPreviewList} />
      <Faq faqData={specializationData.specializations_all_faqs} />
      <Footer1 />
    </>
  );
};

export default CVSpecializationPage;
