import Image from 'next/image';
import React from 'react'
import { Container , Row , Col } from 'react-bootstrap'
import RatingStar from '../components/CoursePage/RatingStar';
import UniversitySlider from "/components/TopUniversity/Slider/UniversitySlider";
import logo from "/public/images/NMIMS.jpeg"
import { MdPodcasts } from "react-icons/md"
import { AiOutlineVideoCamera ,AiOutlineEye } from "react-icons/ai"
import AddToCompare from "/components/universityListingPage/Modal/AddToCompare";
import HeaderDecider from '../components/Global/HeaderDecider';
import { getMenu } from "../service/MiscService";
import TopUniversityCard from '../components/TopUniversity/TopUniversityCard';


export async function getStaticProps() {
    const menuData = await getMenu().then((response) => response.data);
  
    return {
      props: {
        menuData: menuData,
      },
      revalidate: 10,
    };
  }
const newTop = ({ screenSize , menuData }) => {
  return (
    <>
     <HeaderDecider screenSize={screenSize} menuData={menuData} />
        <Container>
       
            <Row>
                <Col md={3}>
                <div>
                   <p>Specialisation</p>
                </div>

                </Col>
                <Col md={9}>
                <h2 className='my-5'>Best Colleges For Online / Distance MBA in India</h2>
                <Row className='row-cols-3'>
           
            
<TopUniversityCard screenSize={screenSize} />
              
                   
                      
                      
                </Row>
                     
                </Col>
            </Row>
        </Container>
    </>
  )
}

export default newTop