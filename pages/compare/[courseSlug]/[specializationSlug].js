import { useRouter } from "next/router";
import React from "react";
import { Modal, Spinner } from "react-bootstrap";
import Header1 from "../../../components/Global/Header1";
import RealExpert from "/components/Homepage/Slider/RealExpert";
import OurExpert from "/components/Global/OurExpert";
import Footer from "/components/Global/Footer";

import {
  getCourseDetail,
  getCourseSlugs,
  getSpecializationDetail,
} from "../../../service/CourseService";
import { getCounsellorData, getMenu } from "../../../service/MiscService";
import { useState } from "@hookstate/core";
import store from "../../../utils/store";
import { useAuth } from "../../../hooks/auth";
import CompareContent from "../../../components/Comparepage/CompareContent/CompareContent";
import AuthModal from "../../../components/Global/Modals/AuthModal";
import { CompareService } from "../../../service/CompareService";
import { useEffect } from "react";
import Head from "next/head";

export async function getStaticPaths() {
  const allSlugs = await getCourseSlugs().then((response) => response.data);
  const paths = allSlugs
    .map((courseSlug) =>
      courseSlug.specializations.map((specializationSlug) => ({
        params: {
          courseSlug: courseSlug.slug,
          specializationSlug: specializationSlug.slug,
        },
      }))
    )
    .flat();

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const courseSlug = context.params.courseSlug;
  const specializationSlug = context.params.specializationSlug;

  const menuData = await getMenu().then((response) => response.data);
  const counsellors = await getCounsellorData().then(
    (response) => response.data
  );

  const courseData = await getCourseDetail(courseSlug).then(
    (response) => response.data.data[0]
  );

  const specializationData = await getSpecializationDetail(
    courseSlug,
    specializationSlug
  ).then((response) => response.data.data[0]);

  if (!specializationData || !courseData) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      menuData: menuData,
      courseData: courseData,
      specializationData: specializationData,
      counsellors: counsellors,
    },
    revalidate: 10,
  };
}

const CompareSpecializationPage = ({
  screenSize,
  menuData,
  courseData,
  specializationData,
  counsellors,
}) => {
  const router = useRouter();
  const { user } = useAuth();

  const [modalSize, setModalSize] = React.useState("md");
  const [modalContent, setModalContent] = React.useState(null);
  const [showModal, setShowModal] = React.useState(false);

  const [compareTitles, setCompareTitles] = React.useState(null);

  const { universitiesToCompare } = useState(store);

  const setupModal = (key) => {
    if (key === "user-auth") {
      setModalSize("md");
      setModalContent(
        <AuthModal menuData={menuData} closeModal={() => router.push("/")} />
      );
      setShowModal(true);
    }
  };

  const prepareCompareData = () => {
    const courseID = courseData.id;
    const specializationID = specializationData.id;
    const { check } = router.query;

    let universities = "";
    let univsPath = "";

    if (Array.isArray(check)) {
      universities = check.map(function (univ) {
        return `check=${univ}`;
      });
      univsPath = universities.join("&");
    } else {
      universities = `check=${check}`;
      univsPath = universities;
    }

    const compareService = new CompareService();
    compareService
      .getCompareBySpecialization(courseID, specializationID, univsPath)
      .then((response) => {
        const univData = response.data.data;
        setCompareTitles(response.data.compare_title);

        univData.forEach(function (university) {
          let universityToCompare = {
            id: university.university.id,
            name: university.university.name,
            prospectus_link: university.university.prospectus_link,
            slug: university.university.slug,
            logo: university.university.logo,
            fee: university.fee,
            fee_details: university.courses_fee_details,
            compare: university.compare,
            sample_certificate: university.university.sample_certificate,
            avg_rating: university.avg_rating,
            about_university: university.university.about,
            university_facts: university.university.universities_facts,
            university_approvals: university.university.approval_details,
            books_lms: university.university.books_lms,
            university_reviews: university.reviews_universities_specializations,
            university_faqs:
              university.universities_faqs_specializations_all_faqs,
            loan_facility: university.university.loan_facility,
          };

          universitiesToCompare.set((prev) => [...prev, universityToCompare]);
        });
      });
  };

  useEffect(() => {
    if (router.isReady) {
      if (router.query.uid) {
        prepareCompareData();
      } else {
        if (user) {
          prepareCompareData();
        } else {
          setupModal("user-auth");
        }
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user, router.isReady]);

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <Head>
        <title>Compare Colleges and Universities</title>
      </Head>
      <Header1 screenSize={screenSize} />
      {!compareTitles ? (
        <div className="w-100">
          <Spinner />
        </div>
      ) : (
        <CompareContent
          screenSize={screenSize}
          compareTitles={compareTitles}
          userID={router.query.uid ? router.query.uid : user}
          courseID={courseData.id}
          courseSlug={courseData.slug}
          specializationID={specializationData.id}
          specializationSlug={specializationData.slug}
        />
      )}
      <RealExpert counsellors={counsellors} />
      <OurExpert />
      <Footer menuData={menuData} />
      <Modal
        show={showModal}
        onHide={() => setShowModal(false)}
        backdrop="static"
        size={modalSize}
        centered>
        {modalContent}
      </Modal>
    </div>
  );
};

export default CompareSpecializationPage;
