import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import styles from "/styles/Checklist.module.scss";
import AnimatedYoutube from "/components/Global/AnimatedYoutube";
import Link from "next/link";
import Image from "next/image";
import checklist1 from "/public/images/checklist/checklist-verified.png";
import checklist2 from "/public/images/checklist/degree.png";
import checklist3 from "/public/images/checklist/data-science.png";
import checklist4 from "/public/images/checklist/registration-form.png";
import checklist5 from "/public/images/checklist/registration-process.png";
import checklist6 from "/public/images/checklist/onlinetest.png";
import mobile from "/public/images/mobile-phone.png";
import { TiTick } from "react-icons/ti";
import Footer from "../components/Global/Footer";
import Header from "../components/Global/Header";
import { getMenu } from "../service/MiscService";
import HeaderDecider from "../components/Global/HeaderDecider";
import CheckList from "../components/CheckList/CheckList";
import Head from "next/head";
import { getSearchSchema } from "../utils/genericSchemas";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

const checklist = ({ screenSize, menuData }) => {
  return (
    <>
      <Head>
        <title>
          Top 8 Facts to Know Before Choosing Right Online University
        </title>
        <meta
          name="description"
          content="Questions and Myths often puzzle students before taking admission to an online university. Here are the top 8 facts related to online universities. Click here."
        />
        <meta name="robots" content="index, follow" />
        <link href="https://collegevidya.com/checklist/" rel="canonical" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Top 8 Facts to Know Before Choosing Right Online University"
        />
        <meta
          property="og:description"
          content="Questions and Myths often puzzle students before taking admission to an online university. Here are the top 8 facts related to online universities. Click here."
        />
        <meta property="og:url" content="https://collegevidya.com/checklist/" />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="Top 8 Facts to Know Before Choosing Right Online University"
        />
        <meta
          property="twitter:description"
          content="Questions and Myths often puzzle students before taking admission to an online university. Here are the top 8 facts related to online universities. Click here."
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/checklist/"
        />
        <meta property="twitter:image" content="" />
        <meta
          name="keywords"
          content="right online university, college vidya cv, college vidya collections, Top 8 Facts about right online university, distance online education compare portal"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/checklist/",
              name: "Top 8 Facts to Know Before Choosing Right Online University",
              description:
                "Questions and Myths often puzzle students before taking admission to an online university. Here are the top 8 facts related to online universities. Click here.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "Top 8 Facts to Know Before Choosing Right Online University",
              description:
                "Questions and Myths often puzzle students before taking admission to an online university. Here are the top 8 facts related to online universities. Click here.",
              url: "https://collegevidya.com/checklist/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/checklist/images/logo.svg",
              address: "India",
            }),
          }}
        />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <div className="mb-4">
        <CheckList />
      </div>

      <Footer menuData={menuData} />
    </>
  );
};

export default checklist;
