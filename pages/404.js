import Link from "next/link";
import { Col, Container, Row } from "react-bootstrap";
import Image from "next/image";
import errorimg from "/public/images/error-img.svg";
import Course from "../components/Homepage/Course";
import styles from "./404.module.css";
import { getMenu } from "../service/MiscService";
import HeaderDecider from "../components/Global/HeaderDecider";
import Footer from "../components/Global/Footer";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

const FourOhFour = ({ screenSize, menuData }) => {
  return (
    <>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <section className="py-5 py-4">
        <Container>
          <Row>
            <Col md={4}>
              <Image src={errorimg} alt="Error Image" />
              <div>
                <h5 className="text-center">
                  Oops, We couldn&apos;t find that page.
                </h5>
                <p className="text-center">
                  Please try again or contact the website administrator to get
                  some help.
                </p>
                <div className="my-2 text-center">
                  <Link href="/">
                    <a className={styles.round_btn}>Back To Homepage</a>
                  </Link>
                </div>
              </div>
            </Col>
            <Col md={8}>
              <Container className="mb-3">
                <Row>
                  <Col md={10} className="mx-auto">
                    <div>
                      <h1 className="text-center" style={{ fontSize: "28px" }}>
                        Compare University &amp; Select Best One
                      </h1>
                    </div>
                  </Col>
                </Row>
              </Container>
              <Course menuData={menuData} />
            </Col>
          </Row>
        </Container>
      </section>
      <Footer menuData={menuData} />
    </>
  );
};

export default FourOhFour;
