import Image from "next/image";
import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { Card } from "primereact/card";
import stamp from "/public/images/stamp.png";
import ceo from "/public/images/ceo-msg.png";
import Sealcard from "/components/OurTrust/Sealcard";
import trust1 from "/public/images/our-trust-01.jpeg";
import trust2 from "/public/images/our-trust-02.jpeg";
import OurExpert from "../components/Global/OurExpert";
import styles from "./OurTrust.module.scss";
import { AiOutlineSafetyCertificate } from "react-icons/ai";
import { FaUniversity } from "react-icons/fa";
import Link from "next/link";
import { HiOutlineNewspaper } from "react-icons/hi";
import Header from "../components/Global/Header";
import Footer from "../components/Global/Footer";
import { getMenu } from "../service/MiscService";
import HeaderDecider from "../components/Global/HeaderDecider";
import Head from "next/head";
import { getSearchSchema } from "../utils/genericSchemas";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

const ourtrust = ({ screenSize, menuData }) => {
  return (
    <>
      <Head>
        <title>
          College Vidya Trust - Online & Distance Education Compare Portal
        </title>
        <meta
          name="description"
          content="College Vidya: A Seal of Trust and Transparency. College Vidya gives you unbiased guidance to choose the best distance & online education university providing the best of its courses. Click to learn more."
        />
        <meta name="robots" content="index, follow" />
        <link href="https://collegevidya.com/our-trust/" rel="canonical" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="College Vidya Trust - Online & Distance Education Compare Portal"
        />
        <meta
          property="og:description"
          content="College Vidya: A Seal of Trust and Transparency. College Vidya gives you unbiased guidance to choose the best distance & online education university providing the best of its courses. Click to learn more."
        />
        <meta property="og:url" content="https://collegevidya.com/our-trust/" />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="College Vidya Trust - Online & Distance Education Compare Portal"
        />
        <meta
          property="twitter:description"
          content="College Vidya: A Seal of Trust and Transparency. College Vidya gives you unbiased guidance to choose the best distance & online education university providing the best of its courses. Click to learn more."
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/our-trust/"
        />
        <meta property="twitter:image" content="" />
        <meta
          name="keywords"
          content="college vidya, collegevidya seal of trust and transparency, college vidya tv, distance online education compare portal, college vidya compare"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/our-trust/",
              name: "College Vidya Trust - Online & Distance Education Compare Portal",
              description:
                "College Vidya: A Seal of Trust and Transparency. College Vidya gives you unbiased guidance to choose the best distance & online education university providing the best of its courses. Click to learn more.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "College Vidya Trust - Online & Distance Education Compare Portal",
              description:
                "College Vidya: A Seal of Trust and Transparency. College Vidya gives you unbiased guidance to choose the best distance & online education university providing the best of its courses. Click to learn more.",
              url: "https://collegevidya.com/our-trust/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo.svg",
              address: "India",
            }),
          }}
        />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <div className={`${styles.wrapper} py-3 py-sm-5`}>
        <Container>
          <Row>
            <Col md={7}>
              <h1 className={`${styles.bannerHead}`}>
                Trust Comes First Always.
              </h1>
              <p>
                Collegevidya is the smartest and most intuitive platform for
                Compare university. Thrive digitally as we guide your career
                with the right comparing tools.
              </p>
              <p>Right University For Your Right Degree</p>
              <p>#chunowahijohaisahi</p>

              <Link href="/media">
                <a className="d-inline-block bg-dark rounded px-4 py-3 text-white mb-4">
                  <HiOutlineNewspaper className="me-2" />
                  College vidya in News
                </a>
              </Link>
            </Col>
            <Col md={5}>
              <Image className={`${styles.stamp_img}`} src={stamp} alt="" />
            </Col>
          </Row>
          <Row>
            <Col md={4}>
              <Card className="mb-2">
                <div className="d-flex">
                  <AiOutlineSafetyCertificate className="me-1" fontSize={30} />
                  India&apos;s Only Portal with Unbiased Guidance
                </div>
              </Card>
            </Col>
            <Col md={4}>
              <Card className="mb-2">
                <div className="d-flex">
                  <FaUniversity className="me-1" fontSize={30} />
                  Only UGC-DEB approved University Listed
                </div>
              </Card>
            </Col>
            <Col md={4}>
              <Card className="mb-2">
                <div className="d-flex">
                  <AiOutlineSafetyCertificate className="me-1" fontSize={30} />
                  Distance &amp; Online Universities at One Click
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
      <Container>
        <Row>
          <h1 className="text-center my-5">CEO Message For Students </h1>
          <div className="text-center">
            <Image src={ceo} alt="" />
          </div>
        </Row>
        <h1 className="text-center my-5">
          College Vidya: A Seal of Trust and Transparency
        </h1>
        <Row>
          <Sealcard />
        </Row>
        <Row>
          <Col
            md={6}
            className="d-flex align-items-start flex-column justify-content-center">
            <h4 className="h4">Compare as many Universities as you Want</h4>
            <p>
              You can compare all the universities based on your Distance &amp;
              Online Course and select the university that fits your parameters.
            </p>
          </Col>
          <Col
            md={6}
            className="d-flex align-items-center justify-content-center">
            <Image src={trust1} alt="" />
          </Col>
          <Col
            md={6}
            className="d-flex align-items-center justify-content-center">
            <Image src={trust2} alt="" />
          </Col>
          <Col
            md={6}
            className="d-flex align-items-start flex-column justify-content-center">
            <h4 className="h4">Get a Personalized Mentor</h4>
            <p>
              Personalized Mentorship with a dedicated Mentor for you and
              connect over text, phone or Video Consultation.
            </p>
          </Col>
        </Row>
        <Row>
          <div className="border p-5 mb-5 position-relative">
            <p className="m-0 position-absolute top-0 start-50 translate-middle-x bg-primary text-white px-3 rounded-bottom">
              Know More
            </p>

            <div className="more_about_us">
              <h2>Why College Vidya </h2>
              <ul>
                <li>
                  College Vidya gives you unbiased guidance to choose the best
                  distance &amp; online education university providing the best
                  of its courses.
                </li>
                <li>
                  College Vidya is the first that has introduced Compare feature
                  where you can compare all the distance &amp; Online colleges
                  and universities.
                </li>
                <li>
                  We also have introduced the College Vidya Score which gives
                  scores to all the colleges and universities yearly as per its
                  performances and the quality of education they are providing.
                  Also reviews from the alumni and present students.
                </li>
              </ul>
              <h2>Benefits of Distance Learning </h2>
              <ul>
                <li>
                  Distance &amp; Online Education allows you to study wherever
                  and whenever you want as it offers the flexibility of time and
                  money by giving online classes both live and recorded video
                  lectures with no barrier.
                </li>
                <li>
                  Learning at your own pace as there is no fixed time to study
                  you can attend classes or look into the recorded lectures.
                </li>
                <li>
                  Distance &amp; Online enables you to have industry expert
                  guest lectures where you’ll be having a genuine and advanced
                  experience of learning and strategies and tactics of how to
                  take a step forward.
                </li>
                <li>
                  With the Distance &amp; Online education program, one has the
                  advantage of continuing the course along with the current job
                  as it’s a distance program in which classes held online can be
                  taken anytime.
                </li>
                <li>
                  While pursuing the Distance &amp; Online program in parallel
                  with the job can get a hike promotion in your career.
                </li>
                <li>
                  All the Distance &amp; Online education programs are
                  cost-effective. They have a lesser fee structure as compare to
                  regular programs.
                </li>
                <li>
                  Distance &amp; Online education eventually can boost your
                  skill set by inculcating experience, credentials along with
                  the current job or course if you’re pursuing.
                </li>
                <li>
                  The Industry revised the curriculum by adding new technologies
                  and the latest syllabus in the programs to serve quality of
                  education and not quantity.
                </li>
              </ul>
              <h2>Key Factors When College Vidya Lists Universities </h2>
              <ul>
                <li>
                  College Vidya only lists universities that are approved by
                  UGC-DEB, AICTE, NAAC, and NIRF.
                </li>
                <li>
                  College Vidya examines the Learning Management system. If they
                  match the parameters, then only it gets listed.
                </li>
                <li>
                  Placement Support Cells is also the parameter for the
                  university to get on College Vidya.
                </li>
                <li>
                  Other parameters include Alumni Connect, Faculty, Placement
                  Records, and EMI facilities.
                </li>
              </ul>
            </div>
          </div>
          <OurExpert />
        </Row>
      </Container>
      <Footer menuData={menuData} />
    </>
  );
};

export default ourtrust;
