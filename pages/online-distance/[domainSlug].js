import { Col, Container, Nav, Row, Tab } from "react-bootstrap";

import CourseCard from "/components/Homepage/CourseCard";
import styles from "../../components/Homepage/CourseCard.module.scss";
import Header from "../../components/Global/Header";
import { getMenu } from "../../service/MiscService";
import OurExperts from "../../components/Global/OurExpert";
import Footer from "../../components/Global/Footer";
import Head from "next/head";
import { getSearchSchema } from "../../utils/genericSchemas";

export async function getStaticPaths() {
  const allSlugs = ["online-distance-ug-course", "online-distance-pg-course"];
  const paths = allSlugs.map((domainSlug) => {
    return {
      params: {
        domainSlug: domainSlug,
      },
    };
  });

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps(context) {
  const domainSlug = context.params.domainSlug;
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
      domainSlug: domainSlug === "online-distance-ug-course" ? "UG" : "PG",
    },
    revalidate: 10,
  };
}

const DomainPage = ({ menuData, domainSlug }) => {
  return (
    <>
      <Head>
        <title>Online & Distance {domainSlug} Courses | College vidya</title>
        <meta
          name="description"
          content="Student/Working Professionals have you confused? Try College vidya’s online & distance education compare tool for online and distance PG courses in India."
        />
        <meta name="robots" content="index, follow" />
        <link
          href="https://collegevidya.com/online-distance/online-distance-pg-course/"
          rel="canonical"
        />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Online & Distance PG Courses | College vidya"
        />
        <meta
          property="og:description"
          content="Student/Working Professionals have you confused? Try College vidya’s online & distance education compare tool for online and distance PG courses in India."
        />
        <meta
          property="og:url"
          content="https://collegevidya.com/online-distance/online-distance-pg-course/"
        />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya/"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="Online & Distance PG Courses | College vidya"
        />
        <meta
          property="twitter:description"
          content="Student/Working Professionals have you confused? Try College vidya’s online & distance education compare tool for online and distance PG courses in India."
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/online-distance/online-distance-pg-course/"
        />
        <meta property="twitter:image" content="" />
        <meta
          name="keywords"
          content="college vidya pg course, online distance pg course, collegevidya compare portal, distance education compare portal, online distance education compare tool"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: `https://collegevidya.com/online-distance/online-distance-${domainSlug.toLowerCase()}-course/`,
              name: `Online & Distance ${domainSlug} Courses | College vidya`,
              description: `Student/Working Professionals have you confused? Try College vidya's online & distance education compare tool for online and distance ${domainSlug} courses in India.`,
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: `Online & Distance ${domainSlug} Courses | College vidya`,
              description: `Student/Working Professionals have you confused? Try College vidya’s online & distance education compare tool for online and distance ${domainSlug} courses in India.`,
              url: `https://collegevidya.com/online-distance/online-distance-${domainSlug.toLowerCase()}-course/`,
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo.svg",
              address: "India",
            }),
          }}
        />
      </Head>
      <Header menuData={menuData} />
      <section className="pt-5">
        <Container>
          <Row>
            <Col className="text-center">
              <h2 className="fw-bold">Online Distance {domainSlug} Courses</h2>
            </Col>
          </Row>
        </Container>
      </section>
      <section className="pt-5">
        <Container>
          <Tab.Container defaultActiveKey={0}>
            <Row>
              <Col sm={3} className={`${styles.course_domain}`}>
                <Nav
                  variant="pills"
                  className={`${styles.course_nav} flex-lg-column py-4 px-3 p-sm-3`}>
                  {menuData
                    .filter((domain) => domain.name.includes(domainSlug))
                    .map((oneDomain) => oneDomain.courses)[0]
                    .map((course, index) => (
                      <Nav.Item key={course.id}>
                        <Nav.Link eventKey={index}>{course.name}</Nav.Link>
                      </Nav.Item>
                    ))}
                </Nav>
              </Col>
              <Col sm={9} className={`${styles.course_domain_content}`}>
                <Tab.Content className={`${styles.course_tabcontent}`}>
                  {menuData
                    .filter((domain) => domain.name.includes(domainSlug))
                    .map((oneDomain) => oneDomain.courses)[0]
                    .map((course, index) => (
                      <Tab.Pane key={course.id} eventKey={index}>
                        <div className="d-flex flex-wrap">
                          {course.specializations
                            .filter(
                              (specialization) => specialization.status === 1
                            )
                            .map((specialization) => (
                              <CourseCard
                                key={specialization.id}
                                title={specialization.display_name}
                                university_number={
                                  specialization.university_count
                                }
                                icon={specialization.icon}
                                link={`/courses/${course.slug}/${specialization.slug}/`}
                                label={specialization.duration}
                              />
                            ))}
                        </div>
                      </Tab.Pane>
                    ))}
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        </Container>
      </section>
      <OurExperts />
      <Footer menuData={menuData} />
    </>
  );
};

export default DomainPage;
