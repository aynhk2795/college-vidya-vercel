import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Image from "next/image";
import MediaCard from "../components/Media/MediaCard";
import LeaderCard from "../components/Media/LeaderCard";
import news from "/public/images/uparrow.png";
import OurExpert from "/components/Global/OurExpert";
import styles from "/styles/Media.module.scss";
import Header from "../components/Global/Header";
import Footer from "../components/Global/Footer";
import { getMenu, getVoiceOfLeaders } from "../service/MiscService";
import HeaderDecider from "../components/Global/HeaderDecider";
import Head from "next/head";
import { getSearchSchema } from "../utils/genericSchemas";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);
  const voiceOfLeaders = await getVoiceOfLeaders().then(
    (response) => response.data
  );

  return {
    props: {
      menuData: menuData,
      voiceOfLeaders: voiceOfLeaders,
    },
    revalidate: 10,
  };
}

const media = ({ screenSize, menuData, voiceOfLeaders }) => {
  return (
    <>
      <Head>
        <title>College Vidya In Media | Latest News, Videos and more</title>
        <meta
          name="description"
          content="College Vidya has been coverage by top media houses like Forbes India, Hindustan Times, CNBC Awaz, etc. Click here for detailed information."
        />
        <meta name="robots" content="index, follow" />
        <link href="https://collegevidya.com/media/" rel="canonical" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="College Vidya In Media | Latest News, Videos and more"
        />
        <meta
          property="og:description"
          content="College Vidya has been coverage by top media houses like Forbes India, Hindustan Times, CNBC Awaz, etc. Click here for detailed information."
        />
        <meta property="og:url" content="https://collegevidya.com/media/" />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content=" https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="College Vidya In Media | Latest News, Videos and more"
        />
        <meta
          property="twitter:description"
          content="College Vidya has been coverage by top media houses like Forbes India, Hindustan Times, CNBC Awaz, etc. Click here for detailed information."
        />
        <meta
          property="twitter:url"
          content=" https://collegevidya.com/media/"
        />
        <meta property="twitter:image" content="" />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/media/",
              name: "College Vidya In Media | Latest News, Videos and more",
              description:
                "College Vidya has been coverage by top media houses like Forbes India, Hindustan Times, CNBC Awaz, etc. Click here for detailed information.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "College Vidya In Media | Latest News, Videos and more",
              description:
                "College Vidya has been coverage by top media houses like Forbes India, Hindustan Times, CNBC Awaz, etc. Click here for detailed information.",
              url: "https://collegevidya.com/media/",
              email: "info@collegevidya.com",
              telephone: "1800-420-5757",
              logo: "https://collegevidya.com/images/logo2.png",
              address: "India",
            }),
          }}
        />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <div className={`${styles.mediaWrap} py-5 `}>
        <Container>
          <Row>
            <Col md={4}>
              <h1>College Vidya Newsroom</h1>
              <Image src={news} alt="" />
            </Col>
            <Col md={8}>
              <Row>
                <MediaCard />
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
      <Container>
        <h1 className="text-center my-5">Voice of our Leaders</h1>
        <Row>
          <LeaderCard voiceOfLeaders={voiceOfLeaders} />
          <OurExpert />
        </Row>
      </Container>
      <Footer menuData={menuData} />
    </>
  );
};

export default media;
