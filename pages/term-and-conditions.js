import Head from "next/head";
import React from "react";
import { Container } from "react-bootstrap";
import Footer from "../components/Global/Footer";
import HeaderDecider from "../components/Global/HeaderDecider";
import OurExpert from "../components/Global/OurExpert";
import { getMenu } from "../service/MiscService";
import { getSearchSchema } from "../utils/genericSchemas";
import HeroSection from "/components/Global/Jumbotron/HeroSection";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

const Terms = ({ screenSize, menuData }) => {
  return (
    <>
      <Head>
        <title>Terms and Conditions - College Vidya</title>
        <meta
          name="description"
          content="Further to the Terms of Use provided on Edutra Consulting Services Pvt Ltd is parents organization of Collegevidya.com , we disclaim all liabilities…"
        />
        <meta name="robots" content="index, follow" />
        <link
          href="https://collegevidya.com/term-and-conditions/"
          rel="canonical"
        />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Terms and Conditions - College Vidya"
        />
        <meta
          property="og:description"
          content="Further to the Terms of Use provided on Edutra Consulting Services Pvt Ltd is parents organization of Collegevidya.com , we disclaim all liabilities…"
        />
        <meta
          property="og:url"
          content="https://collegevidya.com/terms-and-conditions/"
        />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="Terms and Conditions - College Vidya"
        />
        <meta
          property="twitter:description"
          content="Further to the Terms of Use provided on Edutra Consulting Services Pvt Ltd is parents organization of Collegevidya.com , we disclaim all liabilities…"
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/terms-and-conditions/"
        />
        <meta property="twitter:image" content="" />
        <meta
          name="keywords"
          content="term and conditions of college vidya, collegevidya term and conditions, collegevidya, top online and distance education compare portal, top online education portal"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/term-and-conditions/",
              name: "Terms and Conditions - College Vidya",
              description:
                "Further to the Terms of Use provided on Edutra Consulting Services Pvt Ltd is parents organization of Collegevidya.com , we disclaim all liabilities…",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "Terms and Conditions - College Vidya",
              description:
                "Further to the Terms of Use provided on Edutra Consulting Services Pvt Ltd is parents organization of Collegevidya.com , we disclaim all liabilities…",
              url: "https://collegevidya.com/term-and-conditions/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo2.png",
              address: "India",
            }),
          }}
        />
      </Head>
      <HeaderDecider screenSize={screenSize} menuData={menuData} />
      <HeroSection title="Terms & Conditions" />
      <Container className="my-5">
        <p>
          Further to the Terms of Use provided on &quot;Edutra Consulting
          Services Pvt Ltd&quot; is parents organization of Collegevidya.com ,
          we disclaim all liabilities and warranties with respect to payment of
          fees (&quot;Fees&quot;) that is applicable to the programs you have
          opted for through the &quot;Edutra Consulting Services Pvt Ltd&quot;
          is parents organization of Collegevidya.com. Such Fee shall include
          but not be limited to course fee. Further the User agrees and
          understands that:
        </p>
        <ul>
          <li className="mb-3">
            It shall be the responsibility of the User to make all necessary
            payments regarding the Fee, well in advance of the last date
            specified by the &quot;Edutra Consulting Services Pvt Ltd&quot; is
            parents organization of Collegevidya.com.
          </li>
          <li className="mb-3">
            The online payment facility provided by the &quot;Edutra Consulting
            Services Pvt Ltd&quot; is parents organization of Collegevidya.com
            is neither banking nor a financial service but is merely a
            facilitator for ease of payment (&quot;Payment Gateway Service
            Provider&quot;). The Payment Gateway Service Provider facilitates
            online payments, collection and remittance of money for the
            transactions on the &quot;Edutra Consulting Services Pvt Ltd&quot;
            is parents organization of Collegevidya.com by using existing
            authorized banking infrastructure and credit card payment gateway
            networks. Further, by providing such payment facility through the
            Payment Gateway Service Provider, the &quot;Edutra Consulting
            Services Pvt Ltd&quot; is parents organization of Collegevidya.com
            is neither acting as a trustee nor acting in a fiduciary capacity
            with respect to the payments made by the User against the purchase
            of Services on the Website. The User further understands and agrees
            that, all payments made by the User through the Payment Gateway
            Service Provider shall take a minimum of one-two (1-2) working days
            to be effectively processed. The User shall make the payment of the
            applicable Fee through the Payment Gateway Service Provider at
            his/her own risk. &quot;Edutra Consulting Services Pvt Ltd&quot; is
            parents organization of Collegevidya.com shall not be responsible
            for any failure or delay in processing the Fee paid by the User
            through the Payment Gateway Service Provider.
          </li>
          <li className="mb-3">
            While availing any of the payment method/s available on the Website
            &quot;Edutra Consulting Services Pvt Ltd&quot; is parents
            organization of Collegevidya.com will not be responsible or assume
            any liability, whatsoever in respect of any loss or damage arising
            directly or indirectly to the User due to (a) Lack of authorization
            for any transactions (b) Any payment issues arising out of the
            transaction; (c) decline of such transaction for any reason; or (d)
            use, or inability to use the payment page maintained by the Payment
            Gateway Service Provider.
          </li>
          <li className="mb-3">
            &quot;Edutra Consulting Services Pvt Ltd&quot; is parents
            organization of Collegevidya.com shall not be liable for any loss or
            damages suffered by the User due to any failure, delay or
            interruption on part of the Payment Gateway Service Provider,
            including but not limited to, partial or total failure of any
            network terminal, data processing system, computer tele-transmission
            or telecommunications system or other circumstances which is solely
            in the control of the Payment Gateway Service Provider.
          </li>
          <li className="mb-3">
            In the event of rejection of Services by &quot;Edutra Consulting
            Services Pvt Ltd&quot; is parents organization of Collegevidya.com
            due to delayed payments by the User, the User shall provide a
            written request to us along with a payment receipt validating the
            payment being made for the Information and Communication Technology
            (ICT) enabled Course(s). Upon verification of such payment receipt,
            the &quot;Edutra Consulting Services Pvt Ltd&quot; is parents
            organization of Collegevidya.com shall consider such request for
            refund of payments, in accordance with the Edutra Consulting
            Services Pvt Ltd is parents organization of Collegevidya.com
            internal refund policies published on the company website
          </li>
          <li className="mb-3">
            In the event of rejection of Services by &quot;Edutra Consulting
            Services Pvt Ltd&quot; is parents organization of Collegevidya.com
            due to delayed payments by the User, the User shall provide a
            written request to us along with a payment receipt validating the
            payment being made for the Information and Communication Technology
            (ICT) enabled Course(s). Upon verification of such payment receipt,
            the &quot;Edutra Consulting Services Pvt Ltd&quot; is parents
            organization of Collegevidya.com shall consider such request for
            refund of payments, in accordance with the &quot;Edutra Consulting
            Services Pvt Ltd&quot; is parents organization of Collegevidya.com
            internal refund policies published on the company website
            &quot;Edutra Consulting Services Pvt Ltd&quot; is parents
            organization of Collegevidya.com, the Payment Gateway Service
            Provider shall resolve the complaints reported by the User, subject
            to its internal policies and rules.
          </li>
        </ul>
      </Container>
      <OurExpert />

      <Footer menuData={menuData} />
    </>
  );
};

export default Terms;
