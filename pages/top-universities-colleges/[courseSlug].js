import Footer from "../../components/Global/Footer";
import Header from "../../components/Global/Header";
import MainHeading from "/components/Global/Heading/MainHeading";
import { getCourseDetail, getCourseSlugs } from "../../service/CourseService";
import { getMenu } from "../../service/MiscService";
import { Col, Container, Form, Row } from "react-bootstrap";
import TopUniversityBar from "/components/TopUniversity/TopUniversityBar";
import Search from "../../components/TopUniversity/Modal/Search";
import { generate } from "shortid";
import { AiFillStar } from "react-icons/ai";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { getTopUniversity } from "../../service/TopUniversityService";
import Head from "next/head";
import { getSearchSchema } from "../../utils/genericSchemas";
import styles from "./TopUniversityCoursePage.module.scss"
import { Slider } from 'primereact/slider';
import TopUniversityCard from "../../components/TopUniversity/TopUniversityCard";


export async function getStaticPaths() {
  const allSlugs = await getCourseSlugs().then((response) => response.data);
  const paths = allSlugs.map((courseSlug) => {
    return {
      params: {
        courseSlug: courseSlug.slug,
      },
    };
  });

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const courseSlug = context.params.courseSlug;

  const courseData = await getCourseDetail(courseSlug).then(
    (response) => response.data.data[0]
  );

  const menuData = await getMenu().then((response) => response.data);

  if (!courseData) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      menuData: menuData,
      courseData: courseData,
    },
    revalidate: 10,
  };
}

const starlist1 = [<AiFillStar key={generate()} className="text-warning" />];
const starlist2 = [
  <AiFillStar key={generate()} className="text-warning" />,
  <AiFillStar key={generate()} className="text-warning" />,
];
const starlist3 = [
  <AiFillStar key={generate()} className="text-warning" />,
  <AiFillStar key={generate()} className="text-warning" />,
  <AiFillStar key={generate()} className="text-warning" />,
];
const starlist4 = [
  <AiFillStar key={generate()} className="text-warning" />,
  <AiFillStar key={generate()} className="text-warning" />,
  <AiFillStar key={generate()} className="text-warning" />,
  <AiFillStar key={generate()} className="text-warning" />,
];
const starlist5 = [
  <AiFillStar key={generate()} className="text-warning" />,
  <AiFillStar key={generate()} className="text-warning" />,
  <AiFillStar key={generate()} className="text-warning" />,
  <AiFillStar key={generate()} className="text-warning" />,
  <AiFillStar key={generate()} className="text-warning" />,
];

const TopUniversityCoursePage = ({ menuData, courseData , screenSize }) => {
  const router = useRouter();
  const [value5, setValue5] = useState([20,80]);

  const [topUniversityList, setTopUniversityList] = useState([]);

  useEffect(() => {
    const courseID = courseData.id;

    let formData = new FormData();
    formData.append("cid", courseID);

    getTopUniversity(formData).then((response) =>
      setTopUniversityList(response.data.data)
    );
  }, [courseData]);

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Head>
        <title>{courseData.name}: Top Colleges & Universities in India</title>
        <meta
          name="description"
          content={`Find top colleges & universities for ${courseData.name} courses in India includes approvals, college vidya score, college rating, fees, placement and more.`}
        />
        <meta name="robots" content="index, follow" />
        <link
          href={`https://collegevidya.com/top-universities-colleges/${courseData.slug}/`}
          rel="canonical"
        />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content={`${courseData.name}: Top Colleges & Universities In India`}
        />
        <meta
          property="og:description"
          content={`Find top colleges & universities for ${courseData.name} courses in India includes approvals, college vidya score, college rating, fees, placement and more.`}
        />
        <meta
          property="og:url"
          content={`https://collegevidya.com/top-universities-colleges/${courseData.slug}/`}
        />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content={`${courseData.name}: Top Colleges & Universities In India`}
        />
        <meta
          property="twitter:description"
          content={`Find top colleges & universities for ${courseData.name} courses in India includes approvals, college vidya score, college rating, fees, placement and more.`}
        />
        <meta
          property="twitter:url"
          content={`https://collegevidya.com/top-universities-colleges/${courseData.slug}/`}
        />
        <meta property="twitter:image" content="" />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: `https://collegevidya.com/top-universities-colleges/${courseData.slug}/`,
              name: `${courseData.name}: Top Colleges & Universities In India`,
              description: `Find top colleges & universities for ${courseData.name} courses in India includes approvals, college vidya score, college rating, fees, placement and more.`,
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: `${courseData.name}: Top Colleges & Universities In India`,
              description: `Find top colleges & universities for ${courseData.name} courses in India includes approvals, college vidya score, college rating, fees, placement and more.`,
              url: `https://collegevidya.com/top-universities-colleges/${courseData.slug}/`,
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo.svg",
              address: "India",
            }),
          }}
        />
      </Head>
      <Header menuData={menuData} />
      <div className="bg-grey py-5">
     
        <Container>
        
          <Row>
          
          <Row>
            <Col md={3}>
            <h3 className="mb-4">Search By Filters</h3>
            </Col>
            <Col md={9} className="ps-4">
              {/* <h1 className="h4 mb-4 ps-4">Best Colleges For Online / Distance MBA in India</h1> */}
              <MainHeading
              title={`Best Colleges For ${courseData.name} in India`}
            />
              </Col>
          </Row>
            <Col md={3} className={` ${styles.filter_wrap} pe-0`}>
              <div className="bg-white py-4 rounded shadow-2 mb-3 px-4" style={{height:"440px",overflowY:"scroll"}}>
                <p className="fw-bold d-flex justify-content-between bg-white">
                  <span>Specialisations</span>
                  <span>
                    <Search />
                  </span>
                </p>
                {menuData
                  .map((domain) => domain.courses)
                  .flat()
                  .filter((course) => course.id === courseData.id)[0]
                  .specializations.slice(0, 10)
                  .map((specialization) => (
                    <Form.Group
                      key={specialization.id}
                      className="mb-1 py-2 fs-14 text-secondary d-flex align-items-center"
                      controlId={specialization.id}>
                      <Form.Check type="radio" name="specialization" label={specialization.name} className={`${styles.form_radio} d-flex align-items-start gap-2`} />
                    </Form.Group>
                  ))}
              </div>
              <div className={` ${styles.budget_slider} bg-white py-4 rounded shadow-2 mb-3 px-4`}>
                <p className="fw-bold">Budget</p>
                <p className="textsecondary">₹ {value5[0]} - ₹ {value5[1]}</p>
                <Slider value={value5} onChange={(e) => setValue5(e.value)} range />
            
              </div>
              <div className="bg-white py-4 px-4 card mb-2">
                <p className="fw-bold">Students Rating</p>
                <Form.Group className="mb-1" controlId="formBasicCheckbox">
                  <Form.Check type="radio" name="rating" label={starlist1} />
                </Form.Group>
                <Form.Group className="mb-1" controlId="formBasicCheckbox">
                  <Form.Check type="radio" name="rating" label={starlist2} />
                </Form.Group>
                <Form.Group className="mb-1" controlId="formBasicCheckbox">
                  <Form.Check type="radio" name="rating" label={starlist3} />
                </Form.Group>
                <Form.Group className="mb-1" controlId="formBasicCheckbox">
                  <Form.Check type="radio" name="rating" label={starlist4} />
                </Form.Group>
                <Form.Group className="mb-1" controlId="formBasicCheckbox">
                  <Form.Check type="radio" name="rating" label={starlist5} />
                </Form.Group>
              </div>
            </Col>
            <Col md={9}>
            <div>
            {/* <MainHeading
              title={`Best Colleges For ${courseData.name} in India`}
            /> */}
            </div>
          
              {/* <TopUniversityBar topUniversityList={topUniversityList} /> */}
              <Row className={`row-cols-${screenSize.width < 786 ? 1 : screenSize.width < 991 ? 2 : 3 } ps-3 `}>
              
            
           <TopUniversityCard topUniversityList={topUniversityList} screenSize={screenSize} />
           <TopUniversityCard topUniversityList={topUniversityList} screenSize={screenSize} />
           <TopUniversityCard topUniversityList={topUniversityList} screenSize={screenSize} />
           <TopUniversityCard topUniversityList={topUniversityList} screenSize={screenSize} />
           <TopUniversityCard topUniversityList={topUniversityList} screenSize={screenSize} />
           <TopUniversityCard topUniversityList={topUniversityList} screenSize={screenSize} />
           <TopUniversityCard topUniversityList={topUniversityList} screenSize={screenSize} />
                         
                              
                                 
                                 
                           </Row>
            </Col>
          </Row>
        </Container>
      </div>
      <Footer menuData={menuData} />
    </>
  );
};

export default TopUniversityCoursePage;
