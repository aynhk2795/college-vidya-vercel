import Head from "next/head";
import React from "react";
import { Container } from "react-bootstrap";
import Footer from "../components/Global/Footer";
import Header from "../components/Global/Header";
import OurExpert from "../components/Global/OurExpert";
import { getMenu } from "../service/MiscService";
import { getSearchSchema } from "../utils/genericSchemas";
import HeroSection from "/components/Global/Jumbotron/HeroSection";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

const Refund = ({ menuData }) => {
  return (
    <>
      <Head>
        <title>Refund Policy (All Your Answers) - College Vidya</title>
        <meta
          name="description"
          content="No refunds would be given if the student has provided wrong or incomplete email address…"
        />
        <meta name="robots" content="index, follow" />
        <link href="https://collegevidya.com/refund-policy/" rel="canonical" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Refund Policy (All Your Answers) - College Vidya"
        />
        <meta
          property="og:description"
          content="No refunds would be given if the student has provided wrong or incomplete email address…"
        />
        <meta
          property="og:url"
          content="https://collegevidya.com/refund-policy/"
        />
        <meta property="og:site_name" content="College Vidya" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidya"
        />
        <meta property="og:image" content="" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidya" />
        <meta
          property="twitter:title"
          content="Refund Policy (All Your Answers) - College Vidya"
        />
        <meta
          property="twitter:description"
          content="No refunds would be given if the student has provided wrong or incomplete email address…"
        />
        <meta
          property="twitter:url"
          content="https://collegevidya.com/refund-policy/"
        />
        <meta property="twitter:image" content="" />
        <meta
          name="keywords"
          content="why college vidya, refund policy college vidya, collegevidya refund policy, collegevidya, top online and distance education compare portal, top online education portal"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidya.com/refund-policy/",
              name: "Refund Policy (All Your Answers) - College Vidya",
              description:
                "No refunds would be given if the student has provided wrong or incomplete email address…",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: ["#seo-h1"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidya.com/",
              sameAs: [
                "https://facebook.com/collegevidya",
                "https://twitter.com/collegevidya",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidya",
              ],
              logo: "https://collegevidya.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18004205757",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "Refund Policy (All Your Answers) - College Vidya",
              description:
                "No refunds would be given if the student has provided wrong or incomplete email address…",
              url: "https://collegevidya.com/refund-policy/",
              email: "info@collegevidya.com",
              telephone: "18004205757",
              logo: "https://collegevidya.com/images/logo.svg",
              address: "India",
            }),
          }}
        />
      </Head>
      <Header menuData={menuData} />
      <HeroSection title="Refund Policy" />
      <Container className="my-5">
        <ul className="list-unstyled">
          <li
            className="mb-4 py-3 px-3 rounded"
            style={{ backgroundColor: "antiquewhite" }}>
            No refunds will be offered on any successful Consultation with our
            Senior Mentor
          </li>
          <li
            className="mb-4 py-3 px-3 rounded"
            style={{ backgroundColor: "antiquewhite" }}>
            If the user is not able to connect with our Senior Mentor due to any
            issue, then we are open to extending full refund
          </li>
          <li
            className="mb-4 py-3 px-3 rounded"
            style={{ backgroundColor: "antiquewhite" }}>
            No refunds would be given if the student has provided wrong or
            incomplete email address
          </li>
        </ul>
        <OurExpert />
      </Container>
      <Footer menuData={menuData} />
    </>
  );
};

export default Refund;
