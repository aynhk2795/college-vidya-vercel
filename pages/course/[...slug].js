import { Downgraded, useState } from "@hookstate/core";
import { useRouter } from "next/router";
import { useEffect } from "react";
import CoursePage from "../../components/CoursePage/CoursePage";
import { getMenu } from "../../service/MiscService";
// import CourseSpecializationPage from '../../components/CoursePage/CourseSpecializationPage';
import FourOhFour from "../404";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10,
  };
}

export async function getStaticPaths() {
  const paths = [];

  return { paths, fallback: "blocking" };
}

const CoursePath = ({ menuData }) => {
  const router = useRouter();
  const pageToLoad = useState(null);

  useEffect(() => {
    if (router.query.slug) {
      // if(router.query.slug.length === 2) {
      //   pageToLoad.set(<CourseSpecializationPage courseSlug={router.query.slug[0]} specializationSlug={router.query.slug[1]} />);
      // }
      if (router.query.slug.length === 1) {
        pageToLoad.set(
          <CoursePage menuData={menuData} courseSlug={router.query.slug[0]} />
        );
      } else {
        pageToLoad.set(<FourOhFour />);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router]);

  return <>{pageToLoad.attach(Downgraded).get()}</>;
};

export default CoursePath;
