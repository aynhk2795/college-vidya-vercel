import React, { useState } from "react";
import Banner from "../components/UniversityPage/Banner";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { Container } from "react-bootstrap";
import { Link } from "react-scroll";
import Course from "../components/UniversityPage/Slider/Course";
import OtherUniversities from "../components/UniversityPage/Slider/OtherUniversities";
import Faq from "../components/UniversityPage/Tabs/Faq.js";
import Testimonial from "../components/UniversityPage/Slider/Testimonial";
import Blog from "../components/UniversityPage/Slider/Blog";
import Video from "../components/UniversityPage/Slider/Video";
import { BsChevronRight } from "react-icons/bs";
import WriteReview from "../components/UniversityPage/Modal/WriteReview.js";
import AllReview from "../components/UniversityPage/Modal/AllReview";
import About from "../components/UniversityPage/Tabs/About";
import UniversityFacts from "../components/UniversityPage/Tabs/UniversityFacts";
import ApprovedBy from "../components/UniversityPage/Tabs/ApprovedBy";
import SampleCerficate from "../components/UniversityPage/Tabs/SampleCerficate";
import ReviewBar from "../components/UniversityPage/Tabs/Review/ReviewBar";
import ReviewCard from "../components/UniversityPage/Tabs/Review/ReviewCard";
import { TabView, TabPanel } from "primereact/tabview";
import Placement from "../components/UniversityPage/Tabs/Placement";
import Header from "../components/Global/Header";
import Footer from "../components/Global/Footer";
import { getMenu } from "../service/MiscService";

export async function getStaticProps() {
  const menuData = await getMenu().then((response) => response.data);

  return {
    props: {
      menuData: menuData,
    },
    revalidate: 10
  }
}

const Universitypage = ({ menuData }) => {
  const [activeIndex1, setActiveIndex1] = useState(0);
  const [isActive, setActive] = useState(false);
  return (
    <>
      <Header menuData={menuData} />
      <div>
        <Banner
          title="NMIMS Distance Learning"
          tagline="Ranked Among Top 10 Universities in India"
        />

        <Container className="my-5">
          <Row>
            <Col sm="3" className="d-none d-lg-block">
              <ul
                className={`university_page_list_items list-unstyled sticky-top shadow-sm p-4`}>
                <li className="mb-3 cursor-pointer">
                  <Link
                    duration={10}
                    activeClass="active"
                    to="tab1"
                    spy={true}
                    smooth={true}>
                    <span>About</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>

                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab2" spy={true} smooth={true}>
                    <span>University Facts</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab3" spy={true} smooth={true}>
                    <span>Approved By</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab4" spy={true} smooth={true}>
                    <span>Courses</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab5" spy={true} smooth={true}>
                    <span>Sample Certificate</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab6" spy={true} smooth={true}>
                    <span>Other Universities</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab7" spy={true} smooth={true}>
                    <span>Review</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab8" spy={true} smooth={true}>
                    <span>Faq</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab9" spy={true} smooth={true}>
                    <span>Blog / Video</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab10" spy={true} smooth={true}>
                    <span>Testimonial</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
                <li className="mb-3 cursor-pointer">
                  <Link duration={10} to="tab11" spy={true} smooth={true}>
                    <span>Placement Partners</span>
                    <span>
                      <BsChevronRight />
                    </span>
                  </Link>
                </li>
              </ul>
            </Col>
            <Col sm="12" md={12} lg={9} xl={9}>
              <div className="shadow-sm p-4">
                <div id="tab1" className="mb-4">
                  <About />
                </div>
                <div id="tab2" className="mb-4">
                  <UniversityFacts />
                </div>
                <div id="tab3" className="mb-4">
                  <ApprovedBy />
                </div>
                <div id="tab4" className="mb-4">
                  <Course />
                </div>
                <div id="tab5" className="mb-4">
                  <SampleCerficate />
                </div>
                <div id="tab6" className="mb-4">
                  <OtherUniversities />
                </div>
                <div id="tab7" className="mb-4">
                  <ReviewBar />

                  <WriteReview />
                  <h6>Top reviews from India</h6>

                  <ReviewCard />
                  <AllReview />
                </div>
                <div id="tab8" className="mb-4">
                  <Faq />
                </div>
                <div id="tab9" className={`mb-4 blog_video_tab`}>
                  <TabView
                    activeIndex={activeIndex1}
                    onTabChange={(e) => setActiveIndex1(e.index)}>
                    <TabPanel header="Blog">
                      <Blog />
                    </TabPanel>
                    <TabPanel header="Video">
                      <Video />
                    </TabPanel>
                  </TabView>
                </div>
                <div id="tab10" className="mb-4">
                  <Testimonial />
                </div>
                <div id="tab11" className="mb-4">
                  <Placement />
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <Footer menuData={menuData} />
    </>
  );
};

export default Universitypage;
